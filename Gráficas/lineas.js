new Morris.Line({
    // ID of the element in which to draw the chart.
    element: 'myfirstchart',
    // Chart data records -- each entry in this array corresponds to a point on
    // the chart.
    data: [
      { Metros: '0',  value: 1.80 },
      { Metros: '5',  value: 1.80 },
      { Metros: '10', value: 1.80 },
      { Metros: '15',  value: 1.80 },
      { Metros: '20', value: 1.80 },
      { Metros: '25',  value: 1.80 },
      { Metros: '30', value: 1.80 },
      { Metros: '35',  value: 1.85 },
      { Metros: '40', value: 2 },
      { Metros: '45',  value: 1.90 },
      { Metros: '50', value: 1.30},
      { Metros: '55',  value: 0.5 },
      { Metros: '60', value: 0 }
    ],
    parseTime: false,
    // The name of the data record attribute that contains x-values.
    xkey: 'Metros',
    // A list of names of data record attributes that contain y-values.
    ykeys: ['value'],
    // Labels for the ykeys -- will be displayed when you hover over the
    // chart.
    labels: ['Value']
  });