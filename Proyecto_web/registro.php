<?php
include_once 'registro.php';
?>
<!DOCTYPE html>
<html lang="es">
<meta charset="UTF-8">
<title>Registro</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="Estilos.css?v=<?php echo time(); ?>">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="icon" href="img/AR_LOGO_TINTA_BLANCA.ico">

<body class="body">
<?php
include_once 'header.php';
?>
<section class="section-inc">
<div >
   <h2>Registro</h2> 
<form  action="includes/registro.inc.php" method="post">
<input type="text" name="name" placeholder="Tu nombre.."> 
<input type="text" name="email" placeholder="Tu correo.."> 
<input type="text" name="uid" placeholder="Nombre de usuario.."> 
<input type="password" name="pwd" placeholder="Contraseña.."> 
<input type="password" name="pwdrepeat" placeholder="Repita su contraseña.."> 
<button type="submit" name="submit">Completar registro</button>
</form>
</div>
<?php 
if(isset($_GET["error"])){
    if($_GET["error"] == "emptyinput"){
        echo "<p>Rellena todos los campos </p>";
    }

    if($_GET["error"] == "invaliduid"){
        echo "<p>Caracteres invalidos en el nombre de usuario</p>";
    }

    if($_GET["error"] == "invalidemail"){
        echo "<p>el email no existe!</p>";
    }

    if($_GET["error"] == "usernametaken"){
        echo "<p>El usuario ya esta en uso</p>";
    }

    if($_GET["error"] == "passwordsdontmatch"){
        echo "<p>La contraseña no coincide</p>";
    }

    if($_GET["error"] == "none"){
        echo "<p>Te has registrado correctamente!</p>";
    }






}
?>


</body>
</section>

