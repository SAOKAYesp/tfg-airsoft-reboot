<?php
include_once 'contacto.php';
?>
<!DOCTYPE html>
<html lang="es">
<meta charset="UTF-8">
<title>AIRSOFT REBOOT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="Estilos.css?v=<?php echo time(); ?>">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="icon" href="img/AR_LOGO_TINTA_BLANCA.ico">

<body class="body">
<?php
include_once 'header.php';
?>

    <div style="width: 80%; height: 50%; margin-top: 5vh; margin-left: auto; margin-right: auto; border-radius: 25px;" class="inicio"
        id="texto4">
        <div id="foto4" class="imagenes">
            <img src="img/AR_LOGO_TINTA_BLANCA.png" style="width: 15%; height: 40%;" alt="falta imagen">
        </div>
        <div>
            <p>¿Quieres contactar con nosotros?</p>
            <br>
            <p>Para contactar con nosotros puedes hablarnos a la cuenta de <a
                    href="https://www.instagram.com/airsoft.reboot/">instagram</a> o a nuestro correo
                electrónico: <a href="mailto:airsoft.reboot.web@gmail.com">airsoft.reboot.web@gmail.com</a></p>

            <a href="https://www.instagram.com/airsoft.reboot/"><img src="img/iconos/instagram.png"
                    style="margin-left: 0%; width: 7%; height: 7%;" alt="falta imagen"></a>

            <a href="https://www.youtube.com/channel/UCC8SYk1bQoRykHvZRGbvWbg"><img src="img/iconos/youtube.png"
                    style="width: 7%; height: 7%;" alt="falta imagen"></a>
            <br>
            <div style="margin-top: 15px;">
                <p>También puedes dejar un aviso en esta dirección y te atenderemos lo antes posible.</p>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6081.028533621796!2d-3.424170649269937!3d40.35312002827361!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd4238a67d460569%3A0xdd773b479a92fee!2sNavy%207%20Airsoft!5e0!3m2!1ses!2ses!4v1655669200375!5m2!1ses!2ses" 
                    width="700"
                     height="300" style="border:0;border-radius: 25px;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                <style>
                    .mapouter {
                        position: relative;
                        text-align: right;
                        height: 300px;
                        width: 700px;
                    }
                </style>
                <style>
                    .gmap_canvas {
                        overflow: hidden;
                        background: none !important;
                        height: 300px;
                        width: 700px;
                    }
                </style>
            </div>
        </div>

    </div>

</body>

</html>