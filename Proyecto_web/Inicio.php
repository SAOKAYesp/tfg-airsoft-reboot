<?php
include_once 'inicio.php';
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <title>AIRSOFT REBOOT</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="Estilos.css?v=<?php echo time(); ?>">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
  <link rel="icon" href="img/AR_LOGO_TINTA_BLANCA.ico">
  <style>
    .carousel-inner img {
      width: 100%;
      height: 100%;
    }
  </style>
</head>

<body class="body">
  
<?php
include_once 'header.php';
?>


  <div class="container mt-3">
    <h1 id="titulo">AIRSOFT REBOOT</h1>
    <div id="myCarousel" class="carousel slide">


      <ul class="carousel-indicators">
        <li class="item1 active"></li>
        <li class="item2"></li>
        <li class="item3"></li>
      </ul>


      <div class="carousel-inner" style="border-radius: 25px;">
        <div class="carousel-item active">
          <img src="img/carrousel/airsoft.jpg" alt="Carrousel 1" width="1100" height="500">
        </div>
        <div class="carousel-item">
          <img src="img/carrousel/sniper.jpg" alt="Carrousel 2" width="1100" height="500">
        </div>
        <div class="carousel-item">
          <img src="img/carrousel/tactico.jpg" alt="Carrousel 3" width="1100" height="500">
        </div>
      </div>

      <a class="carousel-control-prev" href="#myCarousel">
        <span class="carousel-control-prev-icon"></span>
      </a>
      <a class="carousel-control-next" href="#myCarousel">
        <span class="carousel-control-next-icon"></span>
      </a>
    </div>
  </div>

  <script>
    $(document).ready(function () {

      $("#myCarousel").carousel();


      $(".item1").click(function () {
        $("#myCarousel").carousel(0);
      });
      $(".item2").click(function () {
        $("#myCarousel").carousel(1);
      });
      $(".item3").click(function () {
        $("#myCarousel").carousel(2);
      });


      $(".carousel-control-prev").click(function () {
        $("#myCarousel").carousel("prev");
      });
      $(".carousel-control-next").click(function () {
        $("#myCarousel").carousel("next");
      });
    });
  </script>

  <div style="width: 100%; height: 24%;" class="inicio">
    <div id="foto1" class="imagenes">
    </div>
    <div id="texto1">
      <p>¿Qué es Airsoft Reboot?</p>
      <br>
      <p>Es una herramienta sencilla de usar y de gran ayuda para aquellas personas que tengan como hobby el airsoft,
        gracias
        a nuestro configurador podras hacerte presets con diferentes piezas para saber un rendimiento estimado del arma
        antes de comprarla,
        de esta forma podrás saber como quieres que sea tu replica. Además en nuestra página encontrarás tutoriales y
        ayudas en <a href="consejos.html">"consejos"</a> para novatos.</p>
    </div>
  </div>

  <div style="width: 100%; height: 24%;" class="inicio">
    <div id="foto2" class="imagenes">
    </div>
    <div id="texto2">
      <p> ¿Cómo se usa?</p>
      <br>
      <p>Nuestro configurador es noob friendly y es muy visual, no necesitas tener conocimientos para usarlo,
        simplemente ve al apartado de <a href="configurador.html">"configurador"</a> y
        configura la replica a tu gusto!</p>
    </div>
  </div>


  <div style="width: 100%; height: 24%;" class="inicio">
    <div id="foto3" class="imagenes">
    </div>
    <div id="texto3">
      <p>¿Necesitas ayuda?</p>
      <br>
      <p>Si tienes alguna duda o problema puedes contactar con nosotros desde el apartado <a
          href="contacto.html">"contacto"</a>,
        donde podrás encontrar nuestro canal de Youtube y nuestro instagram.</p>
    </div>
  </div>
</body>

<footer id="copy">
  &copy; Copyright 2022 Airsoft Reboot
</footer>

</html>
