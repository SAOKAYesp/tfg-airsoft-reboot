<?php
include_once 'login.php';
?>
<!DOCTYPE html>
<html lang="es">
<meta charset="UTF-8">
<title>Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="Estilos.css?v=<?php echo time(); ?>">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
<link rel="icon" href="img/AR_LOGO_TINTA_BLANCA.ico">

<body class="body">
<?php
include_once 'header.php';
?>
<section class="section-inc">
<div >
   <h2>Login</h2> 
<form  action="includes/login.inc.php" method="post">
<input type="text" name="uid" placeholder="Usuario.."> 
<input type="password" name="pwd" placeholder="Contraseña.."> 
<br>
<button type="submit" name="submit">Iniciar sesión</button>
</form>
</div>
<?php 
if(isset($_GET["error"])){
    if($_GET["error"] == "emptyinput"){
        echo "<p>Rellena todos los campos </p>";
    }

    if($_GET["error"] == "wronglogin"){
        echo "<p>Contraseña incorrecta</p>";
    }



}
?>
</section>

</body>