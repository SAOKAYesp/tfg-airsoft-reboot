<?php
include_once 'consejos.php';
?>
<!DOCTYPE html>
<html lang="es">

<head>
  <title>AIRSOFT REBOOT</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="Estilos.css?v=<?php echo time(); ?>">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="icon" href="img/AR_LOGO_TINTA_BLANCA.ico">
</head>


<body class="body">
<?php
include_once 'header.php';
?>

  <div class="title text-center py-5">
    <h1 id="consejos">CONSEJOS</h1>
  </div>
  <div id="cartitas">


    <!-- CARTA 1-->
    <div class="row mx-5">
      <div class="col">

        <div class="card" style="width: 18rem;">
          <img src="img/tips/posicion_seguridad_foto.jpeg" class="card-img-top" alt="Imagen de tarjeta 1">
          <div class="card-body">
            <h2>Posición de seguridad</h2>
            <p class="card-text">
              Es importante mantener el dedo fuera del gatillo para evitar accidentes. Esta es la
              posición recomendada.
            </p>
            <button id="btn-abrir-popup" class="btn-abrir-popup">APRENDE</button>
          </div>
        </div>
      </div>

      <!-- CARTA 2-->
      <div class="col">

        <div class="card" style="width: 18rem;">
          <img src="img/tips/goma_de_hop.jpg" class="card-img-top" alt="Imagen de tarjeta 2">
          <div class="card-body">
            <h2>CAMBIO DE GOMA</h2>
            <p class="card-text">
              Las gomas de hop son una de las piezas más importantes y con mayor desgaste dentro de una
              réplica.
            </p>
            <button id="btn-abrir-popup2" class="btn-abrir-popup">APRENDE</button>
          </div>
        </div>
      </div>

      <!-- CARTA 3-->
      <div class="col">

        <div class="card" style="width: 18rem;">
          <img src="img/tips/FNX.jpg" class="card-img-top" alt="Imagen de tarjeta 3">
          <div class="card-body">
            <h2>RÉPLICAS GBB Y GASES</h2>
            <p class="card-text">
              Las réplicas GBB son las que funcionan mediante gas o CO2, principalmente pistolas aunque
              también se puede ver en fusiles.
            </p>
            <br>
            <button id="btn-abrir-popup3" class="btn-abrir-popup">APRENDE</button>
          </div>
        </div>
      </div>

      <!-- CARTA 4-->
      <div class="col">

        <div class="card" style="width: 18rem;">
          <img src="img/tips/loadout.jpg" class="card-img-top" alt="Imagen de tarjeta 4">
          <div class="card-body">
            <h2>LOADOUT (EQUIPACIÓN)</h2>
            <p class="card-text">
              Cuando empezamos todos queremos parecernos a militares pero cometemos en los mismos fallos...
            </p>
            <button id="btn-abrir-popup4" class="btn-abrir-popup">APRENDE</button>
          </div>
        </div>
      </div>

      <!-- CARTA 5-->
      <div class="col">

        <div class="card" style="width: 18rem;">
          <img src="img/tips/accesorios.jpg" class="card-img-top" alt="Imagen de tarjeta 5">
          <div class="card-body">
            <h2>ACCESORIOS</h2>
            <p class="card-text">
              Todo el mundo quiere que su réplica se vea lo mejor posible por lo que se le añaden accesorios imitando
              reales.
              Pero hay que tener mucho cuidado al elegir...
            </p>
            <br>
            <button id="btn-abrir-popup5" class="btn-abrir-popup">APRENDE</button>
          </div>
        </div>
      </div>


    </div>


    <!-- CARTA 1-->
    <div class="overlay" id="overlay">
      <div class="popup" id="popup">
        <a href="#" id="btn-cerrar-popup" class="btn-cerrar-popup"><i class="fas fa-times"></i></a>
        <h3>POSICIÓN DE SEGURIDAD</h3>
        <h4>Dedo en el gatillo</h4>
        <form action="">
          <div class="contenedor-inputs">
            <img src="img/tips/TIP-1_02.png" width="525" height="525">
            <img src="img/tips/TIP-1_03.png" width="525" height="525">
          </div>
          <br>
          <input type="submit" class="btn-submit" value="Cerrar">
        </form>
      </div>
    </div>

    <!-- CARTA 2-->
    <div class="overlay" id="overlay2">
      <div class="popup" id="popup2">
        <a href="#" id="btn-cerrar-popup2" class="btn-cerrar-popup"><i class="fas fa-times"></i></a>
        <h3>CAMBIO DE GOMA</h3>
        <h4>Y lubricación</h4>
        <form action="">
          <div class="contenedor-inputs">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/0Qnhu4fwkco"
              title="YouTube video player" frameborder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen></iframe>
          </div>
          <br>
          <input type="submit" class="btn-submit" value="Cerrar">
        </form>
      </div>
    </div>

    <!-- CARTA 3-->
    <div class="overlay" id="overlay3">
      <div class="popup" id="popup3">
        <a href="#" id="btn-cerrar-popup3" class="btn-cerrar-popup"><i class="fas fa-times"></i></a>
        <h3>RÉPLICAS GBB Y GASES</h3>
        <h4>Para un correcto funcionamiento usaremos gas rojo en invierno y verde en verano</h4>
        <form action="">
          <div class="contenedor-inputs">
            <img src="img/tips/redGas.jpg" width="400" height="400">
            <img src="img/tips/greenGas.jpg" width="400" height="400">
          </div>
          <br>
          <input type="submit" class="btn-submit" value="Cerrar">
        </form>
      </div>
    </div>

    <!-- CARTA 4-->
    <div class="overlay" id="overlay4">
      <div class="popup" id="popup4">
        <a href="#" id="btn-cerrar-popup4" class="btn-cerrar-popup"><i class="fas fa-times"></i></a>
        <h3>LOADOUT (EQUIPACIÓN)</h3>
        <h4>Aprenderemos a no cometer los errores más comunes</h4>
        <form action="">
          <div class="contenedor-inputs">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/2fQL8y1Jk9g" title="YouTube video player"
             frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
          <br>
          <input type="submit" class="btn-submit" value="Cerrar">
        </form>
      </div>
    </div>

    <!-- CARTA 5-->
    <div class="overlay" id="overlay5">
      <div class="popup" id="popup5">
        <a href="#" id="btn-cerrar-popup5" class="btn-cerrar-popup"><i class="fas fa-times"></i></a>
        <h3>ACCESORIOS</h3>
        <h4>Aprenderemos a elegir bien los accesorios para no tirar el dinero</h4>
        <form action="">
          <div class="contenedor-inputs">
            <iframe width="560" height="315" src="https://www.youtube.com/embed/PGkSA8ZcCNg"
              title="YouTube video player" frameborder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen></iframe>
          </div>
          <br>
          <input type="submit" class="btn-submit" value="Cerrar">
        </form>
      </div>
    </div>

    <script src="popup.js"></script>
  </div>


  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.7/dist/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
    crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
    crossorigin="anonymous"></script>
</body>

</html>