using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class DropDownHandlerGomaHop : MonoBehaviour
{
    public Componente blankItem;
    public Dropdown GomaHop;
    public double potenciaDrop;
    public double alcanceDrop;
    public double precisionDrop;
    public double cadenciaDrop;
    public float varianzaDrop;
    public double precio;
    public string extra;

    public GameObject GomaHopPieza;
    public List<Componente> itemDatabase = new List<Componente>();

    void Start()
    {
        GomaHop = GetComponent<Dropdown>();
        GomaHopPieza = GameObject.Find("Cilindro.013");

        potenciaDrop = 0;
        alcanceDrop = 0;
        precisionDrop = 0;
        cadenciaDrop = 0;


        List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
        string[] options = new string[data.Count];
        List<string> items = new List<string>();

        for (var i = 0; i < data.Count; i++)
        {
            string idPiezas = data[i]["idPiezas"].ToString();
            string tipo = data[i]["tipo"].ToString();
            string marca = data[i]["marca"].ToString();
            string color = data[i]["color"].ToString();
            string material = data[i]["material"].ToString();
            string potencia = data[i]["potencia"].ToString();
            string precision = data[i]["precision"].ToString();
            string alcance = data[i]["alcance"].ToString();
            string cadencia = data[i]["cadencia"].ToString();
            string estabilidad = data[i]["estabilidad"].ToString();
            string precio = data[i]["precio"].ToString();
            string modelo = data[i]["modelo"].ToString();
            string extra = data[i]["extra"].ToString();

            Componente tempItem = new Componente(blankItem);
            tempItem.tipo = tipo;
            tempItem.marca = marca;
            tempItem.color = color;
            tempItem.material = material;
            tempItem.potencia = potencia;
            tempItem.precision = precision;
            tempItem.alcance = alcance;
            tempItem.cadencia = cadencia;
            tempItem.estabilidad = estabilidad;
            tempItem.precio = precio;
            tempItem.modelo = modelo;
            tempItem.extra = extra;
            items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
            itemDatabase.Add(tempItem);
        }

        foreach (string item in items)
        {
            if (item.Contains("Gomas de hop"))
            {
                GomaHop.options.Add(new Dropdown.OptionData() { text = item });
            }
        }
        GomaHop.RefreshShownValue();
        GomaHop.onValueChanged.AddListener(delegate { dropDownSelected(GomaHop); });

    }

    void dropDownSelected(Dropdown dropdown)
    {
        if (potenciaDrop != 0)
        {
            Window_Graph.setPotencia(0, potenciaDrop);
        }
        if (alcanceDrop != 0)
        {
            Window_Graph.setAlcance(0, alcanceDrop);
        }
        if (varianzaDrop != 0)
        {
            Window_Graph.setVarianza(0, varianzaDrop);
        }
        if (precisionDrop != 0)
        {
            Window_Graph.setPrecision(0, precisionDrop);
        }
        if (cadenciaDrop != 0)
        {
            Window_Graph.setCadencia(0, cadenciaDrop);
        }
        if (precio != 0)
        {
            Window_Graph.setPrecio(0, precio);
        }
        int index = dropdown.value;
        Debug.Log(GomaHop.options[index].text);
        foreach (Componente Componente in itemDatabase)
        {
            string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
            if (comparar == GomaHop.options[index].text)
            {

                potenciaDrop = int.Parse(Componente.potencia);
                alcanceDrop = int.Parse(Componente.alcance);
                varianzaDrop = int.Parse(Componente.estabilidad);
                precisionDrop = int.Parse(Componente.precision);
                cadenciaDrop = int.Parse(Componente.cadencia);
                precio = int.Parse(Componente.precio);
                extra = Componente.extra;

                Window_Graph.setAlcance(alcanceDrop);
                Window_Graph.setPotencia(potenciaDrop);
                Window_Graph.setVarianza(varianzaDrop);
                Window_Graph.setPrecision(precisionDrop);
                Window_Graph.setCadencia(cadenciaDrop);
                Window_Graph.setPrecio(precio,0);

                Debug.Log("potenciaDropDown: " + potenciaDrop);


                var renderer = GomaHopPieza.GetComponent<Renderer>();
                if (Componente.color == "Rojo")
                {
                    renderer.material.SetColor("_Color", Color.red);
                }
                if (Componente.color == "Gris")
                {
                    renderer.material.SetColor("_Color", Color.gray);
                }
                if (Componente.color == "Gris oscuro")
                {
                    renderer.material.SetColor("_Color", Color.gray);
                }
                if (Componente.color == "Gris claro")
                {
                    renderer.material.SetColor("_Color", Color.gray);
                }
                if (Componente.color == "Azul")
                {
                    renderer.material.SetColor("_Color", Color.blue);
                }
                if (Componente.color == "Negro")
                {
                    renderer.material.SetColor("_Color", Color.black);
                }
                if (Componente.color == "Dorado")
                {
                    renderer.material.SetColor("_Color", Color.yellow);
                }
                if (Componente.color == "Plateado")
                {
                    renderer.material.SetColor("_Color", Color.gray);
                }
                if (Componente.color == "Cobre")
                {
                    renderer.material.SetColor("_Color", Color.black);
                }
                if (Componente.color == "")
                {
                    renderer.material.SetColor("_Color", Color.gray);
                }
            }

        }


    }
}