using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

[System.Serializable]
public class Componente
{

    public string tipo, marca, color, material, potencia, precision, alcance, cadencia, estabilidad, precio, modelo, extra;

    public Componente(Componente p)
    {
        tipo = p.tipo;
        marca = p.marca;
        color = p.color;
        material = p.material;
        potencia = p.potencia;
        precision = p.precision;
        alcance = p.alcance;
        cadencia = p.cadencia;
        estabilidad = p.estabilidad;
        precio = p.precio;
        modelo = p.modelo;
        extra = p.extra;
    }
}
