using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.UI;
 
public class HacerCaptura : MonoBehaviour {
    
    public Dropdown Gearbox;
    public Dropdown Gomasdehop;
    public Dropdown nub;
    public Dropdown Camarasdehop;
    public Dropdown cañon;
    public Dropdown Cilindro;
    public Dropdown Cabezadecilindro;
    public Dropdown Cabezadepiston;
    public Dropdown Muelle;
    public Dropdown Guiademuelle;
    public Dropdown Piston;
    public Dropdown Nozzle;
    public Dropdown Tapperplate;
    public Dropdown Engranajes;
    public Dropdown Motor;
    public Dropdown Gatilloelectronico;
    public Dropdown Mosfet;
    public static double alcance;
    public static double potencia;
    public static double precision;
    public static double cadencia;
    public static double varianza;
    public static double precio;
    public string nombreConf;
    public string data;
    public string nombreDescarga;

  
        [DllImport("__Internal")]
        private static extern void descargarTxt(string data, string nombreDescarga);

        [DllImport("__Internal")]
        private static extern void errorNombre();
        
    
    

   public void Start(){
    Gearbox = GameObject.Find("Drop_Gearbox").GetComponent<Dropdown>();
        Gomasdehop = GameObject.Find("Drop_GomaHop").GetComponent<Dropdown>();
        nub = GameObject.Find("Drop_Nub").GetComponent<Dropdown>();
        Camarasdehop = GameObject.Find("Drop_CamaraDeHop").GetComponent<Dropdown>();
        cañon = GameObject.Find("Drop_Cañon").GetComponent<Dropdown>();
        Cilindro = GameObject.Find("Drop_Cilindro").GetComponent<Dropdown>();
        Cabezadecilindro = GameObject.Find("Drop_CabezaDeCilindro").GetComponent<Dropdown>();
        Cabezadepiston = GameObject.Find("Drop_CabezaDePiston").GetComponent<Dropdown>();
        Muelle = GameObject.Find("Drop_Muelle").GetComponent<Dropdown>();
        Guiademuelle = GameObject.Find("Drop_GuiaDeMuelle").GetComponent<Dropdown>();
        Piston = GameObject.Find("Drop_Piston").GetComponent<Dropdown>();
        Tapperplate = GameObject.Find("Drop_TapperPlate").GetComponent<Dropdown>();
        Engranajes = GameObject.Find("Drop_Engranajes").GetComponent<Dropdown>();
        Motor = GameObject.Find("Drop_Motor").GetComponent<Dropdown>();
        Gatilloelectronico = GameObject.Find("Drop_Gatillo").GetComponent<Dropdown>();
        Mosfet = GameObject.Find("Drop_Mosfet").GetComponent<Dropdown>();
        Nozzle = GameObject.Find("Drop_Nozzle").GetComponent<Dropdown>();
        alcance = 0;
        potencia = 0;
        precision = 0;
        cadencia = 0;
        varianza = 0;
        precio = 0;
   }

     public static void setStats(
        double alcanceX,
        double potenciaX,
        double precisionX,
        double cadenciaX,
        double varianzaX,
        double precioX
    )
    {
        alcance = alcanceX;
        potencia = potenciaX;
        precision = precisionX;
        cadencia = cadenciaX;
        varianza = varianzaX;
        precio = precioX;
    }


    public void Captura() {
        
        if(string.IsNullOrEmpty(nombreConf)){
        Debug.Log("El nombre esta vacio");
        error();
        }
        else{
        StartCoroutine(descargarPNG());
        DescargarTxt(data, nombreDescarga);
        }
        
    }
    
    public void Update(){
        nombreConf = GameObject.Find("nombreConfig").GetComponent<InputField>().text;
        data = "NOMBRE: " + nombreConf;
        data = data + " || MUELLE: " + Muelle.options[Muelle.value].text;
        data = data + " || GOMAS DE HOP: " + Gomasdehop.options[Gomasdehop.value].text;
        data = data + " || NUB: " + nub.options[nub.value].text;
        data = data + " || CAMARA DE HOP: " + Camarasdehop.options[Camarasdehop.value].text;
        data = data + " || CAÑON: " + cañon.options[cañon.value].text;
        data = data + " || GUIA DE MUELLE: " + Guiademuelle.options[Guiademuelle.value].text;
        data = data + " || TAPPER PLATE: " + Tapperplate.options[Tapperplate.value].text;
        data = data + " || ENGRANAJES: " + Engranajes.options[Engranajes.value].text;
        data = data + " || MOTOR: " + Motor.options[Motor.value].text;
        data = data + " || GATILLO ELECTRONICO: " + Gatilloelectronico.options[Gatilloelectronico.value].text;
        data = data + " || MOSFET: " + Mosfet.options[Mosfet.value].text;
        data = data + " || NOZZLE: " + Nozzle.options[Nozzle.value].text;

        data = data + " || ALCANCE: " + alcance;       
        data = data + " || POTENCIA: " + potencia; 
        data = data + " || PRECISION: " + precision;  
        data = data + " || CADENCIA: " + cadencia;  
        data = data + " || VARIANZA: " + varianza;  
        data = data + " || PRECIO: " + precio + " EUROS";   

        nombreDescarga = nombreConf + ".txt";
   
    }


     public static void error()
        {
    #if UNITY_WEBGL && !UNITY_EDITOR
            errorNombre();
    #endif
        } 
    
    public static void DescargarTxt(string data, string nombreDescarga)
        {
    #if UNITY_WEBGL && !UNITY_EDITOR
            descargarTxt(data, nombreDescarga);
    #endif
        }

   IEnumerator descargarPNG() {
       
        yield return new WaitForEndOfFrame();
 
        
        int ancho = Screen.width;
        int height = Screen.height;
        var medidas = new Texture2D( ancho, height, TextureFormat.RGB24, false );
 
        
        medidas.ReadPixels( new Rect(0, 0, ancho, height), 0, 0 );
        medidas.Apply();
 
        
        byte[] imagenBytes = medidas.EncodeToPNG();
        Destroy( medidas );
 
     
        string textoCod = System.Convert.ToBase64String (imagenBytes);
   
        var image_url = "image/png;base64," + textoCod;
 
        Debug.Log (image_url);
 
        #if !UNITY_EDITOR
        SaveScreenshotWebGL("preset.png",textoCod);
        #endif
    }
 
    [DllImport("__Internal")]
    private static extern void SaveScreenshotWebGL(string filename, string data);
 
} 