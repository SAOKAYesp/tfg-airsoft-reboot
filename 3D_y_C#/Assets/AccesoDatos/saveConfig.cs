using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Runtime.InteropServices;

public class saveConfig : MonoBehaviour
{
    public Dropdown Gearbox;
    public Dropdown Gomasdehop;
    public Dropdown nub;
    public Dropdown Camarasdehop;
    public Dropdown cañon;
    public Dropdown Cilindro;
    public Dropdown Cabezadecilindro;
    public Dropdown Cabezadepiston;
    public Dropdown Muelle;
    public Dropdown Guiademuelle;
    public Dropdown Piston;
    public Dropdown Nozzle;
    public Dropdown Tapperplate;
    public Dropdown Engranajes;
    public Dropdown Motor;
    public Dropdown Gatilloelectronico;
    public Dropdown Mosfet;
    public static double alcance;
    public static double potencia;
    public static double precision;
    public static double cadencia;
    public static double varianza;
    public static double precio;
    public string nombreConf;

    [DllImport("__Internal")]
    private static extern void todoCorrecto();
    [DllImport("__Internal")]
        private static extern void errorNombre();
     

    private void Start()
    {   
        
        Gearbox = GameObject.Find("Drop_Gearbox").GetComponent<Dropdown>();
        Gomasdehop = GameObject.Find("Drop_GomaHop").GetComponent<Dropdown>();
        nub = GameObject.Find("Drop_Nub").GetComponent<Dropdown>();
        Camarasdehop = GameObject.Find("Drop_CamaraDeHop").GetComponent<Dropdown>();
        cañon = GameObject.Find("Drop_Cañon").GetComponent<Dropdown>();
        Cilindro = GameObject.Find("Drop_Cilindro").GetComponent<Dropdown>();
        Cabezadecilindro = GameObject.Find("Drop_CabezaDeCilindro").GetComponent<Dropdown>();
        Cabezadepiston = GameObject.Find("Drop_CabezaDePiston").GetComponent<Dropdown>();
        Muelle = GameObject.Find("Drop_Muelle").GetComponent<Dropdown>();
        Guiademuelle = GameObject.Find("Drop_GuiaDeMuelle").GetComponent<Dropdown>();
        Piston = GameObject.Find("Drop_Piston").GetComponent<Dropdown>();
        Tapperplate = GameObject.Find("Drop_TapperPlate").GetComponent<Dropdown>();
        Engranajes = GameObject.Find("Drop_Engranajes").GetComponent<Dropdown>();
        Motor = GameObject.Find("Drop_Motor").GetComponent<Dropdown>();
        Gatilloelectronico = GameObject.Find("Drop_Gatillo").GetComponent<Dropdown>();
        Mosfet = GameObject.Find("Drop_Mosfet").GetComponent<Dropdown>();
        Nozzle = GameObject.Find("Drop_Nozzle").GetComponent<Dropdown>();
        alcance = 0;
        potencia = 0;
        precision = 0;
        cadencia = 0;
        varianza = 0;
        precio = 0;
    }

    public static void setStats(
        double alcanceX,
        double potenciaX,
        double precisionX,
        double cadenciaX,
        double varianzaX,
        double precioX
    )
    {
        alcance = alcanceX;
        potencia = potenciaX;
        precision = precisionX;
        cadencia = cadenciaX;
        varianza = varianzaX;
        precio = precioX;
    }


     public static void error()
        {
    #if UNITY_WEBGL && !UNITY_EDITOR
            errorNombre();
    #endif
        } 


    public static void bien()
        {
    #if UNITY_WEBGL && !UNITY_EDITOR
            todoCorrecto();
    #endif
        } 

    public void callSave()
    {
        StartCoroutine(Save());
    }

    IEnumerator Save()
    {   
        nombreConf = GameObject.Find("nombreConfig").GetComponent<InputField>().text;
        if(string.IsNullOrEmpty(nombreConf)){
        Debug.Log("El nombre esta vacio");
        error();
        }

        else{
        string url = "http://localhost/tfg_web/includes/saveConfig.php";
        WWWForm form = new WWWForm();
        form.AddField("nombreConf", nombreConf);
        form.AddField("muelle", Muelle.options[Muelle.value].text);
        form.AddField("Gomasdehop", Gomasdehop.options[Gomasdehop.value].text);
        form.AddField("nub", nub.options[nub.value].text);
        form.AddField("Camarasdehop", Camarasdehop.options[Camarasdehop.value].text);
        form.AddField("cañon", cañon.options[cañon.value].text);
        form.AddField("Cilindro", Cilindro.options[Cilindro.value].text);
        form.AddField("Cabezadecilindro", Cabezadecilindro.options[Cabezadecilindro.value].text);
        form.AddField("Cabezadepiston", Cabezadepiston.options[Cabezadepiston.value].text);
        form.AddField("Guiademuelle", Guiademuelle.options[Guiademuelle.value].text);
        form.AddField("Piston", Piston.options[Piston.value].text);
        form.AddField("Tapperplate", Tapperplate.options[Tapperplate.value].text);
        form.AddField("Engranajes", Engranajes.options[Engranajes.value].text);
        form.AddField("Motor", Motor.options[Motor.value].text);
        form.AddField(
            "Gatilloelectronico",
            Gatilloelectronico.options[Gatilloelectronico.value].text
        );
        form.AddField("Mosfet", Mosfet.options[Mosfet.value].text);
        form.AddField("Nozzle", Nozzle.options[Nozzle.value].text);
        form.AddField("Gearbox", Gearbox.options[Gearbox.value].text);
        form.AddField("alcance", alcance.ToString());
        form.AddField("potencia", potencia.ToString());
        form.AddField("precision", precision.ToString());
        form.AddField("cadencia", cadencia.ToString());
        form.AddField("varianza", varianza.ToString());
        form.AddField("precio", precio.ToString());

        using (UnityWebRequest www = UnityWebRequest.Post(url, form))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
            if (www.downloadHandler.text == "0")
            {
                Debug.Log("User created succesfully");
                bien();
            }
            else
            {
                Debug.Log("error");
            }
        }
        }
    }
}
