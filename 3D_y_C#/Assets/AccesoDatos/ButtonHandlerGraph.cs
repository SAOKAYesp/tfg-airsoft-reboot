using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonHandlerGraph : MonoBehaviour
{   
    public GameObject graphContainer;
    private bool cambiar = false;
    
    public void setGraph(GameObject graphContainer)
    {
    if(cambiar == false){
    graphContainer.SetActive(false);
    cambiar = true;
    }
    else{
    graphContainer.SetActive(true);
    cambiar = false;
    }
}
}