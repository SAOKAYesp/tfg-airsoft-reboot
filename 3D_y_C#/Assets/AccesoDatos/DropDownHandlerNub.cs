using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class DropDownHandlerNub : MonoBehaviour
{
    public Componente blankItem;
    public Dropdown Nub;
    public double potenciaDrop;
    public double alcanceDrop;
    public double precisionDrop;
    public double cadenciaDrop;
    public double precio;
    public List<Componente> itemDatabase = new List<Componente>();

    public GameObject nubPieza;
    void Start()
    {
        Nub = GetComponent<Dropdown>();
        nubPieza = GameObject.Find("Nub");



        List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
        string[] options = new string[data.Count];
        List<string> items = new List<string>();

        for (var i = 0; i < data.Count; i++)
        {
            string idPiezas = data[i]["idPiezas"].ToString();
            string tipo = data[i]["tipo"].ToString();
            string marca = data[i]["marca"].ToString();
            string color = data[i]["color"].ToString();
            string material = data[i]["material"].ToString();
            string potencia = data[i]["potencia"].ToString();
            string precision = data[i]["precision"].ToString();
            string alcance = data[i]["alcance"].ToString();
            string cadencia = data[i]["cadencia"].ToString();
            string estabilidad = data[i]["estabilidad"].ToString();
            string precio = data[i]["precio"].ToString();
            string modelo = data[i]["modelo"].ToString();
            string extra = data[i]["extra"].ToString();

            Componente tempItem = new Componente(blankItem);
            tempItem.tipo = tipo;
            tempItem.marca = marca;
            tempItem.color = color;
            tempItem.material = material;
            tempItem.potencia = potencia;
            tempItem.precision = precision;
            tempItem.alcance = alcance;
            tempItem.cadencia = cadencia;
            tempItem.estabilidad = estabilidad;
            tempItem.precio = precio;
            tempItem.modelo = modelo;
            tempItem.extra = extra;
            items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
            itemDatabase.Add(tempItem);
        }

        foreach (string item in items)
        {
            if (item.Contains("Nub"))
            {
                Nub.options.Add(new Dropdown.OptionData() { text = item });
            }
        }
        Nub.RefreshShownValue();
        Nub.onValueChanged.AddListener(delegate { dropDownSelected(Nub); });
    }

    void dropDownSelected(Dropdown dropdown)
    {
        if (potenciaDrop != 0)
        {
            Window_Graph.setPotencia(0, potenciaDrop);
        }
        if (precisionDrop != 0)
        {
            Window_Graph.setPrecision(0, precisionDrop);
        }
        if (alcanceDrop != 0)
        {
            Window_Graph.setAlcance(0, alcanceDrop);
        }
        if (cadenciaDrop != 0)
        {
            Window_Graph.setCadencia(0, cadenciaDrop);
        }
        if (precio != 0)
        {
            Window_Graph.setPrecio(0, precio);
        }
        int index = dropdown.value;
        Debug.Log(Nub.options[index].text);
        foreach (Componente Componente in itemDatabase)
        {
            string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
            if (comparar == Nub.options[index].text)
            {
                precio = int.Parse(Componente.precio);
                Window_Graph.setPrecio(precio,0);
                //No afecta rendimiento

                var renderer = nubPieza.GetComponent<Renderer>();
                if (Componente.color == "Rojo")
                {
                    renderer.material.SetColor("_Color", Color.red);
                }
                if (Componente.color == "Gris")
                {
                    renderer.material.SetColor("_Color", Color.gray);
                }
                if (Componente.color == "Gris oscuro")
                {
                    renderer.material.SetColor("_Color", Color.gray);
                }
                if (Componente.color == "Gris claro")
                {
                    renderer.material.SetColor("_Color", Color.gray);
                }
                if (Componente.color == "")
                {
                    renderer.material.SetColor("_Color", Color.gray);
                }
                if (Componente.color == "Azul")
                {
                    renderer.material.SetColor("_Color", Color.blue);
                }
                if (Componente.color == "Negro")
                {
                    renderer.material.SetColor("_Color", Color.black);
                }
                if (Componente.color == "Dorado")
                {
                    renderer.material.SetColor("_Color", Color.yellow);
                }
                if (Componente.color == "Plateado")
                {
                    renderer.material.SetColor("_Color", Color.gray);
                }
                if (Componente.color == "Cobre")
                {
                    renderer.material.SetColor("_Color", Color.black);
                }

            }
        }


    }
}