using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Window_Graph : MonoBehaviour
{

    [SerializeField] private Sprite circleSprite;
    private RectTransform graphContainer;
    private RectTransform labelTemplateX;
    private RectTransform labelTemplateY;
    public static double potencia;
    public static double cadencia;
    public static double alcance;
    public static double precision;
    public static double varianza;
    public static double precio;
    public static string extra;
    public GameObject potenciaBar;
    public GameObject cadenciaBar;
    public GameObject alcanceBar;
    public GameObject precisionBar;
    public GameObject varianzaBar;
    public Text txtPotencia;
    public Text txtPrecio;
    public Text txtExtra;
    public Text txtMaxPre;
    public Text txtMaxAlc;

    private void Awake()
    {
        // Valores default de Specna Arms
        potencia = 332;
        cadencia = 16;
        alcance = 45;
        precision = 8;
        varianza = 37;

        graphContainer = transform.Find("graphContainer").GetComponent<RectTransform>();
        labelTemplateY = graphContainer.Find("labelTemplateY").GetComponent<RectTransform>();
        txtPotencia = GameObject.Find("txtPotencia").GetComponent<Text>();
        txtPrecio = GameObject.Find("txtPrecio").GetComponent<Text>();
        txtExtra = GameObject.Find("txtExtras").GetComponent<Text>();
        txtMaxPre = GameObject.Find("txtMaxPre").GetComponent<Text>();
        txtMaxAlc = GameObject.Find("txtMaxAlc").GetComponent<Text>();

        List<int> valueList = new List<int>() { 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60 };
        ShowGraph(valueList);
    }

    private void Update()
    {
        if(precio < 0 ){
            precio = 0;
        }
        
        txtPrecio.text = "PVP TOTAL RECOMENDADO: " + precio.ToString() + " EUROS";
        txtExtra.text = "EXTRA: " + extra;
        txtPotencia.text = "FPS: " + potencia;

        

        // En caso de querer añadir otra vez la barra de potencia.
        //ChangeBar(new Vector2(50, (float)potencia * 6)+5), 25, potenciaBar, "potenciaBar");

        // Condicionales para que la barra no sobresalga del límite y añade un aviso
        if(precision>55){
            ChangeBar(new Vector2(50, (float)(57 * 6)+5), 25, precisionBar, "precisionBar");
            txtMaxPre.text = "MAX";
        }
        else{
            ChangeBar(new Vector2(50, (float)(precision * 6)+5), 25, precisionBar, "precisionBar");
            txtMaxPre.text = "";
        }
        
        if(alcance>=55){
            ChangeBar(new Vector2(120, (float)(57 * 6)+5), 25, alcanceBar, "alcanceBar");
            txtMaxAlc.text = "MAX";
        }
        else{
            ChangeBar(new Vector2(120, (float)(alcance * 6)+5), 25, alcanceBar, "alcanceBar");
            txtMaxAlc.text = "";
        }  

        ChangeBar(new Vector2(195, (float)(cadencia * 6)+5), 25, cadenciaBar, "cadenciaBar");
        ChangeBar(new Vector2(275, (float)(varianza * 6)+5), 25, varianzaBar, "varianzaBar");

        HacerCaptura.setStats(alcance, potencia, precision, cadencia, varianza, precio);
        saveConfig.setStats(alcance, potencia, precision, cadencia, varianza, precio);
    }


    private void ShowGraph(List<int> valueList)
    {
        float graphHeight = graphContainer.sizeDelta.y + 30f;
        float yMaximum = 60f;
        float xSize = 50f;

        int separatorCount = 12;
        for (int i = 0; i < separatorCount; i++)
        {
            float xPosition = xSize + i * xSize;
            float yPosition = (valueList[i] / yMaximum) * graphHeight;

            RectTransform labelY = Instantiate(labelTemplateY);
            labelY.SetParent(graphContainer);
            labelY.gameObject.SetActive(true);
            float normalizedValue = i * 1f / separatorCount;
            labelY.anchoredPosition = new Vector2(-15f, normalizedValue * (graphHeight));
            labelY.GetComponent<Text>().text = Mathf.RoundToInt(normalizedValue * yMaximum).ToString();
        }


    }

    //SET para el tema de enviar y devolver variables
    public static void setPrecision(double precisionEnviada = 0, double precisionRestar = 0)
    {
        precision = precision + precisionEnviada;
        precision = precision - precisionRestar;
    }

    public static void setPotencia(double potenciaEnviada = 0, double potenciaRestar = 0)
    {
        potencia = potencia + potenciaEnviada;
        potencia = potencia - potenciaRestar;
    }

    public static void setAlcance(double alcanceEnviada = 0, double alcanceRestar = 0)
    {
        alcance = alcance + alcanceEnviada;
        alcance = alcance - alcanceRestar;
    }

    public static void setCadencia(double cadenciaEnviada = 0, double cadenciaRestar = 0)
    {
        cadencia = cadencia + cadenciaEnviada;
        cadencia = cadencia - cadenciaRestar;
    }

    public static void setVarianza(float varianzaEnviada = 0, float varianzaRestar = 0)
    {
        varianza = varianza + (varianzaEnviada / 100);
        varianza = varianza - (varianzaRestar / 100);
    }

    public static void setPrecio(double precioEnviada = 0, double precioRestar = 0)
    {
        precio = precio + precioEnviada;
        precio = precio - precioRestar;
    }

    public static void setExtra(string extraEnviada)
    {
        extra = extraEnviada;
    }


    public GameObject ChangeBar(Vector2 graphPosition, float barWidth, GameObject gameObject, string name)
    {
        gameObject = GameObject.Find(name);
        gameObject.transform.SetParent(graphContainer, false);
        RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = new Vector2(graphPosition.x, 0f);
        rectTransform.sizeDelta = new Vector2(barWidth, graphPosition.y);
        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(0, 0);
        rectTransform.pivot = new Vector2(.5f, 0f);
        return gameObject;
    }
}
