using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonHandlerSkybox : MonoBehaviour
{
[SerializeField] private Material SkyBoxForest;
[SerializeField] private Material SkyBoxWarehouse;
private bool cambiar = false;

public void SetSkyBox()
{   
    if(cambiar == false){
    RenderSettings.skybox = SkyBoxForest;
    cambiar = true;
    }
    else{
    RenderSettings.skybox = SkyBoxWarehouse;
    cambiar = false;
    }
    
    
    
}

}