using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;


public class CargarExcel : MonoBehaviour
{
public Componente blankItem;
public List<Componente> itemDatabase = new List<Componente>();
public List<string> items = new List<string>();

    public static void LoaditemData(List<Componente> itemDatabase, List<string> items, Componente blankItem)
    {

        itemDatabase.Clear();
        List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
        for (var i = 0; i < data.Count; i++)
        {
            string idPiezas = data[i]["idPiezas"].ToString();
            string tipo = data[i]["tipo"].ToString();
            string marca = data[i]["marca"].ToString();
            string color = data[i]["color"].ToString();
            string material = data[i]["material"].ToString();
            string potencia = data[i]["potencia"].ToString();
            string precision = data[i]["precision"].ToString();
            string alcance = data[i]["alcance"].ToString();
            string cadencia = data[i]["cadencia"].ToString();
            string estabilidad = data[i]["estabilidad"].ToString();

            Componente tempItem = new Componente(blankItem);
            tempItem.tipo = tipo;
            tempItem.marca = marca;
            tempItem.color = color;
            tempItem.material = material;
            tempItem.potencia = potencia;
            tempItem.precision = precision;
            tempItem.alcance = alcance;
            tempItem.cadencia = cadencia;
            items.Add(tempItem.tipo + " " + tempItem.marca);
            itemDatabase.Add(tempItem);
        }

        return;
      
}

}