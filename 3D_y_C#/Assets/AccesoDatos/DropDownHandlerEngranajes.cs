using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

public class DropDownHandlerEngranajes : MonoBehaviour
{
    public Componente blankItem;
    public Dropdown Engranajes;
    public double potenciaDrop;
    public double alcanceDrop;
    public double precisionDrop;
    public double cadenciaDrop;
    public float varianzaDrop;
    public double precio;
    public string extra;
    public List<Componente> itemDatabase = new List<Componente>();


    void Start()
    {
        Engranajes = GetComponent<Dropdown>();




        List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
        string[] options = new string[data.Count];
        List<string> items = new List<string>();

        for (var i = 0; i < data.Count; i++)
        {
            string idPiezas = data[i]["idPiezas"].ToString();
            string tipo = data[i]["tipo"].ToString();
            string marca = data[i]["marca"].ToString();
            string color = data[i]["color"].ToString();
            string material = data[i]["material"].ToString();
            string potencia = data[i]["potencia"].ToString();
            string precision = data[i]["precision"].ToString();
            string alcance = data[i]["alcance"].ToString();
            string cadencia = data[i]["cadencia"].ToString();
            string estabilidad = data[i]["estabilidad"].ToString();
            string precio = data[i]["precio"].ToString();
            string modelo = data[i]["modelo"].ToString();
            string extra = data[i]["extra"].ToString();

            Componente tempItem = new Componente(blankItem);
            tempItem.tipo = tipo;
            tempItem.marca = marca;
            tempItem.color = color;
            tempItem.material = material;
            tempItem.potencia = potencia;
            tempItem.precision = precision;
            tempItem.alcance = alcance;
            tempItem.cadencia = cadencia;
            tempItem.estabilidad = estabilidad;
            tempItem.precio = precio;
            tempItem.modelo = modelo;
            tempItem.extra = extra;
            items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
            itemDatabase.Add(tempItem);
        }

        foreach (string item in items)
        {
            if (item.Contains("Engranajes"))
            {
                Engranajes.options.Add(new Dropdown.OptionData() { text = item });
            }
        }
        Engranajes.RefreshShownValue();
        Engranajes.onValueChanged.AddListener(delegate { dropDownSelected(Engranajes); });
    }

    void dropDownSelected(Dropdown dropdown)
    {
        if (potenciaDrop != 0)
        {
            Window_Graph.setPotencia(0, potenciaDrop);
        }
        if (alcanceDrop != 0)
        {
            Window_Graph.setAlcance(0, alcanceDrop);
        }
        if (varianzaDrop != 0)
        {
            Window_Graph.setVarianza(0, varianzaDrop);
        }
        if (precisionDrop != 0)
        {
            Window_Graph.setPrecision(0, precisionDrop);
        }
        if (cadenciaDrop != 0)
        {
            Window_Graph.setCadencia(0, cadenciaDrop);
        }
        if (precio != 0)
        {
            Window_Graph.setPrecio(0, precio);
        }
        int index = dropdown.value;
        Debug.Log(Engranajes.options[index].text);
        foreach (Componente Componente in itemDatabase)
        {
            string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
            if (comparar == Engranajes.options[index].text)
            {
                potenciaDrop = int.Parse(Componente.potencia);
                alcanceDrop = int.Parse(Componente.alcance);
                varianzaDrop = int.Parse(Componente.estabilidad);
                precisionDrop = int.Parse(Componente.precision);
                cadenciaDrop = int.Parse(Componente.cadencia);
                precio = int.Parse(Componente.precio);
                extra = Componente.extra;

                Window_Graph.setAlcance(alcanceDrop);
                Window_Graph.setPotencia(potenciaDrop);
                Window_Graph.setVarianza(varianzaDrop);
                Window_Graph.setPrecision(precisionDrop);
                Window_Graph.setCadencia(cadenciaDrop);
                Window_Graph.setPrecio(precio,0);

            }
        }


    }
}