using UnityEngine;
using UnityEditor;
using System.Collections;

public class rotarObjeto : MonoBehaviour
{
  
      float rotSpeed = 40;

      

   void Update()
    {
        if (Input.GetMouseButton(0))
        {
        float rotX = Input.GetAxis("Mouse X") * rotSpeed * Mathf.Deg2Rad;
        float rotY = Input.GetAxis("Mouse Y") * rotSpeed * Mathf.Deg2Rad;

        transform.Rotate(Vector3.up, -rotX, Space.Self);
        transform.Rotate(Vector3.right, rotY, Space.Self);
        }
        
    }
}