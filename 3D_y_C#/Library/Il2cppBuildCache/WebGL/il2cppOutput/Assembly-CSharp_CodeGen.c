﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void ButtonHandlerGraph::setGraph(UnityEngine.GameObject)
extern void ButtonHandlerGraph_setGraph_mB7ED3F8A0C097873F69BCA8EBFF77B4D4F0DBA82 (void);
// 0x00000002 System.Void ButtonHandlerGraph::.ctor()
extern void ButtonHandlerGraph__ctor_mF773335A39FAD4BAF34581BD72CE941A086FC857 (void);
// 0x00000003 System.Void ButtonHandlerSave::screenShot()
extern void ButtonHandlerSave_screenShot_m423D3651B0FD70F9289A7372D6E4D7A6D0A47660 (void);
// 0x00000004 System.Void ButtonHandlerSave::.ctor()
extern void ButtonHandlerSave__ctor_mF44DAE69EE8C0E854E87586B52C67E0DF649EBED (void);
// 0x00000005 System.Void ButtonHandlerSkybox::SetSkyBox()
extern void ButtonHandlerSkybox_SetSkyBox_m5D0EFD5FA7E7C36E6AC07391FE5A3D18C68920E9 (void);
// 0x00000006 System.Void ButtonHandlerSkybox::.ctor()
extern void ButtonHandlerSkybox__ctor_m3520241F16A13F5F59FA77E8CB0E64493E7B69B4 (void);
// 0x00000007 System.Void CargarExcel::LoaditemData(System.Collections.Generic.List`1<Componente>,System.Collections.Generic.List`1<System.String>,Componente)
extern void CargarExcel_LoaditemData_m5EC2A50C04B39A1E3948A4A9A283D2E7C3D23823 (void);
// 0x00000008 System.Void CargarExcel::.ctor()
extern void CargarExcel__ctor_mBFE1D2269B6DF852B2DD98C66B03CDA5F9E792DE (void);
// 0x00000009 System.Void Componente::.ctor(Componente)
extern void Componente__ctor_m34F2CAA35E7DF597BE0E1954DF0FA191F11411A7 (void);
// 0x0000000A System.Void DropDownHandler::Start()
extern void DropDownHandler_Start_mB07AC4048B4D6B90E910182C8DB59A0BF99F8BDE (void);
// 0x0000000B System.Void DropDownHandler::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandler_dropDownSelected_m4E4913C7E94A23169ECBC613BB4859B25D7C98FF (void);
// 0x0000000C System.Void DropDownHandler::.ctor()
extern void DropDownHandler__ctor_m4A19DE59A3DE6FA79DE75D2FF573C28AF75CA522 (void);
// 0x0000000D System.Void DropDownHandler::<Start>b__9_0(System.Int32)
extern void DropDownHandler_U3CStartU3Eb__9_0_m7EE914DA91E2BBBD5628EFBE689C764E269F7B05 (void);
// 0x0000000E System.Void DropDownHandlerCabezaCilindro::Start()
extern void DropDownHandlerCabezaCilindro_Start_mC0ADFCFD4F926AE715581EDCE4E6F1F55CC0E48C (void);
// 0x0000000F System.Void DropDownHandlerCabezaCilindro::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerCabezaCilindro_dropDownSelected_m06444E03DA4EE76091B2FFF31E45D08E8036B4E2 (void);
// 0x00000010 System.Void DropDownHandlerCabezaCilindro::.ctor()
extern void DropDownHandlerCabezaCilindro__ctor_m1846C40448B9B6AD67F26A6E87B952775BBCDF6C (void);
// 0x00000011 System.Void DropDownHandlerCabezaCilindro::<Start>b__10_0(System.Int32)
extern void DropDownHandlerCabezaCilindro_U3CStartU3Eb__10_0_m7826E5D27358E0507E1A60714292A10E7DE2C244 (void);
// 0x00000012 System.Void DropDownHandlerCabezaPiston::Start()
extern void DropDownHandlerCabezaPiston_Start_m3F8799399277DCF04BBC92EB0FBA17F4B09DDE4B (void);
// 0x00000013 System.Void DropDownHandlerCabezaPiston::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerCabezaPiston_dropDownSelected_m4A108C4EA8E6A5B90683C2294773E5299BBE5D2D (void);
// 0x00000014 System.Void DropDownHandlerCabezaPiston::.ctor()
extern void DropDownHandlerCabezaPiston__ctor_m8C6FC2DE5495377405A216864C104E07C961EF27 (void);
// 0x00000015 System.Void DropDownHandlerCabezaPiston::<Start>b__9_0(System.Int32)
extern void DropDownHandlerCabezaPiston_U3CStartU3Eb__9_0_mCD62ABFCE13C5ADEDF0D0D12A07019039BCDF50C (void);
// 0x00000016 System.Void DropDownHandlerCamaraDeHop::Start()
extern void DropDownHandlerCamaraDeHop_Start_m7EB62EA2E253D9DB1A58217B7EC7D8A6EA9105FA (void);
// 0x00000017 System.Void DropDownHandlerCamaraDeHop::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerCamaraDeHop_dropDownSelected_m7C4D83A73410D8D1B8DBA135E095025892A07C93 (void);
// 0x00000018 System.Void DropDownHandlerCamaraDeHop::.ctor()
extern void DropDownHandlerCamaraDeHop__ctor_mA3C24FF42DDC05CFB62AF284A4BD180E9EB64CA9 (void);
// 0x00000019 System.Void DropDownHandlerCamaraDeHop::<Start>b__10_0(System.Int32)
extern void DropDownHandlerCamaraDeHop_U3CStartU3Eb__10_0_m22232370DAAB5FD0AD58BDB8F58476C76506D32B (void);
// 0x0000001A System.Void DropDownHandlerCa?on::Start()
extern void DropDownHandlerCaUF1on_Start_m159365355493D6BC8F5525EB5C9E5ADF1A43A2FF (void);
// 0x0000001B System.Void DropDownHandlerCa?on::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerCaUF1on_dropDownSelected_mFDE0624C433A169376A2B5A76E8B543E7779B4F8 (void);
// 0x0000001C System.Void DropDownHandlerCa?on::.ctor()
extern void DropDownHandlerCaUF1on__ctor_mBBD77EAF5E03C7174DDDCBD47F1C51C2E876AB4E (void);
// 0x0000001D System.Void DropDownHandlerCa?on::<Start>b__9_0(System.Int32)
extern void DropDownHandlerCaUF1on_U3CStartU3Eb__9_0_m5D61B4D8B31667695D01E0AD965A64FF0525B1E8 (void);
// 0x0000001E System.Void DropDownHandlerCilindro::Start()
extern void DropDownHandlerCilindro_Start_m2F617D7493FA5ABBEFF151E0CD883B73BC9A9273 (void);
// 0x0000001F System.Void DropDownHandlerCilindro::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerCilindro_dropDownSelected_m1DCCE8ECF81DE66C676482CE75A1CB059FA54FF8 (void);
// 0x00000020 System.Void DropDownHandlerCilindro::.ctor()
extern void DropDownHandlerCilindro__ctor_m746BEC5103EED1620BD97B000E8650A834F12EC2 (void);
// 0x00000021 System.Void DropDownHandlerCilindro::<Start>b__9_0(System.Int32)
extern void DropDownHandlerCilindro_U3CStartU3Eb__9_0_m4FE9B784EA0505393DF1E0FC5949A7C7B0EEF8EA (void);
// 0x00000022 System.Void DropDownHandlerEngranajes::Start()
extern void DropDownHandlerEngranajes_Start_m73F5038B0BAF85BBFA1BCC508C2BE8165EDEBE94 (void);
// 0x00000023 System.Void DropDownHandlerEngranajes::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerEngranajes_dropDownSelected_mE06EF54196CAA9988B3FC6351922B0474BA35A9C (void);
// 0x00000024 System.Void DropDownHandlerEngranajes::.ctor()
extern void DropDownHandlerEngranajes__ctor_mAA1CDFD0F081D46548EDCAA4029CB428940454C9 (void);
// 0x00000025 System.Void DropDownHandlerEngranajes::<Start>b__8_0(System.Int32)
extern void DropDownHandlerEngranajes_U3CStartU3Eb__8_0_m0197BF88A0690DBA5AFC12D86F1973064855DC6F (void);
// 0x00000026 System.Void DropDownHandlerGatillo::Start()
extern void DropDownHandlerGatillo_Start_mEF7D9CF4F85F811F12207E817E7FE10820657B17 (void);
// 0x00000027 System.Void DropDownHandlerGatillo::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerGatillo_dropDownSelected_mEDF7C3103DCEDEB5434857F68D64FEA4C48CB58A (void);
// 0x00000028 System.Void DropDownHandlerGatillo::.ctor()
extern void DropDownHandlerGatillo__ctor_m02DF4DD14E9CBCE0818044F7799585D404BC893A (void);
// 0x00000029 System.Void DropDownHandlerGatillo::<Start>b__9_0(System.Int32)
extern void DropDownHandlerGatillo_U3CStartU3Eb__9_0_m7605021D1F3FB6AB1F02FC4D793E326B7E57AB0D (void);
// 0x0000002A System.Void DropDownHandlerGearbox::Start()
extern void DropDownHandlerGearbox_Start_m12008C154220184B3ED670800C6918A64B9ED578 (void);
// 0x0000002B System.Void DropDownHandlerGearbox::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerGearbox_dropDownSelected_m1C4476A8AC0D3239A8A5CDA66DDF7871F03E5CD4 (void);
// 0x0000002C System.Void DropDownHandlerGearbox::.ctor()
extern void DropDownHandlerGearbox__ctor_mD3C14E00C85FABF9882078D026DF33C62AE1005D (void);
// 0x0000002D System.Void DropDownHandlerGearbox::<Start>b__9_0(System.Int32)
extern void DropDownHandlerGearbox_U3CStartU3Eb__9_0_m3D099C5DF6640224F8260EC94E6FDE28884B2F84 (void);
// 0x0000002E System.Void DropDownHandlerGomaHop::Start()
extern void DropDownHandlerGomaHop_Start_mBA1C889417DD8A24A9086B6FE01A1BA37841007D (void);
// 0x0000002F System.Void DropDownHandlerGomaHop::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerGomaHop_dropDownSelected_mD62A6DEE1F5020AA969F44269B45F78E30BC385B (void);
// 0x00000030 System.Void DropDownHandlerGomaHop::.ctor()
extern void DropDownHandlerGomaHop__ctor_m45A184EFA9FD6C45530303FBD75B9FD861CE4F74 (void);
// 0x00000031 System.Void DropDownHandlerGomaHop::<Start>b__10_0(System.Int32)
extern void DropDownHandlerGomaHop_U3CStartU3Eb__10_0_mFD3BC39CDCEA0F9AE5F700B310BD68CC56FA0F00 (void);
// 0x00000032 System.Void DropDownHandlerGuiaDeMuelle::Start()
extern void DropDownHandlerGuiaDeMuelle_Start_mFF6702AA590A61AECA0ED9696B064AA34EC94945 (void);
// 0x00000033 System.Void DropDownHandlerGuiaDeMuelle::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerGuiaDeMuelle_dropDownSelected_m8ED51D3A057BC518D4F0BC56217B935C7FE9B958 (void);
// 0x00000034 System.Void DropDownHandlerGuiaDeMuelle::.ctor()
extern void DropDownHandlerGuiaDeMuelle__ctor_m7BFA0E7D65C712A011ACB4AF418348D077C34AAB (void);
// 0x00000035 System.Void DropDownHandlerGuiaDeMuelle::<Start>b__10_0(System.Int32)
extern void DropDownHandlerGuiaDeMuelle_U3CStartU3Eb__10_0_mEBDF6A013A0525EFFC40D23695B486B809AB7A04 (void);
// 0x00000036 System.Void DropDownHandlerMosfet::Start()
extern void DropDownHandlerMosfet_Start_m8764488B9EAAF1F68C22A2C93036755ABE9DC5D5 (void);
// 0x00000037 System.Void DropDownHandlerMosfet::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerMosfet_dropDownSelected_m85D7622AA4B8A79335ABAACD1E829542A45CF939 (void);
// 0x00000038 System.Void DropDownHandlerMosfet::.ctor()
extern void DropDownHandlerMosfet__ctor_m59F42D656318669FA6518A5D3AAFB24E87A0F3B2 (void);
// 0x00000039 System.Void DropDownHandlerMosfet::<Start>b__9_0(System.Int32)
extern void DropDownHandlerMosfet_U3CStartU3Eb__9_0_m68611DC1CA657BDD66D1BF8D5112F8F72E5BAAA4 (void);
// 0x0000003A System.Void DropDownHandlerMuelle::Start()
extern void DropDownHandlerMuelle_Start_m9EF74C482BDEB7C836CC088FAE854350FBA88029 (void);
// 0x0000003B System.Void DropDownHandlerMuelle::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerMuelle_dropDownSelected_mBC002425D5CAB9905CD369135ECA0532B71D3A30 (void);
// 0x0000003C System.Void DropDownHandlerMuelle::.ctor()
extern void DropDownHandlerMuelle__ctor_m9F245F7581CDBCDAE9D342F051912DA551DDBCDE (void);
// 0x0000003D System.Void DropDownHandlerMuelle::<Start>b__10_0(System.Int32)
extern void DropDownHandlerMuelle_U3CStartU3Eb__10_0_m89D2B7B77400498E96EF9BCC512705FCD139D860 (void);
// 0x0000003E System.Void DropDownHandlerNozzle::Start()
extern void DropDownHandlerNozzle_Start_m95475D47F951D8FD5D4AE0AD7B6F393826147AB9 (void);
// 0x0000003F System.Void DropDownHandlerNozzle::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerNozzle_dropDownSelected_m444C58CF44075E33C74046AFAA77D7CAFC6CE109 (void);
// 0x00000040 System.Void DropDownHandlerNozzle::.ctor()
extern void DropDownHandlerNozzle__ctor_m50913D00EBBA2D073B7E02DA60EE5B5B388A5C1D (void);
// 0x00000041 System.Void DropDownHandlerNozzle::<Start>b__9_0(System.Int32)
extern void DropDownHandlerNozzle_U3CStartU3Eb__9_0_mF2E71C44DBA942CB509E65E224374F34C67A2AE2 (void);
// 0x00000042 System.Void DropDownHandlerNub::Start()
extern void DropDownHandlerNub_Start_mC8D3F7C79BC9F16B788030C8DF08DDC3C5DA09B0 (void);
// 0x00000043 System.Void DropDownHandlerNub::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerNub_dropDownSelected_m6BE30047445B5A4C3DFDDD38E3C46E3A05A6B731 (void);
// 0x00000044 System.Void DropDownHandlerNub::.ctor()
extern void DropDownHandlerNub__ctor_m174FFA77081E8452D69D3717456345FD5BDBE6EE (void);
// 0x00000045 System.Void DropDownHandlerNub::<Start>b__9_0(System.Int32)
extern void DropDownHandlerNub_U3CStartU3Eb__9_0_m6BB6A440B6B3E19C10DFFED7764A1EE26922002B (void);
// 0x00000046 System.Void DropDownHandlerPiston::Start()
extern void DropDownHandlerPiston_Start_m099ACFB925912B6360B75A3169E0BB2B10BA5E2D (void);
// 0x00000047 System.Void DropDownHandlerPiston::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerPiston_dropDownSelected_m0631750510D751ECF7C7E7FF1A9B5A3AE6D92AAC (void);
// 0x00000048 System.Void DropDownHandlerPiston::.ctor()
extern void DropDownHandlerPiston__ctor_mE4C5BF578976215B5CC67DFE8391FB18E2289190 (void);
// 0x00000049 System.Void DropDownHandlerPiston::<Start>b__10_0(System.Int32)
extern void DropDownHandlerPiston_U3CStartU3Eb__10_0_m1F3898472DD85D89CA6AEB8D20C4C6864FCCBFA5 (void);
// 0x0000004A System.Void DropDownHandlerTapperPlate::Start()
extern void DropDownHandlerTapperPlate_Start_m78627F44BBE1A7A2F46F55B647934FFA844399EF (void);
// 0x0000004B System.Void DropDownHandlerTapperPlate::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerTapperPlate_dropDownSelected_m01BAB9ED710027172BC0BE0744A767CABBB9451E (void);
// 0x0000004C System.Void DropDownHandlerTapperPlate::.ctor()
extern void DropDownHandlerTapperPlate__ctor_mBD30D891B4A5C557D9F5B0FF1ACEF77C9884EA09 (void);
// 0x0000004D System.Void DropDownHandlerTapperPlate::<Start>b__10_0(System.Int32)
extern void DropDownHandlerTapperPlate_U3CStartU3Eb__10_0_m74B139D5F645F85E9BB44BF986FA95A3E438C04B (void);
// 0x0000004E System.Void HacerCaptura::Captura()
extern void HacerCaptura_Captura_m80BFFF4912AF8DDC6B8A53CEA96549A51D0A6203 (void);
// 0x0000004F System.Collections.IEnumerator HacerCaptura::descargarPNG()
extern void HacerCaptura_descargarPNG_m5E0B75EFC0D628AA40BCD70953B66E9ED46B36FC (void);
// 0x00000050 System.Void HacerCaptura::SaveScreenshotWebGL(System.String,System.String)
extern void HacerCaptura_SaveScreenshotWebGL_m9BBAEFE9E9F672EAF3BB565A4E74100A0EAD90EE (void);
// 0x00000051 System.Void HacerCaptura::.ctor()
extern void HacerCaptura__ctor_mE7C8AA6BD6809FA4CAE818CE51889A0FCB05FE2A (void);
// 0x00000052 System.Void HacerCaptura/<descargarPNG>d__1::.ctor(System.Int32)
extern void U3CdescargarPNGU3Ed__1__ctor_m2E8DDDF699786F07ABEFDABDCF2ADD068D32A315 (void);
// 0x00000053 System.Void HacerCaptura/<descargarPNG>d__1::System.IDisposable.Dispose()
extern void U3CdescargarPNGU3Ed__1_System_IDisposable_Dispose_mC464A5CA3AC92B483613ABA479E2230B0BDE9C70 (void);
// 0x00000054 System.Boolean HacerCaptura/<descargarPNG>d__1::MoveNext()
extern void U3CdescargarPNGU3Ed__1_MoveNext_m8294235C935D9A2079E44647851288B229BCE24A (void);
// 0x00000055 System.Object HacerCaptura/<descargarPNG>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdescargarPNGU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m11E993EB22EAB28A43AEDD2084448F44F250FAD9 (void);
// 0x00000056 System.Void HacerCaptura/<descargarPNG>d__1::System.Collections.IEnumerator.Reset()
extern void U3CdescargarPNGU3Ed__1_System_Collections_IEnumerator_Reset_m8543DADEE595C63CC59515C2EA9C075D3B521DD4 (void);
// 0x00000057 System.Object HacerCaptura/<descargarPNG>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CdescargarPNGU3Ed__1_System_Collections_IEnumerator_get_Current_m4C271FDC1BBA3A8B2CADED6C9A8E0A9FF73AFC78 (void);
// 0x00000058 System.Void PressHandler::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void PressHandler_OnPointerDown_m999EB467CF64F1D175EB39F5994612B0FF4C2E7A (void);
// 0x00000059 System.Void PressHandler::.ctor()
extern void PressHandler__ctor_m3CCBD581BA7F5F0206C0C8D364689B90748D8F21 (void);
// 0x0000005A System.Void PressHandler/ButtonPressEvent::.ctor()
extern void ButtonPressEvent__ctor_mC8D923F1B932DCC6CC42620BED1475435BDCA97C (void);
// 0x0000005B System.Void Window_Graph::Awake()
extern void Window_Graph_Awake_m4DE200BD45BD2A8972E31463180896C6CE0F9344 (void);
// 0x0000005C System.Void Window_Graph::Update()
extern void Window_Graph_Update_m07B54C3C197F08B35635646582FA88E452664D94 (void);
// 0x0000005D System.Void Window_Graph::ShowGraph(System.Collections.Generic.List`1<System.Int32>)
extern void Window_Graph_ShowGraph_m82918D64DD82F36A782E3522A454DF98ECA36C7A (void);
// 0x0000005E System.Void Window_Graph::setPrecision(System.Double,System.Double)
extern void Window_Graph_setPrecision_mD077C0BF48F68C8645F8FE73A7EB1E3F1056E964 (void);
// 0x0000005F System.Void Window_Graph::setPotencia(System.Double,System.Double)
extern void Window_Graph_setPotencia_m50C4C50AEDD22E325CF360039D821FBACFF0A450 (void);
// 0x00000060 System.Void Window_Graph::setAlcance(System.Double,System.Double)
extern void Window_Graph_setAlcance_m6A8534AD78B4E7314CBF5B109CABE1222AA52E5D (void);
// 0x00000061 System.Void Window_Graph::setCadencia(System.Double,System.Double)
extern void Window_Graph_setCadencia_mEEA99F4FC29B53DE4D6AE6C7E7F329C190F381CC (void);
// 0x00000062 System.Void Window_Graph::setVarianza(System.Double,System.Double)
extern void Window_Graph_setVarianza_mEF2CD01BE121E1FBDDF5987480791AAD8B135996 (void);
// 0x00000063 System.Void Window_Graph::setPrecio(System.Double,System.Double)
extern void Window_Graph_setPrecio_mC4B8E418FE79CB751941F0C19083F18135E697FE (void);
// 0x00000064 System.Void Window_Graph::setExtra(System.String)
extern void Window_Graph_setExtra_m7C916C0BDD9F1D8BC8FF8202FC4C93EB1EA916AC (void);
// 0x00000065 UnityEngine.GameObject Window_Graph::ChangeBar(UnityEngine.Vector2,System.Single,UnityEngine.GameObject,System.String)
extern void Window_Graph_ChangeBar_mFE58773E7D5B8177AEAC915A9F72C249BDE6E747 (void);
// 0x00000066 System.Void Window_Graph::.ctor()
extern void Window_Graph__ctor_m70CCCEB054D75A061A090BEBD48689E342A79F1B (void);
// 0x00000067 System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>> LeerExcel::Read(System.String)
extern void LeerExcel_Read_mEF6C3DF44F7195F6D9B764A3053DE3777779FF1D (void);
// 0x00000068 System.Void LeerExcel::.ctor()
extern void LeerExcel__ctor_mD475F2BF78B22BDE15048FD9058EE221880F4DE8 (void);
// 0x00000069 System.Void LeerExcel::.cctor()
extern void LeerExcel__cctor_m7C33F5A931B9A3C94C13298BF7FD0743E96DB063 (void);
// 0x0000006A System.Void changeColor::Start()
extern void changeColor_Start_m3336B28A995D4EC117DF1882A333FEBF3111B9C4 (void);
// 0x0000006B System.Void changeColor::SelectMaterial(System.Int32)
extern void changeColor_SelectMaterial_mE6F15F6E458C44C178D2F3D2F250E06A582EBBAD (void);
// 0x0000006C System.Void changeColor::SetColor(System.Int32)
extern void changeColor_SetColor_mFCC6F560A6AB325EF20DA015A12D879780E9AABC (void);
// 0x0000006D System.Void changeColor::onRed()
extern void changeColor_onRed_m0C5DC98D1D8BEE9DE8F943F62FF203BDE9B62C38 (void);
// 0x0000006E System.Void changeColor::onBlue()
extern void changeColor_onBlue_m3FCF401696ED719497BC48F7DA5D821631938E7E (void);
// 0x0000006F System.Void changeColor::onGreen()
extern void changeColor_onGreen_mC13D70E0A4AAB0FA5D8510588B25D81C6F485BD3 (void);
// 0x00000070 System.Void changeColor::onYellow()
extern void changeColor_onYellow_m778E0F715BCCC855E8AC4A839EBDAA619D27C9D9 (void);
// 0x00000071 System.Void changeColor::.ctor()
extern void changeColor__ctor_mFC5E0AC250AA509C2E1EED1A7984A7845D4C71D6 (void);
// 0x00000072 System.Void lockRotation::Start()
extern void lockRotation_Start_m5EEC8BA2D86FD59B27BB77D62E3F2ADE64C4083C (void);
// 0x00000073 System.Void lockRotation::LateUpdate()
extern void lockRotation_LateUpdate_m4F2BD9A821BBA33F5536DF482377EA356EA5D042 (void);
// 0x00000074 System.Void lockRotation::.ctor()
extern void lockRotation__ctor_mE9082B7F1BEAD35790826035B7C4A7AC656CA6E1 (void);
// 0x00000075 System.Void rotarObjeto::Update()
extern void rotarObjeto_Update_mBF63D41AFACFF57C2D2D29108B73F171CB488535 (void);
// 0x00000076 System.Void rotarObjeto::.ctor()
extern void rotarObjeto__ctor_mA350FB6FE78C618EB5B391084DC8364C5C2CC045 (void);
static Il2CppMethodPointer s_methodPointers[118] = 
{
	ButtonHandlerGraph_setGraph_mB7ED3F8A0C097873F69BCA8EBFF77B4D4F0DBA82,
	ButtonHandlerGraph__ctor_mF773335A39FAD4BAF34581BD72CE941A086FC857,
	ButtonHandlerSave_screenShot_m423D3651B0FD70F9289A7372D6E4D7A6D0A47660,
	ButtonHandlerSave__ctor_mF44DAE69EE8C0E854E87586B52C67E0DF649EBED,
	ButtonHandlerSkybox_SetSkyBox_m5D0EFD5FA7E7C36E6AC07391FE5A3D18C68920E9,
	ButtonHandlerSkybox__ctor_m3520241F16A13F5F59FA77E8CB0E64493E7B69B4,
	CargarExcel_LoaditemData_m5EC2A50C04B39A1E3948A4A9A283D2E7C3D23823,
	CargarExcel__ctor_mBFE1D2269B6DF852B2DD98C66B03CDA5F9E792DE,
	Componente__ctor_m34F2CAA35E7DF597BE0E1954DF0FA191F11411A7,
	DropDownHandler_Start_mB07AC4048B4D6B90E910182C8DB59A0BF99F8BDE,
	DropDownHandler_dropDownSelected_m4E4913C7E94A23169ECBC613BB4859B25D7C98FF,
	DropDownHandler__ctor_m4A19DE59A3DE6FA79DE75D2FF573C28AF75CA522,
	DropDownHandler_U3CStartU3Eb__9_0_m7EE914DA91E2BBBD5628EFBE689C764E269F7B05,
	DropDownHandlerCabezaCilindro_Start_mC0ADFCFD4F926AE715581EDCE4E6F1F55CC0E48C,
	DropDownHandlerCabezaCilindro_dropDownSelected_m06444E03DA4EE76091B2FFF31E45D08E8036B4E2,
	DropDownHandlerCabezaCilindro__ctor_m1846C40448B9B6AD67F26A6E87B952775BBCDF6C,
	DropDownHandlerCabezaCilindro_U3CStartU3Eb__10_0_m7826E5D27358E0507E1A60714292A10E7DE2C244,
	DropDownHandlerCabezaPiston_Start_m3F8799399277DCF04BBC92EB0FBA17F4B09DDE4B,
	DropDownHandlerCabezaPiston_dropDownSelected_m4A108C4EA8E6A5B90683C2294773E5299BBE5D2D,
	DropDownHandlerCabezaPiston__ctor_m8C6FC2DE5495377405A216864C104E07C961EF27,
	DropDownHandlerCabezaPiston_U3CStartU3Eb__9_0_mCD62ABFCE13C5ADEDF0D0D12A07019039BCDF50C,
	DropDownHandlerCamaraDeHop_Start_m7EB62EA2E253D9DB1A58217B7EC7D8A6EA9105FA,
	DropDownHandlerCamaraDeHop_dropDownSelected_m7C4D83A73410D8D1B8DBA135E095025892A07C93,
	DropDownHandlerCamaraDeHop__ctor_mA3C24FF42DDC05CFB62AF284A4BD180E9EB64CA9,
	DropDownHandlerCamaraDeHop_U3CStartU3Eb__10_0_m22232370DAAB5FD0AD58BDB8F58476C76506D32B,
	DropDownHandlerCaUF1on_Start_m159365355493D6BC8F5525EB5C9E5ADF1A43A2FF,
	DropDownHandlerCaUF1on_dropDownSelected_mFDE0624C433A169376A2B5A76E8B543E7779B4F8,
	DropDownHandlerCaUF1on__ctor_mBBD77EAF5E03C7174DDDCBD47F1C51C2E876AB4E,
	DropDownHandlerCaUF1on_U3CStartU3Eb__9_0_m5D61B4D8B31667695D01E0AD965A64FF0525B1E8,
	DropDownHandlerCilindro_Start_m2F617D7493FA5ABBEFF151E0CD883B73BC9A9273,
	DropDownHandlerCilindro_dropDownSelected_m1DCCE8ECF81DE66C676482CE75A1CB059FA54FF8,
	DropDownHandlerCilindro__ctor_m746BEC5103EED1620BD97B000E8650A834F12EC2,
	DropDownHandlerCilindro_U3CStartU3Eb__9_0_m4FE9B784EA0505393DF1E0FC5949A7C7B0EEF8EA,
	DropDownHandlerEngranajes_Start_m73F5038B0BAF85BBFA1BCC508C2BE8165EDEBE94,
	DropDownHandlerEngranajes_dropDownSelected_mE06EF54196CAA9988B3FC6351922B0474BA35A9C,
	DropDownHandlerEngranajes__ctor_mAA1CDFD0F081D46548EDCAA4029CB428940454C9,
	DropDownHandlerEngranajes_U3CStartU3Eb__8_0_m0197BF88A0690DBA5AFC12D86F1973064855DC6F,
	DropDownHandlerGatillo_Start_mEF7D9CF4F85F811F12207E817E7FE10820657B17,
	DropDownHandlerGatillo_dropDownSelected_mEDF7C3103DCEDEB5434857F68D64FEA4C48CB58A,
	DropDownHandlerGatillo__ctor_m02DF4DD14E9CBCE0818044F7799585D404BC893A,
	DropDownHandlerGatillo_U3CStartU3Eb__9_0_m7605021D1F3FB6AB1F02FC4D793E326B7E57AB0D,
	DropDownHandlerGearbox_Start_m12008C154220184B3ED670800C6918A64B9ED578,
	DropDownHandlerGearbox_dropDownSelected_m1C4476A8AC0D3239A8A5CDA66DDF7871F03E5CD4,
	DropDownHandlerGearbox__ctor_mD3C14E00C85FABF9882078D026DF33C62AE1005D,
	DropDownHandlerGearbox_U3CStartU3Eb__9_0_m3D099C5DF6640224F8260EC94E6FDE28884B2F84,
	DropDownHandlerGomaHop_Start_mBA1C889417DD8A24A9086B6FE01A1BA37841007D,
	DropDownHandlerGomaHop_dropDownSelected_mD62A6DEE1F5020AA969F44269B45F78E30BC385B,
	DropDownHandlerGomaHop__ctor_m45A184EFA9FD6C45530303FBD75B9FD861CE4F74,
	DropDownHandlerGomaHop_U3CStartU3Eb__10_0_mFD3BC39CDCEA0F9AE5F700B310BD68CC56FA0F00,
	DropDownHandlerGuiaDeMuelle_Start_mFF6702AA590A61AECA0ED9696B064AA34EC94945,
	DropDownHandlerGuiaDeMuelle_dropDownSelected_m8ED51D3A057BC518D4F0BC56217B935C7FE9B958,
	DropDownHandlerGuiaDeMuelle__ctor_m7BFA0E7D65C712A011ACB4AF418348D077C34AAB,
	DropDownHandlerGuiaDeMuelle_U3CStartU3Eb__10_0_mEBDF6A013A0525EFFC40D23695B486B809AB7A04,
	DropDownHandlerMosfet_Start_m8764488B9EAAF1F68C22A2C93036755ABE9DC5D5,
	DropDownHandlerMosfet_dropDownSelected_m85D7622AA4B8A79335ABAACD1E829542A45CF939,
	DropDownHandlerMosfet__ctor_m59F42D656318669FA6518A5D3AAFB24E87A0F3B2,
	DropDownHandlerMosfet_U3CStartU3Eb__9_0_m68611DC1CA657BDD66D1BF8D5112F8F72E5BAAA4,
	DropDownHandlerMuelle_Start_m9EF74C482BDEB7C836CC088FAE854350FBA88029,
	DropDownHandlerMuelle_dropDownSelected_mBC002425D5CAB9905CD369135ECA0532B71D3A30,
	DropDownHandlerMuelle__ctor_m9F245F7581CDBCDAE9D342F051912DA551DDBCDE,
	DropDownHandlerMuelle_U3CStartU3Eb__10_0_m89D2B7B77400498E96EF9BCC512705FCD139D860,
	DropDownHandlerNozzle_Start_m95475D47F951D8FD5D4AE0AD7B6F393826147AB9,
	DropDownHandlerNozzle_dropDownSelected_m444C58CF44075E33C74046AFAA77D7CAFC6CE109,
	DropDownHandlerNozzle__ctor_m50913D00EBBA2D073B7E02DA60EE5B5B388A5C1D,
	DropDownHandlerNozzle_U3CStartU3Eb__9_0_mF2E71C44DBA942CB509E65E224374F34C67A2AE2,
	DropDownHandlerNub_Start_mC8D3F7C79BC9F16B788030C8DF08DDC3C5DA09B0,
	DropDownHandlerNub_dropDownSelected_m6BE30047445B5A4C3DFDDD38E3C46E3A05A6B731,
	DropDownHandlerNub__ctor_m174FFA77081E8452D69D3717456345FD5BDBE6EE,
	DropDownHandlerNub_U3CStartU3Eb__9_0_m6BB6A440B6B3E19C10DFFED7764A1EE26922002B,
	DropDownHandlerPiston_Start_m099ACFB925912B6360B75A3169E0BB2B10BA5E2D,
	DropDownHandlerPiston_dropDownSelected_m0631750510D751ECF7C7E7FF1A9B5A3AE6D92AAC,
	DropDownHandlerPiston__ctor_mE4C5BF578976215B5CC67DFE8391FB18E2289190,
	DropDownHandlerPiston_U3CStartU3Eb__10_0_m1F3898472DD85D89CA6AEB8D20C4C6864FCCBFA5,
	DropDownHandlerTapperPlate_Start_m78627F44BBE1A7A2F46F55B647934FFA844399EF,
	DropDownHandlerTapperPlate_dropDownSelected_m01BAB9ED710027172BC0BE0744A767CABBB9451E,
	DropDownHandlerTapperPlate__ctor_mBD30D891B4A5C557D9F5B0FF1ACEF77C9884EA09,
	DropDownHandlerTapperPlate_U3CStartU3Eb__10_0_m74B139D5F645F85E9BB44BF986FA95A3E438C04B,
	HacerCaptura_Captura_m80BFFF4912AF8DDC6B8A53CEA96549A51D0A6203,
	HacerCaptura_descargarPNG_m5E0B75EFC0D628AA40BCD70953B66E9ED46B36FC,
	HacerCaptura_SaveScreenshotWebGL_m9BBAEFE9E9F672EAF3BB565A4E74100A0EAD90EE,
	HacerCaptura__ctor_mE7C8AA6BD6809FA4CAE818CE51889A0FCB05FE2A,
	U3CdescargarPNGU3Ed__1__ctor_m2E8DDDF699786F07ABEFDABDCF2ADD068D32A315,
	U3CdescargarPNGU3Ed__1_System_IDisposable_Dispose_mC464A5CA3AC92B483613ABA479E2230B0BDE9C70,
	U3CdescargarPNGU3Ed__1_MoveNext_m8294235C935D9A2079E44647851288B229BCE24A,
	U3CdescargarPNGU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m11E993EB22EAB28A43AEDD2084448F44F250FAD9,
	U3CdescargarPNGU3Ed__1_System_Collections_IEnumerator_Reset_m8543DADEE595C63CC59515C2EA9C075D3B521DD4,
	U3CdescargarPNGU3Ed__1_System_Collections_IEnumerator_get_Current_m4C271FDC1BBA3A8B2CADED6C9A8E0A9FF73AFC78,
	PressHandler_OnPointerDown_m999EB467CF64F1D175EB39F5994612B0FF4C2E7A,
	PressHandler__ctor_m3CCBD581BA7F5F0206C0C8D364689B90748D8F21,
	ButtonPressEvent__ctor_mC8D923F1B932DCC6CC42620BED1475435BDCA97C,
	Window_Graph_Awake_m4DE200BD45BD2A8972E31463180896C6CE0F9344,
	Window_Graph_Update_m07B54C3C197F08B35635646582FA88E452664D94,
	Window_Graph_ShowGraph_m82918D64DD82F36A782E3522A454DF98ECA36C7A,
	Window_Graph_setPrecision_mD077C0BF48F68C8645F8FE73A7EB1E3F1056E964,
	Window_Graph_setPotencia_m50C4C50AEDD22E325CF360039D821FBACFF0A450,
	Window_Graph_setAlcance_m6A8534AD78B4E7314CBF5B109CABE1222AA52E5D,
	Window_Graph_setCadencia_mEEA99F4FC29B53DE4D6AE6C7E7F329C190F381CC,
	Window_Graph_setVarianza_mEF2CD01BE121E1FBDDF5987480791AAD8B135996,
	Window_Graph_setPrecio_mC4B8E418FE79CB751941F0C19083F18135E697FE,
	Window_Graph_setExtra_m7C916C0BDD9F1D8BC8FF8202FC4C93EB1EA916AC,
	Window_Graph_ChangeBar_mFE58773E7D5B8177AEAC915A9F72C249BDE6E747,
	Window_Graph__ctor_m70CCCEB054D75A061A090BEBD48689E342A79F1B,
	LeerExcel_Read_mEF6C3DF44F7195F6D9B764A3053DE3777779FF1D,
	LeerExcel__ctor_mD475F2BF78B22BDE15048FD9058EE221880F4DE8,
	LeerExcel__cctor_m7C33F5A931B9A3C94C13298BF7FD0743E96DB063,
	changeColor_Start_m3336B28A995D4EC117DF1882A333FEBF3111B9C4,
	changeColor_SelectMaterial_mE6F15F6E458C44C178D2F3D2F250E06A582EBBAD,
	changeColor_SetColor_mFCC6F560A6AB325EF20DA015A12D879780E9AABC,
	changeColor_onRed_m0C5DC98D1D8BEE9DE8F943F62FF203BDE9B62C38,
	changeColor_onBlue_m3FCF401696ED719497BC48F7DA5D821631938E7E,
	changeColor_onGreen_mC13D70E0A4AAB0FA5D8510588B25D81C6F485BD3,
	changeColor_onYellow_m778E0F715BCCC855E8AC4A839EBDAA619D27C9D9,
	changeColor__ctor_mFC5E0AC250AA509C2E1EED1A7984A7845D4C71D6,
	lockRotation_Start_m5EEC8BA2D86FD59B27BB77D62E3F2ADE64C4083C,
	lockRotation_LateUpdate_m4F2BD9A821BBA33F5536DF482377EA356EA5D042,
	lockRotation__ctor_mE9082B7F1BEAD35790826035B7C4A7AC656CA6E1,
	rotarObjeto_Update_mBF63D41AFACFF57C2D2D29108B73F171CB488535,
	rotarObjeto__ctor_mA350FB6FE78C618EB5B391084DC8364C5C2CC045,
};
static const int32_t s_InvokerIndices[118] = 
{
	980,
	1142,
	1142,
	1142,
	1142,
	1142,
	1562,
	1142,
	980,
	1142,
	980,
	1142,
	971,
	1142,
	980,
	1142,
	971,
	1142,
	980,
	1142,
	971,
	1142,
	980,
	1142,
	971,
	1142,
	980,
	1142,
	971,
	1142,
	980,
	1142,
	971,
	1142,
	980,
	1142,
	971,
	1142,
	980,
	1142,
	971,
	1142,
	980,
	1142,
	971,
	1142,
	980,
	1142,
	971,
	1142,
	980,
	1142,
	971,
	1142,
	980,
	1142,
	971,
	1142,
	980,
	1142,
	971,
	1142,
	980,
	1142,
	971,
	1142,
	980,
	1142,
	971,
	1142,
	980,
	1142,
	971,
	1142,
	980,
	1142,
	971,
	1142,
	1111,
	1737,
	1142,
	971,
	1142,
	1130,
	1111,
	1142,
	1111,
	980,
	1142,
	1142,
	1142,
	1142,
	980,
	1724,
	1724,
	1724,
	1724,
	1724,
	1724,
	1884,
	212,
	1142,
	1826,
	1142,
	1918,
	1142,
	971,
	971,
	1142,
	1142,
	1142,
	1142,
	1142,
	1142,
	1142,
	1142,
	1142,
	1142,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	118,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
