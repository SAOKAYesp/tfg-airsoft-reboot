﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtualActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<UnityEngine.AsyncOperation>
struct Action_1_tE8693FF0E67CDBA52BAFB211BFF1844D076ABAFB;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tAE94C8F24AD5B94D4EE85CA9FC59E3409D41CAF7;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Object>
struct KeyCollection_tE66790F09E854C19C7F612BEAD203AE626E90A36;
// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct List_1_tE020B120302E34B781278C33685F2DA255337446;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_tBFF9DD9FFA06F20E74F9D7AD36610BD754D353A4;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t2CDCA768E7F493F5EDEBC75AEB200FD621354E35;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73;
// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>
struct List_1_t89B39292AD45371F7FDCB295AAE956D33588BC6E;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t830EC096236A3CEC7189DFA6E0B2E74C5C97780B;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Object>
struct ValueCollection_tC9D91E8A3198E40EA339059703AB10DFC9F5CC2E;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>[]
struct Dictionary_2U5BU5D_tF3731166E97A9D9E091E8FEDF8384CEAC68CF4D3;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Object>[]
struct EntryU5BU5D_t233BB24ED01E2D8D65B0651D54B8E3AD125CAF96;
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
// UnityEngine.Color[]
struct ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.IntPtr[]
struct IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832;
// UnityEngine.Material[]
struct MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t4160E135F02A40F75A63F787D36F31FEC6FE91A9;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C;
// UnityEngine.UI.Dropdown/OptionData[]
struct OptionDataU5BU5D_tF56CF744A9E6A0DBB0AC2072BE52F7C8D2E1947F;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tA0DC06F89C5280C6DD972F6F4C8A56D7F4F79074;
// UnityEngine.Canvas
struct Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860;
// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B;
// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB;
// UnityEngine.UI.Dropdown
struct Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89;
// UnityEngine.Event
struct Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB;
// UnityEngine.UI.FontData
struct FontData_tB8E562846C6CB59C43260F69AE346B9BF3157224;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931;
// HacerCaptura
struct HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3;
// System.Collections.IDictionary
struct IDictionary_t6D03155AF1FA9083817AA5B6AD7DEEACC26AB220;
// System.Collections.IEnumerator
struct IEnumerator_t7B609C2FFA6EB5167D9C62A0C32A21DE2F666DAA;
// UnityEngine.UI.Image
struct Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E;
// UnityEngine.UI.InputField
struct InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140;
// LeerExcel
struct LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// UnityEngine.Mesh
struct Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A;
// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670;
// UnityEngine.RectTransform
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6;
// UnityEngine.UI.Selectable
struct Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712;
// UnityEngine.Sprite
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62;
// UnityEngine.TextAsset
struct TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69;
// UnityEngine.TextGenerator
struct TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC;
// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_tE87B78A3DAED69816B44C99270A734682E093E7A;
// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1;
// UnityEngine.Events.UnityAction
struct UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F;
// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C;
// UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6;
// System.Uri
struct Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// UnityEngine.WWWForm
struct WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663;
// UnityEngine.WaitForSecondsRealtime
struct WaitForSecondsRealtime_tA8CE0AAB4B0C872B843E7973637037D17682BA01;
// Window_Graph
struct Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8;
// changeColor
struct changeColor_tD0F3C37D18D9ACF6C09D5A84FAB68A1AF7B4D81B;
// lockRotation
struct lockRotation_tD8979E52803C5CEE4D8141EE52BA10B5627831AF;
// rotarObjeto
struct rotarObjeto_t91F85E1F039D0014B7DA0994A9C60CC5DB38CAAB;
// saveConfig
struct saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182;
// UnityEngine.UI.Dropdown/DropdownEvent
struct DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059;
// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F;
// UnityEngine.UI.Dropdown/OptionDataList
struct OptionDataList_t53255477D0A9C6980AB48693A520EFBC94DFFB96;
// HacerCaptura/<descargarPNG>d__34
struct U3CdescargarPNGU3Ed__34_tA7E5A53A8875E941CD90ACE64537EAE2DEC6A999;
// UnityEngine.UI.InputField/EndEditEvent
struct EndEditEvent_t946A962BA13CF60BB0BE7AD091DA041FD788E655;
// UnityEngine.UI.InputField/OnChangeEvent
struct OnChangeEvent_tE4829F88300B0E0E0D1B78B453AF25FC1AA55E2F;
// UnityEngine.UI.InputField/OnValidateInput
struct OnValidateInput_t48916A4E9C9FD6204401FF0808C2B7A93D73418B;
// UnityEngine.UI.InputField/SubmitEvent
struct SubmitEvent_t1E0F5A2AB28D0DB55AE18E8DA99147D86492DD5D;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t3482EA130A01FF7EE2EEFE37F66A5215D08CFE24;
// saveConfig/<Save>d__31
struct U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE;

IL2CPP_EXTERN_C RuntimeClass* CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tE020B120302E34B781278C33685F2DA255337446_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Regex_tE773142C2BE45C5D362B0F815AFF831707A51772_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CdescargarPNGU3Ed__34_tA7E5A53A8875E941CD90ACE64537EAE2DEC6A999_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0401651977F38591A9C61098B3E396B6E8C5F20F;
IL2CPP_EXTERN_C String_t* _stringLiteral040A30393EB823D7DA3AC6B441CDFED1AB592B94;
IL2CPP_EXTERN_C String_t* _stringLiteral073287A537BF6B0F7150451A41FBE4918FE5A158;
IL2CPP_EXTERN_C String_t* _stringLiteral07C0962FAE2C663481A460B6596876CC9623671F;
IL2CPP_EXTERN_C String_t* _stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40;
IL2CPP_EXTERN_C String_t* _stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0;
IL2CPP_EXTERN_C String_t* _stringLiteral1799E587C0E03A929C0DB02AC5D972B7ADCAC934;
IL2CPP_EXTERN_C String_t* _stringLiteral17C1421BAF12D0423E2C3BAAA8B016D369DFEB8F;
IL2CPP_EXTERN_C String_t* _stringLiteral2029726709853776916AF8A4F3E6E03CB0E0173E;
IL2CPP_EXTERN_C String_t* _stringLiteral2068FC53A9A4DFCC951852E87B73C9B115FC016B;
IL2CPP_EXTERN_C String_t* _stringLiteral215C8F5F3404D78404F7C939B86F46ED678A9ADA;
IL2CPP_EXTERN_C String_t* _stringLiteral3601F196C4AB858E4A861453B96D8654ADABCA61;
IL2CPP_EXTERN_C String_t* _stringLiteral36328BA2B4A341F864BB9986BAE6A25FC6472C2F;
IL2CPP_EXTERN_C String_t* _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1;
IL2CPP_EXTERN_C String_t* _stringLiteral4027C33FEB9C4FFC83129E78CA7013F55996057E;
IL2CPP_EXTERN_C String_t* _stringLiteral45D2EE73A515C8F1D44B7D03ED19C8403309222B;
IL2CPP_EXTERN_C String_t* _stringLiteral462AF2C3390D0074A52DC6D460AACF9292C8FDE7;
IL2CPP_EXTERN_C String_t* _stringLiteral46D28FF30C29C530049AA60FB209FE1D69FF6472;
IL2CPP_EXTERN_C String_t* _stringLiteral487F72322899612CAD8E0F0B608F4F9C183DA574;
IL2CPP_EXTERN_C String_t* _stringLiteral49E640CB14BEC971E54FAD00F061635E8C0BC12A;
IL2CPP_EXTERN_C String_t* _stringLiteral524126665B399B3ED6EFBBD2FFFC1CC2BCA54442;
IL2CPP_EXTERN_C String_t* _stringLiteral538AF8C1BD2E67A15B89C54853ED84EBB7533E92;
IL2CPP_EXTERN_C String_t* _stringLiteral5CDE804554EB9DE3ACD4D0B597C49D0AE3CB9E3F;
IL2CPP_EXTERN_C String_t* _stringLiteral669724E2ED32B3B0345C44EE89993FF550EB3E0A;
IL2CPP_EXTERN_C String_t* _stringLiteral68806558F5B120B9ACE1D4125D1F2D647B3247BD;
IL2CPP_EXTERN_C String_t* _stringLiteral6B17125BFE29CB975A7E9D3A67FC7104EC9AB7C3;
IL2CPP_EXTERN_C String_t* _stringLiteral6BEAC275522F5FA9B3B3C4D4E0271A9CC978CE7E;
IL2CPP_EXTERN_C String_t* _stringLiteral6D2D60FB81DB361B60B9512761B3ED9FF250BB96;
IL2CPP_EXTERN_C String_t* _stringLiteral70373B1D3A7807A80812C066F0E6FC704D52FEE0;
IL2CPP_EXTERN_C String_t* _stringLiteral751C92708E570ED82DEC3536E3F4B06F89682D49;
IL2CPP_EXTERN_C String_t* _stringLiteral77433E583240E26992DA7E6DFAA703B2EED57EE4;
IL2CPP_EXTERN_C String_t* _stringLiteral77A913898061E87BBE38911BC21627A13F2912E6;
IL2CPP_EXTERN_C String_t* _stringLiteral7C94292527C815E7D8F05CBEF28ACCFD119DA3C3;
IL2CPP_EXTERN_C String_t* _stringLiteral7E32DEAD0A4F06B610A8394B517172BD78EC4187;
IL2CPP_EXTERN_C String_t* _stringLiteral7F78A841F1FB280249A7526BB748F873862DDE5B;
IL2CPP_EXTERN_C String_t* _stringLiteral80A657B65DE79975507DBF69E753C669905069B0;
IL2CPP_EXTERN_C String_t* _stringLiteral8295617198EB635126DF85EA15FD35F7326C2844;
IL2CPP_EXTERN_C String_t* _stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7;
IL2CPP_EXTERN_C String_t* _stringLiteral899C6B6BCFDE33642D1164B699BE502D64A046FC;
IL2CPP_EXTERN_C String_t* _stringLiteral8ED68BAC915B2C7822A6B0DB1979FE7E8EAC1A50;
IL2CPP_EXTERN_C String_t* _stringLiteral8F304EE1AC6BDCD44BB0E10C1513BADDB2598C47;
IL2CPP_EXTERN_C String_t* _stringLiteral930096876CCCD844B17880BFF15CAC5A9CBE2E02;
IL2CPP_EXTERN_C String_t* _stringLiteral93B54C9C484CA2C7C22AEBF0798FB966B9E83084;
IL2CPP_EXTERN_C String_t* _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED;
IL2CPP_EXTERN_C String_t* _stringLiteral98EEF9DC7E3239D315D4315BA2DB13416120A3C9;
IL2CPP_EXTERN_C String_t* _stringLiteral9CA4B2EFA31FB06DA2A5AEAB593048EF298BEF86;
IL2CPP_EXTERN_C String_t* _stringLiteral9FD570D72FF034C02743D728F127044ED2FB9902;
IL2CPP_EXTERN_C String_t* _stringLiteralA3588B77B02D07525328910BD4D164C0D114EE1D;
IL2CPP_EXTERN_C String_t* _stringLiteralA3DAEA94625D971790C73D80BA8B25EEA80D1601;
IL2CPP_EXTERN_C String_t* _stringLiteralA4462CA9ADC2AD6448A6395345473E79E165913C;
IL2CPP_EXTERN_C String_t* _stringLiteralA5CBB81D72049EE48091F7F1EA3A53FBFA052011;
IL2CPP_EXTERN_C String_t* _stringLiteralA5E7510D235B3A166C812C2921FFF64AFCCA79EE;
IL2CPP_EXTERN_C String_t* _stringLiteralA60A1D78963924FF030CFB8157ACEA474FF84DB7;
IL2CPP_EXTERN_C String_t* _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834;
IL2CPP_EXTERN_C String_t* _stringLiteralA8F244FC674653C59552E40566FE156BB9BB79FD;
IL2CPP_EXTERN_C String_t* _stringLiteralAA2CBF3EFC19685B86E12C0FD93190E5329FFB55;
IL2CPP_EXTERN_C String_t* _stringLiteralABC75279EE653F5172A467E2EBAC949CD62C2950;
IL2CPP_EXTERN_C String_t* _stringLiteralB0692D2BC1A870C7A665A88E45BCA74258A93038;
IL2CPP_EXTERN_C String_t* _stringLiteralB7E2FD902EE0982393234AD072DA05521DE56595;
IL2CPP_EXTERN_C String_t* _stringLiteralBB92D45784B92FD1902B1C762D068BC12A9EE415;
IL2CPP_EXTERN_C String_t* _stringLiteralBBE61E41E9AE08EF6A00F1DFA850309FCDB5EBCE;
IL2CPP_EXTERN_C String_t* _stringLiteralBD67F18126E2B17C9F201864D9C76708216787EC;
IL2CPP_EXTERN_C String_t* _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B;
IL2CPP_EXTERN_C String_t* _stringLiteralC49039D31EFC2F2CE6BB1F0E3B3140935DC3C6F8;
IL2CPP_EXTERN_C String_t* _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314;
IL2CPP_EXTERN_C String_t* _stringLiteralCF493073FE6A01071725B91F7750AC1AD244B1F5;
IL2CPP_EXTERN_C String_t* _stringLiteralD2A0BEF1301EDC8FC912D4C386656A742D2BD1FD;
IL2CPP_EXTERN_C String_t* _stringLiteralD2C555C9680CF16F44B65256DE94B93D3D6993CF;
IL2CPP_EXTERN_C String_t* _stringLiteralD3A337ACA27A85E377BF1077DECC29723A71277B;
IL2CPP_EXTERN_C String_t* _stringLiteralD60B5B1ADC6E9FDD23C9FCF4D187BC04E6A19C0C;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralDA51C2EAB493D12813926075FEB2BCD4911C3760;
IL2CPP_EXTERN_C String_t* _stringLiteralDDAD6B1F832793EAB53E91D2DE1597B9E3B68D9C;
IL2CPP_EXTERN_C String_t* _stringLiteralDE7BB59E5C480D1D69B765EBC060F9A234A55470;
IL2CPP_EXTERN_C String_t* _stringLiteralDF510ED9801CFCE3E869CBFF940B43857E871D99;
IL2CPP_EXTERN_C String_t* _stringLiteralDFA41B04A76CCD13BAAC104EAB867684910B5A08;
IL2CPP_EXTERN_C String_t* _stringLiteralE151B09FD3B2BC6AFE6CF2FF0AB3BD777A211779;
IL2CPP_EXTERN_C String_t* _stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50;
IL2CPP_EXTERN_C String_t* _stringLiteralE418A24EB42A2E7CB851EE4B76D111A17AAD4C7B;
IL2CPP_EXTERN_C String_t* _stringLiteralE4CFDEDE86B21B9DC88E2877BF3E1378C75D3333;
IL2CPP_EXTERN_C String_t* _stringLiteralEA6181131D358AFEDA8A7D851A7019B1856B14B9;
IL2CPP_EXTERN_C String_t* _stringLiteralEB0BB5D4FA59FF963FDE2494755AB4182D0F8AA2;
IL2CPP_EXTERN_C String_t* _stringLiteralF02BE9044F64252C704D28D19B7A013ABD051FC1;
IL2CPP_EXTERN_C String_t* _stringLiteralF224AEDDC1FF132CAD6D558128124DB5A3462591;
IL2CPP_EXTERN_C String_t* _stringLiteralF25EA25906AC912ABFEEEAF7EE942AE014C6A026;
IL2CPP_EXTERN_C String_t* _stringLiteralF8202313E3DA7E835479F0BBDB627857D7F020C4;
IL2CPP_EXTERN_C String_t* _stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024;
IL2CPP_EXTERN_C String_t* _stringLiteralF9B72C5E29045728775818D4BD0FB4CE67687B4D;
IL2CPP_EXTERN_C String_t* _stringLiteralFC4ABD7D01F00B4D077136F6A39C82C63CC9C000;
IL2CPP_EXTERN_C String_t* _stringLiteralFCA74A9060DDEE6A3ECAE73E6AB96086770BE7E2;
IL2CPP_EXTERN_C String_t* _stringLiteralFD3946DBC3C021874AF2A9CFA9A5796BFFC26EE6;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m0640480E7E38BB88B0D1F6AD59E697C8EE6AAFA4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mB85C5C0EEF6535E3FC0DBFC14E39FA5A51B6F888_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mC4F3DF292BAD88F4BF193C49CD689FAEBC4570A9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_set_Item_m7CCA97075B48AFB2B97E5A072B94BC7679374341_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisInputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140_mC8A4AE0E76B7AC26736C0FA8510832B97D2F400E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m1592DCB5AA07291F73A76006F0913A64DFB8A9C4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mA293F10765359D70384794AA7DA94A25FA0AC15E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m17F501B5A5C289ECE1B4F3D6EBF05DFA421433F8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mB0CFC5E35B840AE25C5F2B640B98E5D9CE401B08_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mD99081BEFA1AB3526715F489192B0F7F596C183D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m432798BF76671A2FB88A9BF403D2F706ADA37236_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CSaveU3Ed__31_System_Collections_IEnumerator_Reset_m762323724A5978C177AAFDD6849978B9C8C45A7A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CdescargarPNGU3Ed__34_System_Collections_IEnumerator_Reset_m2D3D04EAB98096753239C68AF245F189AD38CCCB_RuntimeMethod_var;
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_com;
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_com;
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_pinvoke;
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com;

struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031;
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB;
struct ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389;
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
struct MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D;
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t233BB24ED01E2D8D65B0651D54B8E3AD125CAF96* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_tE66790F09E854C19C7F612BEAD203AE626E90A36* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_tC9D91E8A3198E40EA339059703AB10DFC9F5CC2E* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct List_1_tE020B120302E34B781278C33685F2DA255337446  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	Dictionary_2U5BU5D_tF3731166E97A9D9E091E8FEDF8384CEAC68CF4D3* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tE020B120302E34B781278C33685F2DA255337446_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	Dictionary_2U5BU5D_tF3731166E97A9D9E091E8FEDF8384CEAC68CF4D3* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	OptionDataU5BU5D_tF56CF744A9E6A0DBB0AC2072BE52F7C8D2E1947F* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	OptionDataU5BU5D_tF56CF744A9E6A0DBB0AC2072BE52F7C8D2E1947F* ___s_emptyArray_5;
};
struct Il2CppArrayBounds;

// LeerExcel
struct LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E  : public RuntimeObject
{
};

struct LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_StaticFields
{
	// System.String LeerExcel::SPLIT_RE
	String_t* ___SPLIT_RE_0;
	// System.String LeerExcel::LINE_SPLIT_RE
	String_t* ___LINE_SPLIT_RE_1;
	// System.Char[] LeerExcel::TRIM_CHARS
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___TRIM_CHARS_2;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// UnityEngine.WWWForm
struct WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045  : public RuntimeObject
{
	// System.Collections.Generic.List`1<System.Byte[]> UnityEngine.WWWForm::formData
	List_1_tBFF9DD9FFA06F20E74F9D7AD36610BD754D353A4* ___formData_0;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::fieldNames
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___fieldNames_1;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::fileNames
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___fileNames_2;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::types
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___types_3;
	// System.Byte[] UnityEngine.WWWForm::boundary
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___boundary_4;
	// System.Boolean UnityEngine.WWWForm::containsFiles
	bool ___containsFiles_5;
};

struct WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_StaticFields
{
	// System.Byte[] UnityEngine.WWWForm::dDash
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___dDash_6;
	// System.Byte[] UnityEngine.WWWForm::crlf
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___crlf_7;
	// System.Byte[] UnityEngine.WWWForm::contentTypeHeader
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___contentTypeHeader_8;
	// System.Byte[] UnityEngine.WWWForm::dispositionHeader
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___dispositionHeader_9;
	// System.Byte[] UnityEngine.WWWForm::endQuote
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___endQuote_10;
	// System.Byte[] UnityEngine.WWWForm::fileNameField
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___fileNameField_11;
	// System.Byte[] UnityEngine.WWWForm::ampersand
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___ampersand_12;
	// System.Byte[] UnityEngine.WWWForm::equal
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___equal_13;
};

// UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
};

// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F  : public RuntimeObject
{
	// System.String UnityEngine.UI.Dropdown/OptionData::m_Text
	String_t* ___m_Text_0;
	// UnityEngine.Sprite UnityEngine.UI.Dropdown/OptionData::m_Image
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_Image_1;
};

// HacerCaptura/<descargarPNG>d__34
struct U3CdescargarPNGU3Ed__34_tA7E5A53A8875E941CD90ACE64537EAE2DEC6A999  : public RuntimeObject
{
	// System.Int32 HacerCaptura/<descargarPNG>d__34::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object HacerCaptura/<descargarPNG>d__34::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
};

// saveConfig/<Save>d__31
struct U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE  : public RuntimeObject
{
	// System.Int32 saveConfig/<Save>d__31::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object saveConfig/<Save>d__31::<>2__current
	RuntimeObject* ___U3CU3E2__current_1;
	// saveConfig saveConfig/<Save>d__31::<>4__this
	saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* ___U3CU3E4__this_2;
	// UnityEngine.Networking.UnityWebRequest saveConfig/<Save>d__31::<www>5__2
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* ___U3CwwwU3E5__2_3;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// System.Byte
struct Byte_t94D9231AC217BE4D2E004C4CD32DF6D099EA41A3 
{
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;
};

// System.Char
struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17 
{
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;
};

struct Char_t521A6F19B456D956AF452D926C32709DC03D6B17_StaticFields
{
	// System.Byte[] System.Char::s_categoryForLatin1
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___s_categoryForLatin1_3;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// System.Double
struct Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F 
{
	// System.Double System.Double::m_value
	double ___m_value_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// UnityEngine.UI.Navigation
struct Navigation_t4D2E201D65749CF4E104E8AC1232CF1D6F14795C 
{
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnRight_5;
};
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t4D2E201D65749CF4E104E8AC1232CF1D6F14795C_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnUp_2;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnDown_3;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnLeft_4;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t4D2E201D65749CF4E104E8AC1232CF1D6F14795C_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnUp_2;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnDown_3;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnLeft_4;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnRight_5;
};

// UnityEngine.Quaternion
struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 
{
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;
};

struct Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974_StaticFields
{
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___identityQuaternion_4;
};

// UnityEngine.Rect
struct Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D 
{
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.UI.SpriteState
struct SpriteState_tC8199570BE6337FB5C49347C97892B4222E5AACD 
{
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_DisabledSprite_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_tC8199570BE6337FB5C49347C97892B4222E5AACD_marshaled_pinvoke
{
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_HighlightedSprite_0;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_PressedSprite_1;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_SelectedSprite_2;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_tC8199570BE6337FB5C49347C97892B4222E5AACD_marshaled_com
{
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_HighlightedSprite_0;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_PressedSprite_1;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_SelectedSprite_2;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_DisabledSprite_3;
};

// UnityEngine.Vector2
struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 
{
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;
};

struct Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7_StaticFields
{
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___negativeInfinityVector_9;
};

// UnityEngine.Vector3
struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 
{
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;
};

struct Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields
{
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___negativeInfinityVector_14;
};

// UnityEngine.Vector4
struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 
{
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;
};

struct Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3_StaticFields
{
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___negativeInfinityVector_8;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
};

// UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.AsyncOperation::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Action`1<UnityEngine.AsyncOperation> UnityEngine.AsyncOperation::m_completeCallback
	Action_1_tE8693FF0E67CDBA52BAFB211BFF1844D076ABAFB* ___m_completeCallback_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};
// Native definition for COM marshalling of UnityEngine.AsyncOperation
struct AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppMethodPointer ___m_completeCallback_1;
};

// UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.CertificateHandler::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.CertificateHandler
struct CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.UI.ColorBlock
struct ColorBlock_tDD7C62E7AFE442652FC98F8D058CE8AE6BFD7C11 
{
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;
};

struct ColorBlock_tDD7C62E7AFE442652FC98F8D058CE8AE6BFD7C11_StaticFields
{
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_tDD7C62E7AFE442652FC98F8D058CE8AE6BFD7C11 ___defaultColorBlock_7;
};

// UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B  : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D
{
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_pinvoke : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B_marshaled_com : public YieldInstruction_tFCE35FD0907950EFEE9BC2890AC664E41C53728D_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.DownloadHandler::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.DownloadHandler
struct DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// System.Exception
struct Exception_t  : public RuntimeObject
{
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t* ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject* ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject* ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_tFD177F8C806A6921AD7150264CCC62FA00CAD832* ___native_trace_ips_15;
	// System.Int32 System.Exception::caught_in_unmanaged
	int32_t ___caught_in_unmanaged_16;
};

struct Exception_t_StaticFields
{
	// System.Object System.Exception::s_EDILock
	RuntimeObject* ___s_EDILock_0;
};
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tCBB85B95DFD1634237140CD892E82D06ECB3F5E6* ____safeSerializationManager_13;
	StackTraceU5BU5D_t32FBCB20930EAF5BAE3F450FF75228E5450DA0DF* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
	int32_t ___caught_in_unmanaged_16;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.UploadHandler::m_Ptr
	intptr_t ___m_Ptr_0;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Networking.UploadHandler
struct UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.SystemException
struct SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295  : public Exception_t
{
};

// UnityEngine.TextAsset
struct TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Texture
struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

struct Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700_StaticFields
{
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;
};

// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Networking.UnityWebRequest::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::m_DownloadHandler
	DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB* ___m_DownloadHandler_1;
	// UnityEngine.Networking.UploadHandler UnityEngine.Networking.UnityWebRequest::m_UploadHandler
	UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6* ___m_UploadHandler_2;
	// UnityEngine.Networking.CertificateHandler UnityEngine.Networking.UnityWebRequest::m_CertificateHandler
	CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804* ___m_CertificateHandler_3;
	// System.Uri UnityEngine.Networking.UnityWebRequest::m_Uri
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___m_Uri_4;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeCertificateHandlerOnDispose>k__BackingField
	bool ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeDownloadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	// System.Boolean UnityEngine.Networking.UnityWebRequest::<disposeUploadHandlerOnDispose>k__BackingField
	bool ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_pinvoke ___m_DownloadHandler_1;
	UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_pinvoke ___m_UploadHandler_2;
	CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_pinvoke ___m_CertificateHandler_3;
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_com
{
	intptr_t ___m_Ptr_0;
	DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB_marshaled_com* ___m_DownloadHandler_1;
	UploadHandler_t7E504B1A83346248A0C8C4AF73A893226CB83EF6_marshaled_com* ___m_UploadHandler_2;
	CertificateHandler_t148B524FA5DB39F3ABADB181CD420FC505C33804_marshaled_com* ___m_CertificateHandler_3;
	Uri_t1500A52B5F71A04F5D05C0852D0F2A0941842A0E* ___m_Uri_4;
	int32_t ___U3CdisposeCertificateHandlerOnDisposeU3Ek__BackingField_5;
	int32_t ___U3CdisposeDownloadHandlerOnDisposeU3Ek__BackingField_6;
	int32_t ___U3CdisposeUploadHandlerOnDisposeU3Ek__BackingField_7;
};

// UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C  : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C
{
	// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequestAsyncOperation::<webRequest>k__BackingField
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* ___U3CwebRequestU3Ek__BackingField_2;
};
// Native definition for P/Invoke marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C_marshaled_pinvoke : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_pinvoke
{
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_pinvoke* ___U3CwebRequestU3Ek__BackingField_2;
};
// Native definition for COM marshalling of UnityEngine.Networking.UnityWebRequestAsyncOperation
struct UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C_marshaled_com : public AsyncOperation_tD2789250E4B098DEDA92B366A577E500A92D2D3C_marshaled_com
{
	UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F_marshaled_com* ___U3CwebRequestU3Ek__BackingField_2;
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// System.NotSupportedException
struct NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A  : public SystemException_tCC48D868298F4C0705279823E34B00F4FBDB7295
{
};

// UnityEngine.Texture2D
struct Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4  : public Texture_t791CBB51219779964E0E8A2ED7C1AA5F92A4A700
{
};

// UnityEngine.Transform
struct Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// UnityEngine.RectTransform
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5  : public Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1
{
};

struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_StaticFields
{
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t3482EA130A01FF7EE2EEFE37F66A5215D08CFE24* ___reapplyDrivenProperties_4;
};

// HacerCaptura
struct HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.UI.Dropdown HacerCaptura::Gearbox
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Gearbox_4;
	// UnityEngine.UI.Dropdown HacerCaptura::Gomasdehop
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Gomasdehop_5;
	// UnityEngine.UI.Dropdown HacerCaptura::nub
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___nub_6;
	// UnityEngine.UI.Dropdown HacerCaptura::Camarasdehop
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Camarasdehop_7;
	// UnityEngine.UI.Dropdown HacerCaptura::ca?on
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___caUF1on_8;
	// UnityEngine.UI.Dropdown HacerCaptura::Cilindro
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Cilindro_9;
	// UnityEngine.UI.Dropdown HacerCaptura::Cabezadecilindro
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Cabezadecilindro_10;
	// UnityEngine.UI.Dropdown HacerCaptura::Cabezadepiston
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Cabezadepiston_11;
	// UnityEngine.UI.Dropdown HacerCaptura::Muelle
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Muelle_12;
	// UnityEngine.UI.Dropdown HacerCaptura::Guiademuelle
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Guiademuelle_13;
	// UnityEngine.UI.Dropdown HacerCaptura::Piston
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Piston_14;
	// UnityEngine.UI.Dropdown HacerCaptura::Nozzle
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Nozzle_15;
	// UnityEngine.UI.Dropdown HacerCaptura::Tapperplate
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Tapperplate_16;
	// UnityEngine.UI.Dropdown HacerCaptura::Engranajes
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Engranajes_17;
	// UnityEngine.UI.Dropdown HacerCaptura::Motor
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Motor_18;
	// UnityEngine.UI.Dropdown HacerCaptura::Gatilloelectronico
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Gatilloelectronico_19;
	// UnityEngine.UI.Dropdown HacerCaptura::Mosfet
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Mosfet_20;
	// System.String HacerCaptura::nombreConf
	String_t* ___nombreConf_27;
	// System.String HacerCaptura::data
	String_t* ___data_28;
	// System.String HacerCaptura::nombreDescarga
	String_t* ___nombreDescarga_29;
};

struct HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields
{
	// System.Double HacerCaptura::alcance
	double ___alcance_21;
	// System.Double HacerCaptura::potencia
	double ___potencia_22;
	// System.Double HacerCaptura::precision
	double ___precision_23;
	// System.Double HacerCaptura::cadencia
	double ___cadencia_24;
	// System.Double HacerCaptura::varianza
	double ___varianza_25;
	// System.Double HacerCaptura::precio
	double ___precio_26;
};

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// Window_Graph
struct Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Sprite Window_Graph::circleSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___circleSprite_4;
	// UnityEngine.RectTransform Window_Graph::graphContainer
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___graphContainer_5;
	// UnityEngine.RectTransform Window_Graph::labelTemplateX
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___labelTemplateX_6;
	// UnityEngine.RectTransform Window_Graph::labelTemplateY
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___labelTemplateY_7;
	// UnityEngine.GameObject Window_Graph::potenciaBar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___potenciaBar_15;
	// UnityEngine.GameObject Window_Graph::cadenciaBar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___cadenciaBar_16;
	// UnityEngine.GameObject Window_Graph::alcanceBar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___alcanceBar_17;
	// UnityEngine.GameObject Window_Graph::precisionBar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___precisionBar_18;
	// UnityEngine.GameObject Window_Graph::varianzaBar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___varianzaBar_19;
	// UnityEngine.UI.Text Window_Graph::txtPotencia
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___txtPotencia_20;
	// UnityEngine.UI.Text Window_Graph::txtPrecio
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___txtPrecio_21;
	// UnityEngine.UI.Text Window_Graph::txtExtra
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___txtExtra_22;
	// UnityEngine.UI.Text Window_Graph::txtMaxPre
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___txtMaxPre_23;
	// UnityEngine.UI.Text Window_Graph::txtMaxAlc
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___txtMaxAlc_24;
};

struct Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields
{
	// System.Double Window_Graph::potencia
	double ___potencia_8;
	// System.Double Window_Graph::cadencia
	double ___cadencia_9;
	// System.Double Window_Graph::alcance
	double ___alcance_10;
	// System.Double Window_Graph::precision
	double ___precision_11;
	// System.Double Window_Graph::varianza
	double ___varianza_12;
	// System.Double Window_Graph::precio
	double ___precio_13;
	// System.String Window_Graph::extra
	String_t* ___extra_14;
};

// changeColor
struct changeColor_tD0F3C37D18D9ACF6C09D5A84FAB68A1AF7B4D81B  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Material changeColor::selectedMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___selectedMaterial_4;
	// UnityEngine.Material[] changeColor::colourableMaterials
	MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* ___colourableMaterials_5;
	// UnityEngine.Color[] changeColor::colors
	ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* ___colors_6;
};

// lockRotation
struct lockRotation_tD8979E52803C5CEE4D8141EE52BA10B5627831AF  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Boolean lockRotation::lockX
	bool ___lockX_4;
	// System.Boolean lockRotation::lockY
	bool ___lockY_5;
	// System.Boolean lockRotation::lockZ
	bool ___lockZ_6;
	// UnityEngine.Vector3 lockRotation::startRotation
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___startRotation_7;
};

// rotarObjeto
struct rotarObjeto_t91F85E1F039D0014B7DA0994A9C60CC5DB38CAAB  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// System.Single rotarObjeto::rotSpeed
	float ___rotSpeed_4;
};

// saveConfig
struct saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.UI.Dropdown saveConfig::Gearbox
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Gearbox_4;
	// UnityEngine.UI.Dropdown saveConfig::Gomasdehop
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Gomasdehop_5;
	// UnityEngine.UI.Dropdown saveConfig::nub
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___nub_6;
	// UnityEngine.UI.Dropdown saveConfig::Camarasdehop
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Camarasdehop_7;
	// UnityEngine.UI.Dropdown saveConfig::ca?on
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___caUF1on_8;
	// UnityEngine.UI.Dropdown saveConfig::Cilindro
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Cilindro_9;
	// UnityEngine.UI.Dropdown saveConfig::Cabezadecilindro
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Cabezadecilindro_10;
	// UnityEngine.UI.Dropdown saveConfig::Cabezadepiston
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Cabezadepiston_11;
	// UnityEngine.UI.Dropdown saveConfig::Muelle
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Muelle_12;
	// UnityEngine.UI.Dropdown saveConfig::Guiademuelle
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Guiademuelle_13;
	// UnityEngine.UI.Dropdown saveConfig::Piston
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Piston_14;
	// UnityEngine.UI.Dropdown saveConfig::Nozzle
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Nozzle_15;
	// UnityEngine.UI.Dropdown saveConfig::Tapperplate
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Tapperplate_16;
	// UnityEngine.UI.Dropdown saveConfig::Engranajes
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Engranajes_17;
	// UnityEngine.UI.Dropdown saveConfig::Motor
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Motor_18;
	// UnityEngine.UI.Dropdown saveConfig::Gatilloelectronico
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Gatilloelectronico_19;
	// UnityEngine.UI.Dropdown saveConfig::Mosfet
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Mosfet_20;
	// System.String saveConfig::nombreConf
	String_t* ___nombreConf_27;
};

struct saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields
{
	// System.Double saveConfig::alcance
	double ___alcance_21;
	// System.Double saveConfig::potencia
	double ___potencia_22;
	// System.Double saveConfig::precision
	double ___precision_23;
	// System.Double saveConfig::cadencia
	double ___cadencia_24;
	// System.Double saveConfig::varianza
	double ___varianza_25;
	// System.Double saveConfig::precio
	double ___precio_26;
};

// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_t58B63D32F48C0DBF50DE2C60794C4676C80EDBE3 ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860* ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2DB4CEFDFF732884866C83F11ABF75F5AE8FFB26* ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t11A1F3B953B365C072A5DCC32677EE1796A962A7* ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tFEBBC94BCC6C9C88277BA04047D2B3FDB6ED7FDA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t5BB0582F926E75E2FE795492679A6CF55A4B4BC4* ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;
};

struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tB905FCB02AE67CBEE5F265FE37A5938FC5D136FE* ___s_VertexHelper_21;
};

// UnityEngine.UI.Selectable
struct Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t4D2E201D65749CF4E104E8AC1232CF1D6F14795C ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_tDD7C62E7AFE442652FC98F8D058CE8AE6BFD7C11 ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_tC8199570BE6337FB5C49347C97892B4222E5AACD ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tA0DC06F89C5280C6DD972F6F4C8A56D7F4F79074* ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t2CDCA768E7F493F5EDEBC75AEB200FD621354E35* ___m_CanvasGroupCache_19;
};

struct Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712_StaticFields
{
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_t4160E135F02A40F75A63F787D36F31FEC6FE91A9* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;
};

// UnityEngine.UI.Dropdown
struct Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89  : public Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712
{
	// UnityEngine.RectTransform UnityEngine.UI.Dropdown::m_Template
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___m_Template_20;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_CaptionText
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___m_CaptionText_21;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_CaptionImage
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___m_CaptionImage_22;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_ItemText
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___m_ItemText_23;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_ItemImage
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___m_ItemImage_24;
	// System.Int32 UnityEngine.UI.Dropdown::m_Value
	int32_t ___m_Value_25;
	// UnityEngine.UI.Dropdown/OptionDataList UnityEngine.UI.Dropdown::m_Options
	OptionDataList_t53255477D0A9C6980AB48693A520EFBC94DFFB96* ___m_Options_26;
	// UnityEngine.UI.Dropdown/DropdownEvent UnityEngine.UI.Dropdown::m_OnValueChanged
	DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* ___m_OnValueChanged_27;
	// System.Single UnityEngine.UI.Dropdown::m_AlphaFadeSpeed
	float ___m_AlphaFadeSpeed_28;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Dropdown
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___m_Dropdown_29;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Blocker
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___m_Blocker_30;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem> UnityEngine.UI.Dropdown::m_Items
	List_1_t89B39292AD45371F7FDCB295AAE956D33588BC6E* ___m_Items_31;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween> UnityEngine.UI.Dropdown::m_AlphaTweenRunner
	TweenRunner_1_t830EC096236A3CEC7189DFA6E0B2E74C5C97780B* ___m_AlphaTweenRunner_32;
	// System.Boolean UnityEngine.UI.Dropdown::validTemplate
	bool ___validTemplate_33;
};

struct Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_StaticFields
{
	// UnityEngine.UI.Dropdown/OptionData UnityEngine.UI.Dropdown::s_NoOptionData
	OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* ___s_NoOptionData_35;
};

// UnityEngine.UI.InputField
struct InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140  : public Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712
{
	// UnityEngine.TouchScreenKeyboard UnityEngine.UI.InputField::m_Keyboard
	TouchScreenKeyboard_tE87B78A3DAED69816B44C99270A734682E093E7A* ___m_Keyboard_20;
	// UnityEngine.UI.Text UnityEngine.UI.InputField::m_TextComponent
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___m_TextComponent_23;
	// UnityEngine.UI.Graphic UnityEngine.UI.InputField::m_Placeholder
	Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* ___m_Placeholder_24;
	// UnityEngine.UI.InputField/ContentType UnityEngine.UI.InputField::m_ContentType
	int32_t ___m_ContentType_25;
	// UnityEngine.UI.InputField/InputType UnityEngine.UI.InputField::m_InputType
	int32_t ___m_InputType_26;
	// System.Char UnityEngine.UI.InputField::m_AsteriskChar
	Il2CppChar ___m_AsteriskChar_27;
	// UnityEngine.TouchScreenKeyboardType UnityEngine.UI.InputField::m_KeyboardType
	int32_t ___m_KeyboardType_28;
	// UnityEngine.UI.InputField/LineType UnityEngine.UI.InputField::m_LineType
	int32_t ___m_LineType_29;
	// System.Boolean UnityEngine.UI.InputField::m_HideMobileInput
	bool ___m_HideMobileInput_30;
	// UnityEngine.UI.InputField/CharacterValidation UnityEngine.UI.InputField::m_CharacterValidation
	int32_t ___m_CharacterValidation_31;
	// System.Int32 UnityEngine.UI.InputField::m_CharacterLimit
	int32_t ___m_CharacterLimit_32;
	// UnityEngine.UI.InputField/SubmitEvent UnityEngine.UI.InputField::m_OnSubmit
	SubmitEvent_t1E0F5A2AB28D0DB55AE18E8DA99147D86492DD5D* ___m_OnSubmit_33;
	// UnityEngine.UI.InputField/EndEditEvent UnityEngine.UI.InputField::m_OnDidEndEdit
	EndEditEvent_t946A962BA13CF60BB0BE7AD091DA041FD788E655* ___m_OnDidEndEdit_34;
	// UnityEngine.UI.InputField/OnChangeEvent UnityEngine.UI.InputField::m_OnValueChanged
	OnChangeEvent_tE4829F88300B0E0E0D1B78B453AF25FC1AA55E2F* ___m_OnValueChanged_35;
	// UnityEngine.UI.InputField/OnValidateInput UnityEngine.UI.InputField::m_OnValidateInput
	OnValidateInput_t48916A4E9C9FD6204401FF0808C2B7A93D73418B* ___m_OnValidateInput_36;
	// UnityEngine.Color UnityEngine.UI.InputField::m_CaretColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_CaretColor_37;
	// System.Boolean UnityEngine.UI.InputField::m_CustomCaretColor
	bool ___m_CustomCaretColor_38;
	// UnityEngine.Color UnityEngine.UI.InputField::m_SelectionColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_SelectionColor_39;
	// System.String UnityEngine.UI.InputField::m_Text
	String_t* ___m_Text_40;
	// System.Single UnityEngine.UI.InputField::m_CaretBlinkRate
	float ___m_CaretBlinkRate_41;
	// System.Int32 UnityEngine.UI.InputField::m_CaretWidth
	int32_t ___m_CaretWidth_42;
	// System.Boolean UnityEngine.UI.InputField::m_ReadOnly
	bool ___m_ReadOnly_43;
	// System.Boolean UnityEngine.UI.InputField::m_ShouldActivateOnSelect
	bool ___m_ShouldActivateOnSelect_44;
	// System.Int32 UnityEngine.UI.InputField::m_CaretPosition
	int32_t ___m_CaretPosition_45;
	// System.Int32 UnityEngine.UI.InputField::m_CaretSelectPosition
	int32_t ___m_CaretSelectPosition_46;
	// UnityEngine.RectTransform UnityEngine.UI.InputField::caretRectTrans
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___caretRectTrans_47;
	// UnityEngine.UIVertex[] UnityEngine.UI.InputField::m_CursorVerts
	UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F* ___m_CursorVerts_48;
	// UnityEngine.TextGenerator UnityEngine.UI.InputField::m_InputTextCache
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_InputTextCache_49;
	// UnityEngine.CanvasRenderer UnityEngine.UI.InputField::m_CachedInputRenderer
	CanvasRenderer_tAB9A55A976C4E3B2B37D0CE5616E5685A8B43860* ___m_CachedInputRenderer_50;
	// System.Boolean UnityEngine.UI.InputField::m_PreventFontCallback
	bool ___m_PreventFontCallback_51;
	// UnityEngine.Mesh UnityEngine.UI.InputField::m_Mesh
	Mesh_t6D9C539763A09BC2B12AEAEF36F6DFFC98AE63D4* ___m_Mesh_52;
	// System.Boolean UnityEngine.UI.InputField::m_AllowInput
	bool ___m_AllowInput_53;
	// System.Boolean UnityEngine.UI.InputField::m_ShouldActivateNextUpdate
	bool ___m_ShouldActivateNextUpdate_54;
	// System.Boolean UnityEngine.UI.InputField::m_UpdateDrag
	bool ___m_UpdateDrag_55;
	// System.Boolean UnityEngine.UI.InputField::m_DragPositionOutOfBounds
	bool ___m_DragPositionOutOfBounds_56;
	// System.Boolean UnityEngine.UI.InputField::m_CaretVisible
	bool ___m_CaretVisible_59;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_BlinkCoroutine
	Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* ___m_BlinkCoroutine_60;
	// System.Single UnityEngine.UI.InputField::m_BlinkStartTime
	float ___m_BlinkStartTime_61;
	// System.Int32 UnityEngine.UI.InputField::m_DrawStart
	int32_t ___m_DrawStart_62;
	// System.Int32 UnityEngine.UI.InputField::m_DrawEnd
	int32_t ___m_DrawEnd_63;
	// UnityEngine.Coroutine UnityEngine.UI.InputField::m_DragCoroutine
	Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* ___m_DragCoroutine_64;
	// System.String UnityEngine.UI.InputField::m_OriginalText
	String_t* ___m_OriginalText_65;
	// System.Boolean UnityEngine.UI.InputField::m_WasCanceled
	bool ___m_WasCanceled_66;
	// System.Boolean UnityEngine.UI.InputField::m_HasDoneFocusTransition
	bool ___m_HasDoneFocusTransition_67;
	// UnityEngine.WaitForSecondsRealtime UnityEngine.UI.InputField::m_WaitForSecondsRealtime
	WaitForSecondsRealtime_tA8CE0AAB4B0C872B843E7973637037D17682BA01* ___m_WaitForSecondsRealtime_68;
	// System.Boolean UnityEngine.UI.InputField::m_TouchKeyboardAllowsInPlaceEditing
	bool ___m_TouchKeyboardAllowsInPlaceEditing_69;
	// System.Boolean UnityEngine.UI.InputField::m_IsCompositionActive
	bool ___m_IsCompositionActive_70;
	// UnityEngine.Event UnityEngine.UI.InputField::m_ProcessingEvent
	Event_tEBC6F24B56CE22B9C9AD1AC6C24A6B83BC3860CB* ___m_ProcessingEvent_73;
};

struct InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140_StaticFields
{
	// System.Char[] UnityEngine.UI.InputField::kSeparators
	CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___kSeparators_21;
	// System.Boolean UnityEngine.UI.InputField::s_IsQuestDevice
	bool ___s_IsQuestDevice_22;
};

// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E  : public Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931
{
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tACF92BE999C791A665BD1ADEABF5BCEB82846670* ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t6073CD0D951EC1256BF74B8F9107D68FC89B99B8* ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_tFF1859CCE176131B909E2044F76443064254679C* ___m_Corners_35;
};

// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62  : public MaskableGraphic_tFC5B6BE351C90DE53744DF2A70940242774B361E
{
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_tB8E562846C6CB59C43260F69AE346B9BF3157224* ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t85D00417640A53953556C01F9D4E7DDE1ABD8FEC* ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tBC532486B45D071A520751A90E819C77BA4E3D2F* ___m_TempVerts_42;
};

struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_StaticFields
{
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___s_DefaultText_40;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031  : public RuntimeArray
{
	ALIGN_FIELD (8) uint8_t m_Items[1];

	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Char[]
struct CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB  : public RuntimeArray
{
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Material[]
struct MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D  : public RuntimeArray
{
	ALIGN_FIELD (8) Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* m_Items[1];

	inline Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389  : public RuntimeArray
{
	ALIGN_FIELD (8) Color_tD001788D726C3A7F1379BEED0260B9591F440C1F m_Items[1];

	inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C  : public RuntimeArray
{
	ALIGN_FIELD (8) int32_t m_Items[1];

	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};


// T UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, int32_t ___index0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_set_Item_m1A840355E8EDAECEA9D0C6F5E51B248FAA449CBD_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___key0, RuntimeObject* ___value1, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m17F501B5A5C289ECE1B4F3D6EBF05DFA421433F8_gshared (List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_gshared_inline (List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* __this, int32_t ___item0, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t List_1_get_Item_mD99081BEFA1AB3526715F489192B0F7F596C183D_gshared (List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* __this, int32_t ___index0, const RuntimeMethod* method) ;
// T UnityEngine.Object::Instantiate<System.Object>(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Object_Instantiate_TisRuntimeObject_mCD6FC6BB14BA9EF1A4B314841EB4D40675E3C1DB_gshared (RuntimeObject* ___original0, const RuntimeMethod* method) ;

// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51 (String_t* ___name0, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<UnityEngine.UI.Dropdown>()
inline Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Boolean System.String::IsNullOrEmpty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A (String_t* ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219 (RuntimeObject* ___message0, const RuntimeMethod* method) ;
// System.Void HacerCaptura::error()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HacerCaptura_error_m0201DF7A79E1BF703064ABF1F31C3CBF85B56A3A (const RuntimeMethod* method) ;
// System.Collections.IEnumerator HacerCaptura::descargarPNG()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* HacerCaptura_descargarPNG_m469AED5E474F6927E47D6597CA5CD81D4142105F (HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3* __this, const RuntimeMethod* method) ;
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812 (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, RuntimeObject* ___routine0, const RuntimeMethod* method) ;
// System.Void HacerCaptura::DescargarTxt(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HacerCaptura_DescargarTxt_mFA814DC1A322790177EF21C355E89B065EC1B2F6 (String_t* ___data0, String_t* ___nombreDescarga1, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<UnityEngine.UI.InputField>()
inline InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140* GameObject_GetComponent_TisInputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140_mC8A4AE0E76B7AC26736C0FA8510832B97D2F400E (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.String UnityEngine.UI.InputField::get_text()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* InputField_get_text_m6E0796350FF559505E4DF17311803962699D6704_inline (InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData> UnityEngine.UI.Dropdown::get_options()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816 (Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.UI.Dropdown::get_value()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline (Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::get_Item(System.Int32)
inline OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366 (List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* (*) (List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55*, int32_t, const RuntimeMethod*))List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared)(__this, ___index0, method);
}
// System.String UnityEngine.UI.Dropdown/OptionData::get_text()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method) ;
// System.String System.Double::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339 (double* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mF8B69BE42B5C5ABCAD3C176FBBE3010E0815D65D (String_t* ___str00, String_t* ___str11, String_t* ___str22, String_t* ___str33, const RuntimeMethod* method) ;
// System.Void HacerCaptura::errorNombre()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HacerCaptura_errorNombre_m4BDC0D400F0F00016E8F247824BB2725B80C0312 (const RuntimeMethod* method) ;
// System.Void HacerCaptura::descargarTxt(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HacerCaptura_descargarTxt_m55158E62D0F4C65D8ACB3B5692B53F0A87766B46 (String_t* ___data0, String_t* ___nombreDescarga1, const RuntimeMethod* method) ;
// System.Void HacerCaptura/<descargarPNG>d__34::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CdescargarPNGU3Ed__34__ctor_mA41CBEFA68CDFA0D467D8FB2107F6D91B2D23B88 (U3CdescargarPNGU3Ed__34_tA7E5A53A8875E941CD90ACE64537EAE2DEC6A999* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForEndOfFrame__ctor_m4AF7E576C01E6B04443BB898B1AE5D645F7D45AB (WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663* __this, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_mCA5D955A53CF6D29C8C7118D517D0FC84AE8056C (const RuntimeMethod* method) ;
// System.Int32 UnityEngine.Screen::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_height_m624DD2D53F34087064E3B9D09AC2207DB4E86CA8 (const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_mECF60A9EC0638EC353C02C8E99B6B465D23BE917 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, int32_t ___width0, int32_t ___height1, int32_t ___textureFormat2, bool ___mipChain3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m18C3033D135097BEE424AAA68D91C706D2647F23 (Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D* __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_ReadPixels_m6B45DF7C051BF599C72ED09691F21A6C769EEBD9 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D ___source0, int32_t ___destX1, int32_t ___destY2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Texture2D::Apply()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_Apply_mA014182C9EE0BBF6EEE3B286854F29E50EB972DC (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* __this, const RuntimeMethod* method) ;
// System.Byte[] UnityEngine.ImageConversion::EncodeToPNG(UnityEngine.Texture2D)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ImageConversion_EncodeToPNG_m0FFFD0F0DC0EC22073BC937A5294067C57008391 (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* ___tex0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA (Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* ___obj0, const RuntimeMethod* method) ;
// System.String System.Convert::ToBase64String(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Convert_ToBase64String_mB276B21511FB01CDE030619C81757E786F91B9F3 (ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* ___inArray0, const RuntimeMethod* method) ;
// System.Void HacerCaptura::SaveScreenshotWebGL(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HacerCaptura_SaveScreenshotWebGL_mD08623AF978D56A33A17C450E4148452367B6EF2 (String_t* ___filename0, String_t* ___data1, const RuntimeMethod* method) ;
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::.ctor()
inline void List_1__ctor_mB0CFC5E35B840AE25C5F2B640B98E5D9CE401B08 (List_1_tE020B120302E34B781278C33685F2DA255337446* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE020B120302E34B781278C33685F2DA255337446*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* Resources_Load_m9608D2902F542C0B7FD52EFED088323448B9FA35 (String_t* ___path0, const RuntimeMethod* method) ;
// System.String UnityEngine.TextAsset::get_text()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* TextAsset_get_text_m36846042E3CF3D9DD337BF3F8B2B1902D10C8FD9 (TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69* __this, const RuntimeMethod* method) ;
// System.String[] System.Text.RegularExpressions.Regex::Split(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* Regex_Split_m01D4D38E8257BB18BFBC08D2D8DCB7F32FB0D018 (String_t* ___input0, String_t* ___pattern1, const RuntimeMethod* method) ;
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::.ctor()
inline void Dictionary_2__ctor_mC4F3DF292BAD88F4BF193C49CD689FAEBC4570A9 (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710*, const RuntimeMethod*))Dictionary_2__ctor_m5B32FBC624618211EB461D59CFBB10E987FD1329_gshared)(__this, method);
}
// System.String System.String::TrimStart(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_TrimStart_m67833D80326BEA11CC3517CE03CD7B16669BCEEC (String_t* __this, CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___trimChars0, const RuntimeMethod* method) ;
// System.String System.String::TrimEnd(System.Char[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_TrimEnd_mD7CFB0999EEEE20E3A869516EBCE07E8AB5BD529 (String_t* __this, CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* ___trimChars0, const RuntimeMethod* method) ;
// System.String System.String::Replace(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Replace_mABDB7003A1D0AEDCAE9FF85E3DFFFBA752D2A166 (String_t* __this, String_t* ___oldValue0, String_t* ___newValue1, const RuntimeMethod* method) ;
// System.Boolean System.Int32::TryParse(System.String,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Int32_TryParse_mFC6BFCB86964E2BCA4052155B10983837A695EA4 (String_t* ___s0, int32_t* ___result1, const RuntimeMethod* method) ;
// System.Boolean System.Single::TryParse(System.String,System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Single_TryParse_mF23E88B4B12DDC9E82179BB2483A714005BF006F (String_t* ___s0, float* ___result1, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(TKey,TValue)
inline void Dictionary_2_set_Item_m7CCA97075B48AFB2B97E5A072B94BC7679374341 (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* __this, String_t* ___key0, RuntimeObject* ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710*, String_t*, RuntimeObject*, const RuntimeMethod*))Dictionary_2_set_Item_m1A840355E8EDAECEA9D0C6F5E51B248FAA449CBD_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::Add(T)
inline void List_1_Add_mA293F10765359D70384794AA7DA94A25FA0AC15E_inline (List_1_tE020B120302E34B781278C33685F2DA255337446* __this, Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tE020B120302E34B781278C33685F2DA255337446*, Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___item0, method);
}
// System.Void saveConfig::errorNombre()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void saveConfig_errorNombre_mF2BC35C6DE825FFE4CBA538B935A2BB19407F47E (const RuntimeMethod* method) ;
// System.Void saveConfig::todoCorrecto()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void saveConfig_todoCorrecto_mA18AA8966091253847C30259F10AD1A214827BAF (const RuntimeMethod* method) ;
// System.Collections.IEnumerator saveConfig::Save()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* saveConfig_Save_m9A7646305B7F0AE74C1AAD993AC03F16EC45532F (saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* __this, const RuntimeMethod* method) ;
// System.Void saveConfig/<Save>d__31::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSaveU3Ed__31__ctor_m9D8B3EB5A48E88E28F2652A7DF75887CE1C89FC7 (U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) ;
// System.Void saveConfig/<Save>d__31::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSaveU3Ed__31_U3CU3Em__Finally1_m8D488E2CE71CF9ED53EF23A773106D0607794BC2 (U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE* __this, const RuntimeMethod* method) ;
// System.Void saveConfig/<Save>d__31::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSaveU3Ed__31_System_IDisposable_Dispose_mD2D34F6605E9503E0D0ADED22CB0C2F7D5B7A08C (U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE* __this, const RuntimeMethod* method) ;
// System.Void saveConfig::error()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void saveConfig_error_m7BB8597BA30D2F230E8FAC765ED166F3539F3EDF (const RuntimeMethod* method) ;
// System.Void UnityEngine.WWWForm::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WWWForm__ctor_mB1AA4D4BE7011A371B590332CC65794270F269F6 (WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.WWWForm::AddField(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D (WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* __this, String_t* ___fieldName0, String_t* ___value1, const RuntimeMethod* method) ;
// UnityEngine.Networking.UnityWebRequest UnityEngine.Networking.UnityWebRequest::Post(System.String,UnityEngine.WWWForm)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* UnityWebRequest_Post_m1F6BF341F959AC95110C738791EF920713B74786 (String_t* ___uri0, WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* ___formData1, const RuntimeMethod* method) ;
// UnityEngine.Networking.UnityWebRequestAsyncOperation UnityEngine.Networking.UnityWebRequest::SendWebRequest()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C* UnityWebRequest_SendWebRequest_mA3CD13983BAA5074A0640EDD661B1E46E6DB6C13 (UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isNetworkError()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityWebRequest_get_isNetworkError_m036684411466688E71E67CDD3703BAC9035A56F0 (UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* __this, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Networking.UnityWebRequest::get_isHttpError()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UnityWebRequest_get_isHttpError_m945BA480A179E05CC9659846414D9521ED648ED5 (UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* __this, const RuntimeMethod* method) ;
// System.String UnityEngine.Networking.UnityWebRequest::get_error()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* UnityWebRequest_get_error_m20A5D813ED59118B7AA1D1E2EB5250178B1F5B6F (UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* __this, const RuntimeMethod* method) ;
// UnityEngine.Networking.DownloadHandler UnityEngine.Networking.UnityWebRequest::get_downloadHandler()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB* UnityWebRequest_get_downloadHandler_m1AA91B23D9D594A4F4FE2975FC356C508528F1D5 (UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* __this, const RuntimeMethod* method) ;
// System.String UnityEngine.Networking.DownloadHandler::get_text()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* DownloadHandler_get_text_mA6DE5CB2647A21E577B963708DC3D0DA4DBFE7D8 (DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB* __this, const RuntimeMethod* method) ;
// System.Void saveConfig::bien()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void saveConfig_bien_mC22BBB0E80B8D093F33CD35074F571D5FD08F959 (const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* Transform_Find_m3087032B0E1C5B96A2D2C27020BAEAE2DA08F932 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, String_t* ___n0, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
inline RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* Component_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m0640480E7E38BB88B0D1F6AD59E697C8EE6AAFA4 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// T UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
inline Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::.ctor()
inline void List_1__ctor_m17F501B5A5C289ECE1B4F3D6EBF05DFA421433F8 (List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73*, const RuntimeMethod*))List_1__ctor_m17F501B5A5C289ECE1B4F3D6EBF05DFA421433F8_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Int32>::Add(T)
inline void List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_inline (List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* __this, int32_t ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73*, int32_t, const RuntimeMethod*))List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_gshared_inline)(__this, ___item0, method);
}
// System.Void Window_Graph::ShowGraph(System.Collections.Generic.List`1<System.Int32>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_ShowGraph_m5FF8330468B0C90C3A7654265E4D07593E1C89EA (Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8* __this, List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* ___valueList0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___x0, float ___y1, const RuntimeMethod* method) ;
// UnityEngine.GameObject Window_Graph::ChangeBar(UnityEngine.Vector2,System.Single,UnityEngine.GameObject,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Window_Graph_ChangeBar_mA32AD57472431EE83767EC57C703484118F93468 (Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___graphPosition0, float ___barWidth1, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObject2, String_t* ___name3, const RuntimeMethod* method) ;
// System.Void HacerCaptura::setStats(System.Double,System.Double,System.Double,System.Double,System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HacerCaptura_setStats_mEAAECBD9E5319611DBF780127D5CB6B0DA5CAC79 (double ___alcanceX0, double ___potenciaX1, double ___precisionX2, double ___cadenciaX3, double ___varianzaX4, double ___precioX5, const RuntimeMethod* method) ;
// System.Void saveConfig::setStats(System.Double,System.Double,System.Double,System.Double,System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void saveConfig_setStats_m11B648967ECBEA5D7FF3E0A9A5B260CCFC953A26 (double ___alcanceX0, double ___potenciaX1, double ___precisionX2, double ___cadenciaX3, double ___varianzaX4, double ___precioX5, const RuntimeMethod* method) ;
// UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 RectTransform_get_sizeDelta_m822A8493F2035677384F1540A2E9E5ACE63010BB (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<System.Int32>::get_Item(System.Int32)
inline int32_t List_1_get_Item_mD99081BEFA1AB3526715F489192B0F7F596C183D (List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73*, int32_t, const RuntimeMethod*))List_1_get_Item_mD99081BEFA1AB3526715F489192B0F7F596C183D_gshared)(__this, ___index0, method);
}
// T UnityEngine.Object::Instantiate<UnityEngine.RectTransform>(T)
inline RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* Object_Instantiate_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m432798BF76671A2FB88A9BF403D2F706ADA37236 (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___original0, const RuntimeMethod* method)
{
	return ((  RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* (*) (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5*, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mCD6FC6BB14BA9EF1A4B314841EB4D40675E3C1DB_gshared)(___original0, method);
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_SetParent_m6677538B60246D958DD91F931C50F969CCBB5250 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___p0, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, bool ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_anchoredPosition_mF903ACE04F6959B1CD67E2B94FABC0263068F965 (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___value0, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
inline Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* Component_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mB85C5C0EEF6535E3FC0DBFC14E39FA5A51B6F888 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_RoundToInt_m6A6E30BA4157D69DA47F02B43108882DDD7C4A70_inline (float ___f0, const RuntimeMethod* method) ;
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5 (int32_t* __this, const RuntimeMethod* method) ;
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_SetParent_m9BDD7B7476714B2D7919B10BDC22CE75C0A0A195 (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* ___parent0, bool ___worldPositionStays1, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<UnityEngine.RectTransform>()
inline RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* GameObject_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m1592DCB5AA07291F73A76006F0913A64DFB8A9C4 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_sizeDelta_mC9A980EA6036E6725EF24CEDF3EE80A9B2B50EE5 (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_anchorMin_m931442ABE3368D6D4309F43DF1D64AB64B0F52E3 (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_anchorMax_m52829ABEDD229ABD3DA20BCA676FA1DCA4A39B7D (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RectTransform_set_pivot_m79D0177D383D432A93C2615F1932B739B1C6E146 (RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_set_color_m5C32DEBB215FF9EE35E7B575297D8C2F29CC2A2D (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___value0, const RuntimeMethod* method) ;
// System.Void changeColor::SelectMaterial(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void changeColor_SelectMaterial_m919B3144A21AC86903CA16CF46A9E9701C3DA655 (changeColor_tD0F3C37D18D9ACF6C09D5A84FAB68A1AF7B4D81B* __this, int32_t ___i0, const RuntimeMethod* method) ;
// System.Void changeColor::SetColor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void changeColor_SetColor_m0F758AB8D50A08344A7903511809C4091644A257 (changeColor_tD0F3C37D18D9ACF6C09D5A84FAB68A1AF7B4D81B* __this, int32_t ___i0, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_red()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline (const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_blue()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline (const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_green()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_green_m336EB73DD4A5B11B7F405CF4BC7F37A466FB4FF7_inline (const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_yellow()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline (const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Transform_get_rotation_m32AF40CA0D50C797DA639A696F8EAEC7524C179C (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Quaternion_get_eulerAngles_m2DB5158B5C3A71FD60FC8A6EE43D3AAA1CFED122_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974* __this, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Euler_mD4601D966F1F58F3FCA01B3FC19A12D0AD0396DD_inline (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m61340DE74726CF0F9946743A727C4D444397331D (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___value0, const RuntimeMethod* method) ;
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetMouseButton_mE545CF4B790C6E202808B827E3141BEC3330DB70 (int32_t ___button0, const RuntimeMethod* method) ;
// System.Single UnityEngine.Input::GetAxis(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxis_m1F49B26F24032F45FB4583C95FB24E6771A161D4 (String_t* ___axisName0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_up_mAB5269BFCBCB1BD241450C9BF2F156303D30E0C3_inline (const RuntimeMethod* method) ;
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,System.Single,UnityEngine.Space)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_m683E67853797040312868B69E963D0E97F433EEB (Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* __this, Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___axis0, float ___angle1, int32_t ___relativeTo2, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_right_m13B7C3EAA64DC921EC23346C56A5A597B5481FF5_inline (const RuntimeMethod* method) ;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Quaternion_Internal_ToEulerRad_m9B2C77284AEE6F2C43B6C42F1F888FB4FC904462 (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 ___rotation0, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, float ___d1, const RuntimeMethod* method) ;
// UnityEngine.Vector3 UnityEngine.Quaternion::Internal_MakePositive(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Quaternion_Internal_MakePositive_m864320DA2D027C186C95B2A5BC2C66B0EB4A6C11 (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___euler0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) ;
// UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Internal_FromEulerRad_m2842B9FFB31CDC0F80B7C2172E22831D11D91E93 (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___euler0, const RuntimeMethod* method) ;
IL2CPP_EXTERN_C void DEFAULT_CALL descargarTxt(char*, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL errorNombre();
IL2CPP_EXTERN_C void DEFAULT_CALL SaveScreenshotWebGL(char*, char*);
IL2CPP_EXTERN_C void DEFAULT_CALL todoCorrecto();
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HacerCaptura::descargarTxt(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HacerCaptura_descargarTxt_m55158E62D0F4C65D8ACB3B5692B53F0A87766B46 (String_t* ___data0, String_t* ___nombreDescarga1, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___data0' to native representation
	char* ____data0_marshaled = NULL;
	____data0_marshaled = il2cpp_codegen_marshal_string(___data0);

	// Marshaling of parameter '___nombreDescarga1' to native representation
	char* ____nombreDescarga1_marshaled = NULL;
	____nombreDescarga1_marshaled = il2cpp_codegen_marshal_string(___nombreDescarga1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(descargarTxt)(____data0_marshaled, ____nombreDescarga1_marshaled);

	// Marshaling cleanup of parameter '___data0' native representation
	il2cpp_codegen_marshal_free(____data0_marshaled);
	____data0_marshaled = NULL;

	// Marshaling cleanup of parameter '___nombreDescarga1' native representation
	il2cpp_codegen_marshal_free(____nombreDescarga1_marshaled);
	____nombreDescarga1_marshaled = NULL;

}
// System.Void HacerCaptura::errorNombre()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HacerCaptura_errorNombre_m4BDC0D400F0F00016E8F247824BB2725B80C0312 (const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(errorNombre)();

}
// System.Void HacerCaptura::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HacerCaptura_Start_m01289BB5D40A2C9043E573D884DA59115B8DA146 (HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral040A30393EB823D7DA3AC6B441CDFED1AB592B94);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1799E587C0E03A929C0DB02AC5D972B7ADCAC934);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2029726709853776916AF8A4F3E6E03CB0E0173E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral215C8F5F3404D78404F7C939B86F46ED678A9ADA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4027C33FEB9C4FFC83129E78CA7013F55996057E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral77A913898061E87BBE38911BC21627A13F2912E6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral80A657B65DE79975507DBF69E753C669905069B0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral899C6B6BCFDE33642D1164B699BE502D64A046FC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8ED68BAC915B2C7822A6B0DB1979FE7E8EAC1A50);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA3588B77B02D07525328910BD4D164C0D114EE1D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA3DAEA94625D971790C73D80BA8B25EEA80D1601);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralABC75279EE653F5172A467E2EBAC949CD62C2950);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB7E2FD902EE0982393234AD072DA05521DE56595);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD60B5B1ADC6E9FDD23C9FCF4D187BC04E6A19C0C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE151B09FD3B2BC6AFE6CF2FF0AB3BD777A211779);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE418A24EB42A2E7CB851EE4B76D111A17AAD4C7B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF25EA25906AC912ABFEEEAF7EE942AE014C6A026);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Gearbox = GameObject.Find("Drop_Gearbox").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0;
		L_0 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralA3DAEA94625D971790C73D80BA8B25EEA80D1601, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_1;
		L_1 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_0, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Gearbox_4 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Gearbox_4), (void*)L_1);
		// Gomasdehop = GameObject.Find("Drop_GomaHop").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral4027C33FEB9C4FFC83129E78CA7013F55996057E, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_3;
		L_3 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_2, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Gomasdehop_5 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Gomasdehop_5), (void*)L_3);
		// nub = GameObject.Find("Drop_Nub").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralB7E2FD902EE0982393234AD072DA05521DE56595, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_5;
		L_5 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_4, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___nub_6 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___nub_6), (void*)L_5);
		// Camarasdehop = GameObject.Find("Drop_CamaraDeHop").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6;
		L_6 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral1799E587C0E03A929C0DB02AC5D972B7ADCAC934, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_7;
		L_7 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_6, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Camarasdehop_7 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Camarasdehop_7), (void*)L_7);
		// ca?on = GameObject.Find("Drop_Ca?on").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_8;
		L_8 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral040A30393EB823D7DA3AC6B441CDFED1AB592B94, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_9;
		L_9 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_8, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___caUF1on_8 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___caUF1on_8), (void*)L_9);
		// Cilindro = GameObject.Find("Drop_Cilindro").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral80A657B65DE79975507DBF69E753C669905069B0, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_11;
		L_11 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_10, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Cilindro_9 = L_11;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Cilindro_9), (void*)L_11);
		// Cabezadecilindro = GameObject.Find("Drop_CabezaDeCilindro").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12;
		L_12 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralD60B5B1ADC6E9FDD23C9FCF4D187BC04E6A19C0C, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_13;
		L_13 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_12, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Cabezadecilindro_10 = L_13;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Cabezadecilindro_10), (void*)L_13);
		// Cabezadepiston = GameObject.Find("Drop_CabezaDePiston").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_14;
		L_14 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralA3588B77B02D07525328910BD4D164C0D114EE1D, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_15;
		L_15 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_14, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Cabezadepiston_11 = L_15;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Cabezadepiston_11), (void*)L_15);
		// Muelle = GameObject.Find("Drop_Muelle").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_16;
		L_16 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralE151B09FD3B2BC6AFE6CF2FF0AB3BD777A211779, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_17;
		L_17 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_16, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Muelle_12 = L_17;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Muelle_12), (void*)L_17);
		// Guiademuelle = GameObject.Find("Drop_GuiaDeMuelle").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_18;
		L_18 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral8ED68BAC915B2C7822A6B0DB1979FE7E8EAC1A50, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_19;
		L_19 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_18, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Guiademuelle_13 = L_19;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Guiademuelle_13), (void*)L_19);
		// Piston = GameObject.Find("Drop_Piston").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_20;
		L_20 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralE418A24EB42A2E7CB851EE4B76D111A17AAD4C7B, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_21;
		L_21 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_20, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Piston_14 = L_21;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Piston_14), (void*)L_21);
		// Tapperplate = GameObject.Find("Drop_TapperPlate").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22;
		L_22 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralF25EA25906AC912ABFEEEAF7EE942AE014C6A026, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_23;
		L_23 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_22, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Tapperplate_16 = L_23;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Tapperplate_16), (void*)L_23);
		// Engranajes = GameObject.Find("Drop_Engranajes").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_24;
		L_24 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral899C6B6BCFDE33642D1164B699BE502D64A046FC, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_25;
		L_25 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_24, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Engranajes_17 = L_25;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Engranajes_17), (void*)L_25);
		// Motor = GameObject.Find("Drop_Motor").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_26;
		L_26 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralABC75279EE653F5172A467E2EBAC949CD62C2950, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_27;
		L_27 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_26, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Motor_18 = L_27;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Motor_18), (void*)L_27);
		// Gatilloelectronico = GameObject.Find("Drop_Gatillo").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_28;
		L_28 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral77A913898061E87BBE38911BC21627A13F2912E6, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_29;
		L_29 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_28, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Gatilloelectronico_19 = L_29;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Gatilloelectronico_19), (void*)L_29);
		// Mosfet = GameObject.Find("Drop_Mosfet").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_30;
		L_30 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral2029726709853776916AF8A4F3E6E03CB0E0173E, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_31;
		L_31 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_30, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Mosfet_20 = L_31;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Mosfet_20), (void*)L_31);
		// Nozzle = GameObject.Find("Drop_Nozzle").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_32;
		L_32 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral215C8F5F3404D78404F7C939B86F46ED678A9ADA, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_33;
		L_33 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_32, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Nozzle_15 = L_33;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Nozzle_15), (void*)L_33);
		// alcance = 0;
		((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___alcance_21 = (0.0);
		// potencia = 0;
		((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___potencia_22 = (0.0);
		// precision = 0;
		((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___precision_23 = (0.0);
		// cadencia = 0;
		((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___cadencia_24 = (0.0);
		// varianza = 0;
		((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___varianza_25 = (0.0);
		// precio = 0;
		((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___precio_26 = (0.0);
		// }
		return;
	}
}
// System.Void HacerCaptura::setStats(System.Double,System.Double,System.Double,System.Double,System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HacerCaptura_setStats_mEAAECBD9E5319611DBF780127D5CB6B0DA5CAC79 (double ___alcanceX0, double ___potenciaX1, double ___precisionX2, double ___cadenciaX3, double ___varianzaX4, double ___precioX5, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// alcance = alcanceX;
		double L_0 = ___alcanceX0;
		((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___alcance_21 = L_0;
		// potencia = potenciaX;
		double L_1 = ___potenciaX1;
		((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___potencia_22 = L_1;
		// precision = precisionX;
		double L_2 = ___precisionX2;
		((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___precision_23 = L_2;
		// cadencia = cadenciaX;
		double L_3 = ___cadenciaX3;
		((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___cadencia_24 = L_3;
		// varianza = varianzaX;
		double L_4 = ___varianzaX4;
		((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___varianza_25 = L_4;
		// precio = precioX;
		double L_5 = ___precioX5;
		((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___precio_26 = L_5;
		// }
		return;
	}
}
// System.Void HacerCaptura::Captura()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HacerCaptura_Captura_m04726B41AFAFE3C4E3EFC47C06618AD224F260BF (HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD2C555C9680CF16F44B65256DE94B93D3D6993CF);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(string.IsNullOrEmpty(nombreConf)){
		String_t* L_0 = __this->___nombreConf_27;
		bool L_1;
		L_1 = String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A(L_0, NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		// Debug.Log("El nombre esta vacio");
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(_stringLiteralD2C555C9680CF16F44B65256DE94B93D3D6993CF, NULL);
		// error();
		HacerCaptura_error_m0201DF7A79E1BF703064ABF1F31C3CBF85B56A3A(NULL);
		return;
	}

IL_001d:
	{
		// StartCoroutine(descargarPNG());
		RuntimeObject* L_2;
		L_2 = HacerCaptura_descargarPNG_m469AED5E474F6927E47D6597CA5CD81D4142105F(__this, NULL);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_3;
		L_3 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(__this, L_2, NULL);
		// DescargarTxt(data, nombreDescarga);
		String_t* L_4 = __this->___data_28;
		String_t* L_5 = __this->___nombreDescarga_29;
		HacerCaptura_DescargarTxt_mFA814DC1A322790177EF21C355E89B065EC1B2F6(L_4, L_5, NULL);
		// }
		return;
	}
}
// System.Void HacerCaptura::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HacerCaptura_Update_m9EED32623F2026B66BE63E3B4463AF624ECC47FF (HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisInputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140_mC8A4AE0E76B7AC26736C0FA8510832B97D2F400E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0401651977F38591A9C61098B3E396B6E8C5F20F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral07C0962FAE2C663481A460B6596876CC9623671F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral462AF2C3390D0074A52DC6D460AACF9292C8FDE7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral487F72322899612CAD8E0F0B608F4F9C183DA574);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral49E640CB14BEC971E54FAD00F061635E8C0BC12A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral538AF8C1BD2E67A15B89C54853ED84EBB7533E92);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral669724E2ED32B3B0345C44EE89993FF550EB3E0A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6D2D60FB81DB361B60B9512761B3ED9FF250BB96);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7C94292527C815E7D8F05CBEF28ACCFD119DA3C3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7E32DEAD0A4F06B610A8394B517172BD78EC4187);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F78A841F1FB280249A7526BB748F873862DDE5B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral930096876CCCD844B17880BFF15CAC5A9CBE2E02);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral93B54C9C484CA2C7C22AEBF0798FB966B9E83084);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9CA4B2EFA31FB06DA2A5AEAB593048EF298BEF86);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA5E7510D235B3A166C812C2921FFF64AFCCA79EE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB0692D2BC1A870C7A665A88E45BCA74258A93038);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBB92D45784B92FD1902B1C762D068BC12A9EE415);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBBE61E41E9AE08EF6A00F1DFA850309FCDB5EBCE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA51C2EAB493D12813926075FEB2BCD4911C3760);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDFA41B04A76CCD13BAAC104EAB867684910B5A08);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEA6181131D358AFEDA8A7D851A7019B1856B14B9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF8202313E3DA7E835479F0BBDB627857D7F020C4);
		s_Il2CppMethodInitialized = true;
	}
	{
		// nombreConf = GameObject.Find("nombreConfig").GetComponent<InputField>().text;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0;
		L_0 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral0401651977F38591A9C61098B3E396B6E8C5F20F, NULL);
		InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140* L_1;
		L_1 = GameObject_GetComponent_TisInputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140_mC8A4AE0E76B7AC26736C0FA8510832B97D2F400E(L_0, GameObject_GetComponent_TisInputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140_mC8A4AE0E76B7AC26736C0FA8510832B97D2F400E_RuntimeMethod_var);
		String_t* L_2;
		L_2 = InputField_get_text_m6E0796350FF559505E4DF17311803962699D6704_inline(L_1, NULL);
		__this->___nombreConf_27 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___nombreConf_27), (void*)L_2);
		// data = "NOMBRE: " + nombreConf;
		String_t* L_3 = __this->___nombreConf_27;
		String_t* L_4;
		L_4 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteral7E32DEAD0A4F06B610A8394B517172BD78EC4187, L_3, NULL);
		__this->___data_28 = L_4;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_4);
		// data = data + " || MUELLE: " + Muelle.options[Muelle.value].text;
		String_t* L_5 = __this->___data_28;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_6 = __this->___Muelle_12;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_7;
		L_7 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_6, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_8 = __this->___Muelle_12;
		int32_t L_9;
		L_9 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_8, NULL);
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_10;
		L_10 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_7, L_9, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_11;
		L_11 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_10, NULL);
		String_t* L_12;
		L_12 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_5, _stringLiteral7F78A841F1FB280249A7526BB748F873862DDE5B, L_11, NULL);
		__this->___data_28 = L_12;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_12);
		// data = data + " || GOMAS DE HOP: " + Gomasdehop.options[Gomasdehop.value].text;
		String_t* L_13 = __this->___data_28;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_14 = __this->___Gomasdehop_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_15;
		L_15 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_14, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_16 = __this->___Gomasdehop_5;
		int32_t L_17;
		L_17 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_16, NULL);
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_18;
		L_18 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_15, L_17, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_19;
		L_19 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_18, NULL);
		String_t* L_20;
		L_20 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_13, _stringLiteral93B54C9C484CA2C7C22AEBF0798FB966B9E83084, L_19, NULL);
		__this->___data_28 = L_20;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_20);
		// data = data + " || NUB: " + nub.options[nub.value].text;
		String_t* L_21 = __this->___data_28;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_22 = __this->___nub_6;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_23;
		L_23 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_22, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_24 = __this->___nub_6;
		int32_t L_25;
		L_25 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_24, NULL);
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_26;
		L_26 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_23, L_25, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_27;
		L_27 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_26, NULL);
		String_t* L_28;
		L_28 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_21, _stringLiteral07C0962FAE2C663481A460B6596876CC9623671F, L_27, NULL);
		__this->___data_28 = L_28;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_28);
		// data = data + " || CAMARA DE HOP: " + Camarasdehop.options[Camarasdehop.value].text;
		String_t* L_29 = __this->___data_28;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_30 = __this->___Camarasdehop_7;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_31;
		L_31 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_30, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_32 = __this->___Camarasdehop_7;
		int32_t L_33;
		L_33 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_32, NULL);
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_34;
		L_34 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_31, L_33, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_35;
		L_35 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_34, NULL);
		String_t* L_36;
		L_36 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_29, _stringLiteral9CA4B2EFA31FB06DA2A5AEAB593048EF298BEF86, L_35, NULL);
		__this->___data_28 = L_36;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_36);
		// data = data + " || CA?ON: " + ca?on.options[ca?on.value].text;
		String_t* L_37 = __this->___data_28;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_38 = __this->___caUF1on_8;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_39;
		L_39 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_38, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_40 = __this->___caUF1on_8;
		int32_t L_41;
		L_41 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_40, NULL);
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_42;
		L_42 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_39, L_41, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_43;
		L_43 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_42, NULL);
		String_t* L_44;
		L_44 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_37, _stringLiteral487F72322899612CAD8E0F0B608F4F9C183DA574, L_43, NULL);
		__this->___data_28 = L_44;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_44);
		// data = data + " || GUIA DE MUELLE: " + Guiademuelle.options[Guiademuelle.value].text;
		String_t* L_45 = __this->___data_28;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_46 = __this->___Guiademuelle_13;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_47;
		L_47 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_46, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_48 = __this->___Guiademuelle_13;
		int32_t L_49;
		L_49 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_48, NULL);
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_50;
		L_50 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_47, L_49, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_51;
		L_51 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_50, NULL);
		String_t* L_52;
		L_52 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_45, _stringLiteralF8202313E3DA7E835479F0BBDB627857D7F020C4, L_51, NULL);
		__this->___data_28 = L_52;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_52);
		// data = data + " || TAPPER PLATE: " + Tapperplate.options[Tapperplate.value].text;
		String_t* L_53 = __this->___data_28;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_54 = __this->___Tapperplate_16;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_55;
		L_55 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_54, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_56 = __this->___Tapperplate_16;
		int32_t L_57;
		L_57 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_56, NULL);
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_58;
		L_58 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_55, L_57, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_59;
		L_59 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_58, NULL);
		String_t* L_60;
		L_60 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_53, _stringLiteral49E640CB14BEC971E54FAD00F061635E8C0BC12A, L_59, NULL);
		__this->___data_28 = L_60;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_60);
		// data = data + " || ENGRANAJES: " + Engranajes.options[Engranajes.value].text;
		String_t* L_61 = __this->___data_28;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_62 = __this->___Engranajes_17;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_63;
		L_63 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_62, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_64 = __this->___Engranajes_17;
		int32_t L_65;
		L_65 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_64, NULL);
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_66;
		L_66 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_63, L_65, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_67;
		L_67 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_66, NULL);
		String_t* L_68;
		L_68 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_61, _stringLiteral7C94292527C815E7D8F05CBEF28ACCFD119DA3C3, L_67, NULL);
		__this->___data_28 = L_68;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_68);
		// data = data + " || MOTOR: " + Motor.options[Motor.value].text;
		String_t* L_69 = __this->___data_28;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_70 = __this->___Motor_18;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_71;
		L_71 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_70, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_72 = __this->___Motor_18;
		int32_t L_73;
		L_73 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_72, NULL);
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_74;
		L_74 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_71, L_73, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_75;
		L_75 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_74, NULL);
		String_t* L_76;
		L_76 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_69, _stringLiteralA5E7510D235B3A166C812C2921FFF64AFCCA79EE, L_75, NULL);
		__this->___data_28 = L_76;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_76);
		// data = data + " || GATILLO ELECTRONICO: " + Gatilloelectronico.options[Gatilloelectronico.value].text;
		String_t* L_77 = __this->___data_28;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_78 = __this->___Gatilloelectronico_19;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_79;
		L_79 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_78, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_80 = __this->___Gatilloelectronico_19;
		int32_t L_81;
		L_81 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_80, NULL);
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_82;
		L_82 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_79, L_81, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_83;
		L_83 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_82, NULL);
		String_t* L_84;
		L_84 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_77, _stringLiteralBB92D45784B92FD1902B1C762D068BC12A9EE415, L_83, NULL);
		__this->___data_28 = L_84;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_84);
		// data = data + " || MOSFET: " + Mosfet.options[Mosfet.value].text;
		String_t* L_85 = __this->___data_28;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_86 = __this->___Mosfet_20;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_87;
		L_87 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_86, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_88 = __this->___Mosfet_20;
		int32_t L_89;
		L_89 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_88, NULL);
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_90;
		L_90 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_87, L_89, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_91;
		L_91 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_90, NULL);
		String_t* L_92;
		L_92 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_85, _stringLiteralDFA41B04A76CCD13BAAC104EAB867684910B5A08, L_91, NULL);
		__this->___data_28 = L_92;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_92);
		// data = data + " || NOZZLE: " + Nozzle.options[Nozzle.value].text;
		String_t* L_93 = __this->___data_28;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_94 = __this->___Nozzle_15;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_95;
		L_95 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_94, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_96 = __this->___Nozzle_15;
		int32_t L_97;
		L_97 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_96, NULL);
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_98;
		L_98 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_95, L_97, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_99;
		L_99 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_98, NULL);
		String_t* L_100;
		L_100 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_93, _stringLiteralBBE61E41E9AE08EF6A00F1DFA850309FCDB5EBCE, L_99, NULL);
		__this->___data_28 = L_100;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_100);
		// data = data + " || ALCANCE: " + alcance;
		String_t* L_101 = __this->___data_28;
		String_t* L_102;
		L_102 = Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339((&((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___alcance_21), NULL);
		String_t* L_103;
		L_103 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_101, _stringLiteral462AF2C3390D0074A52DC6D460AACF9292C8FDE7, L_102, NULL);
		__this->___data_28 = L_103;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_103);
		// data = data + " || POTENCIA: " + potencia;
		String_t* L_104 = __this->___data_28;
		String_t* L_105;
		L_105 = Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339((&((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___potencia_22), NULL);
		String_t* L_106;
		L_106 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_104, _stringLiteral669724E2ED32B3B0345C44EE89993FF550EB3E0A, L_105, NULL);
		__this->___data_28 = L_106;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_106);
		// data = data + " || PRECISION: " + precision;
		String_t* L_107 = __this->___data_28;
		String_t* L_108;
		L_108 = Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339((&((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___precision_23), NULL);
		String_t* L_109;
		L_109 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_107, _stringLiteralDA51C2EAB493D12813926075FEB2BCD4911C3760, L_108, NULL);
		__this->___data_28 = L_109;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_109);
		// data = data + " || CADENCIA: " + cadencia;
		String_t* L_110 = __this->___data_28;
		String_t* L_111;
		L_111 = Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339((&((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___cadencia_24), NULL);
		String_t* L_112;
		L_112 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_110, _stringLiteral930096876CCCD844B17880BFF15CAC5A9CBE2E02, L_111, NULL);
		__this->___data_28 = L_112;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_112);
		// data = data + " || VARIANZA: " + varianza;
		String_t* L_113 = __this->___data_28;
		String_t* L_114;
		L_114 = Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339((&((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___varianza_25), NULL);
		String_t* L_115;
		L_115 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_113, _stringLiteralB0692D2BC1A870C7A665A88E45BCA74258A93038, L_114, NULL);
		__this->___data_28 = L_115;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_115);
		// data = data + " || PRECIO: " + precio + " EUROS";
		String_t* L_116 = __this->___data_28;
		String_t* L_117;
		L_117 = Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339((&((HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_StaticFields*)il2cpp_codegen_static_fields_for(HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3_il2cpp_TypeInfo_var))->___precio_26), NULL);
		String_t* L_118;
		L_118 = String_Concat_mF8B69BE42B5C5ABCAD3C176FBBE3010E0815D65D(L_116, _stringLiteralEA6181131D358AFEDA8A7D851A7019B1856B14B9, L_117, _stringLiteral538AF8C1BD2E67A15B89C54853ED84EBB7533E92, NULL);
		__this->___data_28 = L_118;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___data_28), (void*)L_118);
		// nombreDescarga = nombreConf + ".txt";
		String_t* L_119 = __this->___nombreConf_27;
		String_t* L_120;
		L_120 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(L_119, _stringLiteral6D2D60FB81DB361B60B9512761B3ED9FF250BB96, NULL);
		__this->___nombreDescarga_29 = L_120;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___nombreDescarga_29), (void*)L_120);
		// }
		return;
	}
}
// System.Void HacerCaptura::error()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HacerCaptura_error_m0201DF7A79E1BF703064ABF1F31C3CBF85B56A3A (const RuntimeMethod* method) 
{
	{
		// errorNombre();
		HacerCaptura_errorNombre_m4BDC0D400F0F00016E8F247824BB2725B80C0312(NULL);
		// }
		return;
	}
}
// System.Void HacerCaptura::DescargarTxt(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HacerCaptura_DescargarTxt_mFA814DC1A322790177EF21C355E89B065EC1B2F6 (String_t* ___data0, String_t* ___nombreDescarga1, const RuntimeMethod* method) 
{
	{
		// descargarTxt(data, nombreDescarga);
		String_t* L_0 = ___data0;
		String_t* L_1 = ___nombreDescarga1;
		HacerCaptura_descargarTxt_m55158E62D0F4C65D8ACB3B5692B53F0A87766B46(L_0, L_1, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator HacerCaptura::descargarPNG()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* HacerCaptura_descargarPNG_m469AED5E474F6927E47D6597CA5CD81D4142105F (HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CdescargarPNGU3Ed__34_tA7E5A53A8875E941CD90ACE64537EAE2DEC6A999_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CdescargarPNGU3Ed__34_tA7E5A53A8875E941CD90ACE64537EAE2DEC6A999* L_0 = (U3CdescargarPNGU3Ed__34_tA7E5A53A8875E941CD90ACE64537EAE2DEC6A999*)il2cpp_codegen_object_new(U3CdescargarPNGU3Ed__34_tA7E5A53A8875E941CD90ACE64537EAE2DEC6A999_il2cpp_TypeInfo_var);
		U3CdescargarPNGU3Ed__34__ctor_mA41CBEFA68CDFA0D467D8FB2107F6D91B2D23B88(L_0, 0, NULL);
		return L_0;
	}
}
// System.Void HacerCaptura::SaveScreenshotWebGL(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HacerCaptura_SaveScreenshotWebGL_mD08623AF978D56A33A17C450E4148452367B6EF2 (String_t* ___filename0, String_t* ___data1, const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, char*);

	// Marshaling of parameter '___filename0' to native representation
	char* ____filename0_marshaled = NULL;
	____filename0_marshaled = il2cpp_codegen_marshal_string(___filename0);

	// Marshaling of parameter '___data1' to native representation
	char* ____data1_marshaled = NULL;
	____data1_marshaled = il2cpp_codegen_marshal_string(___data1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(SaveScreenshotWebGL)(____filename0_marshaled, ____data1_marshaled);

	// Marshaling cleanup of parameter '___filename0' native representation
	il2cpp_codegen_marshal_free(____filename0_marshaled);
	____filename0_marshaled = NULL;

	// Marshaling cleanup of parameter '___data1' native representation
	il2cpp_codegen_marshal_free(____data1_marshaled);
	____data1_marshaled = NULL;

}
// System.Void HacerCaptura::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HacerCaptura__ctor_m51F58258A300D2546101D133EA574AEA64FAFB4C (HacerCaptura_t67A3294D432B35A9EBBEB0BA51347E33F754E6D3* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void HacerCaptura/<descargarPNG>d__34::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CdescargarPNGU3Ed__34__ctor_mA41CBEFA68CDFA0D467D8FB2107F6D91B2D23B88 (U3CdescargarPNGU3Ed__34_tA7E5A53A8875E941CD90ACE64537EAE2DEC6A999* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void HacerCaptura/<descargarPNG>d__34::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CdescargarPNGU3Ed__34_System_IDisposable_Dispose_m8BA5409E509A8B1D06E2D10239B8E7DEE4A3CC2E (U3CdescargarPNGU3Ed__34_tA7E5A53A8875E941CD90ACE64537EAE2DEC6A999* __this, const RuntimeMethod* method) 
{
	{
		return;
	}
}
// System.Boolean HacerCaptura/<descargarPNG>d__34::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CdescargarPNGU3Ed__34_MoveNext_m83A335723BA6A2240721C9ECD010F91AE270981F (U3CdescargarPNGU3Ed__34_tA7E5A53A8875E941CD90ACE64537EAE2DEC6A999* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA5CBB81D72049EE48091F7F1EA3A53FBFA052011);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralAA2CBF3EFC19685B86E12C0FD93190E5329FFB55);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* V_3 = NULL;
	String_t* V_4 = NULL;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_002b;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// yield return new WaitForEndOfFrame();
		WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663* L_3 = (WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663*)il2cpp_codegen_object_new(WaitForEndOfFrame_tE38D80923E3F8380069B423968C25ABE50A46663_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m4AF7E576C01E6B04443BB898B1AE5D645F7D45AB(L_3, NULL);
		__this->___U3CU3E2__current_1 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_3);
		__this->___U3CU3E1__state_0 = 1;
		return (bool)1;
	}

IL_002b:
	{
		__this->___U3CU3E1__state_0 = (-1);
		// int ancho = Screen.width;
		int32_t L_4;
		L_4 = Screen_get_width_mCA5D955A53CF6D29C8C7118D517D0FC84AE8056C(NULL);
		V_1 = L_4;
		// int height = Screen.height;
		int32_t L_5;
		L_5 = Screen_get_height_m624DD2D53F34087064E3B9D09AC2207DB4E86CA8(NULL);
		V_2 = L_5;
		// var medidas = new Texture2D( ancho, height, TextureFormat.RGB24, false );
		int32_t L_6 = V_1;
		int32_t L_7 = V_2;
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_8 = (Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4*)il2cpp_codegen_object_new(Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4_il2cpp_TypeInfo_var);
		Texture2D__ctor_mECF60A9EC0638EC353C02C8E99B6B465D23BE917(L_8, L_6, L_7, 3, (bool)0, NULL);
		// medidas.ReadPixels( new Rect(0, 0, ancho, height), 0, 0 );
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_9 = L_8;
		int32_t L_10 = V_1;
		int32_t L_11 = V_2;
		Rect_tA04E0F8A1830E767F40FB27ECD8D309303571F0D L_12;
		memset((&L_12), 0, sizeof(L_12));
		Rect__ctor_m18C3033D135097BEE424AAA68D91C706D2647F23((&L_12), (0.0f), (0.0f), ((float)L_10), ((float)L_11), /*hidden argument*/NULL);
		Texture2D_ReadPixels_m6B45DF7C051BF599C72ED09691F21A6C769EEBD9(L_9, L_12, 0, 0, NULL);
		// medidas.Apply();
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_13 = L_9;
		Texture2D_Apply_mA014182C9EE0BBF6EEE3B286854F29E50EB972DC(L_13, NULL);
		// byte[] imagenBytes = medidas.EncodeToPNG();
		Texture2D_tE6505BC111DD8A424A9DBE8E05D7D09E11FFFCF4* L_14 = L_13;
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_15;
		L_15 = ImageConversion_EncodeToPNG_m0FFFD0F0DC0EC22073BC937A5294067C57008391(L_14, NULL);
		V_3 = L_15;
		// Destroy( medidas );
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		Object_Destroy_mFCDAE6333522488F60597AF019EA90BB1207A5AA(L_14, NULL);
		// string textoCod = System.Convert.ToBase64String (imagenBytes);
		ByteU5BU5D_tA6237BF417AE52AD70CFB4EF24A7A82613DF9031* L_16 = V_3;
		il2cpp_codegen_runtime_class_init_inline(Convert_t7097FF336D592F7C06D88A98349A44646F91EFFC_il2cpp_TypeInfo_var);
		String_t* L_17;
		L_17 = Convert_ToBase64String_mB276B21511FB01CDE030619C81757E786F91B9F3(L_16, NULL);
		V_4 = L_17;
		// var image_url = "image/png;base64," + textoCod;
		String_t* L_18 = V_4;
		String_t* L_19;
		L_19 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteralAA2CBF3EFC19685B86E12C0FD93190E5329FFB55, L_18, NULL);
		// Debug.Log (image_url);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_19, NULL);
		// SaveScreenshotWebGL("preset.png",textoCod);
		String_t* L_20 = V_4;
		HacerCaptura_SaveScreenshotWebGL_mD08623AF978D56A33A17C450E4148452367B6EF2(_stringLiteralA5CBB81D72049EE48091F7F1EA3A53FBFA052011, L_20, NULL);
		// }
		return (bool)0;
	}
}
// System.Object HacerCaptura/<descargarPNG>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CdescargarPNGU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5431F654B2E32A7119E3A1AB4577C672BFFE03B (U3CdescargarPNGU3Ed__34_tA7E5A53A8875E941CD90ACE64537EAE2DEC6A999* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void HacerCaptura/<descargarPNG>d__34::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CdescargarPNGU3Ed__34_System_Collections_IEnumerator_Reset_m2D3D04EAB98096753239C68AF245F189AD38CCCB (U3CdescargarPNGU3Ed__34_tA7E5A53A8875E941CD90ACE64537EAE2DEC6A999* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CdescargarPNGU3Ed__34_System_Collections_IEnumerator_Reset_m2D3D04EAB98096753239C68AF245F189AD38CCCB_RuntimeMethod_var)));
	}
}
// System.Object HacerCaptura/<descargarPNG>d__34::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CdescargarPNGU3Ed__34_System_Collections_IEnumerator_get_Current_m880B10B450145A9169BA22AEA466099B05B1898B (U3CdescargarPNGU3Ed__34_tA7E5A53A8875E941CD90ACE64537EAE2DEC6A999* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>> LeerExcel::Read(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tE020B120302E34B781278C33685F2DA255337446* LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05 (String_t* ___file0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mC4F3DF292BAD88F4BF193C49CD689FAEBC4570A9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_set_Item_m7CCA97075B48AFB2B97E5A072B94BC7679374341_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mA293F10765359D70384794AA7DA94A25FA0AC15E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mB0CFC5E35B840AE25C5F2B640B98E5D9CE401B08_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tE020B120302E34B781278C33685F2DA255337446_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Regex_tE773142C2BE45C5D362B0F815AFF831707A51772_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* V_1 = NULL;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* V_2 = NULL;
	int32_t V_3 = 0;
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* V_4 = NULL;
	Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* V_5 = NULL;
	int32_t V_6 = 0;
	String_t* V_7 = NULL;
	RuntimeObject* V_8 = NULL;
	int32_t V_9 = 0;
	float V_10 = 0.0f;
	{
		// var list = new List<Dictionary<string, object>>();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_0 = (List_1_tE020B120302E34B781278C33685F2DA255337446*)il2cpp_codegen_object_new(List_1_tE020B120302E34B781278C33685F2DA255337446_il2cpp_TypeInfo_var);
		List_1__ctor_mB0CFC5E35B840AE25C5F2B640B98E5D9CE401B08(L_0, List_1__ctor_mB0CFC5E35B840AE25C5F2B640B98E5D9CE401B08_RuntimeMethod_var);
		V_0 = L_0;
		// TextAsset data = Resources.Load(file) as TextAsset;
		String_t* L_1 = ___file0;
		Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C* L_2;
		L_2 = Resources_Load_m9608D2902F542C0B7FD52EFED088323448B9FA35(L_1, NULL);
		// var lines = Regex.Split(data.text, LINE_SPLIT_RE);
		String_t* L_3;
		L_3 = TextAsset_get_text_m36846042E3CF3D9DD337BF3F8B2B1902D10C8FD9(((TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69*)IsInstClass((RuntimeObject*)L_2, TextAsset_t2C64E93DA366D9DE5A8209E1802FA4884AC1BD69_il2cpp_TypeInfo_var)), NULL);
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		String_t* L_4 = ((LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_StaticFields*)il2cpp_codegen_static_fields_for(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var))->___LINE_SPLIT_RE_1;
		il2cpp_codegen_runtime_class_init_inline(Regex_tE773142C2BE45C5D362B0F815AFF831707A51772_il2cpp_TypeInfo_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5;
		L_5 = Regex_Split_m01D4D38E8257BB18BFBC08D2D8DCB7F32FB0D018(L_3, L_4, NULL);
		V_1 = L_5;
		// if (lines.Length <= 1) return list;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_6 = V_1;
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_6)->max_length))) > ((int32_t)1)))
		{
			goto IL_0029;
		}
	}
	{
		// if (lines.Length <= 1) return list;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_7 = V_0;
		return L_7;
	}

IL_0029:
	{
		// var header = Regex.Split(lines[0], SPLIT_RE);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_8 = V_1;
		int32_t L_9 = 0;
		String_t* L_10 = (L_8)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_9));
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		String_t* L_11 = ((LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_StaticFields*)il2cpp_codegen_static_fields_for(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var))->___SPLIT_RE_0;
		il2cpp_codegen_runtime_class_init_inline(Regex_tE773142C2BE45C5D362B0F815AFF831707A51772_il2cpp_TypeInfo_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_12;
		L_12 = Regex_Split_m01D4D38E8257BB18BFBC08D2D8DCB7F32FB0D018(L_10, L_11, NULL);
		V_2 = L_12;
		// for (var i = 1; i < lines.Length; i++)
		V_3 = 1;
		goto IL_00fe;
	}

IL_003e:
	{
		// var values = Regex.Split(lines[i], SPLIT_RE);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_13 = V_1;
		int32_t L_14 = V_3;
		int32_t L_15 = L_14;
		String_t* L_16 = (L_13)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_15));
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		String_t* L_17 = ((LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_StaticFields*)il2cpp_codegen_static_fields_for(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var))->___SPLIT_RE_0;
		il2cpp_codegen_runtime_class_init_inline(Regex_tE773142C2BE45C5D362B0F815AFF831707A51772_il2cpp_TypeInfo_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_18;
		L_18 = Regex_Split_m01D4D38E8257BB18BFBC08D2D8DCB7F32FB0D018(L_16, L_17, NULL);
		V_4 = L_18;
		// if (values.Length == 0 || values[0] == "") continue;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_19 = V_4;
		if (!(((RuntimeArray*)L_19)->max_length))
		{
			goto IL_00fa;
		}
	}
	{
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = V_4;
		int32_t L_21 = 0;
		String_t* L_22 = (L_20)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_21));
		bool L_23;
		L_23 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_22, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
		if (L_23)
		{
			goto IL_00fa;
		}
	}
	{
		// var entry = new Dictionary<string, object>();
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_24 = (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710*)il2cpp_codegen_object_new(Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mC4F3DF292BAD88F4BF193C49CD689FAEBC4570A9(L_24, Dictionary_2__ctor_mC4F3DF292BAD88F4BF193C49CD689FAEBC4570A9_RuntimeMethod_var);
		V_5 = L_24;
		// for (var j = 0; j < header.Length && j < values.Length; j++)
		V_6 = 0;
		goto IL_00e3;
	}

IL_0074:
	{
		// string value = values[j];
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = V_4;
		int32_t L_26 = V_6;
		int32_t L_27 = L_26;
		String_t* L_28 = (L_25)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_27));
		V_7 = L_28;
		// value = value.TrimStart(TRIM_CHARS).TrimEnd(TRIM_CHARS).Replace("\\", "");
		String_t* L_29 = V_7;
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_30 = ((LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_StaticFields*)il2cpp_codegen_static_fields_for(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var))->___TRIM_CHARS_2;
		String_t* L_31;
		L_31 = String_TrimStart_m67833D80326BEA11CC3517CE03CD7B16669BCEEC(L_29, L_30, NULL);
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_32 = ((LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_StaticFields*)il2cpp_codegen_static_fields_for(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var))->___TRIM_CHARS_2;
		String_t* L_33;
		L_33 = String_TrimEnd_mD7CFB0999EEEE20E3A869516EBCE07E8AB5BD529(L_31, L_32, NULL);
		String_t* L_34;
		L_34 = String_Replace_mABDB7003A1D0AEDCAE9FF85E3DFFFBA752D2A166(L_33, _stringLiteral09B11B6CC411D8B9FFB75EAAE9A35B2AF248CE40, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
		V_7 = L_34;
		// object finalvalue = value;
		String_t* L_35 = V_7;
		V_8 = L_35;
		// if (int.TryParse(value, out n))
		String_t* L_36 = V_7;
		bool L_37;
		L_37 = Int32_TryParse_mFC6BFCB86964E2BCA4052155B10983837A695EA4(L_36, (&V_9), NULL);
		if (!L_37)
		{
			goto IL_00bc;
		}
	}
	{
		// finalvalue = n;
		int32_t L_38 = V_9;
		int32_t L_39 = L_38;
		RuntimeObject* L_40 = Box(Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C_il2cpp_TypeInfo_var, &L_39);
		V_8 = L_40;
		goto IL_00d0;
	}

IL_00bc:
	{
		// else if (float.TryParse(value,out f))
		String_t* L_41 = V_7;
		bool L_42;
		L_42 = Single_TryParse_mF23E88B4B12DDC9E82179BB2483A714005BF006F(L_41, (&V_10), NULL);
		if (!L_42)
		{
			goto IL_00d0;
		}
	}
	{
		// finalvalue = f;
		float L_43 = V_10;
		float L_44 = L_43;
		RuntimeObject* L_45 = Box(Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C_il2cpp_TypeInfo_var, &L_44);
		V_8 = L_45;
	}

IL_00d0:
	{
		// entry[header[j]] = finalvalue;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_46 = V_5;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_47 = V_2;
		int32_t L_48 = V_6;
		int32_t L_49 = L_48;
		String_t* L_50 = (L_47)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_49));
		RuntimeObject* L_51 = V_8;
		Dictionary_2_set_Item_m7CCA97075B48AFB2B97E5A072B94BC7679374341(L_46, L_50, L_51, Dictionary_2_set_Item_m7CCA97075B48AFB2B97E5A072B94BC7679374341_RuntimeMethod_var);
		// for (var j = 0; j < header.Length && j < values.Length; j++)
		int32_t L_52 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add(L_52, 1));
	}

IL_00e3:
	{
		// for (var j = 0; j < header.Length && j < values.Length; j++)
		int32_t L_53 = V_6;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_54 = V_2;
		if ((((int32_t)L_53) >= ((int32_t)((int32_t)(((RuntimeArray*)L_54)->max_length)))))
		{
			goto IL_00f2;
		}
	}
	{
		int32_t L_55 = V_6;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_56 = V_4;
		if ((((int32_t)L_55) < ((int32_t)((int32_t)(((RuntimeArray*)L_56)->max_length)))))
		{
			goto IL_0074;
		}
	}

IL_00f2:
	{
		// list.Add(entry);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_57 = V_0;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_58 = V_5;
		List_1_Add_mA293F10765359D70384794AA7DA94A25FA0AC15E_inline(L_57, L_58, List_1_Add_mA293F10765359D70384794AA7DA94A25FA0AC15E_RuntimeMethod_var);
	}

IL_00fa:
	{
		// for (var i = 1; i < lines.Length; i++)
		int32_t L_59 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add(L_59, 1));
	}

IL_00fe:
	{
		// for (var i = 1; i < lines.Length; i++)
		int32_t L_60 = V_3;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_61 = V_1;
		if ((((int32_t)L_60) < ((int32_t)((int32_t)(((RuntimeArray*)L_61)->max_length)))))
		{
			goto IL_003e;
		}
	}
	{
		// return list;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_62 = V_0;
		return L_62;
	}
}
// System.Void LeerExcel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeerExcel__ctor_m45A2D5DA9F28D8A46745468A507FEC442BA296A9 (LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E* __this, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		return;
	}
}
// System.Void LeerExcel::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LeerExcel__cctor_m4EE1653C59FBEED5E4F213D8DDDA580775CD83F6 (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6BEAC275522F5FA9B3B3C4D4E0271A9CC978CE7E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC49039D31EFC2F2CE6BB1F0E3B3140935DC3C6F8);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static string SPLIT_RE = @",(?=(?:[^""]*""[^""]*"")*(?![^""]*""))";
		((LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_StaticFields*)il2cpp_codegen_static_fields_for(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var))->___SPLIT_RE_0 = _stringLiteralC49039D31EFC2F2CE6BB1F0E3B3140935DC3C6F8;
		Il2CppCodeGenWriteBarrier((void**)(&((LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_StaticFields*)il2cpp_codegen_static_fields_for(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var))->___SPLIT_RE_0), (void*)_stringLiteralC49039D31EFC2F2CE6BB1F0E3B3140935DC3C6F8);
		// static string LINE_SPLIT_RE = @"\r\n|\n\r|\n|\r";
		((LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_StaticFields*)il2cpp_codegen_static_fields_for(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var))->___LINE_SPLIT_RE_1 = _stringLiteral6BEAC275522F5FA9B3B3C4D4E0271A9CC978CE7E;
		Il2CppCodeGenWriteBarrier((void**)(&((LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_StaticFields*)il2cpp_codegen_static_fields_for(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var))->___LINE_SPLIT_RE_1), (void*)_stringLiteral6BEAC275522F5FA9B3B3C4D4E0271A9CC978CE7E);
		// static char[] TRIM_CHARS = { '\"' };
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_0 = (CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB*)SZArrayNew(CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t799905CF001DD5F13F7DBB310181FC4D8B7D0AAB* L_1 = L_0;
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)34));
		((LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_StaticFields*)il2cpp_codegen_static_fields_for(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var))->___TRIM_CHARS_2 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&((LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_StaticFields*)il2cpp_codegen_static_fields_for(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var))->___TRIM_CHARS_2), (void*)L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void saveConfig::todoCorrecto()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void saveConfig_todoCorrecto_mA18AA8966091253847C30259F10AD1A214827BAF (const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(todoCorrecto)();

}
// System.Void saveConfig::errorNombre()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void saveConfig_errorNombre_mF2BC35C6DE825FFE4CBA538B935A2BB19407F47E (const RuntimeMethod* method) 
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(errorNombre)();

}
// System.Void saveConfig::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void saveConfig_Start_m3D8EF53111094946B8E8F1CD60A70A890A99B1B2 (saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral040A30393EB823D7DA3AC6B441CDFED1AB592B94);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1799E587C0E03A929C0DB02AC5D972B7ADCAC934);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2029726709853776916AF8A4F3E6E03CB0E0173E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral215C8F5F3404D78404F7C939B86F46ED678A9ADA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4027C33FEB9C4FFC83129E78CA7013F55996057E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral77A913898061E87BBE38911BC21627A13F2912E6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral80A657B65DE79975507DBF69E753C669905069B0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral899C6B6BCFDE33642D1164B699BE502D64A046FC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8ED68BAC915B2C7822A6B0DB1979FE7E8EAC1A50);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA3588B77B02D07525328910BD4D164C0D114EE1D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA3DAEA94625D971790C73D80BA8B25EEA80D1601);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralABC75279EE653F5172A467E2EBAC949CD62C2950);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB7E2FD902EE0982393234AD072DA05521DE56595);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD60B5B1ADC6E9FDD23C9FCF4D187BC04E6A19C0C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE151B09FD3B2BC6AFE6CF2FF0AB3BD777A211779);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE418A24EB42A2E7CB851EE4B76D111A17AAD4C7B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF25EA25906AC912ABFEEEAF7EE942AE014C6A026);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Gearbox = GameObject.Find("Drop_Gearbox").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_0;
		L_0 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralA3DAEA94625D971790C73D80BA8B25EEA80D1601, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_1;
		L_1 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_0, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Gearbox_4 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Gearbox_4), (void*)L_1);
		// Gomasdehop = GameObject.Find("Drop_GomaHop").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2;
		L_2 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral4027C33FEB9C4FFC83129E78CA7013F55996057E, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_3;
		L_3 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_2, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Gomasdehop_5 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Gomasdehop_5), (void*)L_3);
		// nub = GameObject.Find("Drop_Nub").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_4;
		L_4 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralB7E2FD902EE0982393234AD072DA05521DE56595, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_5;
		L_5 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_4, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___nub_6 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___nub_6), (void*)L_5);
		// Camarasdehop = GameObject.Find("Drop_CamaraDeHop").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6;
		L_6 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral1799E587C0E03A929C0DB02AC5D972B7ADCAC934, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_7;
		L_7 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_6, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Camarasdehop_7 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Camarasdehop_7), (void*)L_7);
		// ca?on = GameObject.Find("Drop_Ca?on").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_8;
		L_8 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral040A30393EB823D7DA3AC6B441CDFED1AB592B94, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_9;
		L_9 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_8, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___caUF1on_8 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___caUF1on_8), (void*)L_9);
		// Cilindro = GameObject.Find("Drop_Cilindro").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral80A657B65DE79975507DBF69E753C669905069B0, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_11;
		L_11 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_10, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Cilindro_9 = L_11;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Cilindro_9), (void*)L_11);
		// Cabezadecilindro = GameObject.Find("Drop_CabezaDeCilindro").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12;
		L_12 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralD60B5B1ADC6E9FDD23C9FCF4D187BC04E6A19C0C, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_13;
		L_13 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_12, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Cabezadecilindro_10 = L_13;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Cabezadecilindro_10), (void*)L_13);
		// Cabezadepiston = GameObject.Find("Drop_CabezaDePiston").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_14;
		L_14 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralA3588B77B02D07525328910BD4D164C0D114EE1D, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_15;
		L_15 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_14, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Cabezadepiston_11 = L_15;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Cabezadepiston_11), (void*)L_15);
		// Muelle = GameObject.Find("Drop_Muelle").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_16;
		L_16 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralE151B09FD3B2BC6AFE6CF2FF0AB3BD777A211779, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_17;
		L_17 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_16, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Muelle_12 = L_17;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Muelle_12), (void*)L_17);
		// Guiademuelle = GameObject.Find("Drop_GuiaDeMuelle").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_18;
		L_18 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral8ED68BAC915B2C7822A6B0DB1979FE7E8EAC1A50, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_19;
		L_19 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_18, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Guiademuelle_13 = L_19;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Guiademuelle_13), (void*)L_19);
		// Piston = GameObject.Find("Drop_Piston").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_20;
		L_20 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralE418A24EB42A2E7CB851EE4B76D111A17AAD4C7B, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_21;
		L_21 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_20, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Piston_14 = L_21;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Piston_14), (void*)L_21);
		// Tapperplate = GameObject.Find("Drop_TapperPlate").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22;
		L_22 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralF25EA25906AC912ABFEEEAF7EE942AE014C6A026, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_23;
		L_23 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_22, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Tapperplate_16 = L_23;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Tapperplate_16), (void*)L_23);
		// Engranajes = GameObject.Find("Drop_Engranajes").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_24;
		L_24 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral899C6B6BCFDE33642D1164B699BE502D64A046FC, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_25;
		L_25 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_24, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Engranajes_17 = L_25;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Engranajes_17), (void*)L_25);
		// Motor = GameObject.Find("Drop_Motor").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_26;
		L_26 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralABC75279EE653F5172A467E2EBAC949CD62C2950, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_27;
		L_27 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_26, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Motor_18 = L_27;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Motor_18), (void*)L_27);
		// Gatilloelectronico = GameObject.Find("Drop_Gatillo").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_28;
		L_28 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral77A913898061E87BBE38911BC21627A13F2912E6, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_29;
		L_29 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_28, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Gatilloelectronico_19 = L_29;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Gatilloelectronico_19), (void*)L_29);
		// Mosfet = GameObject.Find("Drop_Mosfet").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_30;
		L_30 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral2029726709853776916AF8A4F3E6E03CB0E0173E, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_31;
		L_31 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_30, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Mosfet_20 = L_31;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Mosfet_20), (void*)L_31);
		// Nozzle = GameObject.Find("Drop_Nozzle").GetComponent<Dropdown>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_32;
		L_32 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral215C8F5F3404D78404F7C939B86F46ED678A9ADA, NULL);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_33;
		L_33 = GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4(L_32, GameObject_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m2A72197B4C37440140B21C026F32F703CCB19BF4_RuntimeMethod_var);
		__this->___Nozzle_15 = L_33;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Nozzle_15), (void*)L_33);
		// alcance = 0;
		((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___alcance_21 = (0.0);
		// potencia = 0;
		((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___potencia_22 = (0.0);
		// precision = 0;
		((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___precision_23 = (0.0);
		// cadencia = 0;
		((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___cadencia_24 = (0.0);
		// varianza = 0;
		((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___varianza_25 = (0.0);
		// precio = 0;
		((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___precio_26 = (0.0);
		// }
		return;
	}
}
// System.Void saveConfig::setStats(System.Double,System.Double,System.Double,System.Double,System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void saveConfig_setStats_m11B648967ECBEA5D7FF3E0A9A5B260CCFC953A26 (double ___alcanceX0, double ___potenciaX1, double ___precisionX2, double ___cadenciaX3, double ___varianzaX4, double ___precioX5, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// alcance = alcanceX;
		double L_0 = ___alcanceX0;
		((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___alcance_21 = L_0;
		// potencia = potenciaX;
		double L_1 = ___potenciaX1;
		((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___potencia_22 = L_1;
		// precision = precisionX;
		double L_2 = ___precisionX2;
		((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___precision_23 = L_2;
		// cadencia = cadenciaX;
		double L_3 = ___cadenciaX3;
		((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___cadencia_24 = L_3;
		// varianza = varianzaX;
		double L_4 = ___varianzaX4;
		((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___varianza_25 = L_4;
		// precio = precioX;
		double L_5 = ___precioX5;
		((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___precio_26 = L_5;
		// }
		return;
	}
}
// System.Void saveConfig::error()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void saveConfig_error_m7BB8597BA30D2F230E8FAC765ED166F3539F3EDF (const RuntimeMethod* method) 
{
	{
		// errorNombre();
		saveConfig_errorNombre_mF2BC35C6DE825FFE4CBA538B935A2BB19407F47E(NULL);
		// }
		return;
	}
}
// System.Void saveConfig::bien()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void saveConfig_bien_mC22BBB0E80B8D093F33CD35074F571D5FD08F959 (const RuntimeMethod* method) 
{
	{
		// todoCorrecto();
		saveConfig_todoCorrecto_mA18AA8966091253847C30259F10AD1A214827BAF(NULL);
		// }
		return;
	}
}
// System.Void saveConfig::callSave()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void saveConfig_callSave_m656171465A8E1A6F511290B77ABFFBEEB9826510 (saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* __this, const RuntimeMethod* method) 
{
	{
		// StartCoroutine(Save());
		RuntimeObject* L_0;
		L_0 = saveConfig_Save_m9A7646305B7F0AE74C1AAD993AC03F16EC45532F(__this, NULL);
		Coroutine_t85EA685566A254C23F3FD77AB5BDFFFF8799596B* L_1;
		L_1 = MonoBehaviour_StartCoroutine_m4CAFF732AA28CD3BDC5363B44A863575530EC812(__this, L_0, NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator saveConfig::Save()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* saveConfig_Save_m9A7646305B7F0AE74C1AAD993AC03F16EC45532F (saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE* L_0 = (U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE*)il2cpp_codegen_object_new(U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE_il2cpp_TypeInfo_var);
		U3CSaveU3Ed__31__ctor_m9D8B3EB5A48E88E28F2652A7DF75887CE1C89FC7(L_0, 0, NULL);
		U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE* L_1 = L_0;
		L_1->___U3CU3E4__this_2 = __this;
		Il2CppCodeGenWriteBarrier((void**)(&L_1->___U3CU3E4__this_2), (void*)__this);
		return L_1;
	}
}
// System.Void saveConfig::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void saveConfig__ctor_m66E66E05E5650AA0DC2341040E1178195C8DC330 (saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void saveConfig/<Save>d__31::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSaveU3Ed__31__ctor_m9D8B3EB5A48E88E28F2652A7DF75887CE1C89FC7 (U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE* __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method) 
{
	{
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->___U3CU3E1__state_0 = L_0;
		return;
	}
}
// System.Void saveConfig/<Save>d__31::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSaveU3Ed__31_System_IDisposable_Dispose_mD2D34F6605E9503E0D0ADED22CB0C2F7D5B7A08C (U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->___U3CU3E1__state_0;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)-3))))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_001a;
		}
	}

IL_0010:
	{
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0013:
			{// begin finally (depth: 1)
				U3CSaveU3Ed__31_U3CU3Em__Finally1_m8D488E2CE71CF9ED53EF23A773106D0607794BC2(__this, NULL);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			goto IL_001a;
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_001a:
	{
		return;
	}
}
// System.Boolean saveConfig/<Save>d__31::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CSaveU3Ed__31_MoveNext_mCB2A61DC38D34516DD6B611386CFD9BD29924643 (U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisInputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140_mC8A4AE0E76B7AC26736C0FA8510832B97D2F400E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral0401651977F38591A9C61098B3E396B6E8C5F20F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2068FC53A9A4DFCC951852E87B73C9B115FC016B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36328BA2B4A341F864BB9986BAE6A25FC6472C2F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral45D2EE73A515C8F1D44B7D03ED19C8403309222B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral46D28FF30C29C530049AA60FB209FE1D69FF6472);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral524126665B399B3ED6EFBBD2FFFC1CC2BCA54442);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6B17125BFE29CB975A7E9D3A67FC7104EC9AB7C3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral77433E583240E26992DA7E6DFAA703B2EED57EE4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8295617198EB635126DF85EA15FD35F7326C2844);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8F304EE1AC6BDCD44BB0E10C1513BADDB2598C47);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA4462CA9ADC2AD6448A6395345473E79E165913C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA8F244FC674653C59552E40566FE156BB9BB79FD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBD67F18126E2B17C9F201864D9C76708216787EC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCF493073FE6A01071725B91F7750AC1AD244B1F5);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD2A0BEF1301EDC8FC912D4C386656A742D2BD1FD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD2C555C9680CF16F44B65256DE94B93D3D6993CF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDE7BB59E5C480D1D69B765EBC060F9A234A55470);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDF510ED9801CFCE3E869CBFF940B43857E871D99);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE4CFDEDE86B21B9DC88E2877BF3E1378C75D3333);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF02BE9044F64252C704D28D19B7A013ABD051FC1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF224AEDDC1FF132CAD6D558128124DB5A3462591);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFC4ABD7D01F00B4D077136F6A39C82C63CC9C000);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFCA74A9060DDEE6A3ECAE73E6AB96086770BE7E2);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFD3946DBC3C021874AF2A9CFA9A5796BFFC26EE6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t V_1 = 0;
	saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* V_2 = NULL;
	String_t* V_3 = NULL;
	WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* V_4 = NULL;
	{
		auto __finallyBlock = il2cpp::utils::Fault([&]
		{

FAULT_04b6:
			{// begin fault (depth: 1)
				U3CSaveU3Ed__31_System_IDisposable_Dispose_mD2D34F6605E9503E0D0ADED22CB0C2F7D5B7A08C(__this, NULL);
				return;
			}// end fault
		});
		try
		{// begin try (depth: 1)
			{
				int32_t L_0 = __this->___U3CU3E1__state_0;
				V_1 = L_0;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_1 = __this->___U3CU3E4__this_2;
				V_2 = L_1;
				int32_t L_2 = V_1;
				if (!L_2)
				{
					goto IL_001f_1;
				}
			}
			{
				int32_t L_3 = V_1;
				if ((((int32_t)L_3) == ((int32_t)1)))
				{
					goto IL_0425_1;
				}
			}
			{
				V_0 = (bool)0;
				goto IL_04bd;
			}

IL_001f_1:
			{
				__this->___U3CU3E1__state_0 = (-1);
				// nombreConf = GameObject.Find("nombreConfig").GetComponent<InputField>().text;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_4 = V_2;
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5;
				L_5 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral0401651977F38591A9C61098B3E396B6E8C5F20F, NULL);
				InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140* L_6;
				L_6 = GameObject_GetComponent_TisInputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140_mC8A4AE0E76B7AC26736C0FA8510832B97D2F400E(L_5, GameObject_GetComponent_TisInputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140_mC8A4AE0E76B7AC26736C0FA8510832B97D2F400E_RuntimeMethod_var);
				String_t* L_7;
				L_7 = InputField_get_text_m6E0796350FF559505E4DF17311803962699D6704_inline(L_6, NULL);
				L_4->___nombreConf_27 = L_7;
				Il2CppCodeGenWriteBarrier((void**)(&L_4->___nombreConf_27), (void*)L_7);
				// if(string.IsNullOrEmpty(nombreConf)){
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_8 = V_2;
				String_t* L_9 = L_8->___nombreConf_27;
				bool L_10;
				L_10 = String_IsNullOrEmpty_m54CF0907E7C4F3AFB2E796A13DC751ECBB8DB64A(L_9, NULL);
				if (!L_10)
				{
					goto IL_0061_1;
				}
			}
			{
				// Debug.Log("El nombre esta vacio");
				il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
				Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(_stringLiteralD2C555C9680CF16F44B65256DE94B93D3D6993CF, NULL);
				// error();
				saveConfig_error_m7BB8597BA30D2F230E8FAC765ED166F3539F3EDF(NULL);
				goto IL_04b2_1;
			}

IL_0061_1:
			{
				// string url = "http://localhost/tfg_web/includes/saveConfig.php";
				V_3 = _stringLiteral2068FC53A9A4DFCC951852E87B73C9B115FC016B;
				// WWWForm form = new WWWForm();
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_11 = (WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045*)il2cpp_codegen_object_new(WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045_il2cpp_TypeInfo_var);
				WWWForm__ctor_mB1AA4D4BE7011A371B590332CC65794270F269F6(L_11, NULL);
				V_4 = L_11;
				// form.AddField("nombreConf", nombreConf);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_12 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_13 = V_2;
				String_t* L_14 = L_13->___nombreConf_27;
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_12, _stringLiteralDE7BB59E5C480D1D69B765EBC060F9A234A55470, L_14, NULL);
				// form.AddField("muelle", Muelle.options[Muelle.value].text);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_15 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_16 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_17 = L_16->___Muelle_12;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_18;
				L_18 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_17, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_19 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_20 = L_19->___Muelle_12;
				int32_t L_21;
				L_21 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_20, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_22;
				L_22 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_18, L_21, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_23;
				L_23 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_22, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_15, _stringLiteralBD67F18126E2B17C9F201864D9C76708216787EC, L_23, NULL);
				// form.AddField("Gomasdehop", Gomasdehop.options[Gomasdehop.value].text);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_24 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_25 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_26 = L_25->___Gomasdehop_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_27;
				L_27 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_26, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_28 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_29 = L_28->___Gomasdehop_5;
				int32_t L_30;
				L_30 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_29, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_31;
				L_31 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_27, L_30, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_32;
				L_32 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_31, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_24, _stringLiteralF02BE9044F64252C704D28D19B7A013ABD051FC1, L_32, NULL);
				// form.AddField("nub", nub.options[nub.value].text);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_33 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_34 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_35 = L_34->___nub_6;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_36;
				L_36 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_35, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_37 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_38 = L_37->___nub_6;
				int32_t L_39;
				L_39 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_38, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_40;
				L_40 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_36, L_39, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_41;
				L_41 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_40, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_33, _stringLiteral524126665B399B3ED6EFBBD2FFFC1CC2BCA54442, L_41, NULL);
				// form.AddField("Camarasdehop", Camarasdehop.options[Camarasdehop.value].text);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_42 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_43 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_44 = L_43->___Camarasdehop_7;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_45;
				L_45 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_44, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_46 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_47 = L_46->___Camarasdehop_7;
				int32_t L_48;
				L_48 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_47, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_49;
				L_49 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_45, L_48, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_50;
				L_50 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_49, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_42, _stringLiteral36328BA2B4A341F864BB9986BAE6A25FC6472C2F, L_50, NULL);
				// form.AddField("ca?on", ca?on.options[ca?on.value].text);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_51 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_52 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_53 = L_52->___caUF1on_8;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_54;
				L_54 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_53, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_55 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_56 = L_55->___caUF1on_8;
				int32_t L_57;
				L_57 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_56, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_58;
				L_58 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_54, L_57, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_59;
				L_59 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_58, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_51, _stringLiteralD2A0BEF1301EDC8FC912D4C386656A742D2BD1FD, L_59, NULL);
				// form.AddField("Cilindro", Cilindro.options[Cilindro.value].text);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_60 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_61 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_62 = L_61->___Cilindro_9;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_63;
				L_63 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_62, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_64 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_65 = L_64->___Cilindro_9;
				int32_t L_66;
				L_66 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_65, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_67;
				L_67 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_63, L_66, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_68;
				L_68 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_67, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_60, _stringLiteralE4CFDEDE86B21B9DC88E2877BF3E1378C75D3333, L_68, NULL);
				// form.AddField("Cabezadecilindro", Cabezadecilindro.options[Cabezadecilindro.value].text);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_69 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_70 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_71 = L_70->___Cabezadecilindro_10;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_72;
				L_72 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_71, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_73 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_74 = L_73->___Cabezadecilindro_10;
				int32_t L_75;
				L_75 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_74, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_76;
				L_76 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_72, L_75, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_77;
				L_77 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_76, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_69, _stringLiteralCF493073FE6A01071725B91F7750AC1AD244B1F5, L_77, NULL);
				// form.AddField("Cabezadepiston", Cabezadepiston.options[Cabezadepiston.value].text);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_78 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_79 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_80 = L_79->___Cabezadepiston_11;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_81;
				L_81 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_80, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_82 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_83 = L_82->___Cabezadepiston_11;
				int32_t L_84;
				L_84 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_83, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_85;
				L_85 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_81, L_84, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_86;
				L_86 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_85, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_78, _stringLiteral8F304EE1AC6BDCD44BB0E10C1513BADDB2598C47, L_86, NULL);
				// form.AddField("Guiademuelle", Guiademuelle.options[Guiademuelle.value].text);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_87 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_88 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_89 = L_88->___Guiademuelle_13;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_90;
				L_90 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_89, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_91 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_92 = L_91->___Guiademuelle_13;
				int32_t L_93;
				L_93 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_92, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_94;
				L_94 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_90, L_93, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_95;
				L_95 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_94, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_87, _stringLiteralFD3946DBC3C021874AF2A9CFA9A5796BFFC26EE6, L_95, NULL);
				// form.AddField("Piston", Piston.options[Piston.value].text);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_96 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_97 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_98 = L_97->___Piston_14;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_99;
				L_99 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_98, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_100 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_101 = L_100->___Piston_14;
				int32_t L_102;
				L_102 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_101, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_103;
				L_103 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_99, L_102, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_104;
				L_104 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_103, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_96, _stringLiteralFC4ABD7D01F00B4D077136F6A39C82C63CC9C000, L_104, NULL);
				// form.AddField("Tapperplate", Tapperplate.options[Tapperplate.value].text);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_105 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_106 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_107 = L_106->___Tapperplate_16;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_108;
				L_108 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_107, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_109 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_110 = L_109->___Tapperplate_16;
				int32_t L_111;
				L_111 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_110, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_112;
				L_112 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_108, L_111, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_113;
				L_113 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_112, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_105, _stringLiteral8295617198EB635126DF85EA15FD35F7326C2844, L_113, NULL);
				// form.AddField("Engranajes", Engranajes.options[Engranajes.value].text);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_114 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_115 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_116 = L_115->___Engranajes_17;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_117;
				L_117 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_116, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_118 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_119 = L_118->___Engranajes_17;
				int32_t L_120;
				L_120 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_119, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_121;
				L_121 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_117, L_120, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_122;
				L_122 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_121, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_114, _stringLiteralA4462CA9ADC2AD6448A6395345473E79E165913C, L_122, NULL);
				// form.AddField("Motor", Motor.options[Motor.value].text);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_123 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_124 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_125 = L_124->___Motor_18;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_126;
				L_126 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_125, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_127 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_128 = L_127->___Motor_18;
				int32_t L_129;
				L_129 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_128, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_130;
				L_130 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_126, L_129, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_131;
				L_131 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_130, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_123, _stringLiteralA8F244FC674653C59552E40566FE156BB9BB79FD, L_131, NULL);
				// form.AddField(
				//     "Gatilloelectronico",
				//     Gatilloelectronico.options[Gatilloelectronico.value].text
				// );
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_132 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_133 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_134 = L_133->___Gatilloelectronico_19;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_135;
				L_135 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_134, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_136 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_137 = L_136->___Gatilloelectronico_19;
				int32_t L_138;
				L_138 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_137, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_139;
				L_139 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_135, L_138, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_140;
				L_140 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_139, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_132, _stringLiteral45D2EE73A515C8F1D44B7D03ED19C8403309222B, L_140, NULL);
				// form.AddField("Mosfet", Mosfet.options[Mosfet.value].text);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_141 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_142 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_143 = L_142->___Mosfet_20;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_144;
				L_144 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_143, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_145 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_146 = L_145->___Mosfet_20;
				int32_t L_147;
				L_147 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_146, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_148;
				L_148 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_144, L_147, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_149;
				L_149 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_148, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_141, _stringLiteral77433E583240E26992DA7E6DFAA703B2EED57EE4, L_149, NULL);
				// form.AddField("Nozzle", Nozzle.options[Nozzle.value].text);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_150 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_151 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_152 = L_151->___Nozzle_15;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_153;
				L_153 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_152, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_154 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_155 = L_154->___Nozzle_15;
				int32_t L_156;
				L_156 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_155, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_157;
				L_157 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_153, L_156, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_158;
				L_158 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_157, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_150, _stringLiteral6B17125BFE29CB975A7E9D3A67FC7104EC9AB7C3, L_158, NULL);
				// form.AddField("Gearbox", Gearbox.options[Gearbox.value].text);
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_159 = V_4;
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_160 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_161 = L_160->___Gearbox_4;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_162;
				L_162 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_161, NULL);
				saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182* L_163 = V_2;
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_164 = L_163->___Gearbox_4;
				int32_t L_165;
				L_165 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_164, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_166;
				L_166 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_162, L_165, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_167;
				L_167 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_166, NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_159, _stringLiteralDF510ED9801CFCE3E869CBFF940B43857E871D99, L_167, NULL);
				// form.AddField("alcance", alcance.ToString());
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_168 = V_4;
				String_t* L_169;
				L_169 = Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339((&((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___alcance_21), NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_168, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, L_169, NULL);
				// form.AddField("potencia", potencia.ToString());
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_170 = V_4;
				String_t* L_171;
				L_171 = Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339((&((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___potencia_22), NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_170, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, L_171, NULL);
				// form.AddField("precision", precision.ToString());
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_172 = V_4;
				String_t* L_173;
				L_173 = Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339((&((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___precision_23), NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_172, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, L_173, NULL);
				// form.AddField("cadencia", cadencia.ToString());
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_174 = V_4;
				String_t* L_175;
				L_175 = Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339((&((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___cadencia_24), NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_174, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, L_175, NULL);
				// form.AddField("varianza", varianza.ToString());
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_176 = V_4;
				String_t* L_177;
				L_177 = Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339((&((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___varianza_25), NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_176, _stringLiteral46D28FF30C29C530049AA60FB209FE1D69FF6472, L_177, NULL);
				// form.AddField("precio", precio.ToString());
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_178 = V_4;
				String_t* L_179;
				L_179 = Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339((&((saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_StaticFields*)il2cpp_codegen_static_fields_for(saveConfig_tA38BE8E40121F2B9BC88EC5855ED1B8DE4C39182_il2cpp_TypeInfo_var))->___precio_26), NULL);
				WWWForm_AddField_m6315BDFDEA6F0F57ABCBCF4B1AE482E1893BEB8D(L_178, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, L_179, NULL);
				// using (UnityWebRequest www = UnityWebRequest.Post(url, form))
				String_t* L_180 = V_3;
				WWWForm_t0DAE123504AB1B2BC17C18714741B92AB3B3B045* L_181 = V_4;
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_182;
				L_182 = UnityWebRequest_Post_m1F6BF341F959AC95110C738791EF920713B74786(L_180, L_181, NULL);
				__this->___U3CwwwU3E5__2_3 = L_182;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CwwwU3E5__2_3), (void*)L_182);
				__this->___U3CU3E1__state_0 = ((int32_t)-3);
				// yield return www.SendWebRequest();
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_183 = __this->___U3CwwwU3E5__2_3;
				UnityWebRequestAsyncOperation_t14BE94558FF3A2CFC2EFBE2511A3A88252042B8C* L_184;
				L_184 = UnityWebRequest_SendWebRequest_mA3CD13983BAA5074A0640EDD661B1E46E6DB6C13(L_183, NULL);
				__this->___U3CU3E2__current_1 = L_184;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CU3E2__current_1), (void*)L_184);
				__this->___U3CU3E1__state_0 = 1;
				V_0 = (bool)1;
				goto IL_04bd;
			}

IL_0425_1:
			{
				__this->___U3CU3E1__state_0 = ((int32_t)-3);
				// if (www.isNetworkError || www.isHttpError)
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_185 = __this->___U3CwwwU3E5__2_3;
				bool L_186;
				L_186 = UnityWebRequest_get_isNetworkError_m036684411466688E71E67CDD3703BAC9035A56F0(L_185, NULL);
				if (L_186)
				{
					goto IL_0447_1;
				}
			}
			{
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_187 = __this->___U3CwwwU3E5__2_3;
				bool L_188;
				L_188 = UnityWebRequest_get_isHttpError_m945BA480A179E05CC9659846414D9521ED648ED5(L_187, NULL);
				if (!L_188)
				{
					goto IL_0459_1;
				}
			}

IL_0447_1:
			{
				// Debug.Log(www.error);
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_189 = __this->___U3CwwwU3E5__2_3;
				String_t* L_190;
				L_190 = UnityWebRequest_get_error_m20A5D813ED59118B7AA1D1E2EB5250178B1F5B6F(L_189, NULL);
				il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
				Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_190, NULL);
				goto IL_046e_1;
			}

IL_0459_1:
			{
				// Debug.Log(www.downloadHandler.text);
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_191 = __this->___U3CwwwU3E5__2_3;
				DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB* L_192;
				L_192 = UnityWebRequest_get_downloadHandler_m1AA91B23D9D594A4F4FE2975FC356C508528F1D5(L_191, NULL);
				String_t* L_193;
				L_193 = DownloadHandler_get_text_mA6DE5CB2647A21E577B963708DC3D0DA4DBFE7D8(L_192, NULL);
				il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
				Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_193, NULL);
			}

IL_046e_1:
			{
				// if (www.downloadHandler.text == "0")
				UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_194 = __this->___U3CwwwU3E5__2_3;
				DownloadHandler_t1B56C7D3F65D97A1E4B566A14A1E783EA8AE4EBB* L_195;
				L_195 = UnityWebRequest_get_downloadHandler_m1AA91B23D9D594A4F4FE2975FC356C508528F1D5(L_194, NULL);
				String_t* L_196;
				L_196 = DownloadHandler_get_text_mA6DE5CB2647A21E577B963708DC3D0DA4DBFE7D8(L_195, NULL);
				bool L_197;
				L_197 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_196, _stringLiteralF944DCD635F9801F7AC90A407FBC479964DEC024, NULL);
				if (!L_197)
				{
					goto IL_049b_1;
				}
			}
			{
				// Debug.Log("User created succesfully");
				il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
				Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(_stringLiteralF224AEDDC1FF132CAD6D558128124DB5A3462591, NULL);
				// bien();
				saveConfig_bien_mC22BBB0E80B8D093F33CD35074F571D5FD08F959(NULL);
				goto IL_04a5_1;
			}

IL_049b_1:
			{
				// Debug.Log("error");
				il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
				Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(_stringLiteralFCA74A9060DDEE6A3ECAE73E6AB96086770BE7E2, NULL);
			}

IL_04a5_1:
			{
				// }
				U3CSaveU3Ed__31_U3CU3Em__Finally1_m8D488E2CE71CF9ED53EF23A773106D0607794BC2(__this, NULL);
				__this->___U3CwwwU3E5__2_3 = (UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F*)NULL;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___U3CwwwU3E5__2_3), (void*)(UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F*)NULL);
			}

IL_04b2_1:
			{
				// }
				V_0 = (bool)0;
				goto IL_04bd;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_04bd:
	{
		bool L_198 = V_0;
		return L_198;
	}
}
// System.Void saveConfig/<Save>d__31::<>m__Finally1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSaveU3Ed__31_U3CU3Em__Finally1_m8D488E2CE71CF9ED53EF23A773106D0607794BC2 (U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->___U3CU3E1__state_0 = (-1);
		UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_0 = __this->___U3CwwwU3E5__2_3;
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		UnityWebRequest_t6233B8E22992FC2364A831C1ACB033EF3260C39F* L_1 = __this->___U3CwwwU3E5__2_3;
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t030E0496B4E0E4E4F086825007979AF51F7248C5_il2cpp_TypeInfo_var, L_1);
	}

IL_001a:
	{
		return;
	}
}
// System.Object saveConfig/<Save>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CSaveU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1360F69CE79A86D420CCBD533CB3B2F05A6C69E8 (U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
// System.Void saveConfig/<Save>d__31::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CSaveU3Ed__31_System_Collections_IEnumerator_Reset_m762323724A5978C177AAFDD6849978B9C8C45A7A (U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE* __this, const RuntimeMethod* method) 
{
	{
		NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A* L_0 = (NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A*)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_t1429765983D409BD2986508963C98D214E4EBF4A_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m1398D0CDE19B36AA3DE9392879738C1EA2439CDF(L_0, NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CSaveU3Ed__31_System_Collections_IEnumerator_Reset_m762323724A5978C177AAFDD6849978B9C8C45A7A_RuntimeMethod_var)));
	}
}
// System.Object saveConfig/<Save>d__31::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* U3CSaveU3Ed__31_System_Collections_IEnumerator_get_Current_m89C4E81C9291F9AD9C60D6627B1AD32B88990F03 (U3CSaveU3Ed__31_t9A37EA6841E287F8F118268065E0127D185B98EE* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = __this->___U3CU3E2__current_1;
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Window_Graph::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_Awake_m9FEFBB9FACB351784E3082313B30625836DDEDD4 (Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m0640480E7E38BB88B0D1F6AD59E697C8EE6AAFA4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m17F501B5A5C289ECE1B4F3D6EBF05DFA421433F8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3601F196C4AB858E4A861453B96D8654ADABCA61);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral68806558F5B120B9ACE1D4125D1F2D647B3247BD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral70373B1D3A7807A80812C066F0E6FC704D52FEE0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9FD570D72FF034C02743D728F127044ED2FB9902);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD3A337ACA27A85E377BF1077DECC29723A71277B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDDAD6B1F832793EAB53E91D2DE1597B9E3B68D9C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF9B72C5E29045728775818D4BD0FB4CE67687B4D);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* V_0 = NULL;
	{
		// potencia = 332;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___potencia_8 = (332.0);
		// cadencia = 16;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___cadencia_9 = (16.0);
		// alcance = 45;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___alcance_10 = (45.0);
		// precision = 8;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precision_11 = (8.0);
		// varianza = 37;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___varianza_12 = (37.0);
		// graphContainer = transform.Find("graphContainer").GetComponent<RectTransform>();
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_1;
		L_1 = Transform_Find_m3087032B0E1C5B96A2D2C27020BAEAE2DA08F932(L_0, _stringLiteralD3A337ACA27A85E377BF1077DECC29723A71277B, NULL);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_2;
		L_2 = Component_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m0640480E7E38BB88B0D1F6AD59E697C8EE6AAFA4(L_1, Component_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m0640480E7E38BB88B0D1F6AD59E697C8EE6AAFA4_RuntimeMethod_var);
		__this->___graphContainer_5 = L_2;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___graphContainer_5), (void*)L_2);
		// labelTemplateY = graphContainer.Find("labelTemplateY").GetComponent<RectTransform>();
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_3 = __this->___graphContainer_5;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_4;
		L_4 = Transform_Find_m3087032B0E1C5B96A2D2C27020BAEAE2DA08F932(L_3, _stringLiteralF9B72C5E29045728775818D4BD0FB4CE67687B4D, NULL);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_5;
		L_5 = Component_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m0640480E7E38BB88B0D1F6AD59E697C8EE6AAFA4(L_4, Component_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m0640480E7E38BB88B0D1F6AD59E697C8EE6AAFA4_RuntimeMethod_var);
		__this->___labelTemplateY_7 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___labelTemplateY_7), (void*)L_5);
		// txtPotencia = GameObject.Find("txtPotencia").GetComponent<Text>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_6;
		L_6 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral70373B1D3A7807A80812C066F0E6FC704D52FEE0, NULL);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_7;
		L_7 = GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F(L_6, GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F_RuntimeMethod_var);
		__this->___txtPotencia_20 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___txtPotencia_20), (void*)L_7);
		// txtPrecio = GameObject.Find("txtPrecio").GetComponent<Text>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_8;
		L_8 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral9FD570D72FF034C02743D728F127044ED2FB9902, NULL);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_9;
		L_9 = GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F(L_8, GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F_RuntimeMethod_var);
		__this->___txtPrecio_21 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___txtPrecio_21), (void*)L_9);
		// txtExtra = GameObject.Find("txtExtras").GetComponent<Text>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_10;
		L_10 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral68806558F5B120B9ACE1D4125D1F2D647B3247BD, NULL);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_11;
		L_11 = GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F(L_10, GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F_RuntimeMethod_var);
		__this->___txtExtra_22 = L_11;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___txtExtra_22), (void*)L_11);
		// txtMaxPre = GameObject.Find("txtMaxPre").GetComponent<Text>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12;
		L_12 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralDDAD6B1F832793EAB53E91D2DE1597B9E3B68D9C, NULL);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_13;
		L_13 = GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F(L_12, GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F_RuntimeMethod_var);
		__this->___txtMaxPre_23 = L_13;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___txtMaxPre_23), (void*)L_13);
		// txtMaxAlc = GameObject.Find("txtMaxAlc").GetComponent<Text>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_14;
		L_14 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral3601F196C4AB858E4A861453B96D8654ADABCA61, NULL);
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_15;
		L_15 = GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F(L_14, GameObject_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mBE6B722369FF149589D3D42A6A8435A9C5045B3F_RuntimeMethod_var);
		__this->___txtMaxAlc_24 = L_15;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___txtMaxAlc_24), (void*)L_15);
		// List<int> valueList = new List<int>() { 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60 };
		List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* L_16 = (List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73*)il2cpp_codegen_object_new(List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73_il2cpp_TypeInfo_var);
		List_1__ctor_m17F501B5A5C289ECE1B4F3D6EBF05DFA421433F8(L_16, List_1__ctor_m17F501B5A5C289ECE1B4F3D6EBF05DFA421433F8_RuntimeMethod_var);
		List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* L_17 = L_16;
		List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_inline(L_17, 5, List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_RuntimeMethod_var);
		List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* L_18 = L_17;
		List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_inline(L_18, ((int32_t)10), List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_RuntimeMethod_var);
		List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* L_19 = L_18;
		List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_inline(L_19, ((int32_t)15), List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_RuntimeMethod_var);
		List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* L_20 = L_19;
		List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_inline(L_20, ((int32_t)20), List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_RuntimeMethod_var);
		List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* L_21 = L_20;
		List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_inline(L_21, ((int32_t)25), List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_RuntimeMethod_var);
		List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* L_22 = L_21;
		List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_inline(L_22, ((int32_t)30), List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_RuntimeMethod_var);
		List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* L_23 = L_22;
		List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_inline(L_23, ((int32_t)35), List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_RuntimeMethod_var);
		List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* L_24 = L_23;
		List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_inline(L_24, ((int32_t)40), List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_RuntimeMethod_var);
		List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* L_25 = L_24;
		List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_inline(L_25, ((int32_t)45), List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_RuntimeMethod_var);
		List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* L_26 = L_25;
		List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_inline(L_26, ((int32_t)50), List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_RuntimeMethod_var);
		List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* L_27 = L_26;
		List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_inline(L_27, ((int32_t)55), List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_RuntimeMethod_var);
		List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* L_28 = L_27;
		List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_inline(L_28, ((int32_t)60), List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_RuntimeMethod_var);
		V_0 = L_28;
		// ShowGraph(valueList);
		List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* L_29 = V_0;
		Window_Graph_ShowGraph_m5FF8330468B0C90C3A7654265E4D07593E1C89EA(__this, L_29, NULL);
		// }
		return;
	}
}
// System.Void Window_Graph::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_Update_m64029026231024A2C6A96C96BAF6C822776F3F7E (Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral073287A537BF6B0F7150451A41FBE4918FE5A158);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral17C1421BAF12D0423E2C3BAAA8B016D369DFEB8F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral538AF8C1BD2E67A15B89C54853ED84EBB7533E92);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5CDE804554EB9DE3ACD4D0B597C49D0AE3CB9E3F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral751C92708E570ED82DEC3536E3F4B06F89682D49);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral98EEF9DC7E3239D315D4315BA2DB13416120A3C9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA60A1D78963924FF030CFB8157ACEA474FF84DB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralEB0BB5D4FA59FF963FDE2494755AB4182D0F8AA2);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(precio < 0 ){
		double L_0 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precio_13;
		if ((!(((double)L_0) < ((double)(0.0)))))
		{
			goto IL_001e;
		}
	}
	{
		// precio = 0;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precio_13 = (0.0);
	}

IL_001e:
	{
		// txtPrecio.text = "PVP TOTAL RECOMENDADO: " + precio.ToString() + " EUROS";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_1 = __this->___txtPrecio_21;
		String_t* L_2;
		L_2 = Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339((&((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precio_13), NULL);
		String_t* L_3;
		L_3 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(_stringLiteral073287A537BF6B0F7150451A41FBE4918FE5A158, L_2, _stringLiteral538AF8C1BD2E67A15B89C54853ED84EBB7533E92, NULL);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_1, L_3);
		// txtExtra.text = "EXTRA: " + extra;
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_4 = __this->___txtExtra_22;
		String_t* L_5 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___extra_14;
		String_t* L_6;
		L_6 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteral17C1421BAF12D0423E2C3BAAA8B016D369DFEB8F, L_5, NULL);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, L_6);
		// txtPotencia.text = "FPS: " + potencia;
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_7 = __this->___txtPotencia_20;
		String_t* L_8;
		L_8 = Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339((&((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___potencia_8), NULL);
		String_t* L_9;
		L_9 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteralEB0BB5D4FA59FF963FDE2494755AB4182D0F8AA2, L_8, NULL);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, L_9);
		// if(precision>55){
		double L_10 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precision_11;
		if ((!(((double)L_10) > ((double)(55.0)))))
		{
			goto IL_00c3;
		}
	}
	{
		// ChangeBar(new Vector2(50, (float)(57 * 6)+5), 25, precisionBar, "precisionBar");
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_11;
		memset((&L_11), 0, sizeof(L_11));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_11), (50.0f), (347.0f), /*hidden argument*/NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12 = __this->___precisionBar_18;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_13;
		L_13 = Window_Graph_ChangeBar_mA32AD57472431EE83767EC57C703484118F93468(__this, L_11, (25.0f), L_12, _stringLiteral751C92708E570ED82DEC3536E3F4B06F89682D49, NULL);
		// txtMaxPre.text = "MAX";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_14 = __this->___txtMaxPre_23;
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_14, _stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50);
		goto IL_010a;
	}

IL_00c3:
	{
		// ChangeBar(new Vector2(50, (float)(precision * 6)+5), 25, precisionBar, "precisionBar");
		double L_15 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precision_11;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_16;
		memset((&L_16), 0, sizeof(L_16));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_16), (50.0f), ((float)il2cpp_codegen_add(((float)((double)il2cpp_codegen_multiply(L_15, (6.0)))), (5.0f))), /*hidden argument*/NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_17 = __this->___precisionBar_18;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_18;
		L_18 = Window_Graph_ChangeBar_mA32AD57472431EE83767EC57C703484118F93468(__this, L_16, (25.0f), L_17, _stringLiteral751C92708E570ED82DEC3536E3F4B06F89682D49, NULL);
		// txtMaxPre.text = "";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_19 = __this->___txtMaxPre_23;
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_19, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
	}

IL_010a:
	{
		// if(alcance>=55){
		double L_20 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___alcance_10;
		if ((!(((double)L_20) >= ((double)(55.0)))))
		{
			goto IL_0152;
		}
	}
	{
		// ChangeBar(new Vector2(120, (float)(57 * 6)+5), 25, alcanceBar, "alcanceBar");
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_21;
		memset((&L_21), 0, sizeof(L_21));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_21), (120.0f), (347.0f), /*hidden argument*/NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_22 = __this->___alcanceBar_17;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_23;
		L_23 = Window_Graph_ChangeBar_mA32AD57472431EE83767EC57C703484118F93468(__this, L_21, (25.0f), L_22, _stringLiteral5CDE804554EB9DE3ACD4D0B597C49D0AE3CB9E3F, NULL);
		// txtMaxAlc.text = "MAX";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_24 = __this->___txtMaxAlc_24;
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_24, _stringLiteralE3730FAB74F10FB4D596B408FFAA85142E1A2E50);
		goto IL_0199;
	}

IL_0152:
	{
		// ChangeBar(new Vector2(120, (float)(alcance * 6)+5), 25, alcanceBar, "alcanceBar");
		double L_25 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___alcance_10;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_26;
		memset((&L_26), 0, sizeof(L_26));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_26), (120.0f), ((float)il2cpp_codegen_add(((float)((double)il2cpp_codegen_multiply(L_25, (6.0)))), (5.0f))), /*hidden argument*/NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_27 = __this->___alcanceBar_17;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_28;
		L_28 = Window_Graph_ChangeBar_mA32AD57472431EE83767EC57C703484118F93468(__this, L_26, (25.0f), L_27, _stringLiteral5CDE804554EB9DE3ACD4D0B597C49D0AE3CB9E3F, NULL);
		// txtMaxAlc.text = "";
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_29 = __this->___txtMaxAlc_24;
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_29, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
	}

IL_0199:
	{
		// ChangeBar(new Vector2(195, (float)(cadencia * 6)+5), 25, cadenciaBar, "cadenciaBar");
		double L_30 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___cadencia_9;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_31;
		memset((&L_31), 0, sizeof(L_31));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_31), (195.0f), ((float)il2cpp_codegen_add(((float)((double)il2cpp_codegen_multiply(L_30, (6.0)))), (5.0f))), /*hidden argument*/NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_32 = __this->___cadenciaBar_16;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_33;
		L_33 = Window_Graph_ChangeBar_mA32AD57472431EE83767EC57C703484118F93468(__this, L_31, (25.0f), L_32, _stringLiteral98EEF9DC7E3239D315D4315BA2DB13416120A3C9, NULL);
		// ChangeBar(new Vector2(275, (float)(varianza * 6)+5), 25, varianzaBar, "varianzaBar");
		double L_34 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___varianza_12;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_35;
		memset((&L_35), 0, sizeof(L_35));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_35), (275.0f), ((float)il2cpp_codegen_add(((float)((double)il2cpp_codegen_multiply(L_34, (6.0)))), (5.0f))), /*hidden argument*/NULL);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_36 = __this->___varianzaBar_19;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_37;
		L_37 = Window_Graph_ChangeBar_mA32AD57472431EE83767EC57C703484118F93468(__this, L_35, (25.0f), L_36, _stringLiteralA60A1D78963924FF030CFB8157ACEA474FF84DB7, NULL);
		// HacerCaptura.setStats(alcance, potencia, precision, cadencia, varianza, precio);
		double L_38 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___alcance_10;
		double L_39 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___potencia_8;
		double L_40 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precision_11;
		double L_41 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___cadencia_9;
		double L_42 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___varianza_12;
		double L_43 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precio_13;
		HacerCaptura_setStats_mEAAECBD9E5319611DBF780127D5CB6B0DA5CAC79(L_38, L_39, L_40, L_41, L_42, L_43, NULL);
		// saveConfig.setStats(alcance, potencia, precision, cadencia, varianza, precio);
		double L_44 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___alcance_10;
		double L_45 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___potencia_8;
		double L_46 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precision_11;
		double L_47 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___cadencia_9;
		double L_48 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___varianza_12;
		double L_49 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precio_13;
		saveConfig_setStats_m11B648967ECBEA5D7FF3E0A9A5B260CCFC953A26(L_44, L_45, L_46, L_47, L_48, L_49, NULL);
		// }
		return;
	}
}
// System.Void Window_Graph::ShowGraph(System.Collections.Generic.List`1<System.Int32>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_ShowGraph_m5FF8330468B0C90C3A7654265E4D07593E1C89EA (Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8* __this, List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* ___valueList0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mB85C5C0EEF6535E3FC0DBFC14E39FA5A51B6F888_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mD99081BEFA1AB3526715F489192B0F7F596C183D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m432798BF76671A2FB88A9BF403D2F706ADA37236_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	{
		// float graphHeight = graphContainer.sizeDelta.y + 30f;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_0 = __this->___graphContainer_5;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_1;
		L_1 = RectTransform_get_sizeDelta_m822A8493F2035677384F1540A2E9E5ACE63010BB(L_0, NULL);
		float L_2 = L_1.___y_1;
		V_0 = ((float)il2cpp_codegen_add(L_2, (30.0f)));
		// float yMaximum = 60f;
		V_1 = (60.0f);
		// float xSize = 50f;
		V_2 = (50.0f);
		// int separatorCount = 12;
		V_3 = ((int32_t)12);
		// for (int i = 0; i < separatorCount; i++)
		V_4 = 0;
		goto IL_009e;
	}

IL_002b:
	{
		// float yPosition = (valueList[i] / yMaximum) * graphHeight;
		List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* L_3 = ___valueList0;
		int32_t L_4 = V_4;
		int32_t L_5;
		L_5 = List_1_get_Item_mD99081BEFA1AB3526715F489192B0F7F596C183D(L_3, L_4, List_1_get_Item_mD99081BEFA1AB3526715F489192B0F7F596C183D_RuntimeMethod_var);
		float L_6 = V_1;
		// RectTransform labelY = Instantiate(labelTemplateY);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_7 = __this->___labelTemplateY_7;
		il2cpp_codegen_runtime_class_init_inline(Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_il2cpp_TypeInfo_var);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_8;
		L_8 = Object_Instantiate_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m432798BF76671A2FB88A9BF403D2F706ADA37236(L_7, Object_Instantiate_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m432798BF76671A2FB88A9BF403D2F706ADA37236_RuntimeMethod_var);
		// labelY.SetParent(graphContainer);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_9 = L_8;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_10 = __this->___graphContainer_5;
		Transform_SetParent_m6677538B60246D958DD91F931C50F969CCBB5250(L_9, L_10, NULL);
		// labelY.gameObject.SetActive(true);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_11 = L_9;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_12;
		L_12 = Component_get_gameObject_m57AEFBB14DB39EC476F740BA000E170355DE691B(L_11, NULL);
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_12, (bool)1, NULL);
		// float normalizedValue = i * 1f / separatorCount;
		int32_t L_13 = V_4;
		int32_t L_14 = V_3;
		V_5 = ((float)(((float)il2cpp_codegen_multiply(((float)L_13), (1.0f)))/((float)L_14)));
		// labelY.anchoredPosition = new Vector2(-15f, normalizedValue * (graphHeight));
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_15 = L_11;
		float L_16 = V_5;
		float L_17 = V_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_18;
		memset((&L_18), 0, sizeof(L_18));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_18), (-15.0f), ((float)il2cpp_codegen_multiply(L_16, L_17)), /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_mF903ACE04F6959B1CD67E2B94FABC0263068F965(L_15, L_18, NULL);
		// labelY.GetComponent<Text>().text = Mathf.RoundToInt(normalizedValue * yMaximum).ToString();
		Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* L_19;
		L_19 = Component_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mB85C5C0EEF6535E3FC0DBFC14E39FA5A51B6F888(L_15, Component_GetComponent_TisText_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62_mB85C5C0EEF6535E3FC0DBFC14E39FA5A51B6F888_RuntimeMethod_var);
		float L_20 = V_5;
		float L_21 = V_1;
		int32_t L_22;
		L_22 = Mathf_RoundToInt_m6A6E30BA4157D69DA47F02B43108882DDD7C4A70_inline(((float)il2cpp_codegen_multiply(L_20, L_21)), NULL);
		V_6 = L_22;
		String_t* L_23;
		L_23 = Int32_ToString_m030E01C24E294D6762FB0B6F37CB541581F55CA5((&V_6), NULL);
		VirtualActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_19, L_23);
		// for (int i = 0; i < separatorCount; i++)
		int32_t L_24 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add(L_24, 1));
	}

IL_009e:
	{
		// for (int i = 0; i < separatorCount; i++)
		int32_t L_25 = V_4;
		int32_t L_26 = V_3;
		if ((((int32_t)L_25) < ((int32_t)L_26)))
		{
			goto IL_002b;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Window_Graph::setPrecision(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6 (double ___precisionEnviada0, double ___precisionRestar1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// precision = precision + precisionEnviada;
		double L_0 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precision_11;
		double L_1 = ___precisionEnviada0;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precision_11 = ((double)il2cpp_codegen_add(L_0, L_1));
		// precision = precision - precisionRestar;
		double L_2 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precision_11;
		double L_3 = ___precisionRestar1;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precision_11 = ((double)il2cpp_codegen_subtract(L_2, L_3));
		// }
		return;
	}
}
// System.Void Window_Graph::setPotencia(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E (double ___potenciaEnviada0, double ___potenciaRestar1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// potencia = potencia + potenciaEnviada;
		double L_0 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___potencia_8;
		double L_1 = ___potenciaEnviada0;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___potencia_8 = ((double)il2cpp_codegen_add(L_0, L_1));
		// potencia = potencia - potenciaRestar;
		double L_2 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___potencia_8;
		double L_3 = ___potenciaRestar1;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___potencia_8 = ((double)il2cpp_codegen_subtract(L_2, L_3));
		// }
		return;
	}
}
// System.Void Window_Graph::setAlcance(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2 (double ___alcanceEnviada0, double ___alcanceRestar1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// alcance = alcance + alcanceEnviada;
		double L_0 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___alcance_10;
		double L_1 = ___alcanceEnviada0;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___alcance_10 = ((double)il2cpp_codegen_add(L_0, L_1));
		// alcance = alcance - alcanceRestar;
		double L_2 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___alcance_10;
		double L_3 = ___alcanceRestar1;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___alcance_10 = ((double)il2cpp_codegen_subtract(L_2, L_3));
		// }
		return;
	}
}
// System.Void Window_Graph::setCadencia(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142 (double ___cadenciaEnviada0, double ___cadenciaRestar1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// cadencia = cadencia + cadenciaEnviada;
		double L_0 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___cadencia_9;
		double L_1 = ___cadenciaEnviada0;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___cadencia_9 = ((double)il2cpp_codegen_add(L_0, L_1));
		// cadencia = cadencia - cadenciaRestar;
		double L_2 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___cadencia_9;
		double L_3 = ___cadenciaRestar1;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___cadencia_9 = ((double)il2cpp_codegen_subtract(L_2, L_3));
		// }
		return;
	}
}
// System.Void Window_Graph::setVarianza(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8 (float ___varianzaEnviada0, float ___varianzaRestar1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// varianza = varianza + (varianzaEnviada / 100);
		double L_0 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___varianza_12;
		float L_1 = ___varianzaEnviada0;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___varianza_12 = ((double)il2cpp_codegen_add(L_0, ((double)((float)(L_1/(100.0f))))));
		// varianza = varianza - (varianzaRestar / 100);
		double L_2 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___varianza_12;
		float L_3 = ___varianzaRestar1;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___varianza_12 = ((double)il2cpp_codegen_subtract(L_2, ((double)((float)(L_3/(100.0f))))));
		// }
		return;
	}
}
// System.Void Window_Graph::setPrecio(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2 (double ___precioEnviada0, double ___precioRestar1, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// precio = precio + precioEnviada;
		double L_0 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precio_13;
		double L_1 = ___precioEnviada0;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precio_13 = ((double)il2cpp_codegen_add(L_0, L_1));
		// precio = precio - precioRestar;
		double L_2 = ((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precio_13;
		double L_3 = ___precioRestar1;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___precio_13 = ((double)il2cpp_codegen_subtract(L_2, L_3));
		// }
		return;
	}
}
// System.Void Window_Graph::setExtra(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_setExtra_m8774145D45E480AAFA50F295A5FB4E00A289FF91 (String_t* ___extraEnviada0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// extra = extraEnviada;
		String_t* L_0 = ___extraEnviada0;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___extra_14 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___extra_14), (void*)L_0);
		// }
		return;
	}
}
// UnityEngine.GameObject Window_Graph::ChangeBar(UnityEngine.Vector2,System.Single,UnityEngine.GameObject,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* Window_Graph_ChangeBar_mA32AD57472431EE83767EC57C703484118F93468 (Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8* __this, Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 ___graphPosition0, float ___barWidth1, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___gameObject2, String_t* ___name3, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m1592DCB5AA07291F73A76006F0913A64DFB8A9C4_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// gameObject = GameObject.Find(name);
		String_t* L_0 = ___name3;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(L_0, NULL);
		___gameObject2 = L_1;
		// gameObject.transform.SetParent(graphContainer, false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = ___gameObject2;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_3;
		L_3 = GameObject_get_transform_m0BC10ADFA1632166AE5544BDF9038A2650C2AE56(L_2, NULL);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_4 = __this->___graphContainer_5;
		Transform_SetParent_m9BDD7B7476714B2D7919B10BDC22CE75C0A0A195(L_3, L_4, (bool)0, NULL);
		// RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_5 = ___gameObject2;
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_6;
		L_6 = GameObject_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m1592DCB5AA07291F73A76006F0913A64DFB8A9C4(L_5, GameObject_GetComponent_TisRectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5_m1592DCB5AA07291F73A76006F0913A64DFB8A9C4_RuntimeMethod_var);
		// rectTransform.anchoredPosition = new Vector2(graphPosition.x, 0f);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_7 = L_6;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_8 = ___graphPosition0;
		float L_9 = L_8.___x_0;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_10;
		memset((&L_10), 0, sizeof(L_10));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_10), L_9, (0.0f), /*hidden argument*/NULL);
		RectTransform_set_anchoredPosition_mF903ACE04F6959B1CD67E2B94FABC0263068F965(L_7, L_10, NULL);
		// rectTransform.sizeDelta = new Vector2(barWidth, graphPosition.y);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_11 = L_7;
		float L_12 = ___barWidth1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_13 = ___graphPosition0;
		float L_14 = L_13.___y_1;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_15), L_12, L_14, /*hidden argument*/NULL);
		RectTransform_set_sizeDelta_mC9A980EA6036E6725EF24CEDF3EE80A9B2B50EE5(L_11, L_15, NULL);
		// rectTransform.anchorMin = new Vector2(0, 0);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_16 = L_11;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_17;
		memset((&L_17), 0, sizeof(L_17));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_17), (0.0f), (0.0f), /*hidden argument*/NULL);
		RectTransform_set_anchorMin_m931442ABE3368D6D4309F43DF1D64AB64B0F52E3(L_16, L_17, NULL);
		// rectTransform.anchorMax = new Vector2(0, 0);
		RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* L_18 = L_16;
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_19;
		memset((&L_19), 0, sizeof(L_19));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_19), (0.0f), (0.0f), /*hidden argument*/NULL);
		RectTransform_set_anchorMax_m52829ABEDD229ABD3DA20BCA676FA1DCA4A39B7D(L_18, L_19, NULL);
		// rectTransform.pivot = new Vector2(.5f, 0f);
		Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7 L_20;
		memset((&L_20), 0, sizeof(L_20));
		Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline((&L_20), (0.5f), (0.0f), /*hidden argument*/NULL);
		RectTransform_set_pivot_m79D0177D383D432A93C2615F1932B739B1C6E146(L_18, L_20, NULL);
		// return gameObject;
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_21 = ___gameObject2;
		return L_21;
	}
}
// System.Void Window_Graph::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph__ctor_m375DCEBA3CB3A5DC82DC1D7C65B46064456552B5 (Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void changeColor::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void changeColor_Start_m0DB7F7FE47459769FF91288ED9C298FA57E4A604 (changeColor_tD0F3C37D18D9ACF6C09D5A84FAB68A1AF7B4D81B* __this, const RuntimeMethod* method) 
{
	{
		// }
		return;
	}
}
// System.Void changeColor::SelectMaterial(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void changeColor_SelectMaterial_m919B3144A21AC86903CA16CF46A9E9701C3DA655 (changeColor_tD0F3C37D18D9ACF6C09D5A84FAB68A1AF7B4D81B* __this, int32_t ___i0, const RuntimeMethod* method) 
{
	{
		// selectedMaterial = colourableMaterials[i];
		MaterialU5BU5D_t2B1D11C42DB07A4400C0535F92DBB87A2E346D3D* L_0 = __this->___colourableMaterials_5;
		int32_t L_1 = ___i0;
		int32_t L_2 = L_1;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_3 = (L_0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_2));
		__this->___selectedMaterial_4 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___selectedMaterial_4), (void*)L_3);
		// }
		return;
	}
}
// System.Void changeColor::SetColor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void changeColor_SetColor_m0F758AB8D50A08344A7903511809C4091644A257 (changeColor_tD0F3C37D18D9ACF6C09D5A84FAB68A1AF7B4D81B* __this, int32_t ___i0, const RuntimeMethod* method) 
{
	{
		// selectedMaterial.color = colors[i];
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_0 = __this->___selectedMaterial_4;
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_1 = __this->___colors_6;
		int32_t L_2 = ___i0;
		int32_t L_3 = L_2;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_4 = (L_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3));
		Material_set_color_m5C32DEBB215FF9EE35E7B575297D8C2F29CC2A2D(L_0, L_4, NULL);
		// }
		return;
	}
}
// System.Void changeColor::onRed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void changeColor_onRed_mDFA04725FC176002465549F0C4C3FD65AFCA36EC (changeColor_tD0F3C37D18D9ACF6C09D5A84FAB68A1AF7B4D81B* __this, const RuntimeMethod* method) 
{
	{
		// SelectMaterial(0);
		changeColor_SelectMaterial_m919B3144A21AC86903CA16CF46A9E9701C3DA655(__this, 0, NULL);
		// SetColor(0);
		changeColor_SetColor_m0F758AB8D50A08344A7903511809C4091644A257(__this, 0, NULL);
		// }
		return;
	}
}
// System.Void changeColor::onBlue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void changeColor_onBlue_m3829CF17D7A43E66C568B02AE510BE65C685A74B (changeColor_tD0F3C37D18D9ACF6C09D5A84FAB68A1AF7B4D81B* __this, const RuntimeMethod* method) 
{
	{
		// SelectMaterial(0);
		changeColor_SelectMaterial_m919B3144A21AC86903CA16CF46A9E9701C3DA655(__this, 0, NULL);
		// SetColor(1);
		changeColor_SetColor_m0F758AB8D50A08344A7903511809C4091644A257(__this, 1, NULL);
		// }
		return;
	}
}
// System.Void changeColor::onGreen()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void changeColor_onGreen_mF3B33DFA50EB79B9C8E5F689253E146AD0D0C291 (changeColor_tD0F3C37D18D9ACF6C09D5A84FAB68A1AF7B4D81B* __this, const RuntimeMethod* method) 
{
	{
		// SelectMaterial(0);
		changeColor_SelectMaterial_m919B3144A21AC86903CA16CF46A9E9701C3DA655(__this, 0, NULL);
		// SetColor(2);
		changeColor_SetColor_m0F758AB8D50A08344A7903511809C4091644A257(__this, 2, NULL);
		// }
		return;
	}
}
// System.Void changeColor::onYellow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void changeColor_onYellow_m9EC07A9CE50641B58DB9A389871E1B0F902E8F1A (changeColor_tD0F3C37D18D9ACF6C09D5A84FAB68A1AF7B4D81B* __this, const RuntimeMethod* method) 
{
	{
		// SelectMaterial(0);
		changeColor_SelectMaterial_m919B3144A21AC86903CA16CF46A9E9701C3DA655(__this, 0, NULL);
		// SetColor(3);
		changeColor_SetColor_m0F758AB8D50A08344A7903511809C4091644A257(__this, 3, NULL);
		// }
		return;
	}
}
// System.Void changeColor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void changeColor__ctor_m31295285FCA0F687164DFB8BB421447E044BD1A9 (changeColor_tD0F3C37D18D9ACF6C09D5A84FAB68A1AF7B4D81B* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public Color[] colors = { Color.red, Color.blue, Color.green, Color.yellow };  //put colors here
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_0 = (ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389*)(ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389*)SZArrayNew(ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389_il2cpp_TypeInfo_var, (uint32_t)4);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_1 = L_0;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_2;
		L_2 = Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline(NULL);
		(L_1)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F)L_2);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_3 = L_1;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_4;
		L_4 = Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline(NULL);
		(L_3)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F)L_4);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_5 = L_3;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_6;
		L_6 = Color_get_green_m336EB73DD4A5B11B7F405CF4BC7F37A466FB4FF7_inline(NULL);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F)L_6);
		ColorU5BU5D_t612261CF293F6FFC3D80AB52259FF0DC2B2CC389* L_7 = L_5;
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_8;
		L_8 = Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline(NULL);
		(L_7)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F)L_8);
		__this->___colors_6 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___colors_6), (void*)L_7);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void lockRotation::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void lockRotation_Start_mF0C2F8F2B641BBB5EE782E8C7BA2DBB6DA32AF5B (lockRotation_tD8979E52803C5CEE4D8141EE52BA10B5627831AF* __this, const RuntimeMethod* method) 
{
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// void Start() { startRotation = transform.rotation.eulerAngles; }
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_1;
		L_1 = Transform_get_rotation_m32AF40CA0D50C797DA639A696F8EAEC7524C179C(L_0, NULL);
		V_0 = L_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Quaternion_get_eulerAngles_m2DB5158B5C3A71FD60FC8A6EE43D3AAA1CFED122_inline((&V_0), NULL);
		__this->___startRotation_7 = L_2;
		// void Start() { startRotation = transform.rotation.eulerAngles; }
		return;
	}
}
// System.Void lockRotation::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void lockRotation_LateUpdate_m193077AB70F7EEF1925BAB1AB166BCDB5A5CD38A (lockRotation_tD8979E52803C5CEE4D8141EE52BA10B5627831AF* __this, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* G_B2_0 = NULL;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* G_B3_1 = NULL;
	float G_B5_0 = 0.0f;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* G_B5_1 = NULL;
	float G_B4_0 = 0.0f;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* G_B4_1 = NULL;
	float G_B6_0 = 0.0f;
	float G_B6_1 = 0.0f;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* G_B6_2 = NULL;
	float G_B8_0 = 0.0f;
	float G_B8_1 = 0.0f;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* G_B8_2 = NULL;
	float G_B7_0 = 0.0f;
	float G_B7_1 = 0.0f;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* G_B7_2 = NULL;
	float G_B9_0 = 0.0f;
	float G_B9_1 = 0.0f;
	float G_B9_2 = 0.0f;
	Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* G_B9_3 = NULL;
	{
		// Vector3 newRotation = transform.rotation.eulerAngles;
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_0;
		L_0 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_1;
		L_1 = Transform_get_rotation_m32AF40CA0D50C797DA639A696F8EAEC7524C179C(L_0, NULL);
		V_1 = L_1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Quaternion_get_eulerAngles_m2DB5158B5C3A71FD60FC8A6EE43D3AAA1CFED122_inline((&V_1), NULL);
		V_0 = L_2;
		// transform.rotation = Quaternion.Euler(
		//     lockX ? startRotation.x : newRotation.x,
		//     lockY ? startRotation.y : newRotation.y,
		//     lockZ ? startRotation.z : newRotation.z
		// );
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_3;
		L_3 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		bool L_4 = __this->___lockX_4;
		G_B1_0 = L_3;
		if (L_4)
		{
			G_B2_0 = L_3;
			goto IL_002a;
		}
	}
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_5 = V_0;
		float L_6 = L_5.___x_2;
		G_B3_0 = L_6;
		G_B3_1 = G_B1_0;
		goto IL_0035;
	}

IL_002a:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_7 = (&__this->___startRotation_7);
		float L_8 = L_7->___x_2;
		G_B3_0 = L_8;
		G_B3_1 = G_B2_0;
	}

IL_0035:
	{
		bool L_9 = __this->___lockY_5;
		G_B4_0 = G_B3_0;
		G_B4_1 = G_B3_1;
		if (L_9)
		{
			G_B5_0 = G_B3_0;
			G_B5_1 = G_B3_1;
			goto IL_0045;
		}
	}
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		float L_11 = L_10.___y_3;
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_0050;
	}

IL_0045:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_12 = (&__this->___startRotation_7);
		float L_13 = L_12->___y_3;
		G_B6_0 = L_13;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_0050:
	{
		bool L_14 = __this->___lockZ_6;
		G_B7_0 = G_B6_0;
		G_B7_1 = G_B6_1;
		G_B7_2 = G_B6_2;
		if (L_14)
		{
			G_B8_0 = G_B6_0;
			G_B8_1 = G_B6_1;
			G_B8_2 = G_B6_2;
			goto IL_0060;
		}
	}
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_15 = V_0;
		float L_16 = L_15.___z_4;
		G_B9_0 = L_16;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		G_B9_3 = G_B7_2;
		goto IL_006b;
	}

IL_0060:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* L_17 = (&__this->___startRotation_7);
		float L_18 = L_17->___z_4;
		G_B9_0 = L_18;
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
		G_B9_3 = G_B8_2;
	}

IL_006b:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_19;
		L_19 = Quaternion_Euler_mD4601D966F1F58F3FCA01B3FC19A12D0AD0396DD_inline(G_B9_2, G_B9_1, G_B9_0, NULL);
		Transform_set_rotation_m61340DE74726CF0F9946743A727C4D444397331D(G_B9_3, L_19, NULL);
		// }
		return;
	}
}
// System.Void lockRotation::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void lockRotation__ctor_m1B543AE91695CA0F5ADEC761A3F018A5E5A0804D (lockRotation_tD8979E52803C5CEE4D8141EE52BA10B5627831AF* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void rotarObjeto::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void rotarObjeto_Update_m332CF26CB9F402A6275A75F54FDA19EDF2A1F94E (rotarObjeto_t91F85E1F039D0014B7DA0994A9C60CC5DB38CAAB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// if (Input.GetMouseButton(0))
		bool L_0;
		L_0 = Input_GetMouseButton_mE545CF4B790C6E202808B827E3141BEC3330DB70(0, NULL);
		if (!L_0)
		{
			goto IL_005d;
		}
	}
	{
		// float rotX = Input.GetAxis("Mouse X") * rotSpeed * Mathf.Deg2Rad;
		float L_1;
		L_1 = Input_GetAxis_m1F49B26F24032F45FB4583C95FB24E6771A161D4(_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7, NULL);
		float L_2 = __this->___rotSpeed_4;
		V_0 = ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_multiply(L_1, L_2)), (0.0174532924f)));
		// float rotY = Input.GetAxis("Mouse Y") * rotSpeed * Mathf.Deg2Rad;
		float L_3;
		L_3 = Input_GetAxis_m1F49B26F24032F45FB4583C95FB24E6771A161D4(_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0, NULL);
		float L_4 = __this->___rotSpeed_4;
		V_1 = ((float)il2cpp_codegen_multiply(((float)il2cpp_codegen_multiply(L_3, L_4)), (0.0174532924f)));
		// transform.Rotate(Vector3.up, -rotX, Space.Self);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_5;
		L_5 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6;
		L_6 = Vector3_get_up_mAB5269BFCBCB1BD241450C9BF2F156303D30E0C3_inline(NULL);
		float L_7 = V_0;
		Transform_Rotate_m683E67853797040312868B69E963D0E97F433EEB(L_5, L_6, ((-L_7)), 1, NULL);
		// transform.Rotate(Vector3.right, rotY, Space.Self);
		Transform_tB27202C6F4E36D225EE28A13E4D662BF99785DB1* L_8;
		L_8 = Component_get_transform_m2919A1D81931E6932C7F06D4C2F0AB8DDA9A5371(__this, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		L_9 = Vector3_get_right_m13B7C3EAA64DC921EC23346C56A5A597B5481FF5_inline(NULL);
		float L_10 = V_1;
		Transform_Rotate_m683E67853797040312868B69E963D0E97F433EEB(L_8, L_9, L_10, 1, NULL);
	}

IL_005d:
	{
		// }
		return;
	}
}
// System.Void rotarObjeto::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void rotarObjeto__ctor_mBFFACF614F3483B46FEB11FF2B88345888A0CF97 (rotarObjeto_t91F85E1F039D0014B7DA0994A9C60CC5DB38CAAB* __this, const RuntimeMethod* method) 
{
	{
		// float rotSpeed = 40;
		__this->___rotSpeed_4 = (40.0f);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* InputField_get_text_m6E0796350FF559505E4DF17311803962699D6704_inline (InputField_tABEA115F23FBD374EBE80D4FAC1D15BD6E37A140* __this, const RuntimeMethod* method) 
{
	{
		// return m_Text;
		String_t* L_0 = __this->___m_Text_40;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline (Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* __this, const RuntimeMethod* method) 
{
	{
		// return m_Value;
		int32_t L_0 = __this->___m_Value_25;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* __this, const RuntimeMethod* method) 
{
	{
		// public string text  { get { return m_Text; }  set { m_Text = value;  } }
		String_t* L_0 = __this->___m_Text_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9525B79969AFFE3254B303A40997A56DEEB6F548_inline (Vector2_t1FD6F485C871E832B347AB2DC8CBA08B739D8DF7* __this, float ___x0, float ___y1, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_0 = L_0;
		float L_1 = ___y1;
		__this->___y_1 = L_1;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Mathf_RoundToInt_m6A6E30BA4157D69DA47F02B43108882DDD7C4A70_inline (float ___f0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		float L_0 = ___f0;
		il2cpp_codegen_runtime_class_init_inline(Math_tEB65DE7CA8B083C412C969C92981C030865486CE_il2cpp_TypeInfo_var);
		double L_1;
		L_1 = bankers_round(((double)L_0));
		V_0 = il2cpp_codegen_cast_double_to_int<int32_t>(L_1);
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (1.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_green_m336EB73DD4A5B11B7F405CF4BC7F37A466FB4FF7_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (0.0f), (1.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (1.0f), (0.921568632f), (0.0156862754f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Quaternion_get_eulerAngles_m2DB5158B5C3A71FD60FC8A6EE43D3AAA1CFED122_inline (Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974* __this, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_0 = (*(Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974*)__this);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1;
		L_1 = Quaternion_Internal_ToEulerRad_m9B2C77284AEE6F2C43B6C42F1F888FB4FC904462(L_0, NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_2;
		L_2 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_1, (57.2957802f), NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		L_3 = Quaternion_Internal_MakePositive_m864320DA2D027C186C95B2A5BC2C66B0EB4A6C11(L_2, NULL);
		V_0 = L_3;
		goto IL_001e;
	}

IL_001e:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4 = V_0;
		return L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 Quaternion_Euler_mD4601D966F1F58F3FCA01B3FC19A12D0AD0396DD_inline (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) 
{
	Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3;
		memset((&L_3), 0, sizeof(L_3));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_4;
		L_4 = Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline(L_3, (0.0174532924f), NULL);
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_5;
		L_5 = Quaternion_Internal_FromEulerRad_m2842B9FFB31CDC0F80B7C2172E22831D11D91E93(L_4, NULL);
		V_0 = L_5;
		goto IL_001b;
	}

IL_001b:
	{
		Quaternion_tDA59F214EF07D7700B26E40E562F267AF7306974 L_6 = V_0;
		return L_6;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_up_mAB5269BFCBCB1BD241450C9BF2F156303D30E0C3_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___upVector_7;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_get_right_m13B7C3EAA64DC921EC23346C56A5A597B5481FF5_inline (const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ((Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2_il2cpp_TypeInfo_var))->___rightVector_10;
		V_0 = L_0;
		goto IL_0009;
	}

IL_0009:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) 
{
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = V_0;
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_6 = V_0;
		int32_t L_7 = V_1;
		RuntimeObject* L_8 = ___item0;
		(L_6)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_7), (RuntimeObject*)L_8);
		return;
	}

IL_0034:
	{
		RuntimeObject* L_9 = ___item0;
		((  void (*) (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D*, RuntimeObject*, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_m0248A96C5334E9A93E6994B7780478BCD994EA3D_gshared_inline (List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73* __this, int32_t ___item0, const RuntimeMethod* method) 
{
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_1 = (Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_4 = V_0;
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* L_6 = V_0;
		int32_t L_7 = V_1;
		int32_t L_8 = ___item0;
		(L_6)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_7), (int32_t)L_8);
		return;
	}

IL_0034:
	{
		int32_t L_9 = ___item0;
		((  void (*) (List_1_t05915E9237850A58106982B7FE4BC5DA4E872E73*, int32_t, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) 
{
	{
		float L_0 = ___r0;
		__this->___r_0 = L_0;
		float L_1 = ___g1;
		__this->___g_1 = L_1;
		float L_2 = ___b2;
		__this->___b_2 = L_2;
		float L_3 = ___a3;
		__this->___a_3 = L_3;
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 Vector3_op_Multiply_m516FE285F5342F922C6EB3FCB33197E9017FF484_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 ___a0, float ___d1, const RuntimeMethod* method) 
{
	Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_0 = ___a0;
		float L_1 = L_0.___x_2;
		float L_2 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_3 = ___a0;
		float L_4 = L_3.___y_3;
		float L_5 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_6 = ___a0;
		float L_7 = L_6.___z_4;
		float L_8 = ___d1;
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline((&L_9), ((float)il2cpp_codegen_multiply(L_1, L_2)), ((float)il2cpp_codegen_multiply(L_4, L_5)), ((float)il2cpp_codegen_multiply(L_7, L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2 L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m376936E6B999EF1ECBE57D990A386303E2283DE0_inline (Vector3_t24C512C7B96BBABAD472002D0BA2BDA40A5A80B2* __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) 
{
	{
		float L_0 = ___x0;
		__this->___x_2 = L_0;
		float L_1 = ___y1;
		__this->___y_3 = L_1;
		float L_2 = ___z2;
		__this->___z_4 = L_2;
		return;
	}
}
