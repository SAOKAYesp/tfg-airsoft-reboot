﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void ButtonHandlerGraph::setGraph(UnityEngine.GameObject)
extern void ButtonHandlerGraph_setGraph_mDD3A764AC1EA0D72B439DBD52D939F25DB88A9B7 (void);
// 0x00000002 System.Void ButtonHandlerGraph::.ctor()
extern void ButtonHandlerGraph__ctor_m3B6D9E00014832204F9671DB57F58D41C2EBF6D2 (void);
// 0x00000003 System.Void ButtonHandlerSave::screenShot()
extern void ButtonHandlerSave_screenShot_m600C460F41F9AEA00C99CCDB1B5493D468923339 (void);
// 0x00000004 System.Void ButtonHandlerSave::.ctor()
extern void ButtonHandlerSave__ctor_m0BBDDB5E29D6F3C25A4AA3A3C61B2D916CBF1478 (void);
// 0x00000005 System.Void ButtonHandlerSkybox::SetSkyBox()
extern void ButtonHandlerSkybox_SetSkyBox_m17DFB923655CF7AB027E69F9EF2D91A46E14E12D (void);
// 0x00000006 System.Void ButtonHandlerSkybox::.ctor()
extern void ButtonHandlerSkybox__ctor_m19A8B4901165F69B9BDCFEB7D2C3A3C5AFF020D4 (void);
// 0x00000007 System.Void CargarExcel::LoaditemData(System.Collections.Generic.List`1<Componente>,System.Collections.Generic.List`1<System.String>,Componente)
extern void CargarExcel_LoaditemData_m4AA0181C9C2A91D3679211AB307B812C9AE72178 (void);
// 0x00000008 System.Void CargarExcel::.ctor()
extern void CargarExcel__ctor_mC267A4375E576A3A7B5648730B84C940761B14FF (void);
// 0x00000009 System.Void Componente::.ctor(Componente)
extern void Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756 (void);
// 0x0000000A System.Void DropDownHandler::Start()
extern void DropDownHandler_Start_m71216813C0F507C83193EFA6A3B053BAED133D43 (void);
// 0x0000000B System.Void DropDownHandler::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandler_dropDownSelected_mAA8F5B15E84690C47CD8C315DF6DCF94C40A0FC8 (void);
// 0x0000000C System.Void DropDownHandler::.ctor()
extern void DropDownHandler__ctor_mCA01DDAD2F63C5E3C14961337C026656F47A5B7D (void);
// 0x0000000D System.Void DropDownHandler::<Start>b__10_0(System.Int32)
extern void DropDownHandler_U3CStartU3Eb__10_0_m167C537067D9AC7384513EE714F65305F32D2183 (void);
// 0x0000000E System.Void DropDownHandlerCabezaCilindro::Start()
extern void DropDownHandlerCabezaCilindro_Start_m41ACA340785BE6E485DD64724D65C6BF2768E090 (void);
// 0x0000000F System.Void DropDownHandlerCabezaCilindro::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerCabezaCilindro_dropDownSelected_mDAE9FEB10D333AD54F00BA2343251E893D5808B3 (void);
// 0x00000010 System.Void DropDownHandlerCabezaCilindro::.ctor()
extern void DropDownHandlerCabezaCilindro__ctor_m0CDAC5E3397EEA8913236638784DC8F78E8C19AE (void);
// 0x00000011 System.Void DropDownHandlerCabezaCilindro::<Start>b__11_0(System.Int32)
extern void DropDownHandlerCabezaCilindro_U3CStartU3Eb__11_0_m6875FB41A9C754225791135E51A92A24F0F70012 (void);
// 0x00000012 System.Void DropDownHandlerCabezaPiston::Start()
extern void DropDownHandlerCabezaPiston_Start_mFB984A8E7D0C8EE2D34678EF7D269B2688AC60FD (void);
// 0x00000013 System.Void DropDownHandlerCabezaPiston::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerCabezaPiston_dropDownSelected_m9FEAB9E9AEDE859C229C91DD5496946EAFF6FA04 (void);
// 0x00000014 System.Void DropDownHandlerCabezaPiston::.ctor()
extern void DropDownHandlerCabezaPiston__ctor_mD1B50AE226A307C5A541BAD4C624BCC62D4540F1 (void);
// 0x00000015 System.Void DropDownHandlerCabezaPiston::<Start>b__11_0(System.Int32)
extern void DropDownHandlerCabezaPiston_U3CStartU3Eb__11_0_m07769265758FD3A769226BE96ACA9252B63A69D3 (void);
// 0x00000016 System.Void DropDownHandlerCamaraDeHop::Start()
extern void DropDownHandlerCamaraDeHop_Start_mF3CBEC015C42DC579E90CAABA610A884B0D2714B (void);
// 0x00000017 System.Void DropDownHandlerCamaraDeHop::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerCamaraDeHop_dropDownSelected_mC9EA5084FE00C36BCB86EDF82DFA6BD4632505A1 (void);
// 0x00000018 System.Void DropDownHandlerCamaraDeHop::.ctor()
extern void DropDownHandlerCamaraDeHop__ctor_mE2A88AD7E1E137844EF5EDA79BC10547EB0554A5 (void);
// 0x00000019 System.Void DropDownHandlerCamaraDeHop::<Start>b__10_0(System.Int32)
extern void DropDownHandlerCamaraDeHop_U3CStartU3Eb__10_0_mAB4601E49DF183C8B62D0285C3E36F79338576E0 (void);
// 0x0000001A System.Void DropDownHandlerCa?on::Start()
extern void DropDownHandlerCaUF1on_Start_mFDC82067CA1B2DF553FBCCD6EBDF5B0DB1B5AC62 (void);
// 0x0000001B System.Void DropDownHandlerCa?on::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerCaUF1on_dropDownSelected_m37BA963612034E0DFD25BC0202DD2980285F31E8 (void);
// 0x0000001C System.Void DropDownHandlerCa?on::.ctor()
extern void DropDownHandlerCaUF1on__ctor_m685379D962D15569205FB724ADADB1B5B4CCF952 (void);
// 0x0000001D System.Void DropDownHandlerCa?on::<Start>b__11_0(System.Int32)
extern void DropDownHandlerCaUF1on_U3CStartU3Eb__11_0_m9B7D9E4E7E364A736329813AB4BA2A14B048A812 (void);
// 0x0000001E System.Void DropDownHandlerCilindro::Start()
extern void DropDownHandlerCilindro_Start_mC7817E2E40A533795170A912F5B1DAA73C26BFC4 (void);
// 0x0000001F System.Void DropDownHandlerCilindro::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerCilindro_dropDownSelected_mDE0E057333B47B24CD2D65ABE0045CF1F23666F3 (void);
// 0x00000020 System.Void DropDownHandlerCilindro::.ctor()
extern void DropDownHandlerCilindro__ctor_m68335735F76552B5AE9716F979FD2482419FBCFD (void);
// 0x00000021 System.Void DropDownHandlerCilindro::<Start>b__11_0(System.Int32)
extern void DropDownHandlerCilindro_U3CStartU3Eb__11_0_m9124746DDB3AD4D17E8D48140234F227D6B09B77 (void);
// 0x00000022 System.Void DropDownHandlerEngranajes::Start()
extern void DropDownHandlerEngranajes_Start_mD94793A84A704ED472E412D57D05B919F8E836AA (void);
// 0x00000023 System.Void DropDownHandlerEngranajes::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerEngranajes_dropDownSelected_m870DD902E40F08063754C3D04D1C61B65090AD87 (void);
// 0x00000024 System.Void DropDownHandlerEngranajes::.ctor()
extern void DropDownHandlerEngranajes__ctor_m5420BD77171B0DB1E5644630A92B3B5C135C2E69 (void);
// 0x00000025 System.Void DropDownHandlerEngranajes::<Start>b__10_0(System.Int32)
extern void DropDownHandlerEngranajes_U3CStartU3Eb__10_0_mD533AC3B640B672A2DF154F464303C994ACF5472 (void);
// 0x00000026 System.Void DropDownHandlerGatillo::Start()
extern void DropDownHandlerGatillo_Start_m7FED2AF654130CDCB52429B612DA164D090873B1 (void);
// 0x00000027 System.Void DropDownHandlerGatillo::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerGatillo_dropDownSelected_m5D8532C5BB3F3EFB3CAEAB54F90EEEF952C7B127 (void);
// 0x00000028 System.Void DropDownHandlerGatillo::.ctor()
extern void DropDownHandlerGatillo__ctor_m0540E5C444B8D6A7F814269123552025CA6FBD0D (void);
// 0x00000029 System.Void DropDownHandlerGatillo::<Start>b__9_0(System.Int32)
extern void DropDownHandlerGatillo_U3CStartU3Eb__9_0_mC9966227B6D0AF0442B643E6C052377502B279CA (void);
// 0x0000002A System.Void DropDownHandlerGearbox::Start()
extern void DropDownHandlerGearbox_Start_mB93D86AE32CF0684241074B6A11DA551A031FF6F (void);
// 0x0000002B System.Void DropDownHandlerGearbox::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerGearbox_dropDownSelected_m2DEB8AF72C0222FEAF1E9C82D27D3E8C328963A3 (void);
// 0x0000002C System.Void DropDownHandlerGearbox::.ctor()
extern void DropDownHandlerGearbox__ctor_m864FBE3C78780ED47D4FE9B86822B5E55945E6A0 (void);
// 0x0000002D System.Void DropDownHandlerGearbox::<Start>b__9_0(System.Int32)
extern void DropDownHandlerGearbox_U3CStartU3Eb__9_0_mAC06C59EE5D60D83407F14666E54194722E96603 (void);
// 0x0000002E System.Void DropDownHandlerGomaHop::Start()
extern void DropDownHandlerGomaHop_Start_m40ACE6395FB2D4761752DA73FE643C5FC43A6869 (void);
// 0x0000002F System.Void DropDownHandlerGomaHop::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerGomaHop_dropDownSelected_mB46A37DDC4F4EB2DACBEBF35E0AF8E7D8748B23C (void);
// 0x00000030 System.Void DropDownHandlerGomaHop::.ctor()
extern void DropDownHandlerGomaHop__ctor_mAFA5EB92C94AE9BB2CF60606023D48A41DA30903 (void);
// 0x00000031 System.Void DropDownHandlerGomaHop::<Start>b__11_0(System.Int32)
extern void DropDownHandlerGomaHop_U3CStartU3Eb__11_0_m4B4BAE68C150400B7C51DBDB63D6C0FE2627BF0A (void);
// 0x00000032 System.Void DropDownHandlerGuiaDeMuelle::Start()
extern void DropDownHandlerGuiaDeMuelle_Start_m98F25071AAD01CE30F704621A4CCCBAA53257F00 (void);
// 0x00000033 System.Void DropDownHandlerGuiaDeMuelle::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerGuiaDeMuelle_dropDownSelected_m733D23725483F1F6490FCDE21D7B4B3BA6AEA443 (void);
// 0x00000034 System.Void DropDownHandlerGuiaDeMuelle::.ctor()
extern void DropDownHandlerGuiaDeMuelle__ctor_mAF9EA2DC830AC0F63EEE915832F6519D974059A5 (void);
// 0x00000035 System.Void DropDownHandlerGuiaDeMuelle::<Start>b__10_0(System.Int32)
extern void DropDownHandlerGuiaDeMuelle_U3CStartU3Eb__10_0_m6339013E14DBF4D778320EA9AD722B604A7F5CDB (void);
// 0x00000036 System.Void DropDownHandlerMosfet::Start()
extern void DropDownHandlerMosfet_Start_m78D4A5D740B095DD384CD1631870B2FB89ECE497 (void);
// 0x00000037 System.Void DropDownHandlerMosfet::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerMosfet_dropDownSelected_mDB38E8B59C4BD1A37B3904DDD4140DD0B908D29D (void);
// 0x00000038 System.Void DropDownHandlerMosfet::.ctor()
extern void DropDownHandlerMosfet__ctor_m86D5296B61216253111F673D9BBF0EFC30126473 (void);
// 0x00000039 System.Void DropDownHandlerMosfet::<Start>b__9_0(System.Int32)
extern void DropDownHandlerMosfet_U3CStartU3Eb__9_0_m65B83DAF56665DAE980B0CE398DF8F6D9FA2C934 (void);
// 0x0000003A System.Void DropDownHandlerMuelle::Start()
extern void DropDownHandlerMuelle_Start_mDA29863D45802136CD5E5049DD123ABCFE5D8980 (void);
// 0x0000003B System.Void DropDownHandlerMuelle::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerMuelle_dropDownSelected_m11D7CA3A3A1DA2257974480DE1B4DEA71AC3DF60 (void);
// 0x0000003C System.Void DropDownHandlerMuelle::.ctor()
extern void DropDownHandlerMuelle__ctor_m363953D24C991C90BC79F47A2887A12140C1EEE1 (void);
// 0x0000003D System.Void DropDownHandlerMuelle::<Start>b__11_0(System.Int32)
extern void DropDownHandlerMuelle_U3CStartU3Eb__11_0_m3D9CAB8C350531D5B12E67230396AB80A0D567F9 (void);
// 0x0000003E System.Void DropDownHandlerNozzle::Start()
extern void DropDownHandlerNozzle_Start_mE513D8D1DB5A9A916BA30B09D9BF61C74E0D5070 (void);
// 0x0000003F System.Void DropDownHandlerNozzle::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerNozzle_dropDownSelected_m904609CE01849C3F4E66CB1B76942E9995CFFDD3 (void);
// 0x00000040 System.Void DropDownHandlerNozzle::.ctor()
extern void DropDownHandlerNozzle__ctor_m851645F8CC0096BAA69B82E65B4BD48CC3E39183 (void);
// 0x00000041 System.Void DropDownHandlerNozzle::<Start>b__11_0(System.Int32)
extern void DropDownHandlerNozzle_U3CStartU3Eb__11_0_mE7253AF7301F0734BE270E30D436097400B56689 (void);
// 0x00000042 System.Void DropDownHandlerNub::Start()
extern void DropDownHandlerNub_Start_mA1703EC4ED324023C6E587C455145ABC3BAD1CE6 (void);
// 0x00000043 System.Void DropDownHandlerNub::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerNub_dropDownSelected_m6B267A9871CA0EC1C84734C8931CE8E992568C02 (void);
// 0x00000044 System.Void DropDownHandlerNub::.ctor()
extern void DropDownHandlerNub__ctor_mFDAD3B0E51BC9E5C8FF931531F80CBA45FEC733C (void);
// 0x00000045 System.Void DropDownHandlerNub::<Start>b__9_0(System.Int32)
extern void DropDownHandlerNub_U3CStartU3Eb__9_0_m2D04559A348CAE089EF1217B21237F6EA2C0B151 (void);
// 0x00000046 System.Void DropDownHandlerPiston::Start()
extern void DropDownHandlerPiston_Start_mAEC4FBFA84579B0963B97647E1CB677CC9E385AE (void);
// 0x00000047 System.Void DropDownHandlerPiston::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerPiston_dropDownSelected_m878407BD641F22179B3AE671147075555421573C (void);
// 0x00000048 System.Void DropDownHandlerPiston::.ctor()
extern void DropDownHandlerPiston__ctor_m4779FBCB6E4CA958D9CAEB15C6F95516D6043189 (void);
// 0x00000049 System.Void DropDownHandlerPiston::<Start>b__11_0(System.Int32)
extern void DropDownHandlerPiston_U3CStartU3Eb__11_0_mEFB1EE1B45CCF22BAAE3D2E819B80540781C4AB8 (void);
// 0x0000004A System.Void DropDownHandlerTapperPlate::Start()
extern void DropDownHandlerTapperPlate_Start_mAC0D9E06037CE03CABBDE405055E0C9795F433AF (void);
// 0x0000004B System.Void DropDownHandlerTapperPlate::dropDownSelected(UnityEngine.UI.Dropdown)
extern void DropDownHandlerTapperPlate_dropDownSelected_m2A302B20964AF7F7E3A2F804454003DDDB968969 (void);
// 0x0000004C System.Void DropDownHandlerTapperPlate::.ctor()
extern void DropDownHandlerTapperPlate__ctor_m894A0E44957BDEDD2C3669B5CC851884B73D66E8 (void);
// 0x0000004D System.Void DropDownHandlerTapperPlate::<Start>b__10_0(System.Int32)
extern void DropDownHandlerTapperPlate_U3CStartU3Eb__10_0_mBAF90C067D081839DA913AFD018DC7468ADDDE56 (void);
// 0x0000004E System.Void HacerCaptura::descargarTxt(System.String,System.String)
extern void HacerCaptura_descargarTxt_m55158E62D0F4C65D8ACB3B5692B53F0A87766B46 (void);
// 0x0000004F System.Void HacerCaptura::errorNombre()
extern void HacerCaptura_errorNombre_m4BDC0D400F0F00016E8F247824BB2725B80C0312 (void);
// 0x00000050 System.Void HacerCaptura::Start()
extern void HacerCaptura_Start_m01289BB5D40A2C9043E573D884DA59115B8DA146 (void);
// 0x00000051 System.Void HacerCaptura::setStats(System.Double,System.Double,System.Double,System.Double,System.Double,System.Double)
extern void HacerCaptura_setStats_mEAAECBD9E5319611DBF780127D5CB6B0DA5CAC79 (void);
// 0x00000052 System.Void HacerCaptura::Captura()
extern void HacerCaptura_Captura_m04726B41AFAFE3C4E3EFC47C06618AD224F260BF (void);
// 0x00000053 System.Void HacerCaptura::Update()
extern void HacerCaptura_Update_m9EED32623F2026B66BE63E3B4463AF624ECC47FF (void);
// 0x00000054 System.Void HacerCaptura::error()
extern void HacerCaptura_error_m0201DF7A79E1BF703064ABF1F31C3CBF85B56A3A (void);
// 0x00000055 System.Void HacerCaptura::DescargarTxt(System.String,System.String)
extern void HacerCaptura_DescargarTxt_mFA814DC1A322790177EF21C355E89B065EC1B2F6 (void);
// 0x00000056 System.Collections.IEnumerator HacerCaptura::descargarPNG()
extern void HacerCaptura_descargarPNG_m469AED5E474F6927E47D6597CA5CD81D4142105F (void);
// 0x00000057 System.Void HacerCaptura::SaveScreenshotWebGL(System.String,System.String)
extern void HacerCaptura_SaveScreenshotWebGL_mD08623AF978D56A33A17C450E4148452367B6EF2 (void);
// 0x00000058 System.Void HacerCaptura::.ctor()
extern void HacerCaptura__ctor_m51F58258A300D2546101D133EA574AEA64FAFB4C (void);
// 0x00000059 System.Void HacerCaptura/<descargarPNG>d__34::.ctor(System.Int32)
extern void U3CdescargarPNGU3Ed__34__ctor_mA41CBEFA68CDFA0D467D8FB2107F6D91B2D23B88 (void);
// 0x0000005A System.Void HacerCaptura/<descargarPNG>d__34::System.IDisposable.Dispose()
extern void U3CdescargarPNGU3Ed__34_System_IDisposable_Dispose_m8BA5409E509A8B1D06E2D10239B8E7DEE4A3CC2E (void);
// 0x0000005B System.Boolean HacerCaptura/<descargarPNG>d__34::MoveNext()
extern void U3CdescargarPNGU3Ed__34_MoveNext_m83A335723BA6A2240721C9ECD010F91AE270981F (void);
// 0x0000005C System.Object HacerCaptura/<descargarPNG>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CdescargarPNGU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5431F654B2E32A7119E3A1AB4577C672BFFE03B (void);
// 0x0000005D System.Void HacerCaptura/<descargarPNG>d__34::System.Collections.IEnumerator.Reset()
extern void U3CdescargarPNGU3Ed__34_System_Collections_IEnumerator_Reset_m2D3D04EAB98096753239C68AF245F189AD38CCCB (void);
// 0x0000005E System.Object HacerCaptura/<descargarPNG>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CdescargarPNGU3Ed__34_System_Collections_IEnumerator_get_Current_m880B10B450145A9169BA22AEA466099B05B1898B (void);
// 0x0000005F System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>> LeerExcel::Read(System.String)
extern void LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05 (void);
// 0x00000060 System.Void LeerExcel::.ctor()
extern void LeerExcel__ctor_m45A2D5DA9F28D8A46745468A507FEC442BA296A9 (void);
// 0x00000061 System.Void LeerExcel::.cctor()
extern void LeerExcel__cctor_m4EE1653C59FBEED5E4F213D8DDDA580775CD83F6 (void);
// 0x00000062 System.Void saveConfig::todoCorrecto()
extern void saveConfig_todoCorrecto_mA18AA8966091253847C30259F10AD1A214827BAF (void);
// 0x00000063 System.Void saveConfig::errorNombre()
extern void saveConfig_errorNombre_mF2BC35C6DE825FFE4CBA538B935A2BB19407F47E (void);
// 0x00000064 System.Void saveConfig::Start()
extern void saveConfig_Start_m3D8EF53111094946B8E8F1CD60A70A890A99B1B2 (void);
// 0x00000065 System.Void saveConfig::setStats(System.Double,System.Double,System.Double,System.Double,System.Double,System.Double)
extern void saveConfig_setStats_m11B648967ECBEA5D7FF3E0A9A5B260CCFC953A26 (void);
// 0x00000066 System.Void saveConfig::error()
extern void saveConfig_error_m7BB8597BA30D2F230E8FAC765ED166F3539F3EDF (void);
// 0x00000067 System.Void saveConfig::bien()
extern void saveConfig_bien_mC22BBB0E80B8D093F33CD35074F571D5FD08F959 (void);
// 0x00000068 System.Void saveConfig::callSave()
extern void saveConfig_callSave_m656171465A8E1A6F511290B77ABFFBEEB9826510 (void);
// 0x00000069 System.Collections.IEnumerator saveConfig::Save()
extern void saveConfig_Save_m9A7646305B7F0AE74C1AAD993AC03F16EC45532F (void);
// 0x0000006A System.Void saveConfig::.ctor()
extern void saveConfig__ctor_m66E66E05E5650AA0DC2341040E1178195C8DC330 (void);
// 0x0000006B System.Void saveConfig/<Save>d__31::.ctor(System.Int32)
extern void U3CSaveU3Ed__31__ctor_m9D8B3EB5A48E88E28F2652A7DF75887CE1C89FC7 (void);
// 0x0000006C System.Void saveConfig/<Save>d__31::System.IDisposable.Dispose()
extern void U3CSaveU3Ed__31_System_IDisposable_Dispose_mD2D34F6605E9503E0D0ADED22CB0C2F7D5B7A08C (void);
// 0x0000006D System.Boolean saveConfig/<Save>d__31::MoveNext()
extern void U3CSaveU3Ed__31_MoveNext_mCB2A61DC38D34516DD6B611386CFD9BD29924643 (void);
// 0x0000006E System.Void saveConfig/<Save>d__31::<>m__Finally1()
extern void U3CSaveU3Ed__31_U3CU3Em__Finally1_m8D488E2CE71CF9ED53EF23A773106D0607794BC2 (void);
// 0x0000006F System.Object saveConfig/<Save>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSaveU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1360F69CE79A86D420CCBD533CB3B2F05A6C69E8 (void);
// 0x00000070 System.Void saveConfig/<Save>d__31::System.Collections.IEnumerator.Reset()
extern void U3CSaveU3Ed__31_System_Collections_IEnumerator_Reset_m762323724A5978C177AAFDD6849978B9C8C45A7A (void);
// 0x00000071 System.Object saveConfig/<Save>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CSaveU3Ed__31_System_Collections_IEnumerator_get_Current_m89C4E81C9291F9AD9C60D6627B1AD32B88990F03 (void);
// 0x00000072 System.Void Window_Graph::Awake()
extern void Window_Graph_Awake_m9FEFBB9FACB351784E3082313B30625836DDEDD4 (void);
// 0x00000073 System.Void Window_Graph::Update()
extern void Window_Graph_Update_m64029026231024A2C6A96C96BAF6C822776F3F7E (void);
// 0x00000074 System.Void Window_Graph::ShowGraph(System.Collections.Generic.List`1<System.Int32>)
extern void Window_Graph_ShowGraph_m5FF8330468B0C90C3A7654265E4D07593E1C89EA (void);
// 0x00000075 System.Void Window_Graph::setPrecision(System.Double,System.Double)
extern void Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6 (void);
// 0x00000076 System.Void Window_Graph::setPotencia(System.Double,System.Double)
extern void Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E (void);
// 0x00000077 System.Void Window_Graph::setAlcance(System.Double,System.Double)
extern void Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2 (void);
// 0x00000078 System.Void Window_Graph::setCadencia(System.Double,System.Double)
extern void Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142 (void);
// 0x00000079 System.Void Window_Graph::setVarianza(System.Single,System.Single)
extern void Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8 (void);
// 0x0000007A System.Void Window_Graph::setPrecio(System.Double,System.Double)
extern void Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2 (void);
// 0x0000007B System.Void Window_Graph::setExtra(System.String)
extern void Window_Graph_setExtra_m8774145D45E480AAFA50F295A5FB4E00A289FF91 (void);
// 0x0000007C UnityEngine.GameObject Window_Graph::ChangeBar(UnityEngine.Vector2,System.Single,UnityEngine.GameObject,System.String)
extern void Window_Graph_ChangeBar_mA32AD57472431EE83767EC57C703484118F93468 (void);
// 0x0000007D System.Void Window_Graph::.ctor()
extern void Window_Graph__ctor_m375DCEBA3CB3A5DC82DC1D7C65B46064456552B5 (void);
// 0x0000007E System.Void changeColor::Start()
extern void changeColor_Start_m0DB7F7FE47459769FF91288ED9C298FA57E4A604 (void);
// 0x0000007F System.Void changeColor::SelectMaterial(System.Int32)
extern void changeColor_SelectMaterial_m919B3144A21AC86903CA16CF46A9E9701C3DA655 (void);
// 0x00000080 System.Void changeColor::SetColor(System.Int32)
extern void changeColor_SetColor_m0F758AB8D50A08344A7903511809C4091644A257 (void);
// 0x00000081 System.Void changeColor::onRed()
extern void changeColor_onRed_mDFA04725FC176002465549F0C4C3FD65AFCA36EC (void);
// 0x00000082 System.Void changeColor::onBlue()
extern void changeColor_onBlue_m3829CF17D7A43E66C568B02AE510BE65C685A74B (void);
// 0x00000083 System.Void changeColor::onGreen()
extern void changeColor_onGreen_mF3B33DFA50EB79B9C8E5F689253E146AD0D0C291 (void);
// 0x00000084 System.Void changeColor::onYellow()
extern void changeColor_onYellow_m9EC07A9CE50641B58DB9A389871E1B0F902E8F1A (void);
// 0x00000085 System.Void changeColor::.ctor()
extern void changeColor__ctor_m31295285FCA0F687164DFB8BB421447E044BD1A9 (void);
// 0x00000086 System.Void lockRotation::Start()
extern void lockRotation_Start_mF0C2F8F2B641BBB5EE782E8C7BA2DBB6DA32AF5B (void);
// 0x00000087 System.Void lockRotation::LateUpdate()
extern void lockRotation_LateUpdate_m193077AB70F7EEF1925BAB1AB166BCDB5A5CD38A (void);
// 0x00000088 System.Void lockRotation::.ctor()
extern void lockRotation__ctor_m1B543AE91695CA0F5ADEC761A3F018A5E5A0804D (void);
// 0x00000089 System.Void rotarObjeto::Update()
extern void rotarObjeto_Update_m332CF26CB9F402A6275A75F54FDA19EDF2A1F94E (void);
// 0x0000008A System.Void rotarObjeto::.ctor()
extern void rotarObjeto__ctor_mBFFACF614F3483B46FEB11FF2B88345888A0CF97 (void);
static Il2CppMethodPointer s_methodPointers[138] = 
{
	ButtonHandlerGraph_setGraph_mDD3A764AC1EA0D72B439DBD52D939F25DB88A9B7,
	ButtonHandlerGraph__ctor_m3B6D9E00014832204F9671DB57F58D41C2EBF6D2,
	ButtonHandlerSave_screenShot_m600C460F41F9AEA00C99CCDB1B5493D468923339,
	ButtonHandlerSave__ctor_m0BBDDB5E29D6F3C25A4AA3A3C61B2D916CBF1478,
	ButtonHandlerSkybox_SetSkyBox_m17DFB923655CF7AB027E69F9EF2D91A46E14E12D,
	ButtonHandlerSkybox__ctor_m19A8B4901165F69B9BDCFEB7D2C3A3C5AFF020D4,
	CargarExcel_LoaditemData_m4AA0181C9C2A91D3679211AB307B812C9AE72178,
	CargarExcel__ctor_mC267A4375E576A3A7B5648730B84C940761B14FF,
	Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756,
	DropDownHandler_Start_m71216813C0F507C83193EFA6A3B053BAED133D43,
	DropDownHandler_dropDownSelected_mAA8F5B15E84690C47CD8C315DF6DCF94C40A0FC8,
	DropDownHandler__ctor_mCA01DDAD2F63C5E3C14961337C026656F47A5B7D,
	DropDownHandler_U3CStartU3Eb__10_0_m167C537067D9AC7384513EE714F65305F32D2183,
	DropDownHandlerCabezaCilindro_Start_m41ACA340785BE6E485DD64724D65C6BF2768E090,
	DropDownHandlerCabezaCilindro_dropDownSelected_mDAE9FEB10D333AD54F00BA2343251E893D5808B3,
	DropDownHandlerCabezaCilindro__ctor_m0CDAC5E3397EEA8913236638784DC8F78E8C19AE,
	DropDownHandlerCabezaCilindro_U3CStartU3Eb__11_0_m6875FB41A9C754225791135E51A92A24F0F70012,
	DropDownHandlerCabezaPiston_Start_mFB984A8E7D0C8EE2D34678EF7D269B2688AC60FD,
	DropDownHandlerCabezaPiston_dropDownSelected_m9FEAB9E9AEDE859C229C91DD5496946EAFF6FA04,
	DropDownHandlerCabezaPiston__ctor_mD1B50AE226A307C5A541BAD4C624BCC62D4540F1,
	DropDownHandlerCabezaPiston_U3CStartU3Eb__11_0_m07769265758FD3A769226BE96ACA9252B63A69D3,
	DropDownHandlerCamaraDeHop_Start_mF3CBEC015C42DC579E90CAABA610A884B0D2714B,
	DropDownHandlerCamaraDeHop_dropDownSelected_mC9EA5084FE00C36BCB86EDF82DFA6BD4632505A1,
	DropDownHandlerCamaraDeHop__ctor_mE2A88AD7E1E137844EF5EDA79BC10547EB0554A5,
	DropDownHandlerCamaraDeHop_U3CStartU3Eb__10_0_mAB4601E49DF183C8B62D0285C3E36F79338576E0,
	DropDownHandlerCaUF1on_Start_mFDC82067CA1B2DF553FBCCD6EBDF5B0DB1B5AC62,
	DropDownHandlerCaUF1on_dropDownSelected_m37BA963612034E0DFD25BC0202DD2980285F31E8,
	DropDownHandlerCaUF1on__ctor_m685379D962D15569205FB724ADADB1B5B4CCF952,
	DropDownHandlerCaUF1on_U3CStartU3Eb__11_0_m9B7D9E4E7E364A736329813AB4BA2A14B048A812,
	DropDownHandlerCilindro_Start_mC7817E2E40A533795170A912F5B1DAA73C26BFC4,
	DropDownHandlerCilindro_dropDownSelected_mDE0E057333B47B24CD2D65ABE0045CF1F23666F3,
	DropDownHandlerCilindro__ctor_m68335735F76552B5AE9716F979FD2482419FBCFD,
	DropDownHandlerCilindro_U3CStartU3Eb__11_0_m9124746DDB3AD4D17E8D48140234F227D6B09B77,
	DropDownHandlerEngranajes_Start_mD94793A84A704ED472E412D57D05B919F8E836AA,
	DropDownHandlerEngranajes_dropDownSelected_m870DD902E40F08063754C3D04D1C61B65090AD87,
	DropDownHandlerEngranajes__ctor_m5420BD77171B0DB1E5644630A92B3B5C135C2E69,
	DropDownHandlerEngranajes_U3CStartU3Eb__10_0_mD533AC3B640B672A2DF154F464303C994ACF5472,
	DropDownHandlerGatillo_Start_m7FED2AF654130CDCB52429B612DA164D090873B1,
	DropDownHandlerGatillo_dropDownSelected_m5D8532C5BB3F3EFB3CAEAB54F90EEEF952C7B127,
	DropDownHandlerGatillo__ctor_m0540E5C444B8D6A7F814269123552025CA6FBD0D,
	DropDownHandlerGatillo_U3CStartU3Eb__9_0_mC9966227B6D0AF0442B643E6C052377502B279CA,
	DropDownHandlerGearbox_Start_mB93D86AE32CF0684241074B6A11DA551A031FF6F,
	DropDownHandlerGearbox_dropDownSelected_m2DEB8AF72C0222FEAF1E9C82D27D3E8C328963A3,
	DropDownHandlerGearbox__ctor_m864FBE3C78780ED47D4FE9B86822B5E55945E6A0,
	DropDownHandlerGearbox_U3CStartU3Eb__9_0_mAC06C59EE5D60D83407F14666E54194722E96603,
	DropDownHandlerGomaHop_Start_m40ACE6395FB2D4761752DA73FE643C5FC43A6869,
	DropDownHandlerGomaHop_dropDownSelected_mB46A37DDC4F4EB2DACBEBF35E0AF8E7D8748B23C,
	DropDownHandlerGomaHop__ctor_mAFA5EB92C94AE9BB2CF60606023D48A41DA30903,
	DropDownHandlerGomaHop_U3CStartU3Eb__11_0_m4B4BAE68C150400B7C51DBDB63D6C0FE2627BF0A,
	DropDownHandlerGuiaDeMuelle_Start_m98F25071AAD01CE30F704621A4CCCBAA53257F00,
	DropDownHandlerGuiaDeMuelle_dropDownSelected_m733D23725483F1F6490FCDE21D7B4B3BA6AEA443,
	DropDownHandlerGuiaDeMuelle__ctor_mAF9EA2DC830AC0F63EEE915832F6519D974059A5,
	DropDownHandlerGuiaDeMuelle_U3CStartU3Eb__10_0_m6339013E14DBF4D778320EA9AD722B604A7F5CDB,
	DropDownHandlerMosfet_Start_m78D4A5D740B095DD384CD1631870B2FB89ECE497,
	DropDownHandlerMosfet_dropDownSelected_mDB38E8B59C4BD1A37B3904DDD4140DD0B908D29D,
	DropDownHandlerMosfet__ctor_m86D5296B61216253111F673D9BBF0EFC30126473,
	DropDownHandlerMosfet_U3CStartU3Eb__9_0_m65B83DAF56665DAE980B0CE398DF8F6D9FA2C934,
	DropDownHandlerMuelle_Start_mDA29863D45802136CD5E5049DD123ABCFE5D8980,
	DropDownHandlerMuelle_dropDownSelected_m11D7CA3A3A1DA2257974480DE1B4DEA71AC3DF60,
	DropDownHandlerMuelle__ctor_m363953D24C991C90BC79F47A2887A12140C1EEE1,
	DropDownHandlerMuelle_U3CStartU3Eb__11_0_m3D9CAB8C350531D5B12E67230396AB80A0D567F9,
	DropDownHandlerNozzle_Start_mE513D8D1DB5A9A916BA30B09D9BF61C74E0D5070,
	DropDownHandlerNozzle_dropDownSelected_m904609CE01849C3F4E66CB1B76942E9995CFFDD3,
	DropDownHandlerNozzle__ctor_m851645F8CC0096BAA69B82E65B4BD48CC3E39183,
	DropDownHandlerNozzle_U3CStartU3Eb__11_0_mE7253AF7301F0734BE270E30D436097400B56689,
	DropDownHandlerNub_Start_mA1703EC4ED324023C6E587C455145ABC3BAD1CE6,
	DropDownHandlerNub_dropDownSelected_m6B267A9871CA0EC1C84734C8931CE8E992568C02,
	DropDownHandlerNub__ctor_mFDAD3B0E51BC9E5C8FF931531F80CBA45FEC733C,
	DropDownHandlerNub_U3CStartU3Eb__9_0_m2D04559A348CAE089EF1217B21237F6EA2C0B151,
	DropDownHandlerPiston_Start_mAEC4FBFA84579B0963B97647E1CB677CC9E385AE,
	DropDownHandlerPiston_dropDownSelected_m878407BD641F22179B3AE671147075555421573C,
	DropDownHandlerPiston__ctor_m4779FBCB6E4CA958D9CAEB15C6F95516D6043189,
	DropDownHandlerPiston_U3CStartU3Eb__11_0_mEFB1EE1B45CCF22BAAE3D2E819B80540781C4AB8,
	DropDownHandlerTapperPlate_Start_mAC0D9E06037CE03CABBDE405055E0C9795F433AF,
	DropDownHandlerTapperPlate_dropDownSelected_m2A302B20964AF7F7E3A2F804454003DDDB968969,
	DropDownHandlerTapperPlate__ctor_m894A0E44957BDEDD2C3669B5CC851884B73D66E8,
	DropDownHandlerTapperPlate_U3CStartU3Eb__10_0_mBAF90C067D081839DA913AFD018DC7468ADDDE56,
	HacerCaptura_descargarTxt_m55158E62D0F4C65D8ACB3B5692B53F0A87766B46,
	HacerCaptura_errorNombre_m4BDC0D400F0F00016E8F247824BB2725B80C0312,
	HacerCaptura_Start_m01289BB5D40A2C9043E573D884DA59115B8DA146,
	HacerCaptura_setStats_mEAAECBD9E5319611DBF780127D5CB6B0DA5CAC79,
	HacerCaptura_Captura_m04726B41AFAFE3C4E3EFC47C06618AD224F260BF,
	HacerCaptura_Update_m9EED32623F2026B66BE63E3B4463AF624ECC47FF,
	HacerCaptura_error_m0201DF7A79E1BF703064ABF1F31C3CBF85B56A3A,
	HacerCaptura_DescargarTxt_mFA814DC1A322790177EF21C355E89B065EC1B2F6,
	HacerCaptura_descargarPNG_m469AED5E474F6927E47D6597CA5CD81D4142105F,
	HacerCaptura_SaveScreenshotWebGL_mD08623AF978D56A33A17C450E4148452367B6EF2,
	HacerCaptura__ctor_m51F58258A300D2546101D133EA574AEA64FAFB4C,
	U3CdescargarPNGU3Ed__34__ctor_mA41CBEFA68CDFA0D467D8FB2107F6D91B2D23B88,
	U3CdescargarPNGU3Ed__34_System_IDisposable_Dispose_m8BA5409E509A8B1D06E2D10239B8E7DEE4A3CC2E,
	U3CdescargarPNGU3Ed__34_MoveNext_m83A335723BA6A2240721C9ECD010F91AE270981F,
	U3CdescargarPNGU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB5431F654B2E32A7119E3A1AB4577C672BFFE03B,
	U3CdescargarPNGU3Ed__34_System_Collections_IEnumerator_Reset_m2D3D04EAB98096753239C68AF245F189AD38CCCB,
	U3CdescargarPNGU3Ed__34_System_Collections_IEnumerator_get_Current_m880B10B450145A9169BA22AEA466099B05B1898B,
	LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05,
	LeerExcel__ctor_m45A2D5DA9F28D8A46745468A507FEC442BA296A9,
	LeerExcel__cctor_m4EE1653C59FBEED5E4F213D8DDDA580775CD83F6,
	saveConfig_todoCorrecto_mA18AA8966091253847C30259F10AD1A214827BAF,
	saveConfig_errorNombre_mF2BC35C6DE825FFE4CBA538B935A2BB19407F47E,
	saveConfig_Start_m3D8EF53111094946B8E8F1CD60A70A890A99B1B2,
	saveConfig_setStats_m11B648967ECBEA5D7FF3E0A9A5B260CCFC953A26,
	saveConfig_error_m7BB8597BA30D2F230E8FAC765ED166F3539F3EDF,
	saveConfig_bien_mC22BBB0E80B8D093F33CD35074F571D5FD08F959,
	saveConfig_callSave_m656171465A8E1A6F511290B77ABFFBEEB9826510,
	saveConfig_Save_m9A7646305B7F0AE74C1AAD993AC03F16EC45532F,
	saveConfig__ctor_m66E66E05E5650AA0DC2341040E1178195C8DC330,
	U3CSaveU3Ed__31__ctor_m9D8B3EB5A48E88E28F2652A7DF75887CE1C89FC7,
	U3CSaveU3Ed__31_System_IDisposable_Dispose_mD2D34F6605E9503E0D0ADED22CB0C2F7D5B7A08C,
	U3CSaveU3Ed__31_MoveNext_mCB2A61DC38D34516DD6B611386CFD9BD29924643,
	U3CSaveU3Ed__31_U3CU3Em__Finally1_m8D488E2CE71CF9ED53EF23A773106D0607794BC2,
	U3CSaveU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1360F69CE79A86D420CCBD533CB3B2F05A6C69E8,
	U3CSaveU3Ed__31_System_Collections_IEnumerator_Reset_m762323724A5978C177AAFDD6849978B9C8C45A7A,
	U3CSaveU3Ed__31_System_Collections_IEnumerator_get_Current_m89C4E81C9291F9AD9C60D6627B1AD32B88990F03,
	Window_Graph_Awake_m9FEFBB9FACB351784E3082313B30625836DDEDD4,
	Window_Graph_Update_m64029026231024A2C6A96C96BAF6C822776F3F7E,
	Window_Graph_ShowGraph_m5FF8330468B0C90C3A7654265E4D07593E1C89EA,
	Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6,
	Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E,
	Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2,
	Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142,
	Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8,
	Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2,
	Window_Graph_setExtra_m8774145D45E480AAFA50F295A5FB4E00A289FF91,
	Window_Graph_ChangeBar_mA32AD57472431EE83767EC57C703484118F93468,
	Window_Graph__ctor_m375DCEBA3CB3A5DC82DC1D7C65B46064456552B5,
	changeColor_Start_m0DB7F7FE47459769FF91288ED9C298FA57E4A604,
	changeColor_SelectMaterial_m919B3144A21AC86903CA16CF46A9E9701C3DA655,
	changeColor_SetColor_m0F758AB8D50A08344A7903511809C4091644A257,
	changeColor_onRed_mDFA04725FC176002465549F0C4C3FD65AFCA36EC,
	changeColor_onBlue_m3829CF17D7A43E66C568B02AE510BE65C685A74B,
	changeColor_onGreen_mF3B33DFA50EB79B9C8E5F689253E146AD0D0C291,
	changeColor_onYellow_m9EC07A9CE50641B58DB9A389871E1B0F902E8F1A,
	changeColor__ctor_m31295285FCA0F687164DFB8BB421447E044BD1A9,
	lockRotation_Start_mF0C2F8F2B641BBB5EE782E8C7BA2DBB6DA32AF5B,
	lockRotation_LateUpdate_m193077AB70F7EEF1925BAB1AB166BCDB5A5CD38A,
	lockRotation__ctor_m1B543AE91695CA0F5ADEC761A3F018A5E5A0804D,
	rotarObjeto_Update_m332CF26CB9F402A6275A75F54FDA19EDF2A1F94E,
	rotarObjeto__ctor_mBFFACF614F3483B46FEB11FF2B88345888A0CF97,
};
static const int32_t s_InvokerIndices[138] = 
{
	2604,
	3185,
	3185,
	3185,
	3185,
	3185,
	4062,
	3185,
	2604,
	3185,
	2604,
	3185,
	2587,
	3185,
	2604,
	3185,
	2587,
	3185,
	2604,
	3185,
	2587,
	3185,
	2604,
	3185,
	2587,
	3185,
	2604,
	3185,
	2587,
	3185,
	2604,
	3185,
	2587,
	3185,
	2604,
	3185,
	2587,
	3185,
	2604,
	3185,
	2587,
	3185,
	2604,
	3185,
	2587,
	3185,
	2604,
	3185,
	2587,
	3185,
	2604,
	3185,
	2587,
	3185,
	2604,
	3185,
	2587,
	3185,
	2604,
	3185,
	2587,
	3185,
	2604,
	3185,
	2587,
	3185,
	2604,
	3185,
	2587,
	3185,
	2604,
	3185,
	2587,
	3185,
	2604,
	3185,
	2587,
	4441,
	4904,
	3185,
	3364,
	3185,
	3185,
	4904,
	4441,
	3104,
	4441,
	3185,
	2587,
	3185,
	3048,
	3104,
	3185,
	3104,
	4671,
	3185,
	4904,
	4904,
	4904,
	3185,
	3364,
	4904,
	4904,
	3185,
	3104,
	3185,
	2587,
	3185,
	3048,
	3185,
	3104,
	3185,
	3104,
	3185,
	3185,
	2604,
	4405,
	4405,
	4405,
	4405,
	4451,
	4405,
	4807,
	466,
	3185,
	3185,
	2587,
	2587,
	3185,
	3185,
	3185,
	3185,
	3185,
	3185,
	3185,
	3185,
	3185,
	3185,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	138,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
