﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtualFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tAE94C8F24AD5B94D4EE85CA9FC59E3409D41CAF7;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Object>
struct KeyCollection_tE66790F09E854C19C7F612BEAD203AE626E90A36;
// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct List_1_tE020B120302E34B781278C33685F2DA255337446;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t2CDCA768E7F493F5EDEBC75AEB200FD621354E35;
// System.Collections.Generic.List`1<Componente>
struct List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083;
// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>
struct List_1_t89B39292AD45371F7FDCB295AAE956D33588BC6E;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t830EC096236A3CEC7189DFA6E0B2E74C5C97780B;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_t7CC0661D6B113117B4CC68761D93AC8DF5DBD66A;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Object>
struct ValueCollection_tC9D91E8A3198E40EA339059703AB10DFC9F5CC2E;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>[]
struct Dictionary_2U5BU5D_tF3731166E97A9D9E091E8FEDF8384CEAC68CF4D3;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Object>[]
struct EntryU5BU5D_t233BB24ED01E2D8D65B0651D54B8E3AD125CAF96;
// Componente[]
struct ComponenteU5BU5D_tCC3DFD5D47FE12641DF26F3B39B0B63F0C819BBC;
// System.Delegate[]
struct DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771;
// System.Int32[]
struct Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C;
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t4160E135F02A40F75A63F787D36F31FEC6FE91A9;
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;
// UnityEngine.UI.Dropdown/OptionData[]
struct OptionDataU5BU5D_tF56CF744A9E6A0DBB0AC2072BE52F7C8D2E1947F;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tA0DC06F89C5280C6DD972F6F4C8A56D7F4F79074;
// ButtonHandlerGraph
struct ButtonHandlerGraph_tDC9C59A005B94B0D6306A5C0FC4F47E2467B470D;
// ButtonHandlerSave
struct ButtonHandlerSave_t64D9EBC89B5FD212EC815917A3E67E66C8305A93;
// ButtonHandlerSkybox
struct ButtonHandlerSkybox_t4BFA57953E7D268FF9BC01289F6766FD1358F049;
// CargarExcel
struct CargarExcel_t6CA25EC14B078230E56573150445E548D27C5E4C;
// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3;
// Componente
struct Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F;
// System.DelegateData
struct DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E;
// DropDownHandler
struct DropDownHandler_t8002662C419AC287ADA29E30AF224815CC7D12C7;
// DropDownHandlerCabezaCilindro
struct DropDownHandlerCabezaCilindro_tC63ABBA0B4BAC7AF05823FABC92495D3538FABEC;
// DropDownHandlerCabezaPiston
struct DropDownHandlerCabezaPiston_t5D4F7822D76A01DD44715998B4C3911DC1DBE889;
// DropDownHandlerCamaraDeHop
struct DropDownHandlerCamaraDeHop_t9C574C9C85718804384E4CA248294DDC9E19F1EB;
// DropDownHandlerCa?on
struct DropDownHandlerCaUF1on_tB8652C4AED37060D00139A5D51D4FBB60BABB0C5;
// DropDownHandlerCilindro
struct DropDownHandlerCilindro_t95B6802C5FF3BA348A912FEDA7D68C8643581D52;
// DropDownHandlerEngranajes
struct DropDownHandlerEngranajes_t062A4B96E3458FE9041B420AB0B343FF68F03D24;
// DropDownHandlerGatillo
struct DropDownHandlerGatillo_t74F9CFF132D5EC7C28BB31711DB8AC2A6DC6B240;
// DropDownHandlerGearbox
struct DropDownHandlerGearbox_t8AD5B054B30C7F3F3A6A0FE5D698EBCF7345BEFE;
// DropDownHandlerGomaHop
struct DropDownHandlerGomaHop_tC3343E792B3FA6C73E6CE2A626D2E7449B9DF73D;
// DropDownHandlerGuiaDeMuelle
struct DropDownHandlerGuiaDeMuelle_tC851957E0FDBCD577B891510FF1DFB787769C1F9;
// DropDownHandlerMosfet
struct DropDownHandlerMosfet_tFE41C42B0AC339D1ECC3E696D89C5C586D28EE40;
// DropDownHandlerMuelle
struct DropDownHandlerMuelle_t0A8E8E1330E2BAD5A501E135B42DCFC94CA3486E;
// DropDownHandlerNozzle
struct DropDownHandlerNozzle_tD13C58D6F1C773675C2AB0134F6C96E7C8D0490A;
// DropDownHandlerNub
struct DropDownHandlerNub_t3291D567FEB2BCA3659581ED9FB356B6C317E48F;
// DropDownHandlerPiston
struct DropDownHandlerPiston_t77866A4A0DE8B23BC9092FB3674F015B06842109;
// DropDownHandlerTapperPlate
struct DropDownHandlerTapperPlate_tAB05E5983A13525B787E9DD6A6504E1E0AFFA449;
// UnityEngine.UI.Dropdown
struct Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89;
// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F;
// UnityEngine.UI.Graphic
struct Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931;
// UnityEngine.UI.Image
struct Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t309E1C8C7CE885A0D2F98C84CEA77A8935688382;
// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_tB826EDF15DC80F71BCBCD8E410FD959A04C33F25;
// UnityEngine.RectTransform
struct RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5;
// UnityEngine.Renderer
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF;
// UnityEngine.UI.Selectable
struct Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712;
// UnityEngine.Sprite
struct Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62;
// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915;
// UnityEngine.UI.Dropdown/DropdownEvent
struct DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059;
// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F;
// UnityEngine.UI.Dropdown/OptionDataList
struct OptionDataList_t53255477D0A9C6980AB48693A520EFBC94DFFB96;

IL2CPP_EXTERN_C RuntimeClass* Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral002D0FA756EF46E5CC0ED7C4EADA2347F3D5A0DC;
IL2CPP_EXTERN_C String_t* _stringLiteral01226F2077A24DD906BC004072EC41D87B39284B;
IL2CPP_EXTERN_C String_t* _stringLiteral047498634A17E8FA61240674787E84D16F1C77AB;
IL2CPP_EXTERN_C String_t* _stringLiteral10FB04B14070C24E4738C39ABE147E7A57E102B7;
IL2CPP_EXTERN_C String_t* _stringLiteral2294E7DC619786B244545E97D823B83D355CF6BE;
IL2CPP_EXTERN_C String_t* _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745;
IL2CPP_EXTERN_C String_t* _stringLiteral26014C007F47E634C331A4FA9ABFFFD635A75C5E;
IL2CPP_EXTERN_C String_t* _stringLiteral2F6D805593B413AC973CE9C5EC17CA4C78508DC9;
IL2CPP_EXTERN_C String_t* _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED;
IL2CPP_EXTERN_C String_t* _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1;
IL2CPP_EXTERN_C String_t* _stringLiteral3CCEDB9B56E2B84E87134D756DA9D4163B98651E;
IL2CPP_EXTERN_C String_t* _stringLiteral4583DC5F865BA10566EA0C1A7052AFBD5F64925F;
IL2CPP_EXTERN_C String_t* _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE;
IL2CPP_EXTERN_C String_t* _stringLiteral4DE573297F69792F2A52321674F0590D7424E99A;
IL2CPP_EXTERN_C String_t* _stringLiteral4F3ABCDAF59D8FA7DA0C2AAA76E9C5784A781CF1;
IL2CPP_EXTERN_C String_t* _stringLiteral4FFDA33A1930AB06F6D39F0F75AC7C29FAFF6119;
IL2CPP_EXTERN_C String_t* _stringLiteral502C0122C400F22C3242F2FEB38F5D42DABE7996;
IL2CPP_EXTERN_C String_t* _stringLiteral6B17125BFE29CB975A7E9D3A67FC7104EC9AB7C3;
IL2CPP_EXTERN_C String_t* _stringLiteral6EE72FABC13DAF84001008FBDB3CD1D7E0CDECF8;
IL2CPP_EXTERN_C String_t* _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51;
IL2CPP_EXTERN_C String_t* _stringLiteral77433E583240E26992DA7E6DFAA703B2EED57EE4;
IL2CPP_EXTERN_C String_t* _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7;
IL2CPP_EXTERN_C String_t* _stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C;
IL2CPP_EXTERN_C String_t* _stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC;
IL2CPP_EXTERN_C String_t* _stringLiteral804BD6674F35C5D81F09F3D4D99EBF580BE9C6E0;
IL2CPP_EXTERN_C String_t* _stringLiteral84F1EA7C59FF2336381FF056AF0BA0BF616B7732;
IL2CPP_EXTERN_C String_t* _stringLiteral858843B1C1F4F954BE4ED57EA5219BE3910A9E8D;
IL2CPP_EXTERN_C String_t* _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9;
IL2CPP_EXTERN_C String_t* _stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510;
IL2CPP_EXTERN_C String_t* _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED;
IL2CPP_EXTERN_C String_t* _stringLiteral9CBEAD6D497673D701FDF242F8766A289057EA14;
IL2CPP_EXTERN_C String_t* _stringLiteralA203CCF97D1AE627C19EB69C6DCA52BE18A4A438;
IL2CPP_EXTERN_C String_t* _stringLiteralA4462CA9ADC2AD6448A6395345473E79E165913C;
IL2CPP_EXTERN_C String_t* _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834;
IL2CPP_EXTERN_C String_t* _stringLiteralA8F244FC674653C59552E40566FE156BB9BB79FD;
IL2CPP_EXTERN_C String_t* _stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF;
IL2CPP_EXTERN_C String_t* _stringLiteralBF81BBF805E3CA6AAA5C990682F8CCF6B2C4F5E7;
IL2CPP_EXTERN_C String_t* _stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E;
IL2CPP_EXTERN_C String_t* _stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116;
IL2CPP_EXTERN_C String_t* _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B;
IL2CPP_EXTERN_C String_t* _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704;
IL2CPP_EXTERN_C String_t* _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314;
IL2CPP_EXTERN_C String_t* _stringLiteralC9905A377B0481F1298530EBB4046F26866045AD;
IL2CPP_EXTERN_C String_t* _stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6;
IL2CPP_EXTERN_C String_t* _stringLiteralCBAEA7AE98BD922B4AF45BBA7ED2B8C50DFC7B99;
IL2CPP_EXTERN_C String_t* _stringLiteralCF87C5E11C6D5B7705B454E6429FD04CB1FBB8E3;
IL2CPP_EXTERN_C String_t* _stringLiteralD0645BCF97335DB32AF235D32E280B4D57A8465D;
IL2CPP_EXTERN_C String_t* _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D;
IL2CPP_EXTERN_C String_t* _stringLiteralDF510ED9801CFCE3E869CBFF940B43857E871D99;
IL2CPP_EXTERN_C String_t* _stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9;
IL2CPP_EXTERN_C String_t* _stringLiteralE4CFDEDE86B21B9DC88E2877BF3E1378C75D3333;
IL2CPP_EXTERN_C String_t* _stringLiteralF16220686C298462264A4DCD7410CE6F941EB0EB;
IL2CPP_EXTERN_C String_t* _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58;
IL2CPP_EXTERN_C String_t* _stringLiteralF83DD1C827D937E89B9D7B6506D0F30C133B3D16;
IL2CPP_EXTERN_C String_t* _stringLiteralFC4ABD7D01F00B4D077136F6A39C82C63CC9C000;
IL2CPP_EXTERN_C String_t* _stringLiteralFEA325339A560D2AFDE40181E7F52A64A5563B63;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandlerCaUF1on_U3CStartU3Eb__11_0_m9B7D9E4E7E364A736329813AB4BA2A14B048A812_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandlerCabezaCilindro_U3CStartU3Eb__11_0_m6875FB41A9C754225791135E51A92A24F0F70012_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandlerCabezaPiston_U3CStartU3Eb__11_0_m07769265758FD3A769226BE96ACA9252B63A69D3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandlerCamaraDeHop_U3CStartU3Eb__10_0_mAB4601E49DF183C8B62D0285C3E36F79338576E0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandlerCilindro_U3CStartU3Eb__11_0_m9124746DDB3AD4D17E8D48140234F227D6B09B77_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandlerEngranajes_U3CStartU3Eb__10_0_mD533AC3B640B672A2DF154F464303C994ACF5472_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandlerGatillo_U3CStartU3Eb__9_0_mC9966227B6D0AF0442B643E6C052377502B279CA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandlerGearbox_U3CStartU3Eb__9_0_mAC06C59EE5D60D83407F14666E54194722E96603_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandlerGomaHop_U3CStartU3Eb__11_0_m4B4BAE68C150400B7C51DBDB63D6C0FE2627BF0A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandlerGuiaDeMuelle_U3CStartU3Eb__10_0_m6339013E14DBF4D778320EA9AD722B604A7F5CDB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandlerMosfet_U3CStartU3Eb__9_0_m65B83DAF56665DAE980B0CE398DF8F6D9FA2C934_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandlerMuelle_U3CStartU3Eb__11_0_m3D9CAB8C350531D5B12E67230396AB80A0D567F9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandlerNozzle_U3CStartU3Eb__11_0_mE7253AF7301F0734BE270E30D436097400B56689_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandlerNub_U3CStartU3Eb__9_0_m2D04559A348CAE089EF1217B21237F6EA2C0B151_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandlerPiston_U3CStartU3Eb__11_0_mEFB1EE1B45CCF22BAAE3D2E819B80540781C4AB8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandlerTapperPlate_U3CStartU3Eb__10_0_mBAF90C067D081839DA913AFD018DC7468ADDDE56_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* DropDownHandler_U3CStartU3Eb__10_0_m167C537067D9AC7384513EE714F65305F32D2183_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_mC017BCCA057EBCF734567FB642B2632F4C94E093_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918;
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tBB65183F1134474D09FF49B95625D25472B9BA8B 
{
};

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710  : public RuntimeObject
{
	// System.Int32[] System.Collections.Generic.Dictionary`2::_buckets
	Int32U5BU5D_t19C97395396A72ECAF310612F0760F165060314C* ____buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::_entries
	EntryU5BU5D_t233BB24ED01E2D8D65B0651D54B8E3AD125CAF96* ____entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::_count
	int32_t ____count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeList
	int32_t ____freeList_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::_freeCount
	int32_t ____freeCount_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::_version
	int32_t ____version_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::_comparer
	RuntimeObject* ____comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_keys
	KeyCollection_tE66790F09E854C19C7F612BEAD203AE626E90A36* ____keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::_values
	ValueCollection_tC9D91E8A3198E40EA339059703AB10DFC9F5CC2E* ____values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject* ____syncRoot_9;
};

// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>
struct List_1_tE020B120302E34B781278C33685F2DA255337446  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	Dictionary_2U5BU5D_tF3731166E97A9D9E091E8FEDF8384CEAC68CF4D3* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tE020B120302E34B781278C33685F2DA255337446_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	Dictionary_2U5BU5D_tF3731166E97A9D9E091E8FEDF8384CEAC68CF4D3* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<Componente>
struct List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ComponenteU5BU5D_tCC3DFD5D47FE12641DF26F3B39B0B63F0C819BBC* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ComponenteU5BU5D_tCC3DFD5D47FE12641DF26F3B39B0B63F0C819BBC* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.Object>
struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<System.String>
struct List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___s_emptyArray_5;
};

// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55  : public RuntimeObject
{
	// T[] System.Collections.Generic.List`1::_items
	OptionDataU5BU5D_tF56CF744A9E6A0DBB0AC2072BE52F7C8D2E1947F* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject* ____syncRoot_4;
};

struct List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55_StaticFields
{
	// T[] System.Collections.Generic.List`1::s_emptyArray
	OptionDataU5BU5D_tF56CF744A9E6A0DBB0AC2072BE52F7C8D2E1947F* ___s_emptyArray_5;
};
struct Il2CppArrayBounds;

// Componente
struct Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F  : public RuntimeObject
{
	// System.String Componente::tipo
	String_t* ___tipo_0;
	// System.String Componente::marca
	String_t* ___marca_1;
	// System.String Componente::color
	String_t* ___color_2;
	// System.String Componente::material
	String_t* ___material_3;
	// System.String Componente::potencia
	String_t* ___potencia_4;
	// System.String Componente::precision
	String_t* ___precision_5;
	// System.String Componente::alcance
	String_t* ___alcance_6;
	// System.String Componente::cadencia
	String_t* ___cadencia_7;
	// System.String Componente::estabilidad
	String_t* ___estabilidad_8;
	// System.String Componente::precio
	String_t* ___precio_9;
	// System.String Componente::modelo
	String_t* ___modelo_10;
	// System.String Componente::extra
	String_t* ___extra_11;
};

// System.String
struct String_t  : public RuntimeObject
{
	// System.Int32 System.String::_stringLength
	int32_t ____stringLength_4;
	// System.Char System.String::_firstChar
	Il2CppChar ____firstChar_5;
};

struct String_t_StaticFields
{
	// System.String System.String::Empty
	String_t* ___Empty_6;
};

// UnityEngine.Events.UnityEventBase
struct UnityEventBase_t4968A4C72559F35C0923E4BD9C042C3A842E1DB8  : public RuntimeObject
{
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t309E1C8C7CE885A0D2F98C84CEA77A8935688382* ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_tB826EDF15DC80F71BCBCD8E410FD959A04C33F25* ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;
};

// System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F  : public RuntimeObject
{
};
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t6D9B272BD21782F0A9A14F2E41F85A50E97A986F_marshaled_com
{
};

// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F  : public RuntimeObject
{
	// System.String UnityEngine.UI.Dropdown/OptionData::m_Text
	String_t* ___m_Text_0;
	// UnityEngine.Sprite UnityEngine.UI.Dropdown/OptionData::m_Image
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_Image_1;
};

// System.Collections.Generic.List`1/Enumerator<Componente>
struct Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ____current_3;
};

// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	RuntimeObject* ____current_3;
};

// System.Collections.Generic.List`1/Enumerator<System.String>
struct Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 
{
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::_list
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ____list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_index
	int32_t ____index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::_version
	int32_t ____version_2;
	// T System.Collections.Generic.List`1/Enumerator::_current
	String_t* ____current_3;
};

// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_t7CC0661D6B113117B4CC68761D93AC8DF5DBD66A  : public UnityEventBase_t4968A4C72559F35C0923E4BD9C042C3A842E1DB8
{
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* ___m_InvokeArray_3;
};

// System.Boolean
struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22 
{
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;
};

struct Boolean_t09A6377A54BE2F9E6985A8149F19234FD7DDFE22_StaticFields
{
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;
};

// UnityEngine.Color
struct Color_tD001788D726C3A7F1379BEED0260B9591F440C1F 
{
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;
};

// System.Double
struct Double_tE150EF3D1D43DEE85D533810AB4C742307EEDE5F 
{
	// System.Double System.Double::m_value
	double ___m_value_0;
};

// System.Int32
struct Int32_t680FF22E76F6EFAD4375103CBBFFA0421349384C 
{
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;
};

// System.IntPtr
struct IntPtr_t 
{
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;
};

struct IntPtr_t_StaticFields
{
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;
};

// UnityEngine.UI.Navigation
struct Navigation_t4D2E201D65749CF4E104E8AC1232CF1D6F14795C 
{
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnRight_5;
};
// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t4D2E201D65749CF4E104E8AC1232CF1D6F14795C_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnUp_2;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnDown_3;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnLeft_4;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t4D2E201D65749CF4E104E8AC1232CF1D6F14795C_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnUp_2;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnDown_3;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnLeft_4;
	Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712* ___m_SelectOnRight_5;
};

// System.Single
struct Single_t4530F2FF86FCB0DC29F35385CA1BD21BE294761C 
{
	// System.Single System.Single::m_value
	float ___m_value_0;
};

// UnityEngine.UI.SpriteState
struct SpriteState_tC8199570BE6337FB5C49347C97892B4222E5AACD 
{
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_DisabledSprite_3;
};
// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_tC8199570BE6337FB5C49347C97892B4222E5AACD_marshaled_pinvoke
{
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_HighlightedSprite_0;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_PressedSprite_1;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_SelectedSprite_2;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_tC8199570BE6337FB5C49347C97892B4222E5AACD_marshaled_com
{
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_HighlightedSprite_0;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_PressedSprite_1;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_SelectedSprite_2;
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___m_DisabledSprite_3;
};

// System.Void
struct Void_t4861ACF8F4594C3437BB48B6E56783494B843915 
{
	union
	{
		struct
		{
		};
		uint8_t Void_t4861ACF8F4594C3437BB48B6E56783494B843915__padding[1];
	};
};

// UnityEngine.UI.ColorBlock
struct ColorBlock_tDD7C62E7AFE442652FC98F8D058CE8AE6BFD7C11 
{
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;
};

struct ColorBlock_tDD7C62E7AFE442652FC98F8D058CE8AE6BFD7C11_StaticFields
{
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_tDD7C62E7AFE442652FC98F8D058CE8AE6BFD7C11 ___defaultColorBlock_7;
};

// System.Delegate
struct Delegate_t  : public RuntimeObject
{
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject* ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.IntPtr System.Delegate::interp_method
	intptr_t ___interp_method_7;
	// System.IntPtr System.Delegate::interp_invoke_impl
	intptr_t ___interp_invoke_impl_8;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t* ___method_info_9;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t* ___original_method_info_10;
	// System.DelegateData System.Delegate::data
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_12;
};
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	intptr_t ___interp_method_7;
	intptr_t ___interp_invoke_impl_8;
	MethodInfo_t* ___method_info_9;
	MethodInfo_t* ___original_method_info_10;
	DelegateData_t9B286B493293CD2D23A5B2B5EF0E5B1324C2B77E* ___data_11;
	int32_t ___method_is_virtual_12;
};

// UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C  : public RuntimeObject
{
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;
};

struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_StaticFields
{
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;
};
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.UI.Dropdown/DropdownEvent
struct DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059  : public UnityEvent_1_t7CC0661D6B113117B4CC68761D93AC8DF5DBD66A
{
};

// UnityEngine.Component
struct Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.GameObject
struct GameObject_t76FEDD663AB33C991A9C9A23129337651094216F  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// UnityEngine.Material
struct Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3  : public Object_tC12DECB6760A7F2CBF65D9DCF18D044C2D97152C
{
};

// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tC5AB7E8F745616680F337909D3A8E6C722CDF771* ___delegates_13;
};
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_13;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_13;
};

// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60  : public MulticastDelegate_t
{
};

// UnityEngine.Behaviour
struct Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.Renderer
struct Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF  : public Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3
{
};

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71  : public Behaviour_t01970CFBBA658497AE30F311C447DB0440BAB7FA
{
};

// ButtonHandlerGraph
struct ButtonHandlerGraph_tDC9C59A005B94B0D6306A5C0FC4F47E2467B470D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.GameObject ButtonHandlerGraph::graphContainer
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___graphContainer_4;
	// System.Boolean ButtonHandlerGraph::cambiar
	bool ___cambiar_5;
};

// ButtonHandlerSave
struct ButtonHandlerSave_t64D9EBC89B5FD212EC815917A3E67E66C8305A93  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// ButtonHandlerSkybox
struct ButtonHandlerSkybox_t4BFA57953E7D268FF9BC01289F6766FD1358F049  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Material ButtonHandlerSkybox::SkyBoxForest
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___SkyBoxForest_4;
	// UnityEngine.Material ButtonHandlerSkybox::SkyBoxWarehouse
	Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___SkyBoxWarehouse_5;
	// System.Boolean ButtonHandlerSkybox::cambiar
	bool ___cambiar_6;
};

// CargarExcel
struct CargarExcel_t6CA25EC14B078230E56573150445E548D27C5E4C  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente CargarExcel::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// System.Collections.Generic.List`1<Componente> CargarExcel::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_5;
	// System.Collections.Generic.List`1<System.String> CargarExcel::items
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___items_6;
};

// DropDownHandler
struct DropDownHandler_t8002662C419AC287ADA29E30AF224815CC7D12C7  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandler::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandler::motor
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___motor_5;
	// System.Double DropDownHandler::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandler::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandler::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandler::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Single DropDownHandler::varianzaDrop
	float ___varianzaDrop_10;
	// System.Double DropDownHandler::precio
	double ___precio_11;
	// System.String DropDownHandler::extra
	String_t* ___extra_12;
	// System.Collections.Generic.List`1<Componente> DropDownHandler::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_13;
};

// DropDownHandlerCabezaCilindro
struct DropDownHandlerCabezaCilindro_tC63ABBA0B4BAC7AF05823FABC92495D3538FABEC  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandlerCabezaCilindro::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandlerCabezaCilindro::CabezaCilindro
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___CabezaCilindro_5;
	// System.Double DropDownHandlerCabezaCilindro::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandlerCabezaCilindro::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandlerCabezaCilindro::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandlerCabezaCilindro::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Single DropDownHandlerCabezaCilindro::varianzaDrop
	float ___varianzaDrop_10;
	// System.Double DropDownHandlerCabezaCilindro::precio
	double ___precio_11;
	// System.String DropDownHandlerCabezaCilindro::extra
	String_t* ___extra_12;
	// UnityEngine.GameObject DropDownHandlerCabezaCilindro::CabezaCilindroPieza
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___CabezaCilindroPieza_13;
	// System.Collections.Generic.List`1<Componente> DropDownHandlerCabezaCilindro::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_14;
};

// DropDownHandlerCabezaPiston
struct DropDownHandlerCabezaPiston_t5D4F7822D76A01DD44715998B4C3911DC1DBE889  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandlerCabezaPiston::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandlerCabezaPiston::CabezaPiston
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___CabezaPiston_5;
	// System.Double DropDownHandlerCabezaPiston::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandlerCabezaPiston::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandlerCabezaPiston::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandlerCabezaPiston::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Single DropDownHandlerCabezaPiston::varianzaDrop
	float ___varianzaDrop_10;
	// System.Double DropDownHandlerCabezaPiston::precio
	double ___precio_11;
	// System.String DropDownHandlerCabezaPiston::extra
	String_t* ___extra_12;
	// System.Collections.Generic.List`1<Componente> DropDownHandlerCabezaPiston::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_13;
	// UnityEngine.GameObject DropDownHandlerCabezaPiston::CabezaPistonPieza
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___CabezaPistonPieza_14;
};

// DropDownHandlerCamaraDeHop
struct DropDownHandlerCamaraDeHop_t9C574C9C85718804384E4CA248294DDC9E19F1EB  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandlerCamaraDeHop::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandlerCamaraDeHop::CamaraDeHop
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___CamaraDeHop_5;
	// System.Double DropDownHandlerCamaraDeHop::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandlerCamaraDeHop::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandlerCamaraDeHop::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandlerCamaraDeHop::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Double DropDownHandlerCamaraDeHop::precio
	double ___precio_10;
	// System.String DropDownHandlerCamaraDeHop::extra
	String_t* ___extra_11;
	// UnityEngine.GameObject DropDownHandlerCamaraDeHop::CamaraHopPieza
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___CamaraHopPieza_12;
	// System.Collections.Generic.List`1<Componente> DropDownHandlerCamaraDeHop::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_13;
};

// DropDownHandlerCa?on
struct DropDownHandlerCaUF1on_tB8652C4AED37060D00139A5D51D4FBB60BABB0C5  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandlerCa?on::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandlerCa?on::Ca?on
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___CaUF1on_5;
	// System.Double DropDownHandlerCa?on::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandlerCa?on::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandlerCa?on::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandlerCa?on::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Single DropDownHandlerCa?on::varianzaDrop
	float ___varianzaDrop_10;
	// System.Double DropDownHandlerCa?on::precio
	double ___precio_11;
	// System.String DropDownHandlerCa?on::extra
	String_t* ___extra_12;
	// UnityEngine.GameObject DropDownHandlerCa?on::Ca?onPieza
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___CaUF1onPieza_13;
	// System.Collections.Generic.List`1<Componente> DropDownHandlerCa?on::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_14;
};

// DropDownHandlerCilindro
struct DropDownHandlerCilindro_t95B6802C5FF3BA348A912FEDA7D68C8643581D52  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandlerCilindro::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandlerCilindro::Cilindro
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Cilindro_5;
	// System.Double DropDownHandlerCilindro::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandlerCilindro::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandlerCilindro::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandlerCilindro::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Single DropDownHandlerCilindro::varianzaDrop
	float ___varianzaDrop_10;
	// System.Double DropDownHandlerCilindro::precio
	double ___precio_11;
	// System.String DropDownHandlerCilindro::extra
	String_t* ___extra_12;
	// UnityEngine.GameObject DropDownHandlerCilindro::CilindroPieza
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___CilindroPieza_13;
	// System.Collections.Generic.List`1<Componente> DropDownHandlerCilindro::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_14;
};

// DropDownHandlerEngranajes
struct DropDownHandlerEngranajes_t062A4B96E3458FE9041B420AB0B343FF68F03D24  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandlerEngranajes::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandlerEngranajes::Engranajes
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Engranajes_5;
	// System.Double DropDownHandlerEngranajes::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandlerEngranajes::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandlerEngranajes::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandlerEngranajes::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Single DropDownHandlerEngranajes::varianzaDrop
	float ___varianzaDrop_10;
	// System.Double DropDownHandlerEngranajes::precio
	double ___precio_11;
	// System.String DropDownHandlerEngranajes::extra
	String_t* ___extra_12;
	// System.Collections.Generic.List`1<Componente> DropDownHandlerEngranajes::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_13;
};

// DropDownHandlerGatillo
struct DropDownHandlerGatillo_t74F9CFF132D5EC7C28BB31711DB8AC2A6DC6B240  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandlerGatillo::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandlerGatillo::Gatillo
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Gatillo_5;
	// System.Double DropDownHandlerGatillo::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandlerGatillo::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandlerGatillo::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandlerGatillo::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Double DropDownHandlerGatillo::precio
	double ___precio_10;
	// System.String DropDownHandlerGatillo::extra
	String_t* ___extra_11;
	// System.Collections.Generic.List`1<Componente> DropDownHandlerGatillo::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_12;
};

// DropDownHandlerGearbox
struct DropDownHandlerGearbox_t8AD5B054B30C7F3F3A6A0FE5D698EBCF7345BEFE  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandlerGearbox::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandlerGearbox::Gearbox
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Gearbox_5;
	// System.Double DropDownHandlerGearbox::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandlerGearbox::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandlerGearbox::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandlerGearbox::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Double DropDownHandlerGearbox::precio
	double ___precio_10;
	// System.String DropDownHandlerGearbox::extra
	String_t* ___extra_11;
	// System.Collections.Generic.List`1<Componente> DropDownHandlerGearbox::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_12;
};

// DropDownHandlerGomaHop
struct DropDownHandlerGomaHop_tC3343E792B3FA6C73E6CE2A626D2E7449B9DF73D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandlerGomaHop::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandlerGomaHop::GomaHop
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___GomaHop_5;
	// System.Double DropDownHandlerGomaHop::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandlerGomaHop::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandlerGomaHop::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandlerGomaHop::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Single DropDownHandlerGomaHop::varianzaDrop
	float ___varianzaDrop_10;
	// System.Double DropDownHandlerGomaHop::precio
	double ___precio_11;
	// System.String DropDownHandlerGomaHop::extra
	String_t* ___extra_12;
	// UnityEngine.GameObject DropDownHandlerGomaHop::GomaHopPieza
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___GomaHopPieza_13;
	// System.Collections.Generic.List`1<Componente> DropDownHandlerGomaHop::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_14;
};

// DropDownHandlerGuiaDeMuelle
struct DropDownHandlerGuiaDeMuelle_tC851957E0FDBCD577B891510FF1DFB787769C1F9  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandlerGuiaDeMuelle::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandlerGuiaDeMuelle::GuiaDeMuelle
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___GuiaDeMuelle_5;
	// System.Double DropDownHandlerGuiaDeMuelle::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandlerGuiaDeMuelle::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandlerGuiaDeMuelle::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandlerGuiaDeMuelle::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Double DropDownHandlerGuiaDeMuelle::precio
	double ___precio_10;
	// System.String DropDownHandlerGuiaDeMuelle::extra
	String_t* ___extra_11;
	// System.Collections.Generic.List`1<Componente> DropDownHandlerGuiaDeMuelle::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_12;
	// UnityEngine.GameObject DropDownHandlerGuiaDeMuelle::GuiaMuellePieza
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___GuiaMuellePieza_13;
};

// DropDownHandlerMosfet
struct DropDownHandlerMosfet_tFE41C42B0AC339D1ECC3E696D89C5C586D28EE40  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandlerMosfet::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandlerMosfet::Mosfet
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Mosfet_5;
	// System.Double DropDownHandlerMosfet::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandlerMosfet::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandlerMosfet::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandlerMosfet::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Double DropDownHandlerMosfet::precio
	double ___precio_10;
	// System.String DropDownHandlerMosfet::extra
	String_t* ___extra_11;
	// System.Collections.Generic.List`1<Componente> DropDownHandlerMosfet::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_12;
};

// DropDownHandlerMuelle
struct DropDownHandlerMuelle_t0A8E8E1330E2BAD5A501E135B42DCFC94CA3486E  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandlerMuelle::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandlerMuelle::Muelle
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Muelle_5;
	// System.Double DropDownHandlerMuelle::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandlerMuelle::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandlerMuelle::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandlerMuelle::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Single DropDownHandlerMuelle::varianzaDrop
	float ___varianzaDrop_10;
	// System.Double DropDownHandlerMuelle::precio
	double ___precio_11;
	// System.String DropDownHandlerMuelle::extra
	String_t* ___extra_12;
	// UnityEngine.GameObject DropDownHandlerMuelle::MuellePieza
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___MuellePieza_13;
	// System.Collections.Generic.List`1<Componente> DropDownHandlerMuelle::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_14;
};

// DropDownHandlerNozzle
struct DropDownHandlerNozzle_tD13C58D6F1C773675C2AB0134F6C96E7C8D0490A  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandlerNozzle::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandlerNozzle::Nozzle
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Nozzle_5;
	// System.Double DropDownHandlerNozzle::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandlerNozzle::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandlerNozzle::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandlerNozzle::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Single DropDownHandlerNozzle::varianzaDrop
	float ___varianzaDrop_10;
	// System.Double DropDownHandlerNozzle::precio
	double ___precio_11;
	// System.String DropDownHandlerNozzle::extra
	String_t* ___extra_12;
	// System.Collections.Generic.List`1<Componente> DropDownHandlerNozzle::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_13;
	// UnityEngine.GameObject DropDownHandlerNozzle::nozzlePieza
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___nozzlePieza_14;
};

// DropDownHandlerNub
struct DropDownHandlerNub_t3291D567FEB2BCA3659581ED9FB356B6C317E48F  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandlerNub::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandlerNub::Nub
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Nub_5;
	// System.Double DropDownHandlerNub::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandlerNub::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandlerNub::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandlerNub::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Double DropDownHandlerNub::precio
	double ___precio_10;
	// System.Collections.Generic.List`1<Componente> DropDownHandlerNub::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_11;
	// UnityEngine.GameObject DropDownHandlerNub::nubPieza
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___nubPieza_12;
};

// DropDownHandlerPiston
struct DropDownHandlerPiston_t77866A4A0DE8B23BC9092FB3674F015B06842109  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandlerPiston::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandlerPiston::Piston
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___Piston_5;
	// System.Double DropDownHandlerPiston::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandlerPiston::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandlerPiston::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandlerPiston::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Single DropDownHandlerPiston::varianzaDrop
	float ___varianzaDrop_10;
	// System.Double DropDownHandlerPiston::precio
	double ___precio_11;
	// System.String DropDownHandlerPiston::extra
	String_t* ___extra_12;
	// System.Collections.Generic.List`1<Componente> DropDownHandlerPiston::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_13;
	// UnityEngine.GameObject DropDownHandlerPiston::PistonPieza
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___PistonPieza_14;
};

// DropDownHandlerTapperPlate
struct DropDownHandlerTapperPlate_tAB05E5983A13525B787E9DD6A6504E1E0AFFA449  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// Componente DropDownHandlerTapperPlate::blankItem
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem_4;
	// UnityEngine.UI.Dropdown DropDownHandlerTapperPlate::TapperPlate
	Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___TapperPlate_5;
	// System.Double DropDownHandlerTapperPlate::potenciaDrop
	double ___potenciaDrop_6;
	// System.Double DropDownHandlerTapperPlate::alcanceDrop
	double ___alcanceDrop_7;
	// System.Double DropDownHandlerTapperPlate::precisionDrop
	double ___precisionDrop_8;
	// System.Double DropDownHandlerTapperPlate::cadenciaDrop
	double ___cadenciaDrop_9;
	// System.Double DropDownHandlerTapperPlate::precio
	double ___precio_10;
	// System.String DropDownHandlerTapperPlate::extra
	String_t* ___extra_11;
	// System.Collections.Generic.List`1<Componente> DropDownHandlerTapperPlate::itemDatabase
	List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase_12;
	// UnityEngine.GameObject DropDownHandlerTapperPlate::TapperPlatePieza
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___TapperPlatePieza_13;
};

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
};

// Window_Graph
struct Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8  : public MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71
{
	// UnityEngine.Sprite Window_Graph::circleSprite
	Sprite_tAFF74BC83CD68037494CB0B4F28CBDF8971CAB99* ___circleSprite_4;
	// UnityEngine.RectTransform Window_Graph::graphContainer
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___graphContainer_5;
	// UnityEngine.RectTransform Window_Graph::labelTemplateX
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___labelTemplateX_6;
	// UnityEngine.RectTransform Window_Graph::labelTemplateY
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___labelTemplateY_7;
	// UnityEngine.GameObject Window_Graph::potenciaBar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___potenciaBar_15;
	// UnityEngine.GameObject Window_Graph::cadenciaBar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___cadenciaBar_16;
	// UnityEngine.GameObject Window_Graph::alcanceBar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___alcanceBar_17;
	// UnityEngine.GameObject Window_Graph::precisionBar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___precisionBar_18;
	// UnityEngine.GameObject Window_Graph::varianzaBar
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___varianzaBar_19;
	// UnityEngine.UI.Text Window_Graph::txtPotencia
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___txtPotencia_20;
	// UnityEngine.UI.Text Window_Graph::txtPrecio
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___txtPrecio_21;
	// UnityEngine.UI.Text Window_Graph::txtExtra
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___txtExtra_22;
	// UnityEngine.UI.Text Window_Graph::txtMaxPre
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___txtMaxPre_23;
	// UnityEngine.UI.Text Window_Graph::txtMaxAlc
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___txtMaxAlc_24;
};

struct Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields
{
	// System.Double Window_Graph::potencia
	double ___potencia_8;
	// System.Double Window_Graph::cadencia
	double ___cadencia_9;
	// System.Double Window_Graph::alcance
	double ___alcance_10;
	// System.Double Window_Graph::precision
	double ___precision_11;
	// System.Double Window_Graph::varianza
	double ___varianza_12;
	// System.Double Window_Graph::precio
	double ___precio_13;
	// System.String Window_Graph::extra
	String_t* ___extra_14;
};

// UnityEngine.UI.Selectable
struct Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712  : public UIBehaviour_tB9D4295827BD2EEDEF0749200C6CA7090C742A9D
{
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t4D2E201D65749CF4E104E8AC1232CF1D6F14795C ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_tDD7C62E7AFE442652FC98F8D058CE8AE6BFD7C11 ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_tC8199570BE6337FB5C49347C97892B4222E5AACD ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tA0DC06F89C5280C6DD972F6F4C8A56D7F4F79074* ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tCBFCA4585A19E2B75465AECFEAC43F4016BF7931* ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t2CDCA768E7F493F5EDEBC75AEB200FD621354E35* ___m_CanvasGroupCache_19;
};

struct Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712_StaticFields
{
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_t4160E135F02A40F75A63F787D36F31FEC6FE91A9* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;
};

// UnityEngine.UI.Dropdown
struct Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89  : public Selectable_t3251808068A17B8E92FB33590A4C2FA66D456712
{
	// UnityEngine.RectTransform UnityEngine.UI.Dropdown::m_Template
	RectTransform_t6C5DA5E41A89E0F488B001E45E58963480E543A5* ___m_Template_20;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_CaptionText
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___m_CaptionText_21;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_CaptionImage
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___m_CaptionImage_22;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_ItemText
	Text_tD60B2346DAA6666BF0D822FF607F0B220C2B9E62* ___m_ItemText_23;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_ItemImage
	Image_tBC1D03F63BF71132E9A5E472B8742F172A011E7E* ___m_ItemImage_24;
	// System.Int32 UnityEngine.UI.Dropdown::m_Value
	int32_t ___m_Value_25;
	// UnityEngine.UI.Dropdown/OptionDataList UnityEngine.UI.Dropdown::m_Options
	OptionDataList_t53255477D0A9C6980AB48693A520EFBC94DFFB96* ___m_Options_26;
	// UnityEngine.UI.Dropdown/DropdownEvent UnityEngine.UI.Dropdown::m_OnValueChanged
	DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* ___m_OnValueChanged_27;
	// System.Single UnityEngine.UI.Dropdown::m_AlphaFadeSpeed
	float ___m_AlphaFadeSpeed_28;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Dropdown
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___m_Dropdown_29;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Blocker
	GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___m_Blocker_30;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem> UnityEngine.UI.Dropdown::m_Items
	List_1_t89B39292AD45371F7FDCB295AAE956D33588BC6E* ___m_Items_31;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween> UnityEngine.UI.Dropdown::m_AlphaTweenRunner
	TweenRunner_1_t830EC096236A3CEC7189DFA6E0B2E74C5C97780B* ___m_AlphaTweenRunner_32;
	// System.Boolean UnityEngine.UI.Dropdown::validTemplate
	bool ___validTemplate_33;
};

struct Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_StaticFields
{
	// UnityEngine.UI.Dropdown/OptionData UnityEngine.UI.Dropdown::s_NoOptionData
	OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* ___s_NoOptionData_35;
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248  : public RuntimeArray
{
	ALIGN_FIELD (8) String_t* m_Items[1];

	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918  : public RuntimeArray
{
	ALIGN_FIELD (8) RuntimeObject* m_Items[1];

	inline RuntimeObject* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, int32_t ___index0, const RuntimeMethod* method) ;
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Object>::get_Item(TKey)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Dictionary_2_get_Item_m4AAAECBE902A211BF2126E6AFA280AEF73A3E0D6_gshared (Dictionary_2_t14FE4A752A83D53771C584E4C8D14E01F2AFD7BA* __this, RuntimeObject* ___key0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) ;
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A List_1_GetEnumerator_mD8294A7FA2BEB1929487127D476F8EC1CDC23BFC_gshared (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mD9DC3E3C3697830A4823047AB29A77DBBB5ED419_gshared (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mE921CC8F29FBBDE7CC3209A0ED0D921D58D00BCB_gshared (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B_gshared (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_gshared (UnityEvent_1_t7CC0661D6B113117B4CC68761D93AC8DF5DBD66A* __this, UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* ___call0, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method) ;

// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92 (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, bool ___value0, const RuntimeMethod* method) ;
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E (MonoBehaviour_t532A11E69716D348D8AA7F854AFCBFCB8AD17F71* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.ScreenCapture::CaptureScreenshot(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ScreenCapture_CaptureScreenshot_m1FAFDB15448C41E697212E54F852DDD196E15CFF (String_t* ___filename0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219 (RuntimeObject* ___message0, const RuntimeMethod* method) ;
// System.Void UnityEngine.RenderSettings::set_skybox(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderSettings_set_skybox_mC520BDF769C5B2824BE2CCC4ADC9CEBEE17A60B4 (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* ___value0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<Componente>::Clear()
inline void List_1_Clear_mC017BCCA057EBCF734567FB642B2632F4C94E093_inline (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*, const RuntimeMethod*))List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline)(__this, method);
}
// System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>> LeerExcel::Read(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tE020B120302E34B781278C33685F2DA255337446* LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05 (String_t* ___file0, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Item(System.Int32)
inline Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752 (List_1_tE020B120302E34B781278C33685F2DA255337446* __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* (*) (List_1_tE020B120302E34B781278C33685F2DA255337446*, int32_t, const RuntimeMethod*))List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared)(__this, ___index0, method);
}
// TValue System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(TKey)
inline RuntimeObject* Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5 (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* __this, String_t* ___key0, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710*, String_t*, const RuntimeMethod*))Dictionary_2_get_Item_m4AAAECBE902A211BF2126E6AFA280AEF73A3E0D6_gshared)(__this, ___key0, method);
}
// System.Void Componente::.ctor(Componente)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756 (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* __this, Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___p0, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<System.String>::Add(T)
inline void List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* __this, String_t* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, String_t*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___item0, method);
}
// System.Void System.Collections.Generic.List`1<Componente>::Add(T)
inline void List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* __this, Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*, Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___item0, method);
}
// System.Int32 System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Object>>::get_Count()
inline int32_t List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline (List_1_tE020B120302E34B781278C33685F2DA255337446* __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tE020B120302E34B781278C33685F2DA255337446*, const RuntimeMethod*))List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Componente>::.ctor()
inline void List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6 (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, const RuntimeMethod*))List_1__ctor_m7F078BB342729BDF11327FD89D7872265328F690_gshared)(__this, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2 (RuntimeObject* __this, const RuntimeMethod* method) ;
// T UnityEngine.Component::GetComponent<UnityEngine.UI.Dropdown>()
inline Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00 (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3* __this, const RuntimeMethod* method)
{
	return ((  Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* (*) (Component_t39FBE53E5EFCF4409111FB22C15FF73717632EC3*, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m7181F81CAEC2CF53F5D2BC79B7425C16E1F80D33_gshared)(__this, method);
}
// System.String System.String::Concat(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0 (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* ___values0, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.String>::GetEnumerator()
inline Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 (*) (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*, const RuntimeMethod*))List_1_GetEnumerator_mD8294A7FA2BEB1929487127D476F8EC1CDC23BFC_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.String>::Dispose()
inline void Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7 (Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1*, const RuntimeMethod*))Enumerator_Dispose_mD9DC3E3C3697830A4823047AB29A77DBBB5ED419_gshared)(__this, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.String>::get_Current()
inline String_t* Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline (Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1* __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1*, const RuntimeMethod*))Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline)(__this, method);
}
// System.Boolean System.String::Contains(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3 (String_t* __this, String_t* ___value0, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData> UnityEngine.UI.Dropdown::get_options()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816 (Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.UI.Dropdown/OptionData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166 (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.UI.Dropdown/OptionData::set_text(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* __this, String_t* ___value0, const RuntimeMethod* method) ;
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::Add(T)
inline void List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline (List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* __this, OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55*, OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*, const RuntimeMethod*))List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.String>::MoveNext()
inline bool Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED (Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1*, const RuntimeMethod*))Enumerator_MoveNext_mE921CC8F29FBBDE7CC3209A0ED0D921D58D00BCB_gshared)(__this, method);
}
// System.Void UnityEngine.UI.Dropdown::RefreshShownValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E (Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* __this, const RuntimeMethod* method) ;
// UnityEngine.UI.Dropdown/DropdownEvent UnityEngine.UI.Dropdown::get_onValueChanged()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline (Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* __this, const RuntimeMethod* method) ;
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* __this, RuntimeObject* ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*, RuntimeObject*, intptr_t, const RuntimeMethod*))UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B_gshared)(__this, ___object0, ___method1, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::AddListener(UnityEngine.Events.UnityAction`1<T0>)
inline void UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE (UnityEvent_1_t7CC0661D6B113117B4CC68761D93AC8DF5DBD66A* __this, UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* ___call0, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_t7CC0661D6B113117B4CC68761D93AC8DF5DBD66A*, UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*, const RuntimeMethod*))UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_gshared)(__this, ___call0, method);
}
// System.Void Window_Graph::setPotencia(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E (double ___potenciaEnviada0, double ___potenciaRestar1, const RuntimeMethod* method) ;
// System.Void Window_Graph::setAlcance(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2 (double ___alcanceEnviada0, double ___alcanceRestar1, const RuntimeMethod* method) ;
// System.Void Window_Graph::setVarianza(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8 (float ___varianzaEnviada0, float ___varianzaRestar1, const RuntimeMethod* method) ;
// System.Void Window_Graph::setPrecision(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6 (double ___precisionEnviada0, double ___precisionRestar1, const RuntimeMethod* method) ;
// System.Void Window_Graph::setCadencia(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142 (double ___cadenciaEnviada0, double ___cadenciaRestar1, const RuntimeMethod* method) ;
// System.Void Window_Graph::setPrecio(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2 (double ___precioEnviada0, double ___precioRestar1, const RuntimeMethod* method) ;
// System.Int32 UnityEngine.UI.Dropdown::get_value()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline (Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* __this, const RuntimeMethod* method) ;
// T System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::get_Item(System.Int32)
inline OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366 (List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* (*) (List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55*, int32_t, const RuntimeMethod*))List_1_get_Item_m33561245D64798C2AB07584C0EC4F240E4839A38_gshared)(__this, ___index0, method);
}
// System.String UnityEngine.UI.Dropdown/OptionData::get_text()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* __this, const RuntimeMethod* method) ;
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<Componente>::GetEnumerator()
inline Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443 (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 (*) (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*, const RuntimeMethod*))List_1_GetEnumerator_mD8294A7FA2BEB1929487127D476F8EC1CDC23BFC_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Componente>::Dispose()
inline void Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A (Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80* __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80*, const RuntimeMethod*))Enumerator_Dispose_mD9DC3E3C3697830A4823047AB29A77DBBB5ED419_gshared)(__this, method);
}
// T System.Collections.Generic.List`1/Enumerator<Componente>::get_Current()
inline Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline (Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80* __this, const RuntimeMethod* method)
{
	return ((  Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* (*) (Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80*, const RuntimeMethod*))Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline)(__this, method);
}
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method) ;
// System.Int32 System.Int32::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767 (String_t* ___s0, const RuntimeMethod* method) ;
// System.Boolean System.Collections.Generic.List`1/Enumerator<Componente>::MoveNext()
inline bool Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0 (Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80* __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80*, const RuntimeMethod*))Enumerator_MoveNext_mE921CC8F29FBBDE7CC3209A0ED0D921D58D00BCB_gshared)(__this, method);
}
// System.Void DropDownHandler::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandler_dropDownSelected_mAA8F5B15E84690C47CD8C315DF6DCF94C40A0FC8 (DropDownHandler_t8002662C419AC287ADA29E30AF224815CC7D12C7* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51 (String_t* ___name0, const RuntimeMethod* method) ;
// T UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
inline Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* __this, const RuntimeMethod* method)
{
	return ((  Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* (*) (GameObject_t76FEDD663AB33C991A9C9A23129337651094216F*, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_m6EAED4AA356F0F48288F67899E5958792395563B_gshared)(__this, method);
}
// UnityEngine.Material UnityEngine.Renderer::get_material()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656 (Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* __this, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_red()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline (const RuntimeMethod* method) ;
// System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F (Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* __this, String_t* ___name0, Color_tD001788D726C3A7F1379BEED0260B9591F440C1F ___value1, const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_gray()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline (const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_blue()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline (const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_black()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline (const RuntimeMethod* method) ;
// UnityEngine.Color UnityEngine.Color::get_yellow()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline (const RuntimeMethod* method) ;
// System.Void DropDownHandlerCabezaCilindro::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCabezaCilindro_dropDownSelected_mDAE9FEB10D333AD54F00BA2343251E893D5808B3 (DropDownHandlerCabezaCilindro_tC63ABBA0B4BAC7AF05823FABC92495D3538FABEC* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// System.Void DropDownHandlerCabezaPiston::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCabezaPiston_dropDownSelected_m9FEAB9E9AEDE859C229C91DD5496946EAFF6FA04 (DropDownHandlerCabezaPiston_t5D4F7822D76A01DD44715998B4C3911DC1DBE889* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// System.Void Window_Graph::setExtra(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Window_Graph_setExtra_m8774145D45E480AAFA50F295A5FB4E00A289FF91_inline (String_t* ___extraEnviada0, const RuntimeMethod* method) ;
// System.Void DropDownHandlerCamaraDeHop::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCamaraDeHop_dropDownSelected_mC9EA5084FE00C36BCB86EDF82DFA6BD4632505A1 (DropDownHandlerCamaraDeHop_t9C574C9C85718804384E4CA248294DDC9E19F1EB* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// System.Void DropDownHandlerCa?on::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCaUF1on_dropDownSelected_m37BA963612034E0DFD25BC0202DD2980285F31E8 (DropDownHandlerCaUF1on_tB8652C4AED37060D00139A5D51D4FBB60BABB0C5* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// System.Void DropDownHandlerCilindro::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCilindro_dropDownSelected_mDE0E057333B47B24CD2D65ABE0045CF1F23666F3 (DropDownHandlerCilindro_t95B6802C5FF3BA348A912FEDA7D68C8643581D52* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// System.Void DropDownHandlerEngranajes::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerEngranajes_dropDownSelected_m870DD902E40F08063754C3D04D1C61B65090AD87 (DropDownHandlerEngranajes_t062A4B96E3458FE9041B420AB0B343FF68F03D24* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// System.Double System.Double::Parse(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double Double_Parse_mBED785C952A63E8D714E429A4A704BCC4D92931B (String_t* ___s0, const RuntimeMethod* method) ;
// System.Void DropDownHandlerGatillo::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGatillo_dropDownSelected_m5D8532C5BB3F3EFB3CAEAB54F90EEEF952C7B127 (DropDownHandlerGatillo_t74F9CFF132D5EC7C28BB31711DB8AC2A6DC6B240* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// System.Void DropDownHandlerGearbox::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGearbox_dropDownSelected_m2DEB8AF72C0222FEAF1E9C82D27D3E8C328963A3 (DropDownHandlerGearbox_t8AD5B054B30C7F3F3A6A0FE5D698EBCF7345BEFE* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// System.String System.Double::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339 (double* __this, const RuntimeMethod* method) ;
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method) ;
// System.Void DropDownHandlerGomaHop::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGomaHop_dropDownSelected_mB46A37DDC4F4EB2DACBEBF35E0AF8E7D8748B23C (DropDownHandlerGomaHop_tC3343E792B3FA6C73E6CE2A626D2E7449B9DF73D* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// System.Void DropDownHandlerGuiaDeMuelle::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGuiaDeMuelle_dropDownSelected_m733D23725483F1F6490FCDE21D7B4B3BA6AEA443 (DropDownHandlerGuiaDeMuelle_tC851957E0FDBCD577B891510FF1DFB787769C1F9* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// System.Void DropDownHandlerMosfet::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerMosfet_dropDownSelected_mDB38E8B59C4BD1A37B3904DDD4140DD0B908D29D (DropDownHandlerMosfet_tFE41C42B0AC339D1ECC3E696D89C5C586D28EE40* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// System.Void DropDownHandlerMuelle::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerMuelle_dropDownSelected_m11D7CA3A3A1DA2257974480DE1B4DEA71AC3DF60 (DropDownHandlerMuelle_t0A8E8E1330E2BAD5A501E135B42DCFC94CA3486E* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// System.Void DropDownHandlerNozzle::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerNozzle_dropDownSelected_m904609CE01849C3F4E66CB1B76942E9995CFFDD3 (DropDownHandlerNozzle_tD13C58D6F1C773675C2AB0134F6C96E7C8D0490A* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// System.Void DropDownHandlerNub::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerNub_dropDownSelected_m6B267A9871CA0EC1C84734C8931CE8E992568C02 (DropDownHandlerNub_t3291D567FEB2BCA3659581ED9FB356B6C317E48F* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// System.Void DropDownHandlerPiston::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerPiston_dropDownSelected_m878407BD641F22179B3AE671147075555421573C (DropDownHandlerPiston_t77866A4A0DE8B23BC9092FB3674F015B06842109* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// System.Void DropDownHandlerTapperPlate::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerTapperPlate_dropDownSelected_m2A302B20964AF7F7E3A2F804454003DDDB968969 (DropDownHandlerTapperPlate_tAB05E5983A13525B787E9DD6A6504E1E0AFFA449* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) ;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) ;
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Clear_m48B57EC27CADC3463CA98A33373D557DA587FF1B (RuntimeArray* ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method) ;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ButtonHandlerGraph::setGraph(UnityEngine.GameObject)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonHandlerGraph_setGraph_mDD3A764AC1EA0D72B439DBD52D939F25DB88A9B7 (ButtonHandlerGraph_tDC9C59A005B94B0D6306A5C0FC4F47E2467B470D* __this, GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* ___graphContainer0, const RuntimeMethod* method) 
{
	{
		// if(cambiar == false){
		bool L_0 = __this->___cambiar_5;
		if (L_0)
		{
			goto IL_0017;
		}
	}
	{
		// graphContainer.SetActive(false);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1 = ___graphContainer0;
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_1, (bool)0, NULL);
		// cambiar = true;
		__this->___cambiar_5 = (bool)1;
		return;
	}

IL_0017:
	{
		// graphContainer.SetActive(true);
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_2 = ___graphContainer0;
		GameObject_SetActive_m638E92E1E75E519E5B24CF150B08CA8E0CDFAB92(L_2, (bool)1, NULL);
		// cambiar = false;
		__this->___cambiar_5 = (bool)0;
		// }
		return;
	}
}
// System.Void ButtonHandlerGraph::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonHandlerGraph__ctor_m3B6D9E00014832204F9671DB57F58D41C2EBF6D2 (ButtonHandlerGraph_tDC9C59A005B94B0D6306A5C0FC4F47E2467B470D* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ButtonHandlerSave::screenShot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonHandlerSave_screenShot_m600C460F41F9AEA00C99CCDB1B5493D468923339 (ButtonHandlerSave_t64D9EBC89B5FD212EC815917A3E67E66C8305A93* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral002D0FA756EF46E5CC0ED7C4EADA2347F3D5A0DC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA203CCF97D1AE627C19EB69C6DCA52BE18A4A438);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ScreenCapture.CaptureScreenshot("Preset.png");
		ScreenCapture_CaptureScreenshot_m1FAFDB15448C41E697212E54F852DDD196E15CFF(_stringLiteralA203CCF97D1AE627C19EB69C6DCA52BE18A4A438, NULL);
		// Debug.Log("Captura guardada");
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(_stringLiteral002D0FA756EF46E5CC0ED7C4EADA2347F3D5A0DC, NULL);
		// }
		return;
	}
}
// System.Void ButtonHandlerSave::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonHandlerSave__ctor_m0BBDDB5E29D6F3C25A4AA3A3C61B2D916CBF1478 (ButtonHandlerSave_t64D9EBC89B5FD212EC815917A3E67E66C8305A93* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ButtonHandlerSkybox::SetSkyBox()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonHandlerSkybox_SetSkyBox_m17DFB923655CF7AB027E69F9EF2D91A46E14E12D (ButtonHandlerSkybox_t4BFA57953E7D268FF9BC01289F6766FD1358F049* __this, const RuntimeMethod* method) 
{
	{
		// if(cambiar == false){
		bool L_0 = __this->___cambiar_6;
		if (L_0)
		{
			goto IL_001b;
		}
	}
	{
		// RenderSettings.skybox = SkyBoxForest;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_1 = __this->___SkyBoxForest_4;
		RenderSettings_set_skybox_mC520BDF769C5B2824BE2CCC4ADC9CEBEE17A60B4(L_1, NULL);
		// cambiar = true;
		__this->___cambiar_6 = (bool)1;
		return;
	}

IL_001b:
	{
		// RenderSettings.skybox = SkyBoxWarehouse;
		Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_2 = __this->___SkyBoxWarehouse_5;
		RenderSettings_set_skybox_mC520BDF769C5B2824BE2CCC4ADC9CEBEE17A60B4(L_2, NULL);
		// cambiar = false;
		__this->___cambiar_6 = (bool)0;
		// }
		return;
	}
}
// System.Void ButtonHandlerSkybox::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ButtonHandlerSkybox__ctor_m19A8B4901165F69B9BDCFEB7D2C3A3C5AFF020D4 (ButtonHandlerSkybox_t4BFA57953E7D268FF9BC01289F6766FD1358F049* __this, const RuntimeMethod* method) 
{
	{
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CargarExcel::LoaditemData(System.Collections.Generic.List`1<Componente>,System.Collections.Generic.List`1<System.String>,Componente)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CargarExcel_LoaditemData_m4AA0181C9C2A91D3679211AB307B812C9AE72178 (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* ___itemDatabase0, List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* ___items1, Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___blankItem2, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_mC017BCCA057EBCF734567FB642B2632F4C94E093_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_10 = NULL;
	{
		// itemDatabase.Clear();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = ___itemDatabase0;
		List_1_Clear_mC017BCCA057EBCF734567FB642B2632F4C94E093_inline(L_0, List_1_Clear_mC017BCCA057EBCF734567FB642B2632F4C94E093_RuntimeMethod_var);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_1;
		L_1 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_1;
		// for (var i = 0; i < data.Count; i++)
		V_1 = 0;
		goto IL_017c;
	}

IL_0018:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2 = V_0;
		int32_t L_3 = V_1;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_4;
		L_4 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_2, L_3, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_5;
		L_5 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_4, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_6;
		L_6 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_7 = V_0;
		int32_t L_8 = V_1;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_9;
		L_9 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_7, L_8, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_10;
		L_10 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_9, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_11;
		L_11 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		V_2 = L_11;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_12 = V_0;
		int32_t L_13 = V_1;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_14;
		L_14 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_12, L_13, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_15;
		L_15 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_14, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_3 = L_16;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_17 = V_0;
		int32_t L_18 = V_1;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_19;
		L_19 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_17, L_18, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_20;
		L_20 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_19, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_21;
		L_21 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		V_4 = L_21;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_22 = V_0;
		int32_t L_23 = V_1;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_24;
		L_24 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_22, L_23, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_25;
		L_25 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_24, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_26;
		L_26 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		V_5 = L_26;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_27 = V_0;
		int32_t L_28 = V_1;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_29;
		L_29 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_27, L_28, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_30;
		L_30 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_29, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_31;
		L_31 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		V_6 = L_31;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_32 = V_0;
		int32_t L_33 = V_1;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_34;
		L_34 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_32, L_33, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_35;
		L_35 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_34, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_36;
		L_36 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		V_7 = L_36;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_37 = V_0;
		int32_t L_38 = V_1;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_39;
		L_39 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_37, L_38, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_40;
		L_40 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_39, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_41;
		L_41 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_40);
		V_8 = L_41;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_42 = V_0;
		int32_t L_43 = V_1;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_44;
		L_44 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_42, L_43, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_45;
		L_45 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_44, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_46;
		L_46 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_45);
		V_9 = L_46;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_47 = V_0;
		int32_t L_48 = V_1;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_49;
		L_49 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_47, L_48, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_50;
		L_50 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_49, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_51;
		L_51 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_50);
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_52 = ___blankItem2;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_53 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_53, L_52, NULL);
		V_10 = L_53;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_54 = V_10;
		String_t* L_55 = V_2;
		L_54->___tipo_0 = L_55;
		Il2CppCodeGenWriteBarrier((void**)(&L_54->___tipo_0), (void*)L_55);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_56 = V_10;
		String_t* L_57 = V_3;
		L_56->___marca_1 = L_57;
		Il2CppCodeGenWriteBarrier((void**)(&L_56->___marca_1), (void*)L_57);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_58 = V_10;
		String_t* L_59 = V_4;
		L_58->___color_2 = L_59;
		Il2CppCodeGenWriteBarrier((void**)(&L_58->___color_2), (void*)L_59);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_60 = V_10;
		String_t* L_61 = V_5;
		L_60->___material_3 = L_61;
		Il2CppCodeGenWriteBarrier((void**)(&L_60->___material_3), (void*)L_61);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_62 = V_10;
		String_t* L_63 = V_6;
		L_62->___potencia_4 = L_63;
		Il2CppCodeGenWriteBarrier((void**)(&L_62->___potencia_4), (void*)L_63);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_64 = V_10;
		String_t* L_65 = V_7;
		L_64->___precision_5 = L_65;
		Il2CppCodeGenWriteBarrier((void**)(&L_64->___precision_5), (void*)L_65);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_66 = V_10;
		String_t* L_67 = V_8;
		L_66->___alcance_6 = L_67;
		Il2CppCodeGenWriteBarrier((void**)(&L_66->___alcance_6), (void*)L_67);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_68 = V_10;
		String_t* L_69 = V_9;
		L_68->___cadencia_7 = L_69;
		Il2CppCodeGenWriteBarrier((void**)(&L_68->___cadencia_7), (void*)L_69);
		// items.Add(tempItem.tipo + " " + tempItem.marca);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_70 = ___items1;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_71 = V_10;
		String_t* L_72 = L_71->___tipo_0;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = V_10;
		String_t* L_74 = L_73->___marca_1;
		String_t* L_75;
		L_75 = String_Concat_m9B13B47FCB3DF61144D9647DDA05F527377251B0(L_72, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745, L_74, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_70, L_75, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_76 = ___itemDatabase0;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_77 = V_10;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_76, L_77, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_78 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add(L_78, 1));
	}

IL_017c:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_79 = V_1;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_80 = V_0;
		int32_t L_81;
		L_81 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_80, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_79) < ((int32_t)L_81)))
		{
			goto IL_0018;
		}
	}
	{
		// return;
		return;
	}
}
// System.Void CargarExcel::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CargarExcel__ctor_mC267A4375E576A3A7B5648730B84C940761B14FF (CargarExcel_t6CA25EC14B078230E56573150445E548D27C5E4C* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_5), (void*)L_0);
		// public List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_1 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_1, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		__this->___items_6 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___items_6), (void*)L_1);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Componente::.ctor(Componente)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756 (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* __this, Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* ___p0, const RuntimeMethod* method) 
{
	{
		// public Componente(Componente p)
		Object__ctor_mE837C6B9FA8C6D5D109F4B2EC885D79919AC0EA2(__this, NULL);
		// tipo = p.tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_0 = ___p0;
		String_t* L_1 = L_0->___tipo_0;
		__this->___tipo_0 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___tipo_0), (void*)L_1);
		// marca = p.marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_2 = ___p0;
		String_t* L_3 = L_2->___marca_1;
		__this->___marca_1 = L_3;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___marca_1), (void*)L_3);
		// color = p.color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_4 = ___p0;
		String_t* L_5 = L_4->___color_2;
		__this->___color_2 = L_5;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___color_2), (void*)L_5);
		// material = p.material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_6 = ___p0;
		String_t* L_7 = L_6->___material_3;
		__this->___material_3 = L_7;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___material_3), (void*)L_7);
		// potencia = p.potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_8 = ___p0;
		String_t* L_9 = L_8->___potencia_4;
		__this->___potencia_4 = L_9;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___potencia_4), (void*)L_9);
		// precision = p.precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_10 = ___p0;
		String_t* L_11 = L_10->___precision_5;
		__this->___precision_5 = L_11;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___precision_5), (void*)L_11);
		// alcance = p.alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_12 = ___p0;
		String_t* L_13 = L_12->___alcance_6;
		__this->___alcance_6 = L_13;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___alcance_6), (void*)L_13);
		// cadencia = p.cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_14 = ___p0;
		String_t* L_15 = L_14->___cadencia_7;
		__this->___cadencia_7 = L_15;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___cadencia_7), (void*)L_15);
		// estabilidad = p.estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_16 = ___p0;
		String_t* L_17 = L_16->___estabilidad_8;
		__this->___estabilidad_8 = L_17;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___estabilidad_8), (void*)L_17);
		// precio = p.precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_18 = ___p0;
		String_t* L_19 = L_18->___precio_9;
		__this->___precio_9 = L_19;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___precio_9), (void*)L_19);
		// modelo = p.modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_20 = ___p0;
		String_t* L_21 = L_20->___modelo_10;
		__this->___modelo_10 = L_21;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___modelo_10), (void*)L_21);
		// extra = p.extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_22 = ___p0;
		String_t* L_23 = L_22->___extra_11;
		__this->___extra_11 = L_23;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_11), (void*)L_23);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandler::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandler_Start_m71216813C0F507C83193EFA6A3B053BAED133D43 (DropDownHandler_t8002662C419AC287ADA29E30AF224815CC7D12C7* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandler_U3CStartU3Eb__10_0_m167C537067D9AC7384513EE714F65305F32D2183_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA8F244FC674653C59552E40566FE156BB9BB79FD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// motor = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___motor_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___motor_5), (void*)L_0);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_1;
		L_1 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_1;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2 = V_0;
		int32_t L_3;
		L_3 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_2, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_4 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_3);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_5 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_5, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_5;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_022e;
	}

IL_0030:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_6 = V_0;
		int32_t L_7 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_8;
		L_8 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_6, L_7, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_9;
		L_9 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_8, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_10;
		L_10 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_11 = V_0;
		int32_t L_12 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_13;
		L_13 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_11, L_12, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_14;
		L_14 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_13, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_15;
		L_15 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		V_3 = L_15;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_16 = V_0;
		int32_t L_17 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_18;
		L_18 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_16, L_17, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_19;
		L_19 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_18, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_20;
		L_20 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_19);
		V_4 = L_20;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_21 = V_0;
		int32_t L_22 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_23;
		L_23 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_21, L_22, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_24;
		L_24 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_23, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_25;
		L_25 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_24);
		V_5 = L_25;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_26 = V_0;
		int32_t L_27 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_28;
		L_28 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_26, L_27, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_29;
		L_29 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_28, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_30;
		L_30 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_29);
		V_6 = L_30;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_31 = V_0;
		int32_t L_32 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_33;
		L_33 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_31, L_32, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_34;
		L_34 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_33, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_35;
		L_35 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		V_7 = L_35;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_36 = V_0;
		int32_t L_37 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_38;
		L_38 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_36, L_37, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_39;
		L_39 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_38, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_40;
		L_40 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_39);
		V_8 = L_40;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_41 = V_0;
		int32_t L_42 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_43;
		L_43 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_41, L_42, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_44;
		L_44 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_43, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_45;
		L_45 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_44);
		V_9 = L_45;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_46 = V_0;
		int32_t L_47 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_48;
		L_48 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_46, L_47, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_49;
		L_49 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_48, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_50;
		L_50 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_49);
		V_10 = L_50;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_51 = V_0;
		int32_t L_52 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_53;
		L_53 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_51, L_52, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_54;
		L_54 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_53, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_55;
		L_55 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_54);
		V_11 = L_55;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_56 = V_0;
		int32_t L_57 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_58;
		L_58 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_56, L_57, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_59;
		L_59 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_58, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_60;
		L_60 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_59);
		V_12 = L_60;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_61 = V_0;
		int32_t L_62 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_63;
		L_63 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_61, L_62, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_64;
		L_64 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_63, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_65;
		L_65 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_64);
		V_13 = L_65;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_66 = V_0;
		int32_t L_67 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_68;
		L_68 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_66, L_67, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_69;
		L_69 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_68, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_70;
		L_70 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_69);
		V_14 = L_70;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_71 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_72, L_71, NULL);
		V_15 = L_72;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = V_15;
		String_t* L_74 = V_3;
		L_73->___tipo_0 = L_74;
		Il2CppCodeGenWriteBarrier((void**)(&L_73->___tipo_0), (void*)L_74);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_75 = V_15;
		String_t* L_76 = V_4;
		L_75->___marca_1 = L_76;
		Il2CppCodeGenWriteBarrier((void**)(&L_75->___marca_1), (void*)L_76);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_77 = V_15;
		String_t* L_78 = V_5;
		L_77->___color_2 = L_78;
		Il2CppCodeGenWriteBarrier((void**)(&L_77->___color_2), (void*)L_78);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_79 = V_15;
		String_t* L_80 = V_6;
		L_79->___material_3 = L_80;
		Il2CppCodeGenWriteBarrier((void**)(&L_79->___material_3), (void*)L_80);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_81 = V_15;
		String_t* L_82 = V_7;
		L_81->___potencia_4 = L_82;
		Il2CppCodeGenWriteBarrier((void**)(&L_81->___potencia_4), (void*)L_82);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_83 = V_15;
		String_t* L_84 = V_8;
		L_83->___precision_5 = L_84;
		Il2CppCodeGenWriteBarrier((void**)(&L_83->___precision_5), (void*)L_84);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_85 = V_15;
		String_t* L_86 = V_9;
		L_85->___alcance_6 = L_86;
		Il2CppCodeGenWriteBarrier((void**)(&L_85->___alcance_6), (void*)L_86);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_87 = V_15;
		String_t* L_88 = V_10;
		L_87->___cadencia_7 = L_88;
		Il2CppCodeGenWriteBarrier((void**)(&L_87->___cadencia_7), (void*)L_88);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_89 = V_15;
		String_t* L_90 = V_11;
		L_89->___estabilidad_8 = L_90;
		Il2CppCodeGenWriteBarrier((void**)(&L_89->___estabilidad_8), (void*)L_90);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_91 = V_15;
		String_t* L_92 = V_12;
		L_91->___precio_9 = L_92;
		Il2CppCodeGenWriteBarrier((void**)(&L_91->___precio_9), (void*)L_92);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_93 = V_15;
		String_t* L_94 = V_13;
		L_93->___modelo_10 = L_94;
		Il2CppCodeGenWriteBarrier((void**)(&L_93->___modelo_10), (void*)L_94);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_95 = V_15;
		String_t* L_96 = V_14;
		L_95->___extra_11 = L_96;
		Il2CppCodeGenWriteBarrier((void**)(&L_95->___extra_11), (void*)L_96);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_97 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_98 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = L_98;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_100 = V_15;
		String_t* L_101 = L_100->___tipo_0;
		ArrayElementTypeCheck (L_99, L_101);
		(L_99)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_101);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_102 = L_99;
		ArrayElementTypeCheck (L_102, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_102)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_102;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_104 = V_15;
		String_t* L_105 = L_104->___marca_1;
		ArrayElementTypeCheck (L_103, L_105);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_105);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_106 = L_103;
		ArrayElementTypeCheck (L_106, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_106)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_106;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_108 = V_15;
		String_t* L_109 = L_108->___modelo_10;
		ArrayElementTypeCheck (L_107, L_109);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_109);
		String_t* L_110;
		L_110 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_107, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_97, L_110, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_111 = __this->___itemDatabase_13;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_112 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_111, L_112, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_113 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_113, 1));
	}

IL_022e:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_115 = V_0;
		int32_t L_116;
		L_116 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_115, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_114) < ((int32_t)L_116)))
		{
			goto IL_0030;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_117 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_118;
		L_118 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_117, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_118;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0283:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0278_1;
			}

IL_0244_1:
			{
				// foreach (string item in items)
				String_t* L_119;
				L_119 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_119;
				// if (item.Contains("Motor"))
				String_t* L_120 = V_17;
				bool L_121;
				L_121 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_120, _stringLiteralA8F244FC674653C59552E40566FE156BB9BB79FD, NULL);
				if (!L_121)
				{
					goto IL_0278_1;
				}
			}
			{
				// motor.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_122 = __this->___motor_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_123;
				L_123 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_122, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_124 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_124, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_125 = L_124;
				String_t* L_126 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_125, L_126, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_123, L_125, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_0278_1:
			{
				// foreach (string item in items)
				bool L_127;
				L_127 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_127)
				{
					goto IL_0244_1;
				}
			}
			{
				goto IL_0291;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0291:
	{
		// motor.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_128 = __this->___motor_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_128, NULL);
		// motor.onValueChanged.AddListener(delegate { dropDownSelected(motor); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_129 = __this->___motor_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_130;
		L_130 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_129, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_131 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_131, __this, (intptr_t)((void*)DropDownHandler_U3CStartU3Eb__10_0_m167C537067D9AC7384513EE714F65305F32D2183_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_130, L_131, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandler::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandler_dropDownSelected_mAA8F5B15E84690C47CD8C315DF6DCF94C40A0FC8 (DropDownHandler_t8002662C419AC287ADA29E30AF224815CC7D12C7* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (alcanceDrop != 0)
		double L_2 = __this->___alcanceDrop_7;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_3 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (varianzaDrop != 0)
		float L_4 = __this->___varianzaDrop_10;
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_0067;
		}
	}
	{
		// Window_Graph.setVarianza(0, varianzaDrop);
		float L_5 = __this->___varianzaDrop_10;
		Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8((0.0f), L_5, NULL);
	}

IL_0067:
	{
		// if (precisionDrop != 0)
		double L_6 = __this->___precisionDrop_8;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_008c;
		}
	}
	{
		// Window_Graph.setPrecision(0, precisionDrop);
		double L_7 = __this->___precisionDrop_8;
		Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6((0.0), L_7, NULL);
	}

IL_008c:
	{
		// if (cadenciaDrop != 0)
		double L_8 = __this->___cadenciaDrop_9;
		if ((((double)L_8) == ((double)(0.0))))
		{
			goto IL_00b1;
		}
	}
	{
		// Window_Graph.setCadencia(0, cadenciaDrop);
		double L_9 = __this->___cadenciaDrop_9;
		Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142((0.0), L_9, NULL);
	}

IL_00b1:
	{
		// if (precio != 0)
		double L_10 = __this->___precio_11;
		if ((((double)L_10) == ((double)(0.0))))
		{
			goto IL_00d6;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_11 = __this->___precio_11;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_11, NULL);
	}

IL_00d6:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_12 = ___dropdown0;
		int32_t L_13;
		L_13 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_12, NULL);
		V_0 = L_13;
		// Debug.Log(motor.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_14 = __this->___motor_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_15;
		L_15 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_14, NULL);
		int32_t L_16 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_17;
		L_17 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_15, L_16, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_18;
		L_18 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_17, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_18, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_19 = __this->___itemDatabase_13;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_20;
		L_20 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_19, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_20;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0261:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0253_1;
			}

IL_0109_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_21;
				L_21 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_21;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_24 = V_2;
				String_t* L_25 = L_24->___tipo_0;
				ArrayElementTypeCheck (L_23, L_25);
				(L_23)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_25);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_23;
				ArrayElementTypeCheck (L_26, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_26)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_27 = L_26;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_28 = V_2;
				String_t* L_29 = L_28->___marca_1;
				ArrayElementTypeCheck (L_27, L_29);
				(L_27)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_29);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_30 = L_27;
				ArrayElementTypeCheck (L_30, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_30)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_31 = L_30;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_32 = V_2;
				String_t* L_33 = L_32->___modelo_10;
				ArrayElementTypeCheck (L_31, L_33);
				(L_31)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_33);
				String_t* L_34;
				L_34 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_31, NULL);
				// if (comparar == motor.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_35 = __this->___motor_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_36;
				L_36 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_35, NULL);
				int32_t L_37 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_38;
				L_38 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_36, L_37, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_39;
				L_39 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_38, NULL);
				bool L_40;
				L_40 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_34, L_39, NULL);
				if (!L_40)
				{
					goto IL_0253_1;
				}
			}
			{
				// potenciaDrop = int.Parse(Componente.potencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_41 = V_2;
				String_t* L_42 = L_41->___potencia_4;
				int32_t L_43;
				L_43 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_42, NULL);
				__this->___potenciaDrop_6 = ((double)L_43);
				// alcanceDrop = int.Parse(Componente.alcance);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_44 = V_2;
				String_t* L_45 = L_44->___alcance_6;
				int32_t L_46;
				L_46 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_45, NULL);
				__this->___alcanceDrop_7 = ((double)L_46);
				// varianzaDrop = int.Parse(Componente.estabilidad);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_47 = V_2;
				String_t* L_48 = L_47->___estabilidad_8;
				int32_t L_49;
				L_49 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_48, NULL);
				__this->___varianzaDrop_10 = ((float)L_49);
				// precisionDrop = int.Parse(Componente.precision);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_50 = V_2;
				String_t* L_51 = L_50->___precision_5;
				int32_t L_52;
				L_52 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_51, NULL);
				__this->___precisionDrop_8 = ((double)L_52);
				// cadenciaDrop = int.Parse(Componente.cadencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_53 = V_2;
				String_t* L_54 = L_53->___cadencia_7;
				int32_t L_55;
				L_55 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_54, NULL);
				__this->___cadenciaDrop_9 = ((double)L_55);
				// precio = int.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_56 = V_2;
				String_t* L_57 = L_56->___precio_9;
				int32_t L_58;
				L_58 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_57, NULL);
				__this->___precio_11 = ((double)L_58);
				// extra = Componente.extra;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_59 = V_2;
				String_t* L_60 = L_59->___extra_11;
				__this->___extra_12 = L_60;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_12), (void*)L_60);
				// Window_Graph.setAlcance(alcanceDrop);
				double L_61 = __this->___alcanceDrop_7;
				Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2(L_61, (0.0), NULL);
				// Window_Graph.setPotencia(potenciaDrop);
				double L_62 = __this->___potenciaDrop_6;
				Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E(L_62, (0.0), NULL);
				// Window_Graph.setVarianza(varianzaDrop);
				float L_63 = __this->___varianzaDrop_10;
				Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8(L_63, (0.0f), NULL);
				// Window_Graph.setPrecision(precisionDrop);
				double L_64 = __this->___precisionDrop_8;
				Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6(L_64, (0.0), NULL);
				// Window_Graph.setCadencia(cadenciaDrop);
				double L_65 = __this->___cadenciaDrop_9;
				Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142(L_65, (0.0), NULL);
				// Window_Graph.setPrecio(precio,0);
				double L_66 = __this->___precio_11;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_66, (0.0), NULL);
			}

IL_0253_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_67;
				L_67 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_67)
				{
					goto IL_0109_1;
				}
			}
			{
				goto IL_026f;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_026f:
	{
		// }
		return;
	}
}
// System.Void DropDownHandler::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandler__ctor_mCA01DDAD2F63C5E3C14961337C026656F47A5B7D (DropDownHandler_t8002662C419AC287ADA29E30AF224815CC7D12C7* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_13 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_13), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandler::<Start>b__10_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandler_U3CStartU3Eb__10_0_m167C537067D9AC7384513EE714F65305F32D2183 (DropDownHandler_t8002662C419AC287ADA29E30AF224815CC7D12C7* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// motor.onValueChanged.AddListener(delegate { dropDownSelected(motor); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___motor_5;
		DropDownHandler_dropDownSelected_mAA8F5B15E84690C47CD8C315DF6DCF94C40A0FC8(__this, L_0, NULL);
		// motor.onValueChanged.AddListener(delegate { dropDownSelected(motor); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandlerCabezaCilindro::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCabezaCilindro_Start_m41ACA340785BE6E485DD64724D65C6BF2768E090 (DropDownHandlerCabezaCilindro_tC63ABBA0B4BAC7AF05823FABC92495D3538FABEC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandlerCabezaCilindro_U3CStartU3Eb__11_0_m6875FB41A9C754225791135E51A92A24F0F70012_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4583DC5F865BA10566EA0C1A7052AFBD5F64925F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral804BD6674F35C5D81F09F3D4D99EBF580BE9C6E0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// CabezaCilindro = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___CabezaCilindro_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___CabezaCilindro_5), (void*)L_0);
		// CabezaCilindroPieza = GameObject.Find("CabezaCilindro");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral804BD6674F35C5D81F09F3D4D99EBF580BE9C6E0, NULL);
		__this->___CabezaCilindroPieza_13 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___CabezaCilindroPieza_13), (void*)L_1);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2;
		L_2 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_2;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_3 = V_0;
		int32_t L_4;
		L_4 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_3, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_4);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_6 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_6, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_6;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_023e;
	}

IL_0040:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_7 = V_0;
		int32_t L_8 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_9;
		L_9 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_7, L_8, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_10;
		L_10 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_9, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_11;
		L_11 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_12 = V_0;
		int32_t L_13 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_14;
		L_14 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_12, L_13, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_15;
		L_15 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_14, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_3 = L_16;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_17 = V_0;
		int32_t L_18 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_19;
		L_19 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_17, L_18, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_20;
		L_20 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_19, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_21;
		L_21 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		V_4 = L_21;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_22 = V_0;
		int32_t L_23 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_24;
		L_24 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_22, L_23, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_25;
		L_25 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_24, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_26;
		L_26 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		V_5 = L_26;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_27 = V_0;
		int32_t L_28 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_29;
		L_29 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_27, L_28, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_30;
		L_30 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_29, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_31;
		L_31 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		V_6 = L_31;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_32 = V_0;
		int32_t L_33 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_34;
		L_34 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_32, L_33, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_35;
		L_35 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_34, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_36;
		L_36 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		V_7 = L_36;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_37 = V_0;
		int32_t L_38 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_39;
		L_39 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_37, L_38, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_40;
		L_40 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_39, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_41;
		L_41 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_40);
		V_8 = L_41;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_42 = V_0;
		int32_t L_43 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_44;
		L_44 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_42, L_43, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_45;
		L_45 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_44, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_46;
		L_46 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_45);
		V_9 = L_46;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_47 = V_0;
		int32_t L_48 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_49;
		L_49 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_47, L_48, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_50;
		L_50 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_49, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_51;
		L_51 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_50);
		V_10 = L_51;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_52 = V_0;
		int32_t L_53 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_54;
		L_54 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_52, L_53, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_55;
		L_55 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_54, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_56;
		L_56 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_55);
		V_11 = L_56;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_57 = V_0;
		int32_t L_58 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_59;
		L_59 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_57, L_58, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_60;
		L_60 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_59, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_61;
		L_61 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_60);
		V_12 = L_61;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_62 = V_0;
		int32_t L_63 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_64;
		L_64 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_62, L_63, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_65;
		L_65 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_64, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_66;
		L_66 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_65);
		V_13 = L_66;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_67 = V_0;
		int32_t L_68 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_69;
		L_69 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_67, L_68, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_70;
		L_70 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_69, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_71;
		L_71 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_70);
		V_14 = L_71;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_73, L_72, NULL);
		V_15 = L_73;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_74 = V_15;
		String_t* L_75 = V_3;
		L_74->___tipo_0 = L_75;
		Il2CppCodeGenWriteBarrier((void**)(&L_74->___tipo_0), (void*)L_75);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_76 = V_15;
		String_t* L_77 = V_4;
		L_76->___marca_1 = L_77;
		Il2CppCodeGenWriteBarrier((void**)(&L_76->___marca_1), (void*)L_77);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_78 = V_15;
		String_t* L_79 = V_5;
		L_78->___color_2 = L_79;
		Il2CppCodeGenWriteBarrier((void**)(&L_78->___color_2), (void*)L_79);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_80 = V_15;
		String_t* L_81 = V_6;
		L_80->___material_3 = L_81;
		Il2CppCodeGenWriteBarrier((void**)(&L_80->___material_3), (void*)L_81);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_82 = V_15;
		String_t* L_83 = V_7;
		L_82->___potencia_4 = L_83;
		Il2CppCodeGenWriteBarrier((void**)(&L_82->___potencia_4), (void*)L_83);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_84 = V_15;
		String_t* L_85 = V_8;
		L_84->___precision_5 = L_85;
		Il2CppCodeGenWriteBarrier((void**)(&L_84->___precision_5), (void*)L_85);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_86 = V_15;
		String_t* L_87 = V_9;
		L_86->___alcance_6 = L_87;
		Il2CppCodeGenWriteBarrier((void**)(&L_86->___alcance_6), (void*)L_87);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_88 = V_15;
		String_t* L_89 = V_10;
		L_88->___cadencia_7 = L_89;
		Il2CppCodeGenWriteBarrier((void**)(&L_88->___cadencia_7), (void*)L_89);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_90 = V_15;
		String_t* L_91 = V_11;
		L_90->___estabilidad_8 = L_91;
		Il2CppCodeGenWriteBarrier((void**)(&L_90->___estabilidad_8), (void*)L_91);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_92 = V_15;
		String_t* L_93 = V_12;
		L_92->___precio_9 = L_93;
		Il2CppCodeGenWriteBarrier((void**)(&L_92->___precio_9), (void*)L_93);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_94 = V_15;
		String_t* L_95 = V_13;
		L_94->___modelo_10 = L_95;
		Il2CppCodeGenWriteBarrier((void**)(&L_94->___modelo_10), (void*)L_95);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_96 = V_15;
		String_t* L_97 = V_14;
		L_96->___extra_11 = L_97;
		Il2CppCodeGenWriteBarrier((void**)(&L_96->___extra_11), (void*)L_97);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_98 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_100 = L_99;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_101 = V_15;
		String_t* L_102 = L_101->___tipo_0;
		ArrayElementTypeCheck (L_100, L_102);
		(L_100)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_102);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_100;
		ArrayElementTypeCheck (L_103, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_104 = L_103;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_15;
		String_t* L_106 = L_105->___marca_1;
		ArrayElementTypeCheck (L_104, L_106);
		(L_104)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_106);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_104;
		ArrayElementTypeCheck (L_107, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_108 = L_107;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_109 = V_15;
		String_t* L_110 = L_109->___modelo_10;
		ArrayElementTypeCheck (L_108, L_110);
		(L_108)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_110);
		String_t* L_111;
		L_111 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_108, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_98, L_111, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_112 = __this->___itemDatabase_14;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_113 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_112, L_113, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_114, 1));
	}

IL_023e:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_115 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_116 = V_0;
		int32_t L_117;
		L_117 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_116, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_115) < ((int32_t)L_117)))
		{
			goto IL_0040;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_118 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_119;
		L_119 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_118, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_119;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0293:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0288_1;
			}

IL_0254_1:
			{
				// foreach (string item in items)
				String_t* L_120;
				L_120 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_120;
				// if (item.Contains("Cabeza de cilindro"))
				String_t* L_121 = V_17;
				bool L_122;
				L_122 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_121, _stringLiteral4583DC5F865BA10566EA0C1A7052AFBD5F64925F, NULL);
				if (!L_122)
				{
					goto IL_0288_1;
				}
			}
			{
				// CabezaCilindro.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_123 = __this->___CabezaCilindro_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_124;
				L_124 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_123, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_125 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_125, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_126 = L_125;
				String_t* L_127 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_126, L_127, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_124, L_126, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_0288_1:
			{
				// foreach (string item in items)
				bool L_128;
				L_128 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_128)
				{
					goto IL_0254_1;
				}
			}
			{
				goto IL_02a1;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_02a1:
	{
		// CabezaCilindro.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_129 = __this->___CabezaCilindro_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_129, NULL);
		// CabezaCilindro.onValueChanged.AddListener(delegate { dropDownSelected(CabezaCilindro); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_130 = __this->___CabezaCilindro_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_131;
		L_131 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_130, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_132 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_132, __this, (intptr_t)((void*)DropDownHandlerCabezaCilindro_U3CStartU3Eb__11_0_m6875FB41A9C754225791135E51A92A24F0F70012_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_131, L_132, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandlerCabezaCilindro::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCabezaCilindro_dropDownSelected_mDAE9FEB10D333AD54F00BA2343251E893D5808B3 (DropDownHandlerCabezaCilindro_tC63ABBA0B4BAC7AF05823FABC92495D3538FABEC* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4DE573297F69792F2A52321674F0590D7424E99A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* V_3 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (alcanceDrop != 0)
		double L_2 = __this->___alcanceDrop_7;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_3 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (varianzaDrop != 0)
		float L_4 = __this->___varianzaDrop_10;
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_0067;
		}
	}
	{
		// Window_Graph.setVarianza(0, varianzaDrop);
		float L_5 = __this->___varianzaDrop_10;
		Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8((0.0f), L_5, NULL);
	}

IL_0067:
	{
		// if (precisionDrop != 0)
		double L_6 = __this->___precisionDrop_8;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_008c;
		}
	}
	{
		// Window_Graph.setPrecision(0, precisionDrop);
		double L_7 = __this->___precisionDrop_8;
		Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6((0.0), L_7, NULL);
	}

IL_008c:
	{
		// if (cadenciaDrop != 0)
		double L_8 = __this->___cadenciaDrop_9;
		if ((((double)L_8) == ((double)(0.0))))
		{
			goto IL_00b1;
		}
	}
	{
		// Window_Graph.setCadencia(0, cadenciaDrop);
		double L_9 = __this->___cadenciaDrop_9;
		Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142((0.0), L_9, NULL);
	}

IL_00b1:
	{
		// if (precio != 0)
		double L_10 = __this->___precio_11;
		if ((((double)L_10) == ((double)(0.0))))
		{
			goto IL_00d6;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_11 = __this->___precio_11;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_11, NULL);
	}

IL_00d6:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_12 = ___dropdown0;
		int32_t L_13;
		L_13 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_12, NULL);
		V_0 = L_13;
		// Debug.Log(CabezaCilindro.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_14 = __this->___CabezaCilindro_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_15;
		L_15 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_14, NULL);
		int32_t L_16 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_17;
		L_17 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_15, L_16, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_18;
		L_18 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_17, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_18, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_19 = __this->___itemDatabase_14;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_20;
		L_20 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_19, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_20;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_03f3:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_03e5_1;
			}

IL_0109_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_21;
				L_21 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_21;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_24 = V_2;
				String_t* L_25 = L_24->___tipo_0;
				ArrayElementTypeCheck (L_23, L_25);
				(L_23)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_25);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_23;
				ArrayElementTypeCheck (L_26, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_26)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_27 = L_26;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_28 = V_2;
				String_t* L_29 = L_28->___marca_1;
				ArrayElementTypeCheck (L_27, L_29);
				(L_27)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_29);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_30 = L_27;
				ArrayElementTypeCheck (L_30, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_30)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_31 = L_30;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_32 = V_2;
				String_t* L_33 = L_32->___modelo_10;
				ArrayElementTypeCheck (L_31, L_33);
				(L_31)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_33);
				String_t* L_34;
				L_34 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_31, NULL);
				// if (comparar == CabezaCilindro.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_35 = __this->___CabezaCilindro_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_36;
				L_36 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_35, NULL);
				int32_t L_37 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_38;
				L_38 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_36, L_37, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_39;
				L_39 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_38, NULL);
				bool L_40;
				L_40 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_34, L_39, NULL);
				if (!L_40)
				{
					goto IL_03e5_1;
				}
			}
			{
				// potenciaDrop = int.Parse(Componente.potencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_41 = V_2;
				String_t* L_42 = L_41->___potencia_4;
				int32_t L_43;
				L_43 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_42, NULL);
				__this->___potenciaDrop_6 = ((double)L_43);
				// alcanceDrop = int.Parse(Componente.alcance);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_44 = V_2;
				String_t* L_45 = L_44->___alcance_6;
				int32_t L_46;
				L_46 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_45, NULL);
				__this->___alcanceDrop_7 = ((double)L_46);
				// varianzaDrop = int.Parse(Componente.estabilidad);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_47 = V_2;
				String_t* L_48 = L_47->___estabilidad_8;
				int32_t L_49;
				L_49 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_48, NULL);
				__this->___varianzaDrop_10 = ((float)L_49);
				// precisionDrop = int.Parse(Componente.precision);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_50 = V_2;
				String_t* L_51 = L_50->___precision_5;
				int32_t L_52;
				L_52 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_51, NULL);
				__this->___precisionDrop_8 = ((double)L_52);
				// cadenciaDrop = int.Parse(Componente.cadencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_53 = V_2;
				String_t* L_54 = L_53->___cadencia_7;
				int32_t L_55;
				L_55 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_54, NULL);
				__this->___cadenciaDrop_9 = ((double)L_55);
				// precio = int.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_56 = V_2;
				String_t* L_57 = L_56->___precio_9;
				int32_t L_58;
				L_58 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_57, NULL);
				__this->___precio_11 = ((double)L_58);
				// extra = Componente.extra;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_59 = V_2;
				String_t* L_60 = L_59->___extra_11;
				__this->___extra_12 = L_60;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_12), (void*)L_60);
				// Window_Graph.setAlcance(alcanceDrop);
				double L_61 = __this->___alcanceDrop_7;
				Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2(L_61, (0.0), NULL);
				// Window_Graph.setPotencia(potenciaDrop);
				double L_62 = __this->___potenciaDrop_6;
				Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E(L_62, (0.0), NULL);
				// Window_Graph.setVarianza(varianzaDrop);
				float L_63 = __this->___varianzaDrop_10;
				Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8(L_63, (0.0f), NULL);
				// Window_Graph.setPrecision(precisionDrop);
				double L_64 = __this->___precisionDrop_8;
				Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6(L_64, (0.0), NULL);
				// Window_Graph.setCadencia(cadenciaDrop);
				double L_65 = __this->___cadenciaDrop_9;
				Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142(L_65, (0.0), NULL);
				// Window_Graph.setPrecio(precio,0);
				double L_66 = __this->___precio_11;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_66, (0.0), NULL);
				// var renderer = CabezaCilindroPieza.GetComponent<Renderer>();
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_67 = __this->___CabezaCilindroPieza_13;
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_68;
				L_68 = GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A(L_67, GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
				V_3 = L_68;
				// if (Componente.color == "Rojo")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_69 = V_2;
				String_t* L_70 = L_69->___color_2;
				bool L_71;
				L_71 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_70, _stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C, NULL);
				if (!L_71)
				{
					goto IL_0286_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.red);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_72 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_73;
				L_73 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_72, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_74;
				L_74 = Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_73, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_74, NULL);
			}

IL_0286_1:
			{
				// if (Componente.color == "Gris")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_75 = V_2;
				String_t* L_76 = L_75->___color_2;
				bool L_77;
				L_77 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_76, _stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116, NULL);
				if (!L_77)
				{
					goto IL_02ad_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_78 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_79;
				L_79 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_78, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_80;
				L_80 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_79, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_80, NULL);
			}

IL_02ad_1:
			{
				// if (Componente.color == "Gris oscuro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_81 = V_2;
				String_t* L_82 = L_81->___color_2;
				bool L_83;
				L_83 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_82, _stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC, NULL);
				if (!L_83)
				{
					goto IL_02d4_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_84 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_85;
				L_85 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_84, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_86;
				L_86 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_85, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_86, NULL);
			}

IL_02d4_1:
			{
				// if (Componente.color == "Gris claro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_87 = V_2;
				String_t* L_88 = L_87->___color_2;
				bool L_89;
				L_89 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_88, _stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6, NULL);
				if (!L_89)
				{
					goto IL_02fb_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_90 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_91;
				L_91 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_90, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_92;
				L_92 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_91, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_92, NULL);
			}

IL_02fb_1:
			{
				// if (Componente.color == "Azul")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_93 = V_2;
				String_t* L_94 = L_93->___color_2;
				bool L_95;
				L_95 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_94, _stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510, NULL);
				if (!L_95)
				{
					goto IL_0322_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.blue);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_96 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_97;
				L_97 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_96, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_98;
				L_98 = Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_97, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_98, NULL);
			}

IL_0322_1:
			{
				// if (Componente.color == "Negro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_99 = V_2;
				String_t* L_100 = L_99->___color_2;
				bool L_101;
				L_101 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_100, _stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9, NULL);
				if (!L_101)
				{
					goto IL_0349_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_102 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_103;
				L_103 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_102, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_104;
				L_104 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_103, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_104, NULL);
			}

IL_0349_1:
			{
				// if (Componente.color == "Dorado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_2;
				String_t* L_106 = L_105->___color_2;
				bool L_107;
				L_107 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_106, _stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E, NULL);
				if (!L_107)
				{
					goto IL_0370_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.yellow);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_108 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_109;
				L_109 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_108, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_110;
				L_110 = Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_109, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_110, NULL);
			}

IL_0370_1:
			{
				// if (Componente.color == "Plateado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_111 = V_2;
				String_t* L_112 = L_111->___color_2;
				bool L_113;
				L_113 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_112, _stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF, NULL);
				if (!L_113)
				{
					goto IL_0397_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_114 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_115;
				L_115 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_114, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_116;
				L_116 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_115, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_116, NULL);
			}

IL_0397_1:
			{
				// if (Componente.color == "Cobre")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_117 = V_2;
				String_t* L_118 = L_117->___color_2;
				bool L_119;
				L_119 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_118, _stringLiteral4DE573297F69792F2A52321674F0590D7424E99A, NULL);
				if (!L_119)
				{
					goto IL_03be_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_120 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_121;
				L_121 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_120, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_122;
				L_122 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_121, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_122, NULL);
			}

IL_03be_1:
			{
				// if (Componente.color == "")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_123 = V_2;
				String_t* L_124 = L_123->___color_2;
				bool L_125;
				L_125 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_124, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
				if (!L_125)
				{
					goto IL_03e5_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_126 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_127;
				L_127 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_126, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_128;
				L_128 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_127, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_128, NULL);
			}

IL_03e5_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_129;
				L_129 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_129)
				{
					goto IL_0109_1;
				}
			}
			{
				goto IL_0401;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0401:
	{
		// }
		return;
	}
}
// System.Void DropDownHandlerCabezaCilindro::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCabezaCilindro__ctor_m0CDAC5E3397EEA8913236638784DC8F78E8C19AE (DropDownHandlerCabezaCilindro_tC63ABBA0B4BAC7AF05823FABC92495D3538FABEC* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_14 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_14), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandlerCabezaCilindro::<Start>b__11_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCabezaCilindro_U3CStartU3Eb__11_0_m6875FB41A9C754225791135E51A92A24F0F70012 (DropDownHandlerCabezaCilindro_tC63ABBA0B4BAC7AF05823FABC92495D3538FABEC* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// CabezaCilindro.onValueChanged.AddListener(delegate { dropDownSelected(CabezaCilindro); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___CabezaCilindro_5;
		DropDownHandlerCabezaCilindro_dropDownSelected_mDAE9FEB10D333AD54F00BA2343251E893D5808B3(__this, L_0, NULL);
		// CabezaCilindro.onValueChanged.AddListener(delegate { dropDownSelected(CabezaCilindro); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandlerCabezaPiston::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCabezaPiston_Start_mFB984A8E7D0C8EE2D34678EF7D269B2688AC60FD (DropDownHandlerCabezaPiston_t5D4F7822D76A01DD44715998B4C3911DC1DBE889* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandlerCabezaPiston_U3CStartU3Eb__11_0_m07769265758FD3A769226BE96ACA9252B63A69D3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral10FB04B14070C24E4738C39ABE147E7A57E102B7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4F3ABCDAF59D8FA7DA0C2AAA76E9C5784A781CF1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// CabezaPiston = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___CabezaPiston_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___CabezaPiston_5), (void*)L_0);
		// CabezaPistonPieza = GameObject.Find("CabezaPiston");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral10FB04B14070C24E4738C39ABE147E7A57E102B7, NULL);
		__this->___CabezaPistonPieza_14 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___CabezaPistonPieza_14), (void*)L_1);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2;
		L_2 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_2;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_3 = V_0;
		int32_t L_4;
		L_4 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_3, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_4);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_6 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_6, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_6;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_023e;
	}

IL_0040:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_7 = V_0;
		int32_t L_8 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_9;
		L_9 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_7, L_8, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_10;
		L_10 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_9, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_11;
		L_11 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_12 = V_0;
		int32_t L_13 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_14;
		L_14 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_12, L_13, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_15;
		L_15 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_14, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_3 = L_16;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_17 = V_0;
		int32_t L_18 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_19;
		L_19 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_17, L_18, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_20;
		L_20 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_19, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_21;
		L_21 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		V_4 = L_21;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_22 = V_0;
		int32_t L_23 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_24;
		L_24 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_22, L_23, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_25;
		L_25 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_24, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_26;
		L_26 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		V_5 = L_26;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_27 = V_0;
		int32_t L_28 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_29;
		L_29 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_27, L_28, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_30;
		L_30 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_29, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_31;
		L_31 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		V_6 = L_31;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_32 = V_0;
		int32_t L_33 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_34;
		L_34 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_32, L_33, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_35;
		L_35 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_34, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_36;
		L_36 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		V_7 = L_36;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_37 = V_0;
		int32_t L_38 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_39;
		L_39 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_37, L_38, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_40;
		L_40 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_39, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_41;
		L_41 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_40);
		V_8 = L_41;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_42 = V_0;
		int32_t L_43 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_44;
		L_44 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_42, L_43, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_45;
		L_45 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_44, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_46;
		L_46 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_45);
		V_9 = L_46;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_47 = V_0;
		int32_t L_48 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_49;
		L_49 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_47, L_48, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_50;
		L_50 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_49, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_51;
		L_51 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_50);
		V_10 = L_51;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_52 = V_0;
		int32_t L_53 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_54;
		L_54 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_52, L_53, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_55;
		L_55 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_54, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_56;
		L_56 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_55);
		V_11 = L_56;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_57 = V_0;
		int32_t L_58 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_59;
		L_59 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_57, L_58, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_60;
		L_60 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_59, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_61;
		L_61 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_60);
		V_12 = L_61;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_62 = V_0;
		int32_t L_63 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_64;
		L_64 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_62, L_63, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_65;
		L_65 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_64, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_66;
		L_66 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_65);
		V_13 = L_66;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_67 = V_0;
		int32_t L_68 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_69;
		L_69 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_67, L_68, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_70;
		L_70 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_69, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_71;
		L_71 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_70);
		V_14 = L_71;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_73, L_72, NULL);
		V_15 = L_73;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_74 = V_15;
		String_t* L_75 = V_3;
		L_74->___tipo_0 = L_75;
		Il2CppCodeGenWriteBarrier((void**)(&L_74->___tipo_0), (void*)L_75);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_76 = V_15;
		String_t* L_77 = V_4;
		L_76->___marca_1 = L_77;
		Il2CppCodeGenWriteBarrier((void**)(&L_76->___marca_1), (void*)L_77);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_78 = V_15;
		String_t* L_79 = V_5;
		L_78->___color_2 = L_79;
		Il2CppCodeGenWriteBarrier((void**)(&L_78->___color_2), (void*)L_79);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_80 = V_15;
		String_t* L_81 = V_6;
		L_80->___material_3 = L_81;
		Il2CppCodeGenWriteBarrier((void**)(&L_80->___material_3), (void*)L_81);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_82 = V_15;
		String_t* L_83 = V_7;
		L_82->___potencia_4 = L_83;
		Il2CppCodeGenWriteBarrier((void**)(&L_82->___potencia_4), (void*)L_83);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_84 = V_15;
		String_t* L_85 = V_8;
		L_84->___precision_5 = L_85;
		Il2CppCodeGenWriteBarrier((void**)(&L_84->___precision_5), (void*)L_85);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_86 = V_15;
		String_t* L_87 = V_9;
		L_86->___alcance_6 = L_87;
		Il2CppCodeGenWriteBarrier((void**)(&L_86->___alcance_6), (void*)L_87);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_88 = V_15;
		String_t* L_89 = V_10;
		L_88->___cadencia_7 = L_89;
		Il2CppCodeGenWriteBarrier((void**)(&L_88->___cadencia_7), (void*)L_89);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_90 = V_15;
		String_t* L_91 = V_11;
		L_90->___estabilidad_8 = L_91;
		Il2CppCodeGenWriteBarrier((void**)(&L_90->___estabilidad_8), (void*)L_91);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_92 = V_15;
		String_t* L_93 = V_12;
		L_92->___precio_9 = L_93;
		Il2CppCodeGenWriteBarrier((void**)(&L_92->___precio_9), (void*)L_93);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_94 = V_15;
		String_t* L_95 = V_13;
		L_94->___modelo_10 = L_95;
		Il2CppCodeGenWriteBarrier((void**)(&L_94->___modelo_10), (void*)L_95);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_96 = V_15;
		String_t* L_97 = V_14;
		L_96->___extra_11 = L_97;
		Il2CppCodeGenWriteBarrier((void**)(&L_96->___extra_11), (void*)L_97);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_98 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_100 = L_99;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_101 = V_15;
		String_t* L_102 = L_101->___tipo_0;
		ArrayElementTypeCheck (L_100, L_102);
		(L_100)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_102);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_100;
		ArrayElementTypeCheck (L_103, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_104 = L_103;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_15;
		String_t* L_106 = L_105->___marca_1;
		ArrayElementTypeCheck (L_104, L_106);
		(L_104)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_106);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_104;
		ArrayElementTypeCheck (L_107, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_108 = L_107;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_109 = V_15;
		String_t* L_110 = L_109->___modelo_10;
		ArrayElementTypeCheck (L_108, L_110);
		(L_108)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_110);
		String_t* L_111;
		L_111 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_108, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_98, L_111, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_112 = __this->___itemDatabase_13;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_113 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_112, L_113, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_114, 1));
	}

IL_023e:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_115 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_116 = V_0;
		int32_t L_117;
		L_117 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_116, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_115) < ((int32_t)L_117)))
		{
			goto IL_0040;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_118 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_119;
		L_119 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_118, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_119;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0293:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0288_1;
			}

IL_0254_1:
			{
				// foreach (string item in items)
				String_t* L_120;
				L_120 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_120;
				// if (item.Contains("Cabeza de pist?n"))
				String_t* L_121 = V_17;
				bool L_122;
				L_122 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_121, _stringLiteral4F3ABCDAF59D8FA7DA0C2AAA76E9C5784A781CF1, NULL);
				if (!L_122)
				{
					goto IL_0288_1;
				}
			}
			{
				// CabezaPiston.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_123 = __this->___CabezaPiston_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_124;
				L_124 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_123, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_125 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_125, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_126 = L_125;
				String_t* L_127 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_126, L_127, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_124, L_126, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_0288_1:
			{
				// foreach (string item in items)
				bool L_128;
				L_128 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_128)
				{
					goto IL_0254_1;
				}
			}
			{
				goto IL_02a1;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_02a1:
	{
		// CabezaPiston.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_129 = __this->___CabezaPiston_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_129, NULL);
		// CabezaPiston.onValueChanged.AddListener(delegate { dropDownSelected(CabezaPiston); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_130 = __this->___CabezaPiston_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_131;
		L_131 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_130, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_132 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_132, __this, (intptr_t)((void*)DropDownHandlerCabezaPiston_U3CStartU3Eb__11_0_m07769265758FD3A769226BE96ACA9252B63A69D3_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_131, L_132, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandlerCabezaPiston::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCabezaPiston_dropDownSelected_m9FEAB9E9AEDE859C229C91DD5496946EAFF6FA04 (DropDownHandlerCabezaPiston_t5D4F7822D76A01DD44715998B4C3911DC1DBE889* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4DE573297F69792F2A52321674F0590D7424E99A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* V_3 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (alcanceDrop != 0)
		double L_2 = __this->___alcanceDrop_7;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_3 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (varianzaDrop != 0)
		float L_4 = __this->___varianzaDrop_10;
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_0067;
		}
	}
	{
		// Window_Graph.setVarianza(0, varianzaDrop);
		float L_5 = __this->___varianzaDrop_10;
		Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8((0.0f), L_5, NULL);
	}

IL_0067:
	{
		// if (precisionDrop != 0)
		double L_6 = __this->___precisionDrop_8;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_008c;
		}
	}
	{
		// Window_Graph.setPrecision(0, precisionDrop);
		double L_7 = __this->___precisionDrop_8;
		Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6((0.0), L_7, NULL);
	}

IL_008c:
	{
		// if (cadenciaDrop != 0)
		double L_8 = __this->___cadenciaDrop_9;
		if ((((double)L_8) == ((double)(0.0))))
		{
			goto IL_00b1;
		}
	}
	{
		// Window_Graph.setCadencia(0, cadenciaDrop);
		double L_9 = __this->___cadenciaDrop_9;
		Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142((0.0), L_9, NULL);
	}

IL_00b1:
	{
		// if (precio != 0)
		double L_10 = __this->___precio_11;
		if ((((double)L_10) == ((double)(0.0))))
		{
			goto IL_00d6;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_11 = __this->___precio_11;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_11, NULL);
	}

IL_00d6:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_12 = ___dropdown0;
		int32_t L_13;
		L_13 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_12, NULL);
		V_0 = L_13;
		// Debug.Log(CabezaPiston.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_14 = __this->___CabezaPiston_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_15;
		L_15 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_14, NULL);
		int32_t L_16 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_17;
		L_17 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_15, L_16, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_18;
		L_18 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_17, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_18, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_19 = __this->___itemDatabase_13;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_20;
		L_20 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_19, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_20;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_03f3:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_03e5_1;
			}

IL_0109_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_21;
				L_21 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_21;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_24 = V_2;
				String_t* L_25 = L_24->___tipo_0;
				ArrayElementTypeCheck (L_23, L_25);
				(L_23)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_25);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_23;
				ArrayElementTypeCheck (L_26, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_26)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_27 = L_26;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_28 = V_2;
				String_t* L_29 = L_28->___marca_1;
				ArrayElementTypeCheck (L_27, L_29);
				(L_27)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_29);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_30 = L_27;
				ArrayElementTypeCheck (L_30, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_30)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_31 = L_30;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_32 = V_2;
				String_t* L_33 = L_32->___modelo_10;
				ArrayElementTypeCheck (L_31, L_33);
				(L_31)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_33);
				String_t* L_34;
				L_34 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_31, NULL);
				// if (comparar == CabezaPiston.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_35 = __this->___CabezaPiston_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_36;
				L_36 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_35, NULL);
				int32_t L_37 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_38;
				L_38 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_36, L_37, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_39;
				L_39 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_38, NULL);
				bool L_40;
				L_40 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_34, L_39, NULL);
				if (!L_40)
				{
					goto IL_03e5_1;
				}
			}
			{
				// potenciaDrop = int.Parse(Componente.potencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_41 = V_2;
				String_t* L_42 = L_41->___potencia_4;
				int32_t L_43;
				L_43 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_42, NULL);
				__this->___potenciaDrop_6 = ((double)L_43);
				// alcanceDrop = int.Parse(Componente.alcance);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_44 = V_2;
				String_t* L_45 = L_44->___alcance_6;
				int32_t L_46;
				L_46 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_45, NULL);
				__this->___alcanceDrop_7 = ((double)L_46);
				// varianzaDrop = int.Parse(Componente.estabilidad);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_47 = V_2;
				String_t* L_48 = L_47->___estabilidad_8;
				int32_t L_49;
				L_49 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_48, NULL);
				__this->___varianzaDrop_10 = ((float)L_49);
				// precisionDrop = int.Parse(Componente.precision);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_50 = V_2;
				String_t* L_51 = L_50->___precision_5;
				int32_t L_52;
				L_52 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_51, NULL);
				__this->___precisionDrop_8 = ((double)L_52);
				// cadenciaDrop = int.Parse(Componente.cadencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_53 = V_2;
				String_t* L_54 = L_53->___cadencia_7;
				int32_t L_55;
				L_55 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_54, NULL);
				__this->___cadenciaDrop_9 = ((double)L_55);
				// precio = int.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_56 = V_2;
				String_t* L_57 = L_56->___precio_9;
				int32_t L_58;
				L_58 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_57, NULL);
				__this->___precio_11 = ((double)L_58);
				// extra = Componente.extra;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_59 = V_2;
				String_t* L_60 = L_59->___extra_11;
				__this->___extra_12 = L_60;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_12), (void*)L_60);
				// Window_Graph.setAlcance(alcanceDrop);
				double L_61 = __this->___alcanceDrop_7;
				Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2(L_61, (0.0), NULL);
				// Window_Graph.setPotencia(potenciaDrop);
				double L_62 = __this->___potenciaDrop_6;
				Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E(L_62, (0.0), NULL);
				// Window_Graph.setVarianza(varianzaDrop);
				float L_63 = __this->___varianzaDrop_10;
				Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8(L_63, (0.0f), NULL);
				// Window_Graph.setPrecision(precisionDrop);
				double L_64 = __this->___precisionDrop_8;
				Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6(L_64, (0.0), NULL);
				// Window_Graph.setCadencia(cadenciaDrop);
				double L_65 = __this->___cadenciaDrop_9;
				Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142(L_65, (0.0), NULL);
				// Window_Graph.setPrecio(precio,0);
				double L_66 = __this->___precio_11;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_66, (0.0), NULL);
				// var renderer = CabezaPistonPieza.GetComponent<Renderer>();
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_67 = __this->___CabezaPistonPieza_14;
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_68;
				L_68 = GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A(L_67, GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
				V_3 = L_68;
				// if (Componente.color == "Rojo")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_69 = V_2;
				String_t* L_70 = L_69->___color_2;
				bool L_71;
				L_71 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_70, _stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C, NULL);
				if (!L_71)
				{
					goto IL_0286_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.red);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_72 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_73;
				L_73 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_72, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_74;
				L_74 = Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_73, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_74, NULL);
			}

IL_0286_1:
			{
				// if (Componente.color == "Gris")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_75 = V_2;
				String_t* L_76 = L_75->___color_2;
				bool L_77;
				L_77 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_76, _stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116, NULL);
				if (!L_77)
				{
					goto IL_02ad_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_78 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_79;
				L_79 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_78, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_80;
				L_80 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_79, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_80, NULL);
			}

IL_02ad_1:
			{
				// if (Componente.color == "Gris oscuro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_81 = V_2;
				String_t* L_82 = L_81->___color_2;
				bool L_83;
				L_83 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_82, _stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC, NULL);
				if (!L_83)
				{
					goto IL_02d4_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_84 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_85;
				L_85 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_84, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_86;
				L_86 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_85, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_86, NULL);
			}

IL_02d4_1:
			{
				// if (Componente.color == "Gris claro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_87 = V_2;
				String_t* L_88 = L_87->___color_2;
				bool L_89;
				L_89 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_88, _stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6, NULL);
				if (!L_89)
				{
					goto IL_02fb_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_90 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_91;
				L_91 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_90, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_92;
				L_92 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_91, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_92, NULL);
			}

IL_02fb_1:
			{
				// if (Componente.color == "Azul")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_93 = V_2;
				String_t* L_94 = L_93->___color_2;
				bool L_95;
				L_95 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_94, _stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510, NULL);
				if (!L_95)
				{
					goto IL_0322_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.blue);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_96 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_97;
				L_97 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_96, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_98;
				L_98 = Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_97, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_98, NULL);
			}

IL_0322_1:
			{
				// if (Componente.color == "Negro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_99 = V_2;
				String_t* L_100 = L_99->___color_2;
				bool L_101;
				L_101 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_100, _stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9, NULL);
				if (!L_101)
				{
					goto IL_0349_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_102 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_103;
				L_103 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_102, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_104;
				L_104 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_103, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_104, NULL);
			}

IL_0349_1:
			{
				// if (Componente.color == "Dorado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_2;
				String_t* L_106 = L_105->___color_2;
				bool L_107;
				L_107 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_106, _stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E, NULL);
				if (!L_107)
				{
					goto IL_0370_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.yellow);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_108 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_109;
				L_109 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_108, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_110;
				L_110 = Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_109, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_110, NULL);
			}

IL_0370_1:
			{
				// if (Componente.color == "Plateado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_111 = V_2;
				String_t* L_112 = L_111->___color_2;
				bool L_113;
				L_113 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_112, _stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF, NULL);
				if (!L_113)
				{
					goto IL_0397_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_114 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_115;
				L_115 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_114, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_116;
				L_116 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_115, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_116, NULL);
			}

IL_0397_1:
			{
				// if (Componente.color == "Cobre")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_117 = V_2;
				String_t* L_118 = L_117->___color_2;
				bool L_119;
				L_119 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_118, _stringLiteral4DE573297F69792F2A52321674F0590D7424E99A, NULL);
				if (!L_119)
				{
					goto IL_03be_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_120 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_121;
				L_121 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_120, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_122;
				L_122 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_121, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_122, NULL);
			}

IL_03be_1:
			{
				// if (Componente.color == "")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_123 = V_2;
				String_t* L_124 = L_123->___color_2;
				bool L_125;
				L_125 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_124, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
				if (!L_125)
				{
					goto IL_03e5_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_126 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_127;
				L_127 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_126, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_128;
				L_128 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_127, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_128, NULL);
			}

IL_03e5_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_129;
				L_129 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_129)
				{
					goto IL_0109_1;
				}
			}
			{
				goto IL_0401;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0401:
	{
		// }
		return;
	}
}
// System.Void DropDownHandlerCabezaPiston::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCabezaPiston__ctor_mD1B50AE226A307C5A541BAD4C624BCC62D4540F1 (DropDownHandlerCabezaPiston_t5D4F7822D76A01DD44715998B4C3911DC1DBE889* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_13 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_13), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandlerCabezaPiston::<Start>b__11_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCabezaPiston_U3CStartU3Eb__11_0_m07769265758FD3A769226BE96ACA9252B63A69D3 (DropDownHandlerCabezaPiston_t5D4F7822D76A01DD44715998B4C3911DC1DBE889* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// CabezaPiston.onValueChanged.AddListener(delegate { dropDownSelected(CabezaPiston); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___CabezaPiston_5;
		DropDownHandlerCabezaPiston_dropDownSelected_m9FEAB9E9AEDE859C229C91DD5496946EAFF6FA04(__this, L_0, NULL);
		// CabezaPiston.onValueChanged.AddListener(delegate { dropDownSelected(CabezaPiston); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandlerCamaraDeHop::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCamaraDeHop_Start_mF3CBEC015C42DC579E90CAABA610A884B0D2714B (DropDownHandlerCamaraDeHop_t9C574C9C85718804384E4CA248294DDC9E19F1EB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandlerCamaraDeHop_U3CStartU3Eb__10_0_mAB4601E49DF183C8B62D0285C3E36F79338576E0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3CCEDB9B56E2B84E87134D756DA9D4163B98651E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFEA325339A560D2AFDE40181E7F52A64A5563B63);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// CamaraDeHop = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___CamaraDeHop_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___CamaraDeHop_5), (void*)L_0);
		// CamaraHopPieza = GameObject.Find("C?maraDeHop");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral3CCEDB9B56E2B84E87134D756DA9D4163B98651E, NULL);
		__this->___CamaraHopPieza_12 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___CamaraHopPieza_12), (void*)L_1);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2;
		L_2 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_2;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_3 = V_0;
		int32_t L_4;
		L_4 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_3, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_4);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_6 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_6, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_6;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_023e;
	}

IL_0040:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_7 = V_0;
		int32_t L_8 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_9;
		L_9 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_7, L_8, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_10;
		L_10 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_9, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_11;
		L_11 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_12 = V_0;
		int32_t L_13 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_14;
		L_14 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_12, L_13, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_15;
		L_15 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_14, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_3 = L_16;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_17 = V_0;
		int32_t L_18 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_19;
		L_19 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_17, L_18, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_20;
		L_20 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_19, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_21;
		L_21 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		V_4 = L_21;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_22 = V_0;
		int32_t L_23 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_24;
		L_24 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_22, L_23, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_25;
		L_25 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_24, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_26;
		L_26 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		V_5 = L_26;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_27 = V_0;
		int32_t L_28 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_29;
		L_29 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_27, L_28, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_30;
		L_30 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_29, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_31;
		L_31 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		V_6 = L_31;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_32 = V_0;
		int32_t L_33 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_34;
		L_34 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_32, L_33, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_35;
		L_35 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_34, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_36;
		L_36 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		V_7 = L_36;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_37 = V_0;
		int32_t L_38 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_39;
		L_39 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_37, L_38, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_40;
		L_40 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_39, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_41;
		L_41 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_40);
		V_8 = L_41;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_42 = V_0;
		int32_t L_43 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_44;
		L_44 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_42, L_43, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_45;
		L_45 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_44, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_46;
		L_46 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_45);
		V_9 = L_46;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_47 = V_0;
		int32_t L_48 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_49;
		L_49 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_47, L_48, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_50;
		L_50 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_49, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_51;
		L_51 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_50);
		V_10 = L_51;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_52 = V_0;
		int32_t L_53 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_54;
		L_54 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_52, L_53, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_55;
		L_55 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_54, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_56;
		L_56 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_55);
		V_11 = L_56;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_57 = V_0;
		int32_t L_58 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_59;
		L_59 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_57, L_58, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_60;
		L_60 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_59, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_61;
		L_61 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_60);
		V_12 = L_61;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_62 = V_0;
		int32_t L_63 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_64;
		L_64 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_62, L_63, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_65;
		L_65 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_64, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_66;
		L_66 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_65);
		V_13 = L_66;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_67 = V_0;
		int32_t L_68 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_69;
		L_69 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_67, L_68, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_70;
		L_70 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_69, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_71;
		L_71 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_70);
		V_14 = L_71;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_73, L_72, NULL);
		V_15 = L_73;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_74 = V_15;
		String_t* L_75 = V_3;
		L_74->___tipo_0 = L_75;
		Il2CppCodeGenWriteBarrier((void**)(&L_74->___tipo_0), (void*)L_75);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_76 = V_15;
		String_t* L_77 = V_4;
		L_76->___marca_1 = L_77;
		Il2CppCodeGenWriteBarrier((void**)(&L_76->___marca_1), (void*)L_77);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_78 = V_15;
		String_t* L_79 = V_5;
		L_78->___color_2 = L_79;
		Il2CppCodeGenWriteBarrier((void**)(&L_78->___color_2), (void*)L_79);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_80 = V_15;
		String_t* L_81 = V_6;
		L_80->___material_3 = L_81;
		Il2CppCodeGenWriteBarrier((void**)(&L_80->___material_3), (void*)L_81);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_82 = V_15;
		String_t* L_83 = V_7;
		L_82->___potencia_4 = L_83;
		Il2CppCodeGenWriteBarrier((void**)(&L_82->___potencia_4), (void*)L_83);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_84 = V_15;
		String_t* L_85 = V_8;
		L_84->___precision_5 = L_85;
		Il2CppCodeGenWriteBarrier((void**)(&L_84->___precision_5), (void*)L_85);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_86 = V_15;
		String_t* L_87 = V_9;
		L_86->___alcance_6 = L_87;
		Il2CppCodeGenWriteBarrier((void**)(&L_86->___alcance_6), (void*)L_87);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_88 = V_15;
		String_t* L_89 = V_10;
		L_88->___cadencia_7 = L_89;
		Il2CppCodeGenWriteBarrier((void**)(&L_88->___cadencia_7), (void*)L_89);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_90 = V_15;
		String_t* L_91 = V_11;
		L_90->___estabilidad_8 = L_91;
		Il2CppCodeGenWriteBarrier((void**)(&L_90->___estabilidad_8), (void*)L_91);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_92 = V_15;
		String_t* L_93 = V_12;
		L_92->___precio_9 = L_93;
		Il2CppCodeGenWriteBarrier((void**)(&L_92->___precio_9), (void*)L_93);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_94 = V_15;
		String_t* L_95 = V_13;
		L_94->___modelo_10 = L_95;
		Il2CppCodeGenWriteBarrier((void**)(&L_94->___modelo_10), (void*)L_95);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_96 = V_15;
		String_t* L_97 = V_14;
		L_96->___extra_11 = L_97;
		Il2CppCodeGenWriteBarrier((void**)(&L_96->___extra_11), (void*)L_97);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_98 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_100 = L_99;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_101 = V_15;
		String_t* L_102 = L_101->___tipo_0;
		ArrayElementTypeCheck (L_100, L_102);
		(L_100)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_102);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_100;
		ArrayElementTypeCheck (L_103, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_104 = L_103;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_15;
		String_t* L_106 = L_105->___marca_1;
		ArrayElementTypeCheck (L_104, L_106);
		(L_104)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_106);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_104;
		ArrayElementTypeCheck (L_107, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_108 = L_107;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_109 = V_15;
		String_t* L_110 = L_109->___modelo_10;
		ArrayElementTypeCheck (L_108, L_110);
		(L_108)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_110);
		String_t* L_111;
		L_111 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_108, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_98, L_111, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_112 = __this->___itemDatabase_13;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_113 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_112, L_113, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_114, 1));
	}

IL_023e:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_115 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_116 = V_0;
		int32_t L_117;
		L_117 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_116, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_115) < ((int32_t)L_117)))
		{
			goto IL_0040;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_118 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_119;
		L_119 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_118, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_119;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0293:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0288_1;
			}

IL_0254_1:
			{
				// foreach (string item in items)
				String_t* L_120;
				L_120 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_120;
				// if (item.Contains("C?maras de hop"))
				String_t* L_121 = V_17;
				bool L_122;
				L_122 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_121, _stringLiteralFEA325339A560D2AFDE40181E7F52A64A5563B63, NULL);
				if (!L_122)
				{
					goto IL_0288_1;
				}
			}
			{
				// CamaraDeHop.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_123 = __this->___CamaraDeHop_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_124;
				L_124 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_123, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_125 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_125, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_126 = L_125;
				String_t* L_127 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_126, L_127, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_124, L_126, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_0288_1:
			{
				// foreach (string item in items)
				bool L_128;
				L_128 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_128)
				{
					goto IL_0254_1;
				}
			}
			{
				goto IL_02a1;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_02a1:
	{
		// CamaraDeHop.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_129 = __this->___CamaraDeHop_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_129, NULL);
		// CamaraDeHop.onValueChanged.AddListener(delegate { dropDownSelected(CamaraDeHop); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_130 = __this->___CamaraDeHop_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_131;
		L_131 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_130, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_132 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_132, __this, (intptr_t)((void*)DropDownHandlerCamaraDeHop_U3CStartU3Eb__10_0_mAB4601E49DF183C8B62D0285C3E36F79338576E0_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_131, L_132, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandlerCamaraDeHop::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCamaraDeHop_dropDownSelected_mC9EA5084FE00C36BCB86EDF82DFA6BD4632505A1 (DropDownHandlerCamaraDeHop_t9C574C9C85718804384E4CA248294DDC9E19F1EB* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4DE573297F69792F2A52321674F0590D7424E99A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* V_3 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (precisionDrop != 0)
		double L_2 = __this->___precisionDrop_8;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setPrecision(0, precisionDrop);
		double L_3 = __this->___precisionDrop_8;
		Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (alcanceDrop != 0)
		double L_4 = __this->___alcanceDrop_7;
		if ((((double)L_4) == ((double)(0.0))))
		{
			goto IL_006f;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_5 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_5, NULL);
	}

IL_006f:
	{
		// if (cadenciaDrop != 0)
		double L_6 = __this->___cadenciaDrop_9;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_0094;
		}
	}
	{
		// Window_Graph.setCadencia(0, cadenciaDrop);
		double L_7 = __this->___cadenciaDrop_9;
		Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142((0.0), L_7, NULL);
	}

IL_0094:
	{
		// if (precio != 0)
		double L_8 = __this->___precio_10;
		if ((((double)L_8) == ((double)(0.0))))
		{
			goto IL_00b9;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_9 = __this->___precio_10;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_9, NULL);
	}

IL_00b9:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_10 = ___dropdown0;
		int32_t L_11;
		L_11 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_10, NULL);
		V_0 = L_11;
		// Debug.Log(CamaraDeHop.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_12 = __this->___CamaraDeHop_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_13;
		L_13 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_12, NULL);
		int32_t L_14 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_15;
		L_15 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_13, L_14, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_16;
		L_16 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_15, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_16, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_17 = __this->___itemDatabase_13;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_18;
		L_18 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_17, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_18;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0327:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0319_1;
			}

IL_00ec_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_19;
				L_19 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_19;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_21 = L_20;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_22 = V_2;
				String_t* L_23 = L_22->___tipo_0;
				ArrayElementTypeCheck (L_21, L_23);
				(L_21)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_23);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_24 = L_21;
				ArrayElementTypeCheck (L_24, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_24)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = L_24;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_26 = V_2;
				String_t* L_27 = L_26->___marca_1;
				ArrayElementTypeCheck (L_25, L_27);
				(L_25)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_27);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_28 = L_25;
				ArrayElementTypeCheck (L_28, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_28)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_29 = L_28;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_30 = V_2;
				String_t* L_31 = L_30->___modelo_10;
				ArrayElementTypeCheck (L_29, L_31);
				(L_29)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_31);
				String_t* L_32;
				L_32 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_29, NULL);
				// if (comparar == CamaraDeHop.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_33 = __this->___CamaraDeHop_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_34;
				L_34 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_33, NULL);
				int32_t L_35 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_36;
				L_36 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_34, L_35, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_37;
				L_37 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_36, NULL);
				bool L_38;
				L_38 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_32, L_37, NULL);
				if (!L_38)
				{
					goto IL_0319_1;
				}
			}
			{
				// precio = int.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_39 = V_2;
				String_t* L_40 = L_39->___precio_9;
				int32_t L_41;
				L_41 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_40, NULL);
				__this->___precio_10 = ((double)L_41);
				// extra = Componente.extra;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_42 = V_2;
				String_t* L_43 = L_42->___extra_11;
				__this->___extra_11 = L_43;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_11), (void*)L_43);
				// Window_Graph.setExtra(extra);
				String_t* L_44 = __this->___extra_11;
				Window_Graph_setExtra_m8774145D45E480AAFA50F295A5FB4E00A289FF91_inline(L_44, NULL);
				// Window_Graph.setPrecio(precio,0);
				double L_45 = __this->___precio_10;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_45, (0.0), NULL);
				// var renderer = CamaraHopPieza.GetComponent<Renderer>();
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_46 = __this->___CamaraHopPieza_12;
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_47;
				L_47 = GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A(L_46, GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
				V_3 = L_47;
				// if (Componente.color == "Rojo")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_48 = V_2;
				String_t* L_49 = L_48->___color_2;
				bool L_50;
				L_50 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_49, _stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C, NULL);
				if (!L_50)
				{
					goto IL_01ba_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.red);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_51 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_52;
				L_52 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_51, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_53;
				L_53 = Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_52, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_53, NULL);
			}

IL_01ba_1:
			{
				// if (Componente.color == "Gris")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_54 = V_2;
				String_t* L_55 = L_54->___color_2;
				bool L_56;
				L_56 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_55, _stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116, NULL);
				if (!L_56)
				{
					goto IL_01e1_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_57 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_58;
				L_58 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_57, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_59;
				L_59 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_58, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_59, NULL);
			}

IL_01e1_1:
			{
				// if (Componente.color == "Gris oscuro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_60 = V_2;
				String_t* L_61 = L_60->___color_2;
				bool L_62;
				L_62 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_61, _stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC, NULL);
				if (!L_62)
				{
					goto IL_0208_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_63 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_64;
				L_64 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_63, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_65;
				L_65 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_64, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_65, NULL);
			}

IL_0208_1:
			{
				// if (Componente.color == "Gris claro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_66 = V_2;
				String_t* L_67 = L_66->___color_2;
				bool L_68;
				L_68 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_67, _stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6, NULL);
				if (!L_68)
				{
					goto IL_022f_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_69 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_70;
				L_70 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_69, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_71;
				L_71 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_70, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_71, NULL);
			}

IL_022f_1:
			{
				// if (Componente.color == "Azul")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = V_2;
				String_t* L_73 = L_72->___color_2;
				bool L_74;
				L_74 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_73, _stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510, NULL);
				if (!L_74)
				{
					goto IL_0256_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.blue);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_75 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_76;
				L_76 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_75, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_77;
				L_77 = Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_76, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_77, NULL);
			}

IL_0256_1:
			{
				// if (Componente.color == "Negro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_78 = V_2;
				String_t* L_79 = L_78->___color_2;
				bool L_80;
				L_80 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_79, _stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9, NULL);
				if (!L_80)
				{
					goto IL_027d_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_81 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_82;
				L_82 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_81, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_83;
				L_83 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_82, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_83, NULL);
			}

IL_027d_1:
			{
				// if (Componente.color == "Dorado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_84 = V_2;
				String_t* L_85 = L_84->___color_2;
				bool L_86;
				L_86 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_85, _stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E, NULL);
				if (!L_86)
				{
					goto IL_02a4_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.yellow);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_87 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_88;
				L_88 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_87, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_89;
				L_89 = Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_88, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_89, NULL);
			}

IL_02a4_1:
			{
				// if (Componente.color == "Plateado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_90 = V_2;
				String_t* L_91 = L_90->___color_2;
				bool L_92;
				L_92 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_91, _stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF, NULL);
				if (!L_92)
				{
					goto IL_02cb_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_93 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_94;
				L_94 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_93, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_95;
				L_95 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_94, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_95, NULL);
			}

IL_02cb_1:
			{
				// if (Componente.color == "Cobre")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_96 = V_2;
				String_t* L_97 = L_96->___color_2;
				bool L_98;
				L_98 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_97, _stringLiteral4DE573297F69792F2A52321674F0590D7424E99A, NULL);
				if (!L_98)
				{
					goto IL_02f2_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_99 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_100;
				L_100 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_99, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_101;
				L_101 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_100, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_101, NULL);
			}

IL_02f2_1:
			{
				// if (Componente.color == "")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_102 = V_2;
				String_t* L_103 = L_102->___color_2;
				bool L_104;
				L_104 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_103, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
				if (!L_104)
				{
					goto IL_0319_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_105 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_106;
				L_106 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_105, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_107;
				L_107 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_106, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_107, NULL);
			}

IL_0319_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_108;
				L_108 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_108)
				{
					goto IL_00ec_1;
				}
			}
			{
				goto IL_0335;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0335:
	{
		// }
		return;
	}
}
// System.Void DropDownHandlerCamaraDeHop::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCamaraDeHop__ctor_mE2A88AD7E1E137844EF5EDA79BC10547EB0554A5 (DropDownHandlerCamaraDeHop_t9C574C9C85718804384E4CA248294DDC9E19F1EB* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_13 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_13), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandlerCamaraDeHop::<Start>b__10_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCamaraDeHop_U3CStartU3Eb__10_0_mAB4601E49DF183C8B62D0285C3E36F79338576E0 (DropDownHandlerCamaraDeHop_t9C574C9C85718804384E4CA248294DDC9E19F1EB* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// CamaraDeHop.onValueChanged.AddListener(delegate { dropDownSelected(CamaraDeHop); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___CamaraDeHop_5;
		DropDownHandlerCamaraDeHop_dropDownSelected_mC9EA5084FE00C36BCB86EDF82DFA6BD4632505A1(__this, L_0, NULL);
		// CamaraDeHop.onValueChanged.AddListener(delegate { dropDownSelected(CamaraDeHop); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandlerCa?on::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCaUF1on_Start_mFDC82067CA1B2DF553FBCCD6EBDF5B0DB1B5AC62 (DropDownHandlerCaUF1on_tB8652C4AED37060D00139A5D51D4FBB60BABB0C5* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandlerCaUF1on_U3CStartU3Eb__11_0_m9B7D9E4E7E364A736329813AB4BA2A14B048A812_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2294E7DC619786B244545E97D823B83D355CF6BE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// Ca?on = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___CaUF1on_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___CaUF1on_5), (void*)L_0);
		// Ca?onPieza = GameObject.Find("Ca??n");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral2294E7DC619786B244545E97D823B83D355CF6BE, NULL);
		__this->___CaUF1onPieza_13 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___CaUF1onPieza_13), (void*)L_1);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2;
		L_2 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_2;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_3 = V_0;
		int32_t L_4;
		L_4 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_3, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_4);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_6 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_6, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_6;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_023e;
	}

IL_0040:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_7 = V_0;
		int32_t L_8 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_9;
		L_9 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_7, L_8, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_10;
		L_10 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_9, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_11;
		L_11 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_12 = V_0;
		int32_t L_13 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_14;
		L_14 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_12, L_13, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_15;
		L_15 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_14, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_3 = L_16;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_17 = V_0;
		int32_t L_18 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_19;
		L_19 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_17, L_18, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_20;
		L_20 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_19, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_21;
		L_21 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		V_4 = L_21;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_22 = V_0;
		int32_t L_23 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_24;
		L_24 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_22, L_23, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_25;
		L_25 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_24, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_26;
		L_26 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		V_5 = L_26;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_27 = V_0;
		int32_t L_28 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_29;
		L_29 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_27, L_28, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_30;
		L_30 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_29, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_31;
		L_31 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		V_6 = L_31;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_32 = V_0;
		int32_t L_33 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_34;
		L_34 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_32, L_33, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_35;
		L_35 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_34, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_36;
		L_36 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		V_7 = L_36;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_37 = V_0;
		int32_t L_38 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_39;
		L_39 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_37, L_38, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_40;
		L_40 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_39, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_41;
		L_41 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_40);
		V_8 = L_41;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_42 = V_0;
		int32_t L_43 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_44;
		L_44 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_42, L_43, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_45;
		L_45 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_44, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_46;
		L_46 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_45);
		V_9 = L_46;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_47 = V_0;
		int32_t L_48 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_49;
		L_49 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_47, L_48, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_50;
		L_50 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_49, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_51;
		L_51 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_50);
		V_10 = L_51;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_52 = V_0;
		int32_t L_53 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_54;
		L_54 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_52, L_53, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_55;
		L_55 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_54, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_56;
		L_56 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_55);
		V_11 = L_56;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_57 = V_0;
		int32_t L_58 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_59;
		L_59 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_57, L_58, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_60;
		L_60 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_59, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_61;
		L_61 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_60);
		V_12 = L_61;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_62 = V_0;
		int32_t L_63 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_64;
		L_64 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_62, L_63, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_65;
		L_65 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_64, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_66;
		L_66 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_65);
		V_13 = L_66;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_67 = V_0;
		int32_t L_68 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_69;
		L_69 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_67, L_68, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_70;
		L_70 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_69, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_71;
		L_71 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_70);
		V_14 = L_71;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_73, L_72, NULL);
		V_15 = L_73;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_74 = V_15;
		String_t* L_75 = V_3;
		L_74->___tipo_0 = L_75;
		Il2CppCodeGenWriteBarrier((void**)(&L_74->___tipo_0), (void*)L_75);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_76 = V_15;
		String_t* L_77 = V_4;
		L_76->___marca_1 = L_77;
		Il2CppCodeGenWriteBarrier((void**)(&L_76->___marca_1), (void*)L_77);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_78 = V_15;
		String_t* L_79 = V_5;
		L_78->___color_2 = L_79;
		Il2CppCodeGenWriteBarrier((void**)(&L_78->___color_2), (void*)L_79);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_80 = V_15;
		String_t* L_81 = V_6;
		L_80->___material_3 = L_81;
		Il2CppCodeGenWriteBarrier((void**)(&L_80->___material_3), (void*)L_81);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_82 = V_15;
		String_t* L_83 = V_7;
		L_82->___potencia_4 = L_83;
		Il2CppCodeGenWriteBarrier((void**)(&L_82->___potencia_4), (void*)L_83);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_84 = V_15;
		String_t* L_85 = V_8;
		L_84->___precision_5 = L_85;
		Il2CppCodeGenWriteBarrier((void**)(&L_84->___precision_5), (void*)L_85);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_86 = V_15;
		String_t* L_87 = V_9;
		L_86->___alcance_6 = L_87;
		Il2CppCodeGenWriteBarrier((void**)(&L_86->___alcance_6), (void*)L_87);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_88 = V_15;
		String_t* L_89 = V_10;
		L_88->___cadencia_7 = L_89;
		Il2CppCodeGenWriteBarrier((void**)(&L_88->___cadencia_7), (void*)L_89);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_90 = V_15;
		String_t* L_91 = V_11;
		L_90->___estabilidad_8 = L_91;
		Il2CppCodeGenWriteBarrier((void**)(&L_90->___estabilidad_8), (void*)L_91);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_92 = V_15;
		String_t* L_93 = V_12;
		L_92->___precio_9 = L_93;
		Il2CppCodeGenWriteBarrier((void**)(&L_92->___precio_9), (void*)L_93);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_94 = V_15;
		String_t* L_95 = V_13;
		L_94->___modelo_10 = L_95;
		Il2CppCodeGenWriteBarrier((void**)(&L_94->___modelo_10), (void*)L_95);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_96 = V_15;
		String_t* L_97 = V_14;
		L_96->___extra_11 = L_97;
		Il2CppCodeGenWriteBarrier((void**)(&L_96->___extra_11), (void*)L_97);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_98 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_100 = L_99;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_101 = V_15;
		String_t* L_102 = L_101->___tipo_0;
		ArrayElementTypeCheck (L_100, L_102);
		(L_100)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_102);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_100;
		ArrayElementTypeCheck (L_103, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_104 = L_103;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_15;
		String_t* L_106 = L_105->___marca_1;
		ArrayElementTypeCheck (L_104, L_106);
		(L_104)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_106);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_104;
		ArrayElementTypeCheck (L_107, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_108 = L_107;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_109 = V_15;
		String_t* L_110 = L_109->___modelo_10;
		ArrayElementTypeCheck (L_108, L_110);
		(L_108)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_110);
		String_t* L_111;
		L_111 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_108, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_98, L_111, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_112 = __this->___itemDatabase_14;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_113 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_112, L_113, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_114, 1));
	}

IL_023e:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_115 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_116 = V_0;
		int32_t L_117;
		L_117 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_116, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_115) < ((int32_t)L_117)))
		{
			goto IL_0040;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_118 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_119;
		L_119 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_118, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_119;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0293:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0288_1;
			}

IL_0254_1:
			{
				// foreach (string item in items)
				String_t* L_120;
				L_120 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_120;
				// if (item.Contains("Ca??n"))
				String_t* L_121 = V_17;
				bool L_122;
				L_122 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_121, _stringLiteral2294E7DC619786B244545E97D823B83D355CF6BE, NULL);
				if (!L_122)
				{
					goto IL_0288_1;
				}
			}
			{
				// Ca?on.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_123 = __this->___CaUF1on_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_124;
				L_124 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_123, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_125 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_125, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_126 = L_125;
				String_t* L_127 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_126, L_127, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_124, L_126, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_0288_1:
			{
				// foreach (string item in items)
				bool L_128;
				L_128 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_128)
				{
					goto IL_0254_1;
				}
			}
			{
				goto IL_02a1;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_02a1:
	{
		// Ca?on.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_129 = __this->___CaUF1on_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_129, NULL);
		// Ca?on.onValueChanged.AddListener(delegate { dropDownSelected(Ca?on); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_130 = __this->___CaUF1on_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_131;
		L_131 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_130, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_132 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_132, __this, (intptr_t)((void*)DropDownHandlerCaUF1on_U3CStartU3Eb__11_0_m9B7D9E4E7E364A736329813AB4BA2A14B048A812_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_131, L_132, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandlerCa?on::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCaUF1on_dropDownSelected_m37BA963612034E0DFD25BC0202DD2980285F31E8 (DropDownHandlerCaUF1on_tB8652C4AED37060D00139A5D51D4FBB60BABB0C5* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4DE573297F69792F2A52321674F0590D7424E99A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* V_3 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (alcanceDrop != 0)
		double L_2 = __this->___alcanceDrop_7;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_3 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (varianzaDrop != 0)
		float L_4 = __this->___varianzaDrop_10;
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_0067;
		}
	}
	{
		// Window_Graph.setVarianza(0, varianzaDrop);
		float L_5 = __this->___varianzaDrop_10;
		Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8((0.0f), L_5, NULL);
	}

IL_0067:
	{
		// if (precisionDrop != 0)
		double L_6 = __this->___precisionDrop_8;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_008c;
		}
	}
	{
		// Window_Graph.setPrecision(0, precisionDrop);
		double L_7 = __this->___precisionDrop_8;
		Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6((0.0), L_7, NULL);
	}

IL_008c:
	{
		// if (cadenciaDrop != 0)
		double L_8 = __this->___cadenciaDrop_9;
		if ((((double)L_8) == ((double)(0.0))))
		{
			goto IL_00b1;
		}
	}
	{
		// Window_Graph.setCadencia(0, cadenciaDrop);
		double L_9 = __this->___cadenciaDrop_9;
		Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142((0.0), L_9, NULL);
	}

IL_00b1:
	{
		// if (precio != 0)
		double L_10 = __this->___precio_11;
		if ((((double)L_10) == ((double)(0.0))))
		{
			goto IL_00d6;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_11 = __this->___precio_11;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_11, NULL);
	}

IL_00d6:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_12 = ___dropdown0;
		int32_t L_13;
		L_13 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_12, NULL);
		V_0 = L_13;
		// Debug.Log(Ca?on.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_14 = __this->___CaUF1on_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_15;
		L_15 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_14, NULL);
		int32_t L_16 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_17;
		L_17 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_15, L_16, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_18;
		L_18 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_17, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_18, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_19 = __this->___itemDatabase_14;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_20;
		L_20 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_19, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_20;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_03f3:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_03e5_1;
			}

IL_0109_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_21;
				L_21 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_21;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_24 = V_2;
				String_t* L_25 = L_24->___tipo_0;
				ArrayElementTypeCheck (L_23, L_25);
				(L_23)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_25);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_23;
				ArrayElementTypeCheck (L_26, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_26)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_27 = L_26;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_28 = V_2;
				String_t* L_29 = L_28->___marca_1;
				ArrayElementTypeCheck (L_27, L_29);
				(L_27)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_29);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_30 = L_27;
				ArrayElementTypeCheck (L_30, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_30)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_31 = L_30;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_32 = V_2;
				String_t* L_33 = L_32->___modelo_10;
				ArrayElementTypeCheck (L_31, L_33);
				(L_31)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_33);
				String_t* L_34;
				L_34 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_31, NULL);
				// if (comparar == Ca?on.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_35 = __this->___CaUF1on_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_36;
				L_36 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_35, NULL);
				int32_t L_37 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_38;
				L_38 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_36, L_37, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_39;
				L_39 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_38, NULL);
				bool L_40;
				L_40 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_34, L_39, NULL);
				if (!L_40)
				{
					goto IL_03e5_1;
				}
			}
			{
				// potenciaDrop = int.Parse(Componente.potencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_41 = V_2;
				String_t* L_42 = L_41->___potencia_4;
				int32_t L_43;
				L_43 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_42, NULL);
				__this->___potenciaDrop_6 = ((double)L_43);
				// alcanceDrop = int.Parse(Componente.alcance);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_44 = V_2;
				String_t* L_45 = L_44->___alcance_6;
				int32_t L_46;
				L_46 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_45, NULL);
				__this->___alcanceDrop_7 = ((double)L_46);
				// varianzaDrop = int.Parse(Componente.estabilidad);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_47 = V_2;
				String_t* L_48 = L_47->___estabilidad_8;
				int32_t L_49;
				L_49 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_48, NULL);
				__this->___varianzaDrop_10 = ((float)L_49);
				// precisionDrop = int.Parse(Componente.precision);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_50 = V_2;
				String_t* L_51 = L_50->___precision_5;
				int32_t L_52;
				L_52 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_51, NULL);
				__this->___precisionDrop_8 = ((double)L_52);
				// cadenciaDrop = int.Parse(Componente.cadencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_53 = V_2;
				String_t* L_54 = L_53->___cadencia_7;
				int32_t L_55;
				L_55 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_54, NULL);
				__this->___cadenciaDrop_9 = ((double)L_55);
				// precio = int.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_56 = V_2;
				String_t* L_57 = L_56->___precio_9;
				int32_t L_58;
				L_58 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_57, NULL);
				__this->___precio_11 = ((double)L_58);
				// extra = Componente.extra;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_59 = V_2;
				String_t* L_60 = L_59->___extra_11;
				__this->___extra_12 = L_60;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_12), (void*)L_60);
				// Window_Graph.setAlcance(alcanceDrop);
				double L_61 = __this->___alcanceDrop_7;
				Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2(L_61, (0.0), NULL);
				// Window_Graph.setPotencia(potenciaDrop);
				double L_62 = __this->___potenciaDrop_6;
				Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E(L_62, (0.0), NULL);
				// Window_Graph.setVarianza(varianzaDrop);
				float L_63 = __this->___varianzaDrop_10;
				Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8(L_63, (0.0f), NULL);
				// Window_Graph.setPrecision(precisionDrop);
				double L_64 = __this->___precisionDrop_8;
				Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6(L_64, (0.0), NULL);
				// Window_Graph.setCadencia(cadenciaDrop);
				double L_65 = __this->___cadenciaDrop_9;
				Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142(L_65, (0.0), NULL);
				// Window_Graph.setPrecio(precio,0);
				double L_66 = __this->___precio_11;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_66, (0.0), NULL);
				// var renderer = Ca?onPieza.GetComponent<Renderer>();
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_67 = __this->___CaUF1onPieza_13;
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_68;
				L_68 = GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A(L_67, GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
				V_3 = L_68;
				// if (Componente.color == "Rojo")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_69 = V_2;
				String_t* L_70 = L_69->___color_2;
				bool L_71;
				L_71 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_70, _stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C, NULL);
				if (!L_71)
				{
					goto IL_0286_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.red);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_72 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_73;
				L_73 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_72, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_74;
				L_74 = Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_73, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_74, NULL);
			}

IL_0286_1:
			{
				// if (Componente.color == "Gris")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_75 = V_2;
				String_t* L_76 = L_75->___color_2;
				bool L_77;
				L_77 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_76, _stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116, NULL);
				if (!L_77)
				{
					goto IL_02ad_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_78 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_79;
				L_79 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_78, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_80;
				L_80 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_79, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_80, NULL);
			}

IL_02ad_1:
			{
				// if (Componente.color == "Gris oscuro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_81 = V_2;
				String_t* L_82 = L_81->___color_2;
				bool L_83;
				L_83 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_82, _stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC, NULL);
				if (!L_83)
				{
					goto IL_02d4_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_84 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_85;
				L_85 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_84, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_86;
				L_86 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_85, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_86, NULL);
			}

IL_02d4_1:
			{
				// if (Componente.color == "Gris claro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_87 = V_2;
				String_t* L_88 = L_87->___color_2;
				bool L_89;
				L_89 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_88, _stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6, NULL);
				if (!L_89)
				{
					goto IL_02fb_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_90 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_91;
				L_91 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_90, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_92;
				L_92 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_91, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_92, NULL);
			}

IL_02fb_1:
			{
				// if (Componente.color == "Azul")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_93 = V_2;
				String_t* L_94 = L_93->___color_2;
				bool L_95;
				L_95 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_94, _stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510, NULL);
				if (!L_95)
				{
					goto IL_0322_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.blue);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_96 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_97;
				L_97 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_96, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_98;
				L_98 = Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_97, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_98, NULL);
			}

IL_0322_1:
			{
				// if (Componente.color == "Negro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_99 = V_2;
				String_t* L_100 = L_99->___color_2;
				bool L_101;
				L_101 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_100, _stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9, NULL);
				if (!L_101)
				{
					goto IL_0349_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_102 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_103;
				L_103 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_102, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_104;
				L_104 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_103, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_104, NULL);
			}

IL_0349_1:
			{
				// if (Componente.color == "Dorado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_2;
				String_t* L_106 = L_105->___color_2;
				bool L_107;
				L_107 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_106, _stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E, NULL);
				if (!L_107)
				{
					goto IL_0370_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.yellow);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_108 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_109;
				L_109 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_108, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_110;
				L_110 = Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_109, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_110, NULL);
			}

IL_0370_1:
			{
				// if (Componente.color == "Plateado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_111 = V_2;
				String_t* L_112 = L_111->___color_2;
				bool L_113;
				L_113 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_112, _stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF, NULL);
				if (!L_113)
				{
					goto IL_0397_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_114 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_115;
				L_115 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_114, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_116;
				L_116 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_115, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_116, NULL);
			}

IL_0397_1:
			{
				// if (Componente.color == "Cobre")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_117 = V_2;
				String_t* L_118 = L_117->___color_2;
				bool L_119;
				L_119 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_118, _stringLiteral4DE573297F69792F2A52321674F0590D7424E99A, NULL);
				if (!L_119)
				{
					goto IL_03be_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_120 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_121;
				L_121 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_120, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_122;
				L_122 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_121, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_122, NULL);
			}

IL_03be_1:
			{
				// if (Componente.color == "")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_123 = V_2;
				String_t* L_124 = L_123->___color_2;
				bool L_125;
				L_125 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_124, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
				if (!L_125)
				{
					goto IL_03e5_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_126 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_127;
				L_127 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_126, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_128;
				L_128 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_127, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_128, NULL);
			}

IL_03e5_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_129;
				L_129 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_129)
				{
					goto IL_0109_1;
				}
			}
			{
				goto IL_0401;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0401:
	{
		// }
		return;
	}
}
// System.Void DropDownHandlerCa?on::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCaUF1on__ctor_m685379D962D15569205FB724ADADB1B5B4CCF952 (DropDownHandlerCaUF1on_tB8652C4AED37060D00139A5D51D4FBB60BABB0C5* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_14 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_14), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandlerCa?on::<Start>b__11_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCaUF1on_U3CStartU3Eb__11_0_m9B7D9E4E7E364A736329813AB4BA2A14B048A812 (DropDownHandlerCaUF1on_tB8652C4AED37060D00139A5D51D4FBB60BABB0C5* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// Ca?on.onValueChanged.AddListener(delegate { dropDownSelected(Ca?on); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___CaUF1on_5;
		DropDownHandlerCaUF1on_dropDownSelected_m37BA963612034E0DFD25BC0202DD2980285F31E8(__this, L_0, NULL);
		// Ca?on.onValueChanged.AddListener(delegate { dropDownSelected(Ca?on); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandlerCilindro::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCilindro_Start_mC7817E2E40A533795170A912F5B1DAA73C26BFC4 (DropDownHandlerCilindro_t95B6802C5FF3BA348A912FEDA7D68C8643581D52* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandlerCilindro_U3CStartU3Eb__11_0_m9124746DDB3AD4D17E8D48140234F227D6B09B77_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral858843B1C1F4F954BE4ED57EA5219BE3910A9E8D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE4CFDEDE86B21B9DC88E2877BF3E1378C75D3333);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// Cilindro = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___Cilindro_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Cilindro_5), (void*)L_0);
		// CilindroPieza = GameObject.Find("CilindroAir");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral858843B1C1F4F954BE4ED57EA5219BE3910A9E8D, NULL);
		__this->___CilindroPieza_13 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___CilindroPieza_13), (void*)L_1);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2;
		L_2 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_2;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_3 = V_0;
		int32_t L_4;
		L_4 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_3, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_4);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_6 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_6, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_6;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_023e;
	}

IL_0040:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_7 = V_0;
		int32_t L_8 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_9;
		L_9 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_7, L_8, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_10;
		L_10 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_9, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_11;
		L_11 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_12 = V_0;
		int32_t L_13 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_14;
		L_14 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_12, L_13, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_15;
		L_15 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_14, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_3 = L_16;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_17 = V_0;
		int32_t L_18 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_19;
		L_19 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_17, L_18, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_20;
		L_20 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_19, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_21;
		L_21 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		V_4 = L_21;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_22 = V_0;
		int32_t L_23 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_24;
		L_24 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_22, L_23, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_25;
		L_25 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_24, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_26;
		L_26 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		V_5 = L_26;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_27 = V_0;
		int32_t L_28 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_29;
		L_29 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_27, L_28, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_30;
		L_30 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_29, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_31;
		L_31 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		V_6 = L_31;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_32 = V_0;
		int32_t L_33 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_34;
		L_34 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_32, L_33, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_35;
		L_35 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_34, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_36;
		L_36 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		V_7 = L_36;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_37 = V_0;
		int32_t L_38 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_39;
		L_39 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_37, L_38, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_40;
		L_40 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_39, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_41;
		L_41 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_40);
		V_8 = L_41;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_42 = V_0;
		int32_t L_43 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_44;
		L_44 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_42, L_43, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_45;
		L_45 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_44, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_46;
		L_46 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_45);
		V_9 = L_46;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_47 = V_0;
		int32_t L_48 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_49;
		L_49 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_47, L_48, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_50;
		L_50 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_49, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_51;
		L_51 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_50);
		V_10 = L_51;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_52 = V_0;
		int32_t L_53 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_54;
		L_54 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_52, L_53, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_55;
		L_55 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_54, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_56;
		L_56 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_55);
		V_11 = L_56;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_57 = V_0;
		int32_t L_58 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_59;
		L_59 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_57, L_58, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_60;
		L_60 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_59, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_61;
		L_61 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_60);
		V_12 = L_61;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_62 = V_0;
		int32_t L_63 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_64;
		L_64 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_62, L_63, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_65;
		L_65 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_64, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_66;
		L_66 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_65);
		V_13 = L_66;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_67 = V_0;
		int32_t L_68 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_69;
		L_69 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_67, L_68, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_70;
		L_70 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_69, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_71;
		L_71 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_70);
		V_14 = L_71;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_73, L_72, NULL);
		V_15 = L_73;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_74 = V_15;
		String_t* L_75 = V_3;
		L_74->___tipo_0 = L_75;
		Il2CppCodeGenWriteBarrier((void**)(&L_74->___tipo_0), (void*)L_75);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_76 = V_15;
		String_t* L_77 = V_4;
		L_76->___marca_1 = L_77;
		Il2CppCodeGenWriteBarrier((void**)(&L_76->___marca_1), (void*)L_77);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_78 = V_15;
		String_t* L_79 = V_5;
		L_78->___color_2 = L_79;
		Il2CppCodeGenWriteBarrier((void**)(&L_78->___color_2), (void*)L_79);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_80 = V_15;
		String_t* L_81 = V_6;
		L_80->___material_3 = L_81;
		Il2CppCodeGenWriteBarrier((void**)(&L_80->___material_3), (void*)L_81);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_82 = V_15;
		String_t* L_83 = V_7;
		L_82->___potencia_4 = L_83;
		Il2CppCodeGenWriteBarrier((void**)(&L_82->___potencia_4), (void*)L_83);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_84 = V_15;
		String_t* L_85 = V_8;
		L_84->___precision_5 = L_85;
		Il2CppCodeGenWriteBarrier((void**)(&L_84->___precision_5), (void*)L_85);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_86 = V_15;
		String_t* L_87 = V_9;
		L_86->___alcance_6 = L_87;
		Il2CppCodeGenWriteBarrier((void**)(&L_86->___alcance_6), (void*)L_87);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_88 = V_15;
		String_t* L_89 = V_10;
		L_88->___cadencia_7 = L_89;
		Il2CppCodeGenWriteBarrier((void**)(&L_88->___cadencia_7), (void*)L_89);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_90 = V_15;
		String_t* L_91 = V_11;
		L_90->___estabilidad_8 = L_91;
		Il2CppCodeGenWriteBarrier((void**)(&L_90->___estabilidad_8), (void*)L_91);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_92 = V_15;
		String_t* L_93 = V_12;
		L_92->___precio_9 = L_93;
		Il2CppCodeGenWriteBarrier((void**)(&L_92->___precio_9), (void*)L_93);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_94 = V_15;
		String_t* L_95 = V_13;
		L_94->___modelo_10 = L_95;
		Il2CppCodeGenWriteBarrier((void**)(&L_94->___modelo_10), (void*)L_95);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_96 = V_15;
		String_t* L_97 = V_14;
		L_96->___extra_11 = L_97;
		Il2CppCodeGenWriteBarrier((void**)(&L_96->___extra_11), (void*)L_97);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_98 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_100 = L_99;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_101 = V_15;
		String_t* L_102 = L_101->___tipo_0;
		ArrayElementTypeCheck (L_100, L_102);
		(L_100)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_102);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_100;
		ArrayElementTypeCheck (L_103, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_104 = L_103;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_15;
		String_t* L_106 = L_105->___marca_1;
		ArrayElementTypeCheck (L_104, L_106);
		(L_104)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_106);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_104;
		ArrayElementTypeCheck (L_107, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_108 = L_107;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_109 = V_15;
		String_t* L_110 = L_109->___modelo_10;
		ArrayElementTypeCheck (L_108, L_110);
		(L_108)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_110);
		String_t* L_111;
		L_111 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_108, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_98, L_111, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_112 = __this->___itemDatabase_14;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_113 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_112, L_113, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_114, 1));
	}

IL_023e:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_115 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_116 = V_0;
		int32_t L_117;
		L_117 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_116, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_115) < ((int32_t)L_117)))
		{
			goto IL_0040;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_118 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_119;
		L_119 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_118, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_119;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0293:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0288_1;
			}

IL_0254_1:
			{
				// foreach (string item in items)
				String_t* L_120;
				L_120 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_120;
				// if (item.Contains("Cilindro"))
				String_t* L_121 = V_17;
				bool L_122;
				L_122 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_121, _stringLiteralE4CFDEDE86B21B9DC88E2877BF3E1378C75D3333, NULL);
				if (!L_122)
				{
					goto IL_0288_1;
				}
			}
			{
				// Cilindro.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_123 = __this->___Cilindro_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_124;
				L_124 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_123, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_125 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_125, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_126 = L_125;
				String_t* L_127 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_126, L_127, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_124, L_126, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_0288_1:
			{
				// foreach (string item in items)
				bool L_128;
				L_128 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_128)
				{
					goto IL_0254_1;
				}
			}
			{
				goto IL_02a1;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_02a1:
	{
		// Cilindro.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_129 = __this->___Cilindro_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_129, NULL);
		// Cilindro.onValueChanged.AddListener(delegate { dropDownSelected(Cilindro); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_130 = __this->___Cilindro_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_131;
		L_131 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_130, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_132 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_132, __this, (intptr_t)((void*)DropDownHandlerCilindro_U3CStartU3Eb__11_0_m9124746DDB3AD4D17E8D48140234F227D6B09B77_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_131, L_132, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandlerCilindro::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCilindro_dropDownSelected_mDE0E057333B47B24CD2D65ABE0045CF1F23666F3 (DropDownHandlerCilindro_t95B6802C5FF3BA348A912FEDA7D68C8643581D52* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4DE573297F69792F2A52321674F0590D7424E99A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* V_3 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (alcanceDrop != 0)
		double L_2 = __this->___alcanceDrop_7;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_3 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (varianzaDrop != 0)
		float L_4 = __this->___varianzaDrop_10;
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_0067;
		}
	}
	{
		// Window_Graph.setVarianza(0, varianzaDrop);
		float L_5 = __this->___varianzaDrop_10;
		Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8((0.0f), L_5, NULL);
	}

IL_0067:
	{
		// if (precisionDrop != 0)
		double L_6 = __this->___precisionDrop_8;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_008c;
		}
	}
	{
		// Window_Graph.setPrecision(0, precisionDrop);
		double L_7 = __this->___precisionDrop_8;
		Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6((0.0), L_7, NULL);
	}

IL_008c:
	{
		// if (cadenciaDrop != 0)
		double L_8 = __this->___cadenciaDrop_9;
		if ((((double)L_8) == ((double)(0.0))))
		{
			goto IL_00b1;
		}
	}
	{
		// Window_Graph.setCadencia(0, cadenciaDrop);
		double L_9 = __this->___cadenciaDrop_9;
		Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142((0.0), L_9, NULL);
	}

IL_00b1:
	{
		// if (precio != 0)
		double L_10 = __this->___precio_11;
		if ((((double)L_10) == ((double)(0.0))))
		{
			goto IL_00d6;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_11 = __this->___precio_11;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_11, NULL);
	}

IL_00d6:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_12 = ___dropdown0;
		int32_t L_13;
		L_13 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_12, NULL);
		V_0 = L_13;
		// Debug.Log(Cilindro.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_14 = __this->___Cilindro_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_15;
		L_15 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_14, NULL);
		int32_t L_16 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_17;
		L_17 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_15, L_16, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_18;
		L_18 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_17, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_18, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_19 = __this->___itemDatabase_14;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_20;
		L_20 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_19, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_20;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_03f3:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_03e5_1;
			}

IL_0109_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_21;
				L_21 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_21;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_24 = V_2;
				String_t* L_25 = L_24->___tipo_0;
				ArrayElementTypeCheck (L_23, L_25);
				(L_23)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_25);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_23;
				ArrayElementTypeCheck (L_26, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_26)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_27 = L_26;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_28 = V_2;
				String_t* L_29 = L_28->___marca_1;
				ArrayElementTypeCheck (L_27, L_29);
				(L_27)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_29);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_30 = L_27;
				ArrayElementTypeCheck (L_30, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_30)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_31 = L_30;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_32 = V_2;
				String_t* L_33 = L_32->___modelo_10;
				ArrayElementTypeCheck (L_31, L_33);
				(L_31)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_33);
				String_t* L_34;
				L_34 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_31, NULL);
				// if (comparar == Cilindro.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_35 = __this->___Cilindro_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_36;
				L_36 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_35, NULL);
				int32_t L_37 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_38;
				L_38 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_36, L_37, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_39;
				L_39 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_38, NULL);
				bool L_40;
				L_40 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_34, L_39, NULL);
				if (!L_40)
				{
					goto IL_03e5_1;
				}
			}
			{
				// potenciaDrop = int.Parse(Componente.potencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_41 = V_2;
				String_t* L_42 = L_41->___potencia_4;
				int32_t L_43;
				L_43 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_42, NULL);
				__this->___potenciaDrop_6 = ((double)L_43);
				// alcanceDrop = int.Parse(Componente.alcance);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_44 = V_2;
				String_t* L_45 = L_44->___alcance_6;
				int32_t L_46;
				L_46 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_45, NULL);
				__this->___alcanceDrop_7 = ((double)L_46);
				// varianzaDrop = int.Parse(Componente.estabilidad);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_47 = V_2;
				String_t* L_48 = L_47->___estabilidad_8;
				int32_t L_49;
				L_49 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_48, NULL);
				__this->___varianzaDrop_10 = ((float)L_49);
				// precisionDrop = int.Parse(Componente.precision);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_50 = V_2;
				String_t* L_51 = L_50->___precision_5;
				int32_t L_52;
				L_52 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_51, NULL);
				__this->___precisionDrop_8 = ((double)L_52);
				// cadenciaDrop = int.Parse(Componente.cadencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_53 = V_2;
				String_t* L_54 = L_53->___cadencia_7;
				int32_t L_55;
				L_55 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_54, NULL);
				__this->___cadenciaDrop_9 = ((double)L_55);
				// precio = int.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_56 = V_2;
				String_t* L_57 = L_56->___precio_9;
				int32_t L_58;
				L_58 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_57, NULL);
				__this->___precio_11 = ((double)L_58);
				// extra = Componente.extra;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_59 = V_2;
				String_t* L_60 = L_59->___extra_11;
				__this->___extra_12 = L_60;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_12), (void*)L_60);
				// Window_Graph.setAlcance(alcanceDrop);
				double L_61 = __this->___alcanceDrop_7;
				Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2(L_61, (0.0), NULL);
				// Window_Graph.setPotencia(potenciaDrop);
				double L_62 = __this->___potenciaDrop_6;
				Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E(L_62, (0.0), NULL);
				// Window_Graph.setVarianza(varianzaDrop);
				float L_63 = __this->___varianzaDrop_10;
				Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8(L_63, (0.0f), NULL);
				// Window_Graph.setPrecision(precisionDrop);
				double L_64 = __this->___precisionDrop_8;
				Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6(L_64, (0.0), NULL);
				// Window_Graph.setCadencia(cadenciaDrop);
				double L_65 = __this->___cadenciaDrop_9;
				Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142(L_65, (0.0), NULL);
				// Window_Graph.setPrecio(precio,0);
				double L_66 = __this->___precio_11;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_66, (0.0), NULL);
				// var renderer = CilindroPieza.GetComponent<Renderer>();
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_67 = __this->___CilindroPieza_13;
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_68;
				L_68 = GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A(L_67, GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
				V_3 = L_68;
				// if (Componente.color == "Rojo")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_69 = V_2;
				String_t* L_70 = L_69->___color_2;
				bool L_71;
				L_71 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_70, _stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C, NULL);
				if (!L_71)
				{
					goto IL_0286_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.red);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_72 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_73;
				L_73 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_72, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_74;
				L_74 = Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_73, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_74, NULL);
			}

IL_0286_1:
			{
				// if (Componente.color == "Gris")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_75 = V_2;
				String_t* L_76 = L_75->___color_2;
				bool L_77;
				L_77 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_76, _stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116, NULL);
				if (!L_77)
				{
					goto IL_02ad_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_78 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_79;
				L_79 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_78, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_80;
				L_80 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_79, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_80, NULL);
			}

IL_02ad_1:
			{
				// if (Componente.color == "Gris oscuro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_81 = V_2;
				String_t* L_82 = L_81->___color_2;
				bool L_83;
				L_83 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_82, _stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC, NULL);
				if (!L_83)
				{
					goto IL_02d4_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_84 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_85;
				L_85 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_84, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_86;
				L_86 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_85, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_86, NULL);
			}

IL_02d4_1:
			{
				// if (Componente.color == "Gris claro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_87 = V_2;
				String_t* L_88 = L_87->___color_2;
				bool L_89;
				L_89 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_88, _stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6, NULL);
				if (!L_89)
				{
					goto IL_02fb_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_90 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_91;
				L_91 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_90, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_92;
				L_92 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_91, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_92, NULL);
			}

IL_02fb_1:
			{
				// if (Componente.color == "Azul")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_93 = V_2;
				String_t* L_94 = L_93->___color_2;
				bool L_95;
				L_95 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_94, _stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510, NULL);
				if (!L_95)
				{
					goto IL_0322_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.blue);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_96 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_97;
				L_97 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_96, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_98;
				L_98 = Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_97, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_98, NULL);
			}

IL_0322_1:
			{
				// if (Componente.color == "Negro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_99 = V_2;
				String_t* L_100 = L_99->___color_2;
				bool L_101;
				L_101 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_100, _stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9, NULL);
				if (!L_101)
				{
					goto IL_0349_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_102 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_103;
				L_103 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_102, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_104;
				L_104 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_103, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_104, NULL);
			}

IL_0349_1:
			{
				// if (Componente.color == "Dorado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_2;
				String_t* L_106 = L_105->___color_2;
				bool L_107;
				L_107 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_106, _stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E, NULL);
				if (!L_107)
				{
					goto IL_0370_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.yellow);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_108 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_109;
				L_109 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_108, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_110;
				L_110 = Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_109, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_110, NULL);
			}

IL_0370_1:
			{
				// if (Componente.color == "Plateado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_111 = V_2;
				String_t* L_112 = L_111->___color_2;
				bool L_113;
				L_113 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_112, _stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF, NULL);
				if (!L_113)
				{
					goto IL_0397_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_114 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_115;
				L_115 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_114, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_116;
				L_116 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_115, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_116, NULL);
			}

IL_0397_1:
			{
				// if (Componente.color == "Cobre")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_117 = V_2;
				String_t* L_118 = L_117->___color_2;
				bool L_119;
				L_119 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_118, _stringLiteral4DE573297F69792F2A52321674F0590D7424E99A, NULL);
				if (!L_119)
				{
					goto IL_03be_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_120 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_121;
				L_121 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_120, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_122;
				L_122 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_121, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_122, NULL);
			}

IL_03be_1:
			{
				// if (Componente.color == "")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_123 = V_2;
				String_t* L_124 = L_123->___color_2;
				bool L_125;
				L_125 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_124, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
				if (!L_125)
				{
					goto IL_03e5_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_126 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_127;
				L_127 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_126, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_128;
				L_128 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_127, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_128, NULL);
			}

IL_03e5_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_129;
				L_129 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_129)
				{
					goto IL_0109_1;
				}
			}
			{
				goto IL_0401;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0401:
	{
		// }
		return;
	}
}
// System.Void DropDownHandlerCilindro::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCilindro__ctor_m68335735F76552B5AE9716F979FD2482419FBCFD (DropDownHandlerCilindro_t95B6802C5FF3BA348A912FEDA7D68C8643581D52* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_14 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_14), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandlerCilindro::<Start>b__11_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerCilindro_U3CStartU3Eb__11_0_m9124746DDB3AD4D17E8D48140234F227D6B09B77 (DropDownHandlerCilindro_t95B6802C5FF3BA348A912FEDA7D68C8643581D52* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// Cilindro.onValueChanged.AddListener(delegate { dropDownSelected(Cilindro); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___Cilindro_5;
		DropDownHandlerCilindro_dropDownSelected_mDE0E057333B47B24CD2D65ABE0045CF1F23666F3(__this, L_0, NULL);
		// Cilindro.onValueChanged.AddListener(delegate { dropDownSelected(Cilindro); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandlerEngranajes::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerEngranajes_Start_mD94793A84A704ED472E412D57D05B919F8E836AA (DropDownHandlerEngranajes_t062A4B96E3458FE9041B420AB0B343FF68F03D24* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandlerEngranajes_U3CStartU3Eb__10_0_mD533AC3B640B672A2DF154F464303C994ACF5472_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA4462CA9ADC2AD6448A6395345473E79E165913C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// Engranajes = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___Engranajes_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Engranajes_5), (void*)L_0);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_1;
		L_1 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_1;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2 = V_0;
		int32_t L_3;
		L_3 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_2, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_4 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_3);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_5 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_5, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_5;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_022e;
	}

IL_0030:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_6 = V_0;
		int32_t L_7 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_8;
		L_8 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_6, L_7, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_9;
		L_9 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_8, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_10;
		L_10 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_11 = V_0;
		int32_t L_12 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_13;
		L_13 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_11, L_12, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_14;
		L_14 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_13, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_15;
		L_15 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		V_3 = L_15;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_16 = V_0;
		int32_t L_17 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_18;
		L_18 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_16, L_17, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_19;
		L_19 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_18, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_20;
		L_20 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_19);
		V_4 = L_20;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_21 = V_0;
		int32_t L_22 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_23;
		L_23 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_21, L_22, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_24;
		L_24 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_23, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_25;
		L_25 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_24);
		V_5 = L_25;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_26 = V_0;
		int32_t L_27 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_28;
		L_28 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_26, L_27, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_29;
		L_29 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_28, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_30;
		L_30 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_29);
		V_6 = L_30;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_31 = V_0;
		int32_t L_32 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_33;
		L_33 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_31, L_32, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_34;
		L_34 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_33, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_35;
		L_35 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		V_7 = L_35;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_36 = V_0;
		int32_t L_37 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_38;
		L_38 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_36, L_37, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_39;
		L_39 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_38, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_40;
		L_40 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_39);
		V_8 = L_40;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_41 = V_0;
		int32_t L_42 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_43;
		L_43 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_41, L_42, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_44;
		L_44 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_43, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_45;
		L_45 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_44);
		V_9 = L_45;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_46 = V_0;
		int32_t L_47 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_48;
		L_48 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_46, L_47, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_49;
		L_49 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_48, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_50;
		L_50 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_49);
		V_10 = L_50;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_51 = V_0;
		int32_t L_52 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_53;
		L_53 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_51, L_52, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_54;
		L_54 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_53, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_55;
		L_55 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_54);
		V_11 = L_55;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_56 = V_0;
		int32_t L_57 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_58;
		L_58 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_56, L_57, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_59;
		L_59 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_58, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_60;
		L_60 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_59);
		V_12 = L_60;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_61 = V_0;
		int32_t L_62 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_63;
		L_63 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_61, L_62, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_64;
		L_64 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_63, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_65;
		L_65 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_64);
		V_13 = L_65;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_66 = V_0;
		int32_t L_67 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_68;
		L_68 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_66, L_67, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_69;
		L_69 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_68, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_70;
		L_70 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_69);
		V_14 = L_70;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_71 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_72, L_71, NULL);
		V_15 = L_72;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = V_15;
		String_t* L_74 = V_3;
		L_73->___tipo_0 = L_74;
		Il2CppCodeGenWriteBarrier((void**)(&L_73->___tipo_0), (void*)L_74);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_75 = V_15;
		String_t* L_76 = V_4;
		L_75->___marca_1 = L_76;
		Il2CppCodeGenWriteBarrier((void**)(&L_75->___marca_1), (void*)L_76);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_77 = V_15;
		String_t* L_78 = V_5;
		L_77->___color_2 = L_78;
		Il2CppCodeGenWriteBarrier((void**)(&L_77->___color_2), (void*)L_78);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_79 = V_15;
		String_t* L_80 = V_6;
		L_79->___material_3 = L_80;
		Il2CppCodeGenWriteBarrier((void**)(&L_79->___material_3), (void*)L_80);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_81 = V_15;
		String_t* L_82 = V_7;
		L_81->___potencia_4 = L_82;
		Il2CppCodeGenWriteBarrier((void**)(&L_81->___potencia_4), (void*)L_82);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_83 = V_15;
		String_t* L_84 = V_8;
		L_83->___precision_5 = L_84;
		Il2CppCodeGenWriteBarrier((void**)(&L_83->___precision_5), (void*)L_84);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_85 = V_15;
		String_t* L_86 = V_9;
		L_85->___alcance_6 = L_86;
		Il2CppCodeGenWriteBarrier((void**)(&L_85->___alcance_6), (void*)L_86);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_87 = V_15;
		String_t* L_88 = V_10;
		L_87->___cadencia_7 = L_88;
		Il2CppCodeGenWriteBarrier((void**)(&L_87->___cadencia_7), (void*)L_88);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_89 = V_15;
		String_t* L_90 = V_11;
		L_89->___estabilidad_8 = L_90;
		Il2CppCodeGenWriteBarrier((void**)(&L_89->___estabilidad_8), (void*)L_90);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_91 = V_15;
		String_t* L_92 = V_12;
		L_91->___precio_9 = L_92;
		Il2CppCodeGenWriteBarrier((void**)(&L_91->___precio_9), (void*)L_92);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_93 = V_15;
		String_t* L_94 = V_13;
		L_93->___modelo_10 = L_94;
		Il2CppCodeGenWriteBarrier((void**)(&L_93->___modelo_10), (void*)L_94);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_95 = V_15;
		String_t* L_96 = V_14;
		L_95->___extra_11 = L_96;
		Il2CppCodeGenWriteBarrier((void**)(&L_95->___extra_11), (void*)L_96);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_97 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_98 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = L_98;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_100 = V_15;
		String_t* L_101 = L_100->___tipo_0;
		ArrayElementTypeCheck (L_99, L_101);
		(L_99)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_101);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_102 = L_99;
		ArrayElementTypeCheck (L_102, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_102)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_102;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_104 = V_15;
		String_t* L_105 = L_104->___marca_1;
		ArrayElementTypeCheck (L_103, L_105);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_105);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_106 = L_103;
		ArrayElementTypeCheck (L_106, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_106)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_106;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_108 = V_15;
		String_t* L_109 = L_108->___modelo_10;
		ArrayElementTypeCheck (L_107, L_109);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_109);
		String_t* L_110;
		L_110 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_107, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_97, L_110, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_111 = __this->___itemDatabase_13;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_112 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_111, L_112, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_113 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_113, 1));
	}

IL_022e:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_115 = V_0;
		int32_t L_116;
		L_116 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_115, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_114) < ((int32_t)L_116)))
		{
			goto IL_0030;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_117 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_118;
		L_118 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_117, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_118;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0283:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0278_1;
			}

IL_0244_1:
			{
				// foreach (string item in items)
				String_t* L_119;
				L_119 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_119;
				// if (item.Contains("Engranajes"))
				String_t* L_120 = V_17;
				bool L_121;
				L_121 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_120, _stringLiteralA4462CA9ADC2AD6448A6395345473E79E165913C, NULL);
				if (!L_121)
				{
					goto IL_0278_1;
				}
			}
			{
				// Engranajes.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_122 = __this->___Engranajes_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_123;
				L_123 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_122, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_124 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_124, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_125 = L_124;
				String_t* L_126 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_125, L_126, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_123, L_125, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_0278_1:
			{
				// foreach (string item in items)
				bool L_127;
				L_127 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_127)
				{
					goto IL_0244_1;
				}
			}
			{
				goto IL_0291;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0291:
	{
		// Engranajes.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_128 = __this->___Engranajes_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_128, NULL);
		// Engranajes.onValueChanged.AddListener(delegate { dropDownSelected(Engranajes); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_129 = __this->___Engranajes_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_130;
		L_130 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_129, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_131 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_131, __this, (intptr_t)((void*)DropDownHandlerEngranajes_U3CStartU3Eb__10_0_mD533AC3B640B672A2DF154F464303C994ACF5472_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_130, L_131, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandlerEngranajes::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerEngranajes_dropDownSelected_m870DD902E40F08063754C3D04D1C61B65090AD87 (DropDownHandlerEngranajes_t062A4B96E3458FE9041B420AB0B343FF68F03D24* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (alcanceDrop != 0)
		double L_2 = __this->___alcanceDrop_7;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_3 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (varianzaDrop != 0)
		float L_4 = __this->___varianzaDrop_10;
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_0067;
		}
	}
	{
		// Window_Graph.setVarianza(0, varianzaDrop);
		float L_5 = __this->___varianzaDrop_10;
		Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8((0.0f), L_5, NULL);
	}

IL_0067:
	{
		// if (precisionDrop != 0)
		double L_6 = __this->___precisionDrop_8;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_008c;
		}
	}
	{
		// Window_Graph.setPrecision(0, precisionDrop);
		double L_7 = __this->___precisionDrop_8;
		Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6((0.0), L_7, NULL);
	}

IL_008c:
	{
		// if (cadenciaDrop != 0)
		double L_8 = __this->___cadenciaDrop_9;
		if ((((double)L_8) == ((double)(0.0))))
		{
			goto IL_00b1;
		}
	}
	{
		// Window_Graph.setCadencia(0, cadenciaDrop);
		double L_9 = __this->___cadenciaDrop_9;
		Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142((0.0), L_9, NULL);
	}

IL_00b1:
	{
		// if (precio != 0)
		double L_10 = __this->___precio_11;
		if ((((double)L_10) == ((double)(0.0))))
		{
			goto IL_00d6;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_11 = __this->___precio_11;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_11, NULL);
	}

IL_00d6:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_12 = ___dropdown0;
		int32_t L_13;
		L_13 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_12, NULL);
		V_0 = L_13;
		// Debug.Log(Engranajes.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_14 = __this->___Engranajes_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_15;
		L_15 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_14, NULL);
		int32_t L_16 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_17;
		L_17 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_15, L_16, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_18;
		L_18 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_17, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_18, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_19 = __this->___itemDatabase_13;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_20;
		L_20 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_19, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_20;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0261:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0253_1;
			}

IL_0109_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_21;
				L_21 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_21;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_24 = V_2;
				String_t* L_25 = L_24->___tipo_0;
				ArrayElementTypeCheck (L_23, L_25);
				(L_23)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_25);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_23;
				ArrayElementTypeCheck (L_26, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_26)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_27 = L_26;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_28 = V_2;
				String_t* L_29 = L_28->___marca_1;
				ArrayElementTypeCheck (L_27, L_29);
				(L_27)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_29);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_30 = L_27;
				ArrayElementTypeCheck (L_30, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_30)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_31 = L_30;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_32 = V_2;
				String_t* L_33 = L_32->___modelo_10;
				ArrayElementTypeCheck (L_31, L_33);
				(L_31)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_33);
				String_t* L_34;
				L_34 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_31, NULL);
				// if (comparar == Engranajes.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_35 = __this->___Engranajes_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_36;
				L_36 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_35, NULL);
				int32_t L_37 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_38;
				L_38 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_36, L_37, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_39;
				L_39 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_38, NULL);
				bool L_40;
				L_40 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_34, L_39, NULL);
				if (!L_40)
				{
					goto IL_0253_1;
				}
			}
			{
				// potenciaDrop = int.Parse(Componente.potencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_41 = V_2;
				String_t* L_42 = L_41->___potencia_4;
				int32_t L_43;
				L_43 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_42, NULL);
				__this->___potenciaDrop_6 = ((double)L_43);
				// alcanceDrop = int.Parse(Componente.alcance);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_44 = V_2;
				String_t* L_45 = L_44->___alcance_6;
				int32_t L_46;
				L_46 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_45, NULL);
				__this->___alcanceDrop_7 = ((double)L_46);
				// varianzaDrop = int.Parse(Componente.estabilidad);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_47 = V_2;
				String_t* L_48 = L_47->___estabilidad_8;
				int32_t L_49;
				L_49 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_48, NULL);
				__this->___varianzaDrop_10 = ((float)L_49);
				// precisionDrop = int.Parse(Componente.precision);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_50 = V_2;
				String_t* L_51 = L_50->___precision_5;
				int32_t L_52;
				L_52 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_51, NULL);
				__this->___precisionDrop_8 = ((double)L_52);
				// cadenciaDrop = int.Parse(Componente.cadencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_53 = V_2;
				String_t* L_54 = L_53->___cadencia_7;
				int32_t L_55;
				L_55 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_54, NULL);
				__this->___cadenciaDrop_9 = ((double)L_55);
				// precio = int.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_56 = V_2;
				String_t* L_57 = L_56->___precio_9;
				int32_t L_58;
				L_58 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_57, NULL);
				__this->___precio_11 = ((double)L_58);
				// extra = Componente.extra;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_59 = V_2;
				String_t* L_60 = L_59->___extra_11;
				__this->___extra_12 = L_60;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_12), (void*)L_60);
				// Window_Graph.setAlcance(alcanceDrop);
				double L_61 = __this->___alcanceDrop_7;
				Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2(L_61, (0.0), NULL);
				// Window_Graph.setPotencia(potenciaDrop);
				double L_62 = __this->___potenciaDrop_6;
				Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E(L_62, (0.0), NULL);
				// Window_Graph.setVarianza(varianzaDrop);
				float L_63 = __this->___varianzaDrop_10;
				Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8(L_63, (0.0f), NULL);
				// Window_Graph.setPrecision(precisionDrop);
				double L_64 = __this->___precisionDrop_8;
				Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6(L_64, (0.0), NULL);
				// Window_Graph.setCadencia(cadenciaDrop);
				double L_65 = __this->___cadenciaDrop_9;
				Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142(L_65, (0.0), NULL);
				// Window_Graph.setPrecio(precio,0);
				double L_66 = __this->___precio_11;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_66, (0.0), NULL);
			}

IL_0253_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_67;
				L_67 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_67)
				{
					goto IL_0109_1;
				}
			}
			{
				goto IL_026f;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_026f:
	{
		// }
		return;
	}
}
// System.Void DropDownHandlerEngranajes::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerEngranajes__ctor_m5420BD77171B0DB1E5644630A92B3B5C135C2E69 (DropDownHandlerEngranajes_t062A4B96E3458FE9041B420AB0B343FF68F03D24* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_13 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_13), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandlerEngranajes::<Start>b__10_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerEngranajes_U3CStartU3Eb__10_0_mD533AC3B640B672A2DF154F464303C994ACF5472 (DropDownHandlerEngranajes_t062A4B96E3458FE9041B420AB0B343FF68F03D24* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// Engranajes.onValueChanged.AddListener(delegate { dropDownSelected(Engranajes); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___Engranajes_5;
		DropDownHandlerEngranajes_dropDownSelected_m870DD902E40F08063754C3D04D1C61B65090AD87(__this, L_0, NULL);
		// Engranajes.onValueChanged.AddListener(delegate { dropDownSelected(Engranajes); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandlerGatillo::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGatillo_Start_m7FED2AF654130CDCB52429B612DA164D090873B1 (DropDownHandlerGatillo_t74F9CFF132D5EC7C28BB31711DB8AC2A6DC6B240* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandlerGatillo_U3CStartU3Eb__9_0_mC9966227B6D0AF0442B643E6C052377502B279CA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCBAEA7AE98BD922B4AF45BBA7ED2B8C50DFC7B99);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCF87C5E11C6D5B7705B454E6429FD04CB1FBB8E3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// Gatillo = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___Gatillo_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Gatillo_5), (void*)L_0);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_1;
		L_1 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_1;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2 = V_0;
		int32_t L_3;
		L_3 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_2, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_4 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_3);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_5 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_5, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_5;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_022e;
	}

IL_0030:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_6 = V_0;
		int32_t L_7 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_8;
		L_8 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_6, L_7, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_9;
		L_9 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_8, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_10;
		L_10 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_11 = V_0;
		int32_t L_12 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_13;
		L_13 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_11, L_12, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_14;
		L_14 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_13, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_15;
		L_15 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		V_3 = L_15;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_16 = V_0;
		int32_t L_17 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_18;
		L_18 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_16, L_17, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_19;
		L_19 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_18, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_20;
		L_20 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_19);
		V_4 = L_20;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_21 = V_0;
		int32_t L_22 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_23;
		L_23 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_21, L_22, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_24;
		L_24 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_23, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_25;
		L_25 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_24);
		V_5 = L_25;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_26 = V_0;
		int32_t L_27 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_28;
		L_28 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_26, L_27, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_29;
		L_29 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_28, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_30;
		L_30 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_29);
		V_6 = L_30;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_31 = V_0;
		int32_t L_32 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_33;
		L_33 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_31, L_32, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_34;
		L_34 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_33, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_35;
		L_35 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		V_7 = L_35;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_36 = V_0;
		int32_t L_37 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_38;
		L_38 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_36, L_37, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_39;
		L_39 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_38, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_40;
		L_40 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_39);
		V_8 = L_40;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_41 = V_0;
		int32_t L_42 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_43;
		L_43 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_41, L_42, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_44;
		L_44 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_43, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_45;
		L_45 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_44);
		V_9 = L_45;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_46 = V_0;
		int32_t L_47 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_48;
		L_48 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_46, L_47, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_49;
		L_49 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_48, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_50;
		L_50 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_49);
		V_10 = L_50;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_51 = V_0;
		int32_t L_52 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_53;
		L_53 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_51, L_52, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_54;
		L_54 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_53, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_55;
		L_55 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_54);
		V_11 = L_55;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_56 = V_0;
		int32_t L_57 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_58;
		L_58 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_56, L_57, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_59;
		L_59 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_58, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_60;
		L_60 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_59);
		V_12 = L_60;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_61 = V_0;
		int32_t L_62 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_63;
		L_63 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_61, L_62, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_64;
		L_64 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_63, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_65;
		L_65 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_64);
		V_13 = L_65;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_66 = V_0;
		int32_t L_67 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_68;
		L_68 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_66, L_67, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_69;
		L_69 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_68, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_70;
		L_70 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_69);
		V_14 = L_70;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_71 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_72, L_71, NULL);
		V_15 = L_72;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = V_15;
		String_t* L_74 = V_3;
		L_73->___tipo_0 = L_74;
		Il2CppCodeGenWriteBarrier((void**)(&L_73->___tipo_0), (void*)L_74);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_75 = V_15;
		String_t* L_76 = V_4;
		L_75->___marca_1 = L_76;
		Il2CppCodeGenWriteBarrier((void**)(&L_75->___marca_1), (void*)L_76);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_77 = V_15;
		String_t* L_78 = V_5;
		L_77->___color_2 = L_78;
		Il2CppCodeGenWriteBarrier((void**)(&L_77->___color_2), (void*)L_78);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_79 = V_15;
		String_t* L_80 = V_6;
		L_79->___material_3 = L_80;
		Il2CppCodeGenWriteBarrier((void**)(&L_79->___material_3), (void*)L_80);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_81 = V_15;
		String_t* L_82 = V_7;
		L_81->___potencia_4 = L_82;
		Il2CppCodeGenWriteBarrier((void**)(&L_81->___potencia_4), (void*)L_82);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_83 = V_15;
		String_t* L_84 = V_8;
		L_83->___precision_5 = L_84;
		Il2CppCodeGenWriteBarrier((void**)(&L_83->___precision_5), (void*)L_84);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_85 = V_15;
		String_t* L_86 = V_9;
		L_85->___alcance_6 = L_86;
		Il2CppCodeGenWriteBarrier((void**)(&L_85->___alcance_6), (void*)L_86);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_87 = V_15;
		String_t* L_88 = V_10;
		L_87->___cadencia_7 = L_88;
		Il2CppCodeGenWriteBarrier((void**)(&L_87->___cadencia_7), (void*)L_88);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_89 = V_15;
		String_t* L_90 = V_11;
		L_89->___estabilidad_8 = L_90;
		Il2CppCodeGenWriteBarrier((void**)(&L_89->___estabilidad_8), (void*)L_90);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_91 = V_15;
		String_t* L_92 = V_12;
		L_91->___precio_9 = L_92;
		Il2CppCodeGenWriteBarrier((void**)(&L_91->___precio_9), (void*)L_92);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_93 = V_15;
		String_t* L_94 = V_13;
		L_93->___modelo_10 = L_94;
		Il2CppCodeGenWriteBarrier((void**)(&L_93->___modelo_10), (void*)L_94);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_95 = V_15;
		String_t* L_96 = V_14;
		L_95->___extra_11 = L_96;
		Il2CppCodeGenWriteBarrier((void**)(&L_95->___extra_11), (void*)L_96);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_97 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_98 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = L_98;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_100 = V_15;
		String_t* L_101 = L_100->___tipo_0;
		ArrayElementTypeCheck (L_99, L_101);
		(L_99)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_101);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_102 = L_99;
		ArrayElementTypeCheck (L_102, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_102)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_102;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_104 = V_15;
		String_t* L_105 = L_104->___marca_1;
		ArrayElementTypeCheck (L_103, L_105);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_105);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_106 = L_103;
		ArrayElementTypeCheck (L_106, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_106)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_106;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_108 = V_15;
		String_t* L_109 = L_108->___modelo_10;
		ArrayElementTypeCheck (L_107, L_109);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_109);
		String_t* L_110;
		L_110 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_107, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_97, L_110, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_111 = __this->___itemDatabase_12;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_112 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_111, L_112, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_113 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_113, 1));
	}

IL_022e:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_115 = V_0;
		int32_t L_116;
		L_116 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_115, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_114) < ((int32_t)L_116)))
		{
			goto IL_0030;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_117 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_118;
		L_118 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_117, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_118;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0291:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0286_1;
			}

IL_0244_1:
			{
				// foreach (string item in items)
				String_t* L_119;
				L_119 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_119;
				// if (item.Contains("Gatillo electr?nico") || item.Contains("Gatillos electr?nicos"))
				String_t* L_120 = V_17;
				bool L_121;
				L_121 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_120, _stringLiteralCF87C5E11C6D5B7705B454E6429FD04CB1FBB8E3, NULL);
				if (L_121)
				{
					goto IL_0269_1;
				}
			}
			{
				String_t* L_122 = V_17;
				bool L_123;
				L_123 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_122, _stringLiteralCBAEA7AE98BD922B4AF45BBA7ED2B8C50DFC7B99, NULL);
				if (!L_123)
				{
					goto IL_0286_1;
				}
			}

IL_0269_1:
			{
				// Gatillo.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_124 = __this->___Gatillo_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_125;
				L_125 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_124, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_126 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_126, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_127 = L_126;
				String_t* L_128 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_127, L_128, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_125, L_127, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_0286_1:
			{
				// foreach (string item in items)
				bool L_129;
				L_129 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_129)
				{
					goto IL_0244_1;
				}
			}
			{
				goto IL_029f;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_029f:
	{
		// Gatillo.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_130 = __this->___Gatillo_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_130, NULL);
		// Gatillo.onValueChanged.AddListener(delegate { dropDownSelected(Gatillo); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_131 = __this->___Gatillo_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_132;
		L_132 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_131, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_133 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_133, __this, (intptr_t)((void*)DropDownHandlerGatillo_U3CStartU3Eb__9_0_mC9966227B6D0AF0442B643E6C052377502B279CA_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_132, L_133, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandlerGatillo::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGatillo_dropDownSelected_m5D8532C5BB3F3EFB3CAEAB54F90EEEF952C7B127 (DropDownHandlerGatillo_t74F9CFF132D5EC7C28BB31711DB8AC2A6DC6B240* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (precisionDrop != 0)
		double L_2 = __this->___precisionDrop_8;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setPrecision(0, precisionDrop);
		double L_3 = __this->___precisionDrop_8;
		Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (alcanceDrop != 0)
		double L_4 = __this->___alcanceDrop_7;
		if ((((double)L_4) == ((double)(0.0))))
		{
			goto IL_006f;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_5 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_5, NULL);
	}

IL_006f:
	{
		// if (cadenciaDrop != 0)
		double L_6 = __this->___cadenciaDrop_9;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_0094;
		}
	}
	{
		// Window_Graph.setCadencia(0, cadenciaDrop);
		double L_7 = __this->___cadenciaDrop_9;
		Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142((0.0), L_7, NULL);
	}

IL_0094:
	{
		// if (precio != 0)
		double L_8 = __this->___precio_10;
		if ((((double)L_8) == ((double)(0.0))))
		{
			goto IL_00b9;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_9 = __this->___precio_10;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_9, NULL);
	}

IL_00b9:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_10 = ___dropdown0;
		int32_t L_11;
		L_11 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_10, NULL);
		V_0 = L_11;
		// Debug.Log(Gatillo.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_12 = __this->___Gatillo_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_13;
		L_13 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_12, NULL);
		int32_t L_14 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_15;
		L_15 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_13, L_14, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_16;
		L_16 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_15, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_16, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_17 = __this->___itemDatabase_12;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_18;
		L_18 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_17, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_18;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0191:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0183_1;
			}

IL_00ec_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_19;
				L_19 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_19;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_21 = L_20;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_22 = V_2;
				String_t* L_23 = L_22->___tipo_0;
				ArrayElementTypeCheck (L_21, L_23);
				(L_21)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_23);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_24 = L_21;
				ArrayElementTypeCheck (L_24, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_24)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = L_24;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_26 = V_2;
				String_t* L_27 = L_26->___marca_1;
				ArrayElementTypeCheck (L_25, L_27);
				(L_25)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_27);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_28 = L_25;
				ArrayElementTypeCheck (L_28, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_28)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_29 = L_28;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_30 = V_2;
				String_t* L_31 = L_30->___modelo_10;
				ArrayElementTypeCheck (L_29, L_31);
				(L_29)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_31);
				String_t* L_32;
				L_32 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_29, NULL);
				// if (comparar == Gatillo.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_33 = __this->___Gatillo_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_34;
				L_34 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_33, NULL);
				int32_t L_35 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_36;
				L_36 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_34, L_35, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_37;
				L_37 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_36, NULL);
				bool L_38;
				L_38 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_32, L_37, NULL);
				if (!L_38)
				{
					goto IL_0183_1;
				}
			}
			{
				// precio = double.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_39 = V_2;
				String_t* L_40 = L_39->___precio_9;
				double L_41;
				L_41 = Double_Parse_mBED785C952A63E8D714E429A4A704BCC4D92931B(L_40, NULL);
				__this->___precio_10 = L_41;
				// extra = Componente.extra;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_42 = V_2;
				String_t* L_43 = L_42->___extra_11;
				__this->___extra_11 = L_43;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_11), (void*)L_43);
				// Window_Graph.setExtra(extra);
				String_t* L_44 = __this->___extra_11;
				Window_Graph_setExtra_m8774145D45E480AAFA50F295A5FB4E00A289FF91_inline(L_44, NULL);
				// Window_Graph.setPrecio(precio,0);
				double L_45 = __this->___precio_10;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_45, (0.0), NULL);
			}

IL_0183_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_46;
				L_46 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_46)
				{
					goto IL_00ec_1;
				}
			}
			{
				goto IL_019f;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_019f:
	{
		// }
		return;
	}
}
// System.Void DropDownHandlerGatillo::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGatillo__ctor_m0540E5C444B8D6A7F814269123552025CA6FBD0D (DropDownHandlerGatillo_t74F9CFF132D5EC7C28BB31711DB8AC2A6DC6B240* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_12 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_12), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandlerGatillo::<Start>b__9_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGatillo_U3CStartU3Eb__9_0_mC9966227B6D0AF0442B643E6C052377502B279CA (DropDownHandlerGatillo_t74F9CFF132D5EC7C28BB31711DB8AC2A6DC6B240* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// Gatillo.onValueChanged.AddListener(delegate { dropDownSelected(Gatillo); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___Gatillo_5;
		DropDownHandlerGatillo_dropDownSelected_m5D8532C5BB3F3EFB3CAEAB54F90EEEF952C7B127(__this, L_0, NULL);
		// Gatillo.onValueChanged.AddListener(delegate { dropDownSelected(Gatillo); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandlerGearbox::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGearbox_Start_mB93D86AE32CF0684241074B6A11DA551A031FF6F (DropDownHandlerGearbox_t8AD5B054B30C7F3F3A6A0FE5D698EBCF7345BEFE* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandlerGearbox_U3CStartU3Eb__9_0_mAC06C59EE5D60D83407F14666E54194722E96603_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDF510ED9801CFCE3E869CBFF940B43857E871D99);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// Gearbox = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___Gearbox_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Gearbox_5), (void*)L_0);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_1;
		L_1 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_1;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2 = V_0;
		int32_t L_3;
		L_3 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_2, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_4 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_3);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_5 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_5, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_5;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_022e;
	}

IL_0030:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_6 = V_0;
		int32_t L_7 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_8;
		L_8 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_6, L_7, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_9;
		L_9 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_8, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_10;
		L_10 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_11 = V_0;
		int32_t L_12 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_13;
		L_13 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_11, L_12, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_14;
		L_14 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_13, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_15;
		L_15 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		V_3 = L_15;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_16 = V_0;
		int32_t L_17 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_18;
		L_18 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_16, L_17, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_19;
		L_19 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_18, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_20;
		L_20 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_19);
		V_4 = L_20;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_21 = V_0;
		int32_t L_22 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_23;
		L_23 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_21, L_22, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_24;
		L_24 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_23, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_25;
		L_25 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_24);
		V_5 = L_25;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_26 = V_0;
		int32_t L_27 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_28;
		L_28 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_26, L_27, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_29;
		L_29 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_28, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_30;
		L_30 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_29);
		V_6 = L_30;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_31 = V_0;
		int32_t L_32 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_33;
		L_33 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_31, L_32, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_34;
		L_34 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_33, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_35;
		L_35 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		V_7 = L_35;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_36 = V_0;
		int32_t L_37 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_38;
		L_38 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_36, L_37, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_39;
		L_39 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_38, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_40;
		L_40 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_39);
		V_8 = L_40;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_41 = V_0;
		int32_t L_42 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_43;
		L_43 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_41, L_42, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_44;
		L_44 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_43, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_45;
		L_45 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_44);
		V_9 = L_45;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_46 = V_0;
		int32_t L_47 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_48;
		L_48 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_46, L_47, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_49;
		L_49 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_48, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_50;
		L_50 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_49);
		V_10 = L_50;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_51 = V_0;
		int32_t L_52 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_53;
		L_53 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_51, L_52, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_54;
		L_54 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_53, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_55;
		L_55 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_54);
		V_11 = L_55;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_56 = V_0;
		int32_t L_57 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_58;
		L_58 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_56, L_57, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_59;
		L_59 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_58, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_60;
		L_60 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_59);
		V_12 = L_60;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_61 = V_0;
		int32_t L_62 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_63;
		L_63 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_61, L_62, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_64;
		L_64 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_63, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_65;
		L_65 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_64);
		V_13 = L_65;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_66 = V_0;
		int32_t L_67 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_68;
		L_68 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_66, L_67, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_69;
		L_69 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_68, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_70;
		L_70 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_69);
		V_14 = L_70;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_71 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_72, L_71, NULL);
		V_15 = L_72;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = V_15;
		String_t* L_74 = V_3;
		L_73->___tipo_0 = L_74;
		Il2CppCodeGenWriteBarrier((void**)(&L_73->___tipo_0), (void*)L_74);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_75 = V_15;
		String_t* L_76 = V_4;
		L_75->___marca_1 = L_76;
		Il2CppCodeGenWriteBarrier((void**)(&L_75->___marca_1), (void*)L_76);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_77 = V_15;
		String_t* L_78 = V_5;
		L_77->___color_2 = L_78;
		Il2CppCodeGenWriteBarrier((void**)(&L_77->___color_2), (void*)L_78);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_79 = V_15;
		String_t* L_80 = V_6;
		L_79->___material_3 = L_80;
		Il2CppCodeGenWriteBarrier((void**)(&L_79->___material_3), (void*)L_80);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_81 = V_15;
		String_t* L_82 = V_7;
		L_81->___potencia_4 = L_82;
		Il2CppCodeGenWriteBarrier((void**)(&L_81->___potencia_4), (void*)L_82);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_83 = V_15;
		String_t* L_84 = V_8;
		L_83->___precision_5 = L_84;
		Il2CppCodeGenWriteBarrier((void**)(&L_83->___precision_5), (void*)L_84);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_85 = V_15;
		String_t* L_86 = V_9;
		L_85->___alcance_6 = L_86;
		Il2CppCodeGenWriteBarrier((void**)(&L_85->___alcance_6), (void*)L_86);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_87 = V_15;
		String_t* L_88 = V_10;
		L_87->___cadencia_7 = L_88;
		Il2CppCodeGenWriteBarrier((void**)(&L_87->___cadencia_7), (void*)L_88);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_89 = V_15;
		String_t* L_90 = V_11;
		L_89->___estabilidad_8 = L_90;
		Il2CppCodeGenWriteBarrier((void**)(&L_89->___estabilidad_8), (void*)L_90);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_91 = V_15;
		String_t* L_92 = V_12;
		L_91->___precio_9 = L_92;
		Il2CppCodeGenWriteBarrier((void**)(&L_91->___precio_9), (void*)L_92);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_93 = V_15;
		String_t* L_94 = V_13;
		L_93->___modelo_10 = L_94;
		Il2CppCodeGenWriteBarrier((void**)(&L_93->___modelo_10), (void*)L_94);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_95 = V_15;
		String_t* L_96 = V_14;
		L_95->___extra_11 = L_96;
		Il2CppCodeGenWriteBarrier((void**)(&L_95->___extra_11), (void*)L_96);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_97 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_98 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = L_98;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_100 = V_15;
		String_t* L_101 = L_100->___tipo_0;
		ArrayElementTypeCheck (L_99, L_101);
		(L_99)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_101);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_102 = L_99;
		ArrayElementTypeCheck (L_102, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_102)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_102;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_104 = V_15;
		String_t* L_105 = L_104->___marca_1;
		ArrayElementTypeCheck (L_103, L_105);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_105);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_106 = L_103;
		ArrayElementTypeCheck (L_106, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_106)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_106;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_108 = V_15;
		String_t* L_109 = L_108->___modelo_10;
		ArrayElementTypeCheck (L_107, L_109);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_109);
		String_t* L_110;
		L_110 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_107, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_97, L_110, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_111 = __this->___itemDatabase_12;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_112 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_111, L_112, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_113 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_113, 1));
	}

IL_022e:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_115 = V_0;
		int32_t L_116;
		L_116 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_115, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_114) < ((int32_t)L_116)))
		{
			goto IL_0030;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_117 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_118;
		L_118 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_117, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_118;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0283:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0278_1;
			}

IL_0244_1:
			{
				// foreach (string item in items)
				String_t* L_119;
				L_119 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_119;
				// if (item.Contains("Gearbox"))
				String_t* L_120 = V_17;
				bool L_121;
				L_121 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_120, _stringLiteralDF510ED9801CFCE3E869CBFF940B43857E871D99, NULL);
				if (!L_121)
				{
					goto IL_0278_1;
				}
			}
			{
				// Gearbox.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_122 = __this->___Gearbox_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_123;
				L_123 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_122, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_124 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_124, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_125 = L_124;
				String_t* L_126 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_125, L_126, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_123, L_125, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_0278_1:
			{
				// foreach (string item in items)
				bool L_127;
				L_127 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_127)
				{
					goto IL_0244_1;
				}
			}
			{
				goto IL_0291;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0291:
	{
		// Gearbox.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_128 = __this->___Gearbox_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_128, NULL);
		// Gearbox.onValueChanged.AddListener(delegate { dropDownSelected(Gearbox); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_129 = __this->___Gearbox_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_130;
		L_130 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_129, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_131 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_131, __this, (intptr_t)((void*)DropDownHandlerGearbox_U3CStartU3Eb__9_0_mAC06C59EE5D60D83407F14666E54194722E96603_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_130, L_131, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandlerGearbox::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGearbox_dropDownSelected_m2DEB8AF72C0222FEAF1E9C82D27D3E8C328963A3 (DropDownHandlerGearbox_t8AD5B054B30C7F3F3A6A0FE5D698EBCF7345BEFE* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (precisionDrop != 0)
		double L_2 = __this->___precisionDrop_8;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setPrecision(0, precisionDrop);
		double L_3 = __this->___precisionDrop_8;
		Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (alcanceDrop != 0)
		double L_4 = __this->___alcanceDrop_7;
		if ((((double)L_4) == ((double)(0.0))))
		{
			goto IL_006f;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_5 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_5, NULL);
	}

IL_006f:
	{
		// if (cadenciaDrop != 0)
		double L_6 = __this->___cadenciaDrop_9;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_0094;
		}
	}
	{
		// Window_Graph.setCadencia(0, cadenciaDrop);
		double L_7 = __this->___cadenciaDrop_9;
		Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142((0.0), L_7, NULL);
	}

IL_0094:
	{
		// if (precio != 0)
		double L_8 = __this->___precio_10;
		if ((((double)L_8) == ((double)(0.0))))
		{
			goto IL_00b9;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_9 = __this->___precio_10;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_9, NULL);
	}

IL_00b9:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_10 = ___dropdown0;
		int32_t L_11;
		L_11 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_10, NULL);
		V_0 = L_11;
		// Debug.Log(Gearbox.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_12 = __this->___Gearbox_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_13;
		L_13 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_12, NULL);
		int32_t L_14 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_15;
		L_15 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_13, L_14, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_16;
		L_16 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_15, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_16, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_17 = __this->___itemDatabase_12;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_18;
		L_18 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_17, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_18;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0192:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0184_1;
			}

IL_00ec_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_19;
				L_19 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_19;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_21 = L_20;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_22 = V_2;
				String_t* L_23 = L_22->___tipo_0;
				ArrayElementTypeCheck (L_21, L_23);
				(L_21)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_23);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_24 = L_21;
				ArrayElementTypeCheck (L_24, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_24)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = L_24;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_26 = V_2;
				String_t* L_27 = L_26->___marca_1;
				ArrayElementTypeCheck (L_25, L_27);
				(L_25)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_27);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_28 = L_25;
				ArrayElementTypeCheck (L_28, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_28)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_29 = L_28;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_30 = V_2;
				String_t* L_31 = L_30->___modelo_10;
				ArrayElementTypeCheck (L_29, L_31);
				(L_29)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_31);
				String_t* L_32;
				L_32 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_29, NULL);
				// if (comparar == Gearbox.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_33 = __this->___Gearbox_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_34;
				L_34 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_33, NULL);
				int32_t L_35 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_36;
				L_36 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_34, L_35, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_37;
				L_37 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_36, NULL);
				bool L_38;
				L_38 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_32, L_37, NULL);
				if (!L_38)
				{
					goto IL_0184_1;
				}
			}
			{
				// precio = int.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_39 = V_2;
				String_t* L_40 = L_39->___precio_9;
				int32_t L_41;
				L_41 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_40, NULL);
				__this->___precio_10 = ((double)L_41);
				// extra = Componente.extra;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_42 = V_2;
				String_t* L_43 = L_42->___extra_11;
				__this->___extra_11 = L_43;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_11), (void*)L_43);
				// Window_Graph.setExtra(extra);
				String_t* L_44 = __this->___extra_11;
				Window_Graph_setExtra_m8774145D45E480AAFA50F295A5FB4E00A289FF91_inline(L_44, NULL);
				// Window_Graph.setPrecio(precio,0);
				double L_45 = __this->___precio_10;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_45, (0.0), NULL);
			}

IL_0184_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_46;
				L_46 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_46)
				{
					goto IL_00ec_1;
				}
			}
			{
				goto IL_01a0;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_01a0:
	{
		// }
		return;
	}
}
// System.Void DropDownHandlerGearbox::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGearbox__ctor_m864FBE3C78780ED47D4FE9B86822B5E55945E6A0 (DropDownHandlerGearbox_t8AD5B054B30C7F3F3A6A0FE5D698EBCF7345BEFE* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_12 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_12), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandlerGearbox::<Start>b__9_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGearbox_U3CStartU3Eb__9_0_mAC06C59EE5D60D83407F14666E54194722E96603 (DropDownHandlerGearbox_t8AD5B054B30C7F3F3A6A0FE5D698EBCF7345BEFE* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// Gearbox.onValueChanged.AddListener(delegate { dropDownSelected(Gearbox); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___Gearbox_5;
		DropDownHandlerGearbox_dropDownSelected_m2DEB8AF72C0222FEAF1E9C82D27D3E8C328963A3(__this, L_0, NULL);
		// Gearbox.onValueChanged.AddListener(delegate { dropDownSelected(Gearbox); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandlerGomaHop::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGomaHop_Start_m40ACE6395FB2D4761752DA73FE643C5FC43A6869 (DropDownHandlerGomaHop_tC3343E792B3FA6C73E6CE2A626D2E7449B9DF73D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandlerGomaHop_U3CStartU3Eb__11_0_m4B4BAE68C150400B7C51DBDB63D6C0FE2627BF0A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral047498634A17E8FA61240674787E84D16F1C77AB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC9905A377B0481F1298530EBB4046F26866045AD);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// GomaHop = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___GomaHop_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___GomaHop_5), (void*)L_0);
		// GomaHopPieza = GameObject.Find("Cilindro.013");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralC9905A377B0481F1298530EBB4046F26866045AD, NULL);
		__this->___GomaHopPieza_13 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___GomaHopPieza_13), (void*)L_1);
		// potenciaDrop = 0;
		__this->___potenciaDrop_6 = (0.0);
		// alcanceDrop = 0;
		__this->___alcanceDrop_7 = (0.0);
		// precisionDrop = 0;
		__this->___precisionDrop_8 = (0.0);
		// cadenciaDrop = 0;
		__this->___cadenciaDrop_9 = (0.0);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2;
		L_2 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_2;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_3 = V_0;
		int32_t L_4;
		L_4 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_3, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_4);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_6 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_6, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_6;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_027a;
	}

IL_007c:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_7 = V_0;
		int32_t L_8 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_9;
		L_9 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_7, L_8, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_10;
		L_10 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_9, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_11;
		L_11 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_12 = V_0;
		int32_t L_13 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_14;
		L_14 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_12, L_13, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_15;
		L_15 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_14, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_3 = L_16;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_17 = V_0;
		int32_t L_18 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_19;
		L_19 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_17, L_18, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_20;
		L_20 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_19, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_21;
		L_21 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		V_4 = L_21;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_22 = V_0;
		int32_t L_23 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_24;
		L_24 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_22, L_23, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_25;
		L_25 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_24, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_26;
		L_26 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		V_5 = L_26;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_27 = V_0;
		int32_t L_28 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_29;
		L_29 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_27, L_28, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_30;
		L_30 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_29, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_31;
		L_31 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		V_6 = L_31;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_32 = V_0;
		int32_t L_33 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_34;
		L_34 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_32, L_33, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_35;
		L_35 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_34, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_36;
		L_36 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		V_7 = L_36;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_37 = V_0;
		int32_t L_38 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_39;
		L_39 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_37, L_38, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_40;
		L_40 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_39, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_41;
		L_41 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_40);
		V_8 = L_41;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_42 = V_0;
		int32_t L_43 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_44;
		L_44 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_42, L_43, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_45;
		L_45 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_44, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_46;
		L_46 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_45);
		V_9 = L_46;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_47 = V_0;
		int32_t L_48 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_49;
		L_49 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_47, L_48, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_50;
		L_50 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_49, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_51;
		L_51 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_50);
		V_10 = L_51;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_52 = V_0;
		int32_t L_53 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_54;
		L_54 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_52, L_53, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_55;
		L_55 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_54, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_56;
		L_56 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_55);
		V_11 = L_56;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_57 = V_0;
		int32_t L_58 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_59;
		L_59 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_57, L_58, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_60;
		L_60 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_59, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_61;
		L_61 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_60);
		V_12 = L_61;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_62 = V_0;
		int32_t L_63 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_64;
		L_64 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_62, L_63, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_65;
		L_65 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_64, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_66;
		L_66 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_65);
		V_13 = L_66;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_67 = V_0;
		int32_t L_68 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_69;
		L_69 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_67, L_68, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_70;
		L_70 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_69, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_71;
		L_71 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_70);
		V_14 = L_71;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_73, L_72, NULL);
		V_15 = L_73;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_74 = V_15;
		String_t* L_75 = V_3;
		L_74->___tipo_0 = L_75;
		Il2CppCodeGenWriteBarrier((void**)(&L_74->___tipo_0), (void*)L_75);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_76 = V_15;
		String_t* L_77 = V_4;
		L_76->___marca_1 = L_77;
		Il2CppCodeGenWriteBarrier((void**)(&L_76->___marca_1), (void*)L_77);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_78 = V_15;
		String_t* L_79 = V_5;
		L_78->___color_2 = L_79;
		Il2CppCodeGenWriteBarrier((void**)(&L_78->___color_2), (void*)L_79);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_80 = V_15;
		String_t* L_81 = V_6;
		L_80->___material_3 = L_81;
		Il2CppCodeGenWriteBarrier((void**)(&L_80->___material_3), (void*)L_81);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_82 = V_15;
		String_t* L_83 = V_7;
		L_82->___potencia_4 = L_83;
		Il2CppCodeGenWriteBarrier((void**)(&L_82->___potencia_4), (void*)L_83);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_84 = V_15;
		String_t* L_85 = V_8;
		L_84->___precision_5 = L_85;
		Il2CppCodeGenWriteBarrier((void**)(&L_84->___precision_5), (void*)L_85);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_86 = V_15;
		String_t* L_87 = V_9;
		L_86->___alcance_6 = L_87;
		Il2CppCodeGenWriteBarrier((void**)(&L_86->___alcance_6), (void*)L_87);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_88 = V_15;
		String_t* L_89 = V_10;
		L_88->___cadencia_7 = L_89;
		Il2CppCodeGenWriteBarrier((void**)(&L_88->___cadencia_7), (void*)L_89);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_90 = V_15;
		String_t* L_91 = V_11;
		L_90->___estabilidad_8 = L_91;
		Il2CppCodeGenWriteBarrier((void**)(&L_90->___estabilidad_8), (void*)L_91);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_92 = V_15;
		String_t* L_93 = V_12;
		L_92->___precio_9 = L_93;
		Il2CppCodeGenWriteBarrier((void**)(&L_92->___precio_9), (void*)L_93);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_94 = V_15;
		String_t* L_95 = V_13;
		L_94->___modelo_10 = L_95;
		Il2CppCodeGenWriteBarrier((void**)(&L_94->___modelo_10), (void*)L_95);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_96 = V_15;
		String_t* L_97 = V_14;
		L_96->___extra_11 = L_97;
		Il2CppCodeGenWriteBarrier((void**)(&L_96->___extra_11), (void*)L_97);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_98 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_100 = L_99;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_101 = V_15;
		String_t* L_102 = L_101->___tipo_0;
		ArrayElementTypeCheck (L_100, L_102);
		(L_100)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_102);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_100;
		ArrayElementTypeCheck (L_103, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_104 = L_103;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_15;
		String_t* L_106 = L_105->___marca_1;
		ArrayElementTypeCheck (L_104, L_106);
		(L_104)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_106);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_104;
		ArrayElementTypeCheck (L_107, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_108 = L_107;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_109 = V_15;
		String_t* L_110 = L_109->___modelo_10;
		ArrayElementTypeCheck (L_108, L_110);
		(L_108)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_110);
		String_t* L_111;
		L_111 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_108, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_98, L_111, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_112 = __this->___itemDatabase_14;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_113 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_112, L_113, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_114, 1));
	}

IL_027a:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_115 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_116 = V_0;
		int32_t L_117;
		L_117 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_116, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_115) < ((int32_t)L_117)))
		{
			goto IL_007c;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_118 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_119;
		L_119 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_118, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_119;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_02cf:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_02c4_1;
			}

IL_0290_1:
			{
				// foreach (string item in items)
				String_t* L_120;
				L_120 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_120;
				// if (item.Contains("Gomas de hop"))
				String_t* L_121 = V_17;
				bool L_122;
				L_122 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_121, _stringLiteral047498634A17E8FA61240674787E84D16F1C77AB, NULL);
				if (!L_122)
				{
					goto IL_02c4_1;
				}
			}
			{
				// GomaHop.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_123 = __this->___GomaHop_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_124;
				L_124 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_123, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_125 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_125, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_126 = L_125;
				String_t* L_127 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_126, L_127, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_124, L_126, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_02c4_1:
			{
				// foreach (string item in items)
				bool L_128;
				L_128 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_128)
				{
					goto IL_0290_1;
				}
			}
			{
				goto IL_02dd;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_02dd:
	{
		// GomaHop.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_129 = __this->___GomaHop_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_129, NULL);
		// GomaHop.onValueChanged.AddListener(delegate { dropDownSelected(GomaHop); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_130 = __this->___GomaHop_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_131;
		L_131 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_130, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_132 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_132, __this, (intptr_t)((void*)DropDownHandlerGomaHop_U3CStartU3Eb__11_0_m4B4BAE68C150400B7C51DBDB63D6C0FE2627BF0A_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_131, L_132, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandlerGomaHop::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGomaHop_dropDownSelected_mB46A37DDC4F4EB2DACBEBF35E0AF8E7D8748B23C (DropDownHandlerGomaHop_tC3343E792B3FA6C73E6CE2A626D2E7449B9DF73D* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2F6D805593B413AC973CE9C5EC17CA4C78508DC9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4DE573297F69792F2A52321674F0590D7424E99A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* V_3 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (alcanceDrop != 0)
		double L_2 = __this->___alcanceDrop_7;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_3 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (varianzaDrop != 0)
		float L_4 = __this->___varianzaDrop_10;
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_0067;
		}
	}
	{
		// Window_Graph.setVarianza(0, varianzaDrop);
		float L_5 = __this->___varianzaDrop_10;
		Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8((0.0f), L_5, NULL);
	}

IL_0067:
	{
		// if (precisionDrop != 0)
		double L_6 = __this->___precisionDrop_8;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_008c;
		}
	}
	{
		// Window_Graph.setPrecision(0, precisionDrop);
		double L_7 = __this->___precisionDrop_8;
		Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6((0.0), L_7, NULL);
	}

IL_008c:
	{
		// if (cadenciaDrop != 0)
		double L_8 = __this->___cadenciaDrop_9;
		if ((((double)L_8) == ((double)(0.0))))
		{
			goto IL_00b1;
		}
	}
	{
		// Window_Graph.setCadencia(0, cadenciaDrop);
		double L_9 = __this->___cadenciaDrop_9;
		Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142((0.0), L_9, NULL);
	}

IL_00b1:
	{
		// if (precio != 0)
		double L_10 = __this->___precio_11;
		if ((((double)L_10) == ((double)(0.0))))
		{
			goto IL_00d6;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_11 = __this->___precio_11;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_11, NULL);
	}

IL_00d6:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_12 = ___dropdown0;
		int32_t L_13;
		L_13 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_12, NULL);
		V_0 = L_13;
		// Debug.Log(GomaHop.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_14 = __this->___GomaHop_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_15;
		L_15 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_14, NULL);
		int32_t L_16 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_17;
		L_17 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_15, L_16, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_18;
		L_18 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_17, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_18, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_19 = __this->___itemDatabase_14;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_20;
		L_20 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_19, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_20;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_040d:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_03ff_1;
			}

IL_0109_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_21;
				L_21 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_21;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_24 = V_2;
				String_t* L_25 = L_24->___tipo_0;
				ArrayElementTypeCheck (L_23, L_25);
				(L_23)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_25);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_23;
				ArrayElementTypeCheck (L_26, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_26)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_27 = L_26;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_28 = V_2;
				String_t* L_29 = L_28->___marca_1;
				ArrayElementTypeCheck (L_27, L_29);
				(L_27)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_29);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_30 = L_27;
				ArrayElementTypeCheck (L_30, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_30)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_31 = L_30;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_32 = V_2;
				String_t* L_33 = L_32->___modelo_10;
				ArrayElementTypeCheck (L_31, L_33);
				(L_31)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_33);
				String_t* L_34;
				L_34 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_31, NULL);
				// if (comparar == GomaHop.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_35 = __this->___GomaHop_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_36;
				L_36 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_35, NULL);
				int32_t L_37 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_38;
				L_38 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_36, L_37, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_39;
				L_39 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_38, NULL);
				bool L_40;
				L_40 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_34, L_39, NULL);
				if (!L_40)
				{
					goto IL_03ff_1;
				}
			}
			{
				// potenciaDrop = int.Parse(Componente.potencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_41 = V_2;
				String_t* L_42 = L_41->___potencia_4;
				int32_t L_43;
				L_43 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_42, NULL);
				__this->___potenciaDrop_6 = ((double)L_43);
				// alcanceDrop = int.Parse(Componente.alcance);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_44 = V_2;
				String_t* L_45 = L_44->___alcance_6;
				int32_t L_46;
				L_46 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_45, NULL);
				__this->___alcanceDrop_7 = ((double)L_46);
				// varianzaDrop = int.Parse(Componente.estabilidad);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_47 = V_2;
				String_t* L_48 = L_47->___estabilidad_8;
				int32_t L_49;
				L_49 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_48, NULL);
				__this->___varianzaDrop_10 = ((float)L_49);
				// precisionDrop = int.Parse(Componente.precision);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_50 = V_2;
				String_t* L_51 = L_50->___precision_5;
				int32_t L_52;
				L_52 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_51, NULL);
				__this->___precisionDrop_8 = ((double)L_52);
				// cadenciaDrop = int.Parse(Componente.cadencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_53 = V_2;
				String_t* L_54 = L_53->___cadencia_7;
				int32_t L_55;
				L_55 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_54, NULL);
				__this->___cadenciaDrop_9 = ((double)L_55);
				// precio = int.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_56 = V_2;
				String_t* L_57 = L_56->___precio_9;
				int32_t L_58;
				L_58 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_57, NULL);
				__this->___precio_11 = ((double)L_58);
				// extra = Componente.extra;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_59 = V_2;
				String_t* L_60 = L_59->___extra_11;
				__this->___extra_12 = L_60;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_12), (void*)L_60);
				// Window_Graph.setAlcance(alcanceDrop);
				double L_61 = __this->___alcanceDrop_7;
				Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2(L_61, (0.0), NULL);
				// Window_Graph.setPotencia(potenciaDrop);
				double L_62 = __this->___potenciaDrop_6;
				Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E(L_62, (0.0), NULL);
				// Window_Graph.setVarianza(varianzaDrop);
				float L_63 = __this->___varianzaDrop_10;
				Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8(L_63, (0.0f), NULL);
				// Window_Graph.setPrecision(precisionDrop);
				double L_64 = __this->___precisionDrop_8;
				Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6(L_64, (0.0), NULL);
				// Window_Graph.setCadencia(cadenciaDrop);
				double L_65 = __this->___cadenciaDrop_9;
				Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142(L_65, (0.0), NULL);
				// Window_Graph.setPrecio(precio,0);
				double L_66 = __this->___precio_11;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_66, (0.0), NULL);
				// Debug.Log("potenciaDropDown: " + potenciaDrop);
				double* L_67 = (&__this->___potenciaDrop_6);
				String_t* L_68;
				L_68 = Double_ToString_m7499A5D792419537DCB9470A3675CEF5117DE339(L_67, NULL);
				String_t* L_69;
				L_69 = String_Concat_mAF2CE02CC0CB7460753D0A1A91CCF2B1E9804C5D(_stringLiteral2F6D805593B413AC973CE9C5EC17CA4C78508DC9, L_68, NULL);
				il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
				Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_69, NULL);
				// var renderer = GomaHopPieza.GetComponent<Renderer>();
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_70 = __this->___GomaHopPieza_13;
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_71;
				L_71 = GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A(L_70, GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
				V_3 = L_71;
				// if (Componente.color == "Rojo")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = V_2;
				String_t* L_73 = L_72->___color_2;
				bool L_74;
				L_74 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_73, _stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C, NULL);
				if (!L_74)
				{
					goto IL_02a0_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.red);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_75 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_76;
				L_76 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_75, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_77;
				L_77 = Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_76, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_77, NULL);
			}

IL_02a0_1:
			{
				// if (Componente.color == "Gris")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_78 = V_2;
				String_t* L_79 = L_78->___color_2;
				bool L_80;
				L_80 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_79, _stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116, NULL);
				if (!L_80)
				{
					goto IL_02c7_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_81 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_82;
				L_82 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_81, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_83;
				L_83 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_82, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_83, NULL);
			}

IL_02c7_1:
			{
				// if (Componente.color == "Gris oscuro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_84 = V_2;
				String_t* L_85 = L_84->___color_2;
				bool L_86;
				L_86 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_85, _stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC, NULL);
				if (!L_86)
				{
					goto IL_02ee_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_87 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_88;
				L_88 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_87, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_89;
				L_89 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_88, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_89, NULL);
			}

IL_02ee_1:
			{
				// if (Componente.color == "Gris claro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_90 = V_2;
				String_t* L_91 = L_90->___color_2;
				bool L_92;
				L_92 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_91, _stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6, NULL);
				if (!L_92)
				{
					goto IL_0315_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_93 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_94;
				L_94 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_93, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_95;
				L_95 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_94, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_95, NULL);
			}

IL_0315_1:
			{
				// if (Componente.color == "Azul")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_96 = V_2;
				String_t* L_97 = L_96->___color_2;
				bool L_98;
				L_98 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_97, _stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510, NULL);
				if (!L_98)
				{
					goto IL_033c_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.blue);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_99 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_100;
				L_100 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_99, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_101;
				L_101 = Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_100, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_101, NULL);
			}

IL_033c_1:
			{
				// if (Componente.color == "Negro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_102 = V_2;
				String_t* L_103 = L_102->___color_2;
				bool L_104;
				L_104 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_103, _stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9, NULL);
				if (!L_104)
				{
					goto IL_0363_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_105 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_106;
				L_106 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_105, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_107;
				L_107 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_106, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_107, NULL);
			}

IL_0363_1:
			{
				// if (Componente.color == "Dorado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_108 = V_2;
				String_t* L_109 = L_108->___color_2;
				bool L_110;
				L_110 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_109, _stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E, NULL);
				if (!L_110)
				{
					goto IL_038a_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.yellow);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_111 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_112;
				L_112 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_111, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_113;
				L_113 = Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_112, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_113, NULL);
			}

IL_038a_1:
			{
				// if (Componente.color == "Plateado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_114 = V_2;
				String_t* L_115 = L_114->___color_2;
				bool L_116;
				L_116 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_115, _stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF, NULL);
				if (!L_116)
				{
					goto IL_03b1_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_117 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_118;
				L_118 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_117, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_119;
				L_119 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_118, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_119, NULL);
			}

IL_03b1_1:
			{
				// if (Componente.color == "Cobre")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_120 = V_2;
				String_t* L_121 = L_120->___color_2;
				bool L_122;
				L_122 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_121, _stringLiteral4DE573297F69792F2A52321674F0590D7424E99A, NULL);
				if (!L_122)
				{
					goto IL_03d8_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_123 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_124;
				L_124 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_123, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_125;
				L_125 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_124, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_125, NULL);
			}

IL_03d8_1:
			{
				// if (Componente.color == "")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_126 = V_2;
				String_t* L_127 = L_126->___color_2;
				bool L_128;
				L_128 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_127, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
				if (!L_128)
				{
					goto IL_03ff_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_129 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_130;
				L_130 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_129, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_131;
				L_131 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_130, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_131, NULL);
			}

IL_03ff_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_132;
				L_132 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_132)
				{
					goto IL_0109_1;
				}
			}
			{
				goto IL_041b;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_041b:
	{
		// }
		return;
	}
}
// System.Void DropDownHandlerGomaHop::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGomaHop__ctor_mAFA5EB92C94AE9BB2CF60606023D48A41DA30903 (DropDownHandlerGomaHop_tC3343E792B3FA6C73E6CE2A626D2E7449B9DF73D* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_14 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_14), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandlerGomaHop::<Start>b__11_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGomaHop_U3CStartU3Eb__11_0_m4B4BAE68C150400B7C51DBDB63D6C0FE2627BF0A (DropDownHandlerGomaHop_tC3343E792B3FA6C73E6CE2A626D2E7449B9DF73D* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// GomaHop.onValueChanged.AddListener(delegate { dropDownSelected(GomaHop); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___GomaHop_5;
		DropDownHandlerGomaHop_dropDownSelected_mB46A37DDC4F4EB2DACBEBF35E0AF8E7D8748B23C(__this, L_0, NULL);
		// GomaHop.onValueChanged.AddListener(delegate { dropDownSelected(GomaHop); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandlerGuiaDeMuelle::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGuiaDeMuelle_Start_m98F25071AAD01CE30F704621A4CCCBAA53257F00 (DropDownHandlerGuiaDeMuelle_tC851957E0FDBCD577B891510FF1DFB787769C1F9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandlerGuiaDeMuelle_U3CStartU3Eb__10_0_m6339013E14DBF4D778320EA9AD722B604A7F5CDB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6EE72FABC13DAF84001008FBDB3CD1D7E0CDECF8);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF16220686C298462264A4DCD7410CE6F941EB0EB);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// GuiaDeMuelle = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___GuiaDeMuelle_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___GuiaDeMuelle_5), (void*)L_0);
		// GuiaMuellePieza = GameObject.Find("GuiaMuelleV2");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralF16220686C298462264A4DCD7410CE6F941EB0EB, NULL);
		__this->___GuiaMuellePieza_13 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___GuiaMuellePieza_13), (void*)L_1);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2;
		L_2 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_2;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_3 = V_0;
		int32_t L_4;
		L_4 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_3, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_4);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_6 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_6, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_6;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_023e;
	}

IL_0040:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_7 = V_0;
		int32_t L_8 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_9;
		L_9 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_7, L_8, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_10;
		L_10 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_9, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_11;
		L_11 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_12 = V_0;
		int32_t L_13 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_14;
		L_14 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_12, L_13, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_15;
		L_15 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_14, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_3 = L_16;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_17 = V_0;
		int32_t L_18 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_19;
		L_19 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_17, L_18, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_20;
		L_20 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_19, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_21;
		L_21 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		V_4 = L_21;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_22 = V_0;
		int32_t L_23 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_24;
		L_24 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_22, L_23, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_25;
		L_25 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_24, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_26;
		L_26 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		V_5 = L_26;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_27 = V_0;
		int32_t L_28 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_29;
		L_29 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_27, L_28, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_30;
		L_30 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_29, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_31;
		L_31 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		V_6 = L_31;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_32 = V_0;
		int32_t L_33 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_34;
		L_34 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_32, L_33, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_35;
		L_35 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_34, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_36;
		L_36 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		V_7 = L_36;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_37 = V_0;
		int32_t L_38 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_39;
		L_39 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_37, L_38, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_40;
		L_40 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_39, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_41;
		L_41 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_40);
		V_8 = L_41;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_42 = V_0;
		int32_t L_43 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_44;
		L_44 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_42, L_43, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_45;
		L_45 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_44, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_46;
		L_46 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_45);
		V_9 = L_46;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_47 = V_0;
		int32_t L_48 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_49;
		L_49 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_47, L_48, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_50;
		L_50 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_49, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_51;
		L_51 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_50);
		V_10 = L_51;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_52 = V_0;
		int32_t L_53 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_54;
		L_54 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_52, L_53, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_55;
		L_55 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_54, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_56;
		L_56 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_55);
		V_11 = L_56;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_57 = V_0;
		int32_t L_58 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_59;
		L_59 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_57, L_58, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_60;
		L_60 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_59, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_61;
		L_61 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_60);
		V_12 = L_61;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_62 = V_0;
		int32_t L_63 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_64;
		L_64 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_62, L_63, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_65;
		L_65 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_64, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_66;
		L_66 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_65);
		V_13 = L_66;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_67 = V_0;
		int32_t L_68 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_69;
		L_69 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_67, L_68, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_70;
		L_70 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_69, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_71;
		L_71 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_70);
		V_14 = L_71;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_73, L_72, NULL);
		V_15 = L_73;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_74 = V_15;
		String_t* L_75 = V_3;
		L_74->___tipo_0 = L_75;
		Il2CppCodeGenWriteBarrier((void**)(&L_74->___tipo_0), (void*)L_75);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_76 = V_15;
		String_t* L_77 = V_4;
		L_76->___marca_1 = L_77;
		Il2CppCodeGenWriteBarrier((void**)(&L_76->___marca_1), (void*)L_77);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_78 = V_15;
		String_t* L_79 = V_5;
		L_78->___color_2 = L_79;
		Il2CppCodeGenWriteBarrier((void**)(&L_78->___color_2), (void*)L_79);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_80 = V_15;
		String_t* L_81 = V_6;
		L_80->___material_3 = L_81;
		Il2CppCodeGenWriteBarrier((void**)(&L_80->___material_3), (void*)L_81);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_82 = V_15;
		String_t* L_83 = V_7;
		L_82->___potencia_4 = L_83;
		Il2CppCodeGenWriteBarrier((void**)(&L_82->___potencia_4), (void*)L_83);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_84 = V_15;
		String_t* L_85 = V_8;
		L_84->___precision_5 = L_85;
		Il2CppCodeGenWriteBarrier((void**)(&L_84->___precision_5), (void*)L_85);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_86 = V_15;
		String_t* L_87 = V_9;
		L_86->___alcance_6 = L_87;
		Il2CppCodeGenWriteBarrier((void**)(&L_86->___alcance_6), (void*)L_87);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_88 = V_15;
		String_t* L_89 = V_10;
		L_88->___cadencia_7 = L_89;
		Il2CppCodeGenWriteBarrier((void**)(&L_88->___cadencia_7), (void*)L_89);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_90 = V_15;
		String_t* L_91 = V_11;
		L_90->___estabilidad_8 = L_91;
		Il2CppCodeGenWriteBarrier((void**)(&L_90->___estabilidad_8), (void*)L_91);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_92 = V_15;
		String_t* L_93 = V_12;
		L_92->___precio_9 = L_93;
		Il2CppCodeGenWriteBarrier((void**)(&L_92->___precio_9), (void*)L_93);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_94 = V_15;
		String_t* L_95 = V_13;
		L_94->___modelo_10 = L_95;
		Il2CppCodeGenWriteBarrier((void**)(&L_94->___modelo_10), (void*)L_95);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_96 = V_15;
		String_t* L_97 = V_14;
		L_96->___extra_11 = L_97;
		Il2CppCodeGenWriteBarrier((void**)(&L_96->___extra_11), (void*)L_97);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_98 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_100 = L_99;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_101 = V_15;
		String_t* L_102 = L_101->___tipo_0;
		ArrayElementTypeCheck (L_100, L_102);
		(L_100)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_102);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_100;
		ArrayElementTypeCheck (L_103, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_104 = L_103;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_15;
		String_t* L_106 = L_105->___marca_1;
		ArrayElementTypeCheck (L_104, L_106);
		(L_104)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_106);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_104;
		ArrayElementTypeCheck (L_107, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_108 = L_107;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_109 = V_15;
		String_t* L_110 = L_109->___modelo_10;
		ArrayElementTypeCheck (L_108, L_110);
		(L_108)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_110);
		String_t* L_111;
		L_111 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_108, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_98, L_111, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_112 = __this->___itemDatabase_12;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_113 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_112, L_113, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_114, 1));
	}

IL_023e:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_115 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_116 = V_0;
		int32_t L_117;
		L_117 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_116, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_115) < ((int32_t)L_117)))
		{
			goto IL_0040;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_118 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_119;
		L_119 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_118, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_119;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0293:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0288_1;
			}

IL_0254_1:
			{
				// foreach (string item in items)
				String_t* L_120;
				L_120 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_120;
				// if (item.Contains("Guia de muelle"))
				String_t* L_121 = V_17;
				bool L_122;
				L_122 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_121, _stringLiteral6EE72FABC13DAF84001008FBDB3CD1D7E0CDECF8, NULL);
				if (!L_122)
				{
					goto IL_0288_1;
				}
			}
			{
				// GuiaDeMuelle.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_123 = __this->___GuiaDeMuelle_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_124;
				L_124 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_123, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_125 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_125, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_126 = L_125;
				String_t* L_127 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_126, L_127, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_124, L_126, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_0288_1:
			{
				// foreach (string item in items)
				bool L_128;
				L_128 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_128)
				{
					goto IL_0254_1;
				}
			}
			{
				goto IL_02a1;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_02a1:
	{
		// GuiaDeMuelle.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_129 = __this->___GuiaDeMuelle_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_129, NULL);
		// GuiaDeMuelle.onValueChanged.AddListener(delegate { dropDownSelected(GuiaDeMuelle); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_130 = __this->___GuiaDeMuelle_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_131;
		L_131 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_130, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_132 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_132, __this, (intptr_t)((void*)DropDownHandlerGuiaDeMuelle_U3CStartU3Eb__10_0_m6339013E14DBF4D778320EA9AD722B604A7F5CDB_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_131, L_132, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandlerGuiaDeMuelle::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGuiaDeMuelle_dropDownSelected_m733D23725483F1F6490FCDE21D7B4B3BA6AEA443 (DropDownHandlerGuiaDeMuelle_tC851957E0FDBCD577B891510FF1DFB787769C1F9* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4DE573297F69792F2A52321674F0590D7424E99A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* V_3 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (alcanceDrop != 0)
		double L_2 = __this->___alcanceDrop_7;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_3 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (precio != 0)
		double L_4 = __this->___precio_10;
		if ((((double)L_4) == ((double)(0.0))))
		{
			goto IL_006f;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_5 = __this->___precio_10;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_5, NULL);
	}

IL_006f:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_6 = ___dropdown0;
		int32_t L_7;
		L_7 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_6, NULL);
		V_0 = L_7;
		// Debug.Log(GuiaDeMuelle.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_8 = __this->___GuiaDeMuelle_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_9;
		L_9 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_8, NULL);
		int32_t L_10 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_11;
		L_11 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_9, L_10, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_12;
		L_12 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_11, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_12, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_13 = __this->___itemDatabase_12;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_14;
		L_14 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_13, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_14;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0329:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_031b_1;
			}

IL_00a2_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_15;
				L_15 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_15;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_16 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_17 = L_16;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_18 = V_2;
				String_t* L_19 = L_18->___tipo_0;
				ArrayElementTypeCheck (L_17, L_19);
				(L_17)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_19);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = L_17;
				ArrayElementTypeCheck (L_20, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_20)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_21 = L_20;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_22 = V_2;
				String_t* L_23 = L_22->___marca_1;
				ArrayElementTypeCheck (L_21, L_23);
				(L_21)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_23);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_24 = L_21;
				ArrayElementTypeCheck (L_24, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_24)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = L_24;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_26 = V_2;
				String_t* L_27 = L_26->___modelo_10;
				ArrayElementTypeCheck (L_25, L_27);
				(L_25)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_27);
				String_t* L_28;
				L_28 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_25, NULL);
				// if (comparar == GuiaDeMuelle.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_29 = __this->___GuiaDeMuelle_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_30;
				L_30 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_29, NULL);
				int32_t L_31 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_32;
				L_32 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_30, L_31, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_33;
				L_33 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_32, NULL);
				bool L_34;
				L_34 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_28, L_33, NULL);
				if (!L_34)
				{
					goto IL_031b_1;
				}
			}
			{
				// potenciaDrop = int.Parse(Componente.potencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_35 = V_2;
				String_t* L_36 = L_35->___potencia_4;
				int32_t L_37;
				L_37 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_36, NULL);
				__this->___potenciaDrop_6 = ((double)L_37);
				// alcanceDrop = int.Parse(Componente.alcance);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_38 = V_2;
				String_t* L_39 = L_38->___alcance_6;
				int32_t L_40;
				L_40 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_39, NULL);
				__this->___alcanceDrop_7 = ((double)L_40);
				// precio = int.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_41 = V_2;
				String_t* L_42 = L_41->___precio_9;
				int32_t L_43;
				L_43 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_42, NULL);
				__this->___precio_10 = ((double)L_43);
				// extra = Componente.extra;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_44 = V_2;
				String_t* L_45 = L_44->___extra_11;
				__this->___extra_11 = L_45;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_11), (void*)L_45);
				// Window_Graph.setExtra(extra);
				String_t* L_46 = __this->___extra_11;
				Window_Graph_setExtra_m8774145D45E480AAFA50F295A5FB4E00A289FF91_inline(L_46, NULL);
				// Window_Graph.setPrecio(precio,0);
				double L_47 = __this->___precio_10;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_47, (0.0), NULL);
				// Window_Graph.setAlcance(alcanceDrop);
				double L_48 = __this->___alcanceDrop_7;
				Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2(L_48, (0.0), NULL);
				// Window_Graph.setPotencia(potenciaDrop);
				double L_49 = __this->___potenciaDrop_6;
				Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E(L_49, (0.0), NULL);
				// var renderer = GuiaMuellePieza.GetComponent<Renderer>();
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_50 = __this->___GuiaMuellePieza_13;
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_51;
				L_51 = GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A(L_50, GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
				V_3 = L_51;
				// if (Componente.color == "Rojo")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_52 = V_2;
				String_t* L_53 = L_52->___color_2;
				bool L_54;
				L_54 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_53, _stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C, NULL);
				if (!L_54)
				{
					goto IL_01bc_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.red);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_55 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_56;
				L_56 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_55, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_57;
				L_57 = Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_56, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_57, NULL);
			}

IL_01bc_1:
			{
				// if (Componente.color == "Gris")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_58 = V_2;
				String_t* L_59 = L_58->___color_2;
				bool L_60;
				L_60 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_59, _stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116, NULL);
				if (!L_60)
				{
					goto IL_01e3_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_61 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_62;
				L_62 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_61, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_63;
				L_63 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_62, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_63, NULL);
			}

IL_01e3_1:
			{
				// if (Componente.color == "Gris oscuro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_64 = V_2;
				String_t* L_65 = L_64->___color_2;
				bool L_66;
				L_66 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_65, _stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC, NULL);
				if (!L_66)
				{
					goto IL_020a_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_67 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_68;
				L_68 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_67, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_69;
				L_69 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_68, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_69, NULL);
			}

IL_020a_1:
			{
				// if (Componente.color == "Gris claro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_70 = V_2;
				String_t* L_71 = L_70->___color_2;
				bool L_72;
				L_72 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_71, _stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6, NULL);
				if (!L_72)
				{
					goto IL_0231_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_73 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_74;
				L_74 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_73, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_75;
				L_75 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_74, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_75, NULL);
			}

IL_0231_1:
			{
				// if (Componente.color == "Azul")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_76 = V_2;
				String_t* L_77 = L_76->___color_2;
				bool L_78;
				L_78 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_77, _stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510, NULL);
				if (!L_78)
				{
					goto IL_0258_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.blue);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_79 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_80;
				L_80 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_79, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_81;
				L_81 = Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_80, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_81, NULL);
			}

IL_0258_1:
			{
				// if (Componente.color == "Negro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_82 = V_2;
				String_t* L_83 = L_82->___color_2;
				bool L_84;
				L_84 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_83, _stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9, NULL);
				if (!L_84)
				{
					goto IL_027f_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_85 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_86;
				L_86 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_85, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_87;
				L_87 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_86, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_87, NULL);
			}

IL_027f_1:
			{
				// if (Componente.color == "Dorado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_88 = V_2;
				String_t* L_89 = L_88->___color_2;
				bool L_90;
				L_90 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_89, _stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E, NULL);
				if (!L_90)
				{
					goto IL_02a6_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.yellow);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_91 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_92;
				L_92 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_91, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_93;
				L_93 = Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_92, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_93, NULL);
			}

IL_02a6_1:
			{
				// if (Componente.color == "Plateado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_94 = V_2;
				String_t* L_95 = L_94->___color_2;
				bool L_96;
				L_96 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_95, _stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF, NULL);
				if (!L_96)
				{
					goto IL_02cd_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_97 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_98;
				L_98 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_97, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_99;
				L_99 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_98, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_99, NULL);
			}

IL_02cd_1:
			{
				// if (Componente.color == "Cobre")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_100 = V_2;
				String_t* L_101 = L_100->___color_2;
				bool L_102;
				L_102 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_101, _stringLiteral4DE573297F69792F2A52321674F0590D7424E99A, NULL);
				if (!L_102)
				{
					goto IL_02f4_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_103 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_104;
				L_104 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_103, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_105;
				L_105 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_104, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_105, NULL);
			}

IL_02f4_1:
			{
				// if (Componente.color == "")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_106 = V_2;
				String_t* L_107 = L_106->___color_2;
				bool L_108;
				L_108 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_107, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
				if (!L_108)
				{
					goto IL_031b_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_109 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_110;
				L_110 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_109, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_111;
				L_111 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_110, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_111, NULL);
			}

IL_031b_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_112;
				L_112 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_112)
				{
					goto IL_00a2_1;
				}
			}
			{
				goto IL_0337;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0337:
	{
		// }
		return;
	}
}
// System.Void DropDownHandlerGuiaDeMuelle::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGuiaDeMuelle__ctor_mAF9EA2DC830AC0F63EEE915832F6519D974059A5 (DropDownHandlerGuiaDeMuelle_tC851957E0FDBCD577B891510FF1DFB787769C1F9* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_12 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_12), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandlerGuiaDeMuelle::<Start>b__10_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerGuiaDeMuelle_U3CStartU3Eb__10_0_m6339013E14DBF4D778320EA9AD722B604A7F5CDB (DropDownHandlerGuiaDeMuelle_tC851957E0FDBCD577B891510FF1DFB787769C1F9* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// GuiaDeMuelle.onValueChanged.AddListener(delegate { dropDownSelected(GuiaDeMuelle); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___GuiaDeMuelle_5;
		DropDownHandlerGuiaDeMuelle_dropDownSelected_m733D23725483F1F6490FCDE21D7B4B3BA6AEA443(__this, L_0, NULL);
		// GuiaDeMuelle.onValueChanged.AddListener(delegate { dropDownSelected(GuiaDeMuelle); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandlerMosfet::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerMosfet_Start_m78D4A5D740B095DD384CD1631870B2FB89ECE497 (DropDownHandlerMosfet_tFE41C42B0AC339D1ECC3E696D89C5C586D28EE40* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandlerMosfet_U3CStartU3Eb__9_0_m65B83DAF56665DAE980B0CE398DF8F6D9FA2C934_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral77433E583240E26992DA7E6DFAA703B2EED57EE4);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral84F1EA7C59FF2336381FF056AF0BA0BF616B7732);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// Mosfet = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___Mosfet_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Mosfet_5), (void*)L_0);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_1;
		L_1 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_1;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2 = V_0;
		int32_t L_3;
		L_3 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_2, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_4 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_3);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_5 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_5, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_5;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_022e;
	}

IL_0030:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_6 = V_0;
		int32_t L_7 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_8;
		L_8 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_6, L_7, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_9;
		L_9 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_8, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_10;
		L_10 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_11 = V_0;
		int32_t L_12 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_13;
		L_13 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_11, L_12, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_14;
		L_14 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_13, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_15;
		L_15 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		V_3 = L_15;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_16 = V_0;
		int32_t L_17 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_18;
		L_18 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_16, L_17, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_19;
		L_19 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_18, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_20;
		L_20 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_19);
		V_4 = L_20;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_21 = V_0;
		int32_t L_22 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_23;
		L_23 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_21, L_22, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_24;
		L_24 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_23, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_25;
		L_25 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_24);
		V_5 = L_25;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_26 = V_0;
		int32_t L_27 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_28;
		L_28 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_26, L_27, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_29;
		L_29 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_28, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_30;
		L_30 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_29);
		V_6 = L_30;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_31 = V_0;
		int32_t L_32 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_33;
		L_33 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_31, L_32, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_34;
		L_34 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_33, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_35;
		L_35 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_34);
		V_7 = L_35;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_36 = V_0;
		int32_t L_37 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_38;
		L_38 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_36, L_37, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_39;
		L_39 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_38, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_40;
		L_40 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_39);
		V_8 = L_40;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_41 = V_0;
		int32_t L_42 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_43;
		L_43 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_41, L_42, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_44;
		L_44 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_43, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_45;
		L_45 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_44);
		V_9 = L_45;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_46 = V_0;
		int32_t L_47 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_48;
		L_48 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_46, L_47, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_49;
		L_49 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_48, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_50;
		L_50 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_49);
		V_10 = L_50;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_51 = V_0;
		int32_t L_52 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_53;
		L_53 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_51, L_52, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_54;
		L_54 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_53, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_55;
		L_55 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_54);
		V_11 = L_55;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_56 = V_0;
		int32_t L_57 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_58;
		L_58 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_56, L_57, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_59;
		L_59 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_58, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_60;
		L_60 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_59);
		V_12 = L_60;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_61 = V_0;
		int32_t L_62 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_63;
		L_63 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_61, L_62, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_64;
		L_64 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_63, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_65;
		L_65 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_64);
		V_13 = L_65;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_66 = V_0;
		int32_t L_67 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_68;
		L_68 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_66, L_67, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_69;
		L_69 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_68, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_70;
		L_70 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_69);
		V_14 = L_70;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_71 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_72, L_71, NULL);
		V_15 = L_72;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = V_15;
		String_t* L_74 = V_3;
		L_73->___tipo_0 = L_74;
		Il2CppCodeGenWriteBarrier((void**)(&L_73->___tipo_0), (void*)L_74);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_75 = V_15;
		String_t* L_76 = V_4;
		L_75->___marca_1 = L_76;
		Il2CppCodeGenWriteBarrier((void**)(&L_75->___marca_1), (void*)L_76);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_77 = V_15;
		String_t* L_78 = V_5;
		L_77->___color_2 = L_78;
		Il2CppCodeGenWriteBarrier((void**)(&L_77->___color_2), (void*)L_78);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_79 = V_15;
		String_t* L_80 = V_6;
		L_79->___material_3 = L_80;
		Il2CppCodeGenWriteBarrier((void**)(&L_79->___material_3), (void*)L_80);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_81 = V_15;
		String_t* L_82 = V_7;
		L_81->___potencia_4 = L_82;
		Il2CppCodeGenWriteBarrier((void**)(&L_81->___potencia_4), (void*)L_82);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_83 = V_15;
		String_t* L_84 = V_8;
		L_83->___precision_5 = L_84;
		Il2CppCodeGenWriteBarrier((void**)(&L_83->___precision_5), (void*)L_84);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_85 = V_15;
		String_t* L_86 = V_9;
		L_85->___alcance_6 = L_86;
		Il2CppCodeGenWriteBarrier((void**)(&L_85->___alcance_6), (void*)L_86);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_87 = V_15;
		String_t* L_88 = V_10;
		L_87->___cadencia_7 = L_88;
		Il2CppCodeGenWriteBarrier((void**)(&L_87->___cadencia_7), (void*)L_88);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_89 = V_15;
		String_t* L_90 = V_11;
		L_89->___estabilidad_8 = L_90;
		Il2CppCodeGenWriteBarrier((void**)(&L_89->___estabilidad_8), (void*)L_90);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_91 = V_15;
		String_t* L_92 = V_12;
		L_91->___precio_9 = L_92;
		Il2CppCodeGenWriteBarrier((void**)(&L_91->___precio_9), (void*)L_92);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_93 = V_15;
		String_t* L_94 = V_13;
		L_93->___modelo_10 = L_94;
		Il2CppCodeGenWriteBarrier((void**)(&L_93->___modelo_10), (void*)L_94);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_95 = V_15;
		String_t* L_96 = V_14;
		L_95->___extra_11 = L_96;
		Il2CppCodeGenWriteBarrier((void**)(&L_95->___extra_11), (void*)L_96);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_97 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_98 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = L_98;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_100 = V_15;
		String_t* L_101 = L_100->___tipo_0;
		ArrayElementTypeCheck (L_99, L_101);
		(L_99)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_101);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_102 = L_99;
		ArrayElementTypeCheck (L_102, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_102)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_102;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_104 = V_15;
		String_t* L_105 = L_104->___marca_1;
		ArrayElementTypeCheck (L_103, L_105);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_105);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_106 = L_103;
		ArrayElementTypeCheck (L_106, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_106)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_106;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_108 = V_15;
		String_t* L_109 = L_108->___modelo_10;
		ArrayElementTypeCheck (L_107, L_109);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_109);
		String_t* L_110;
		L_110 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_107, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_97, L_110, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_111 = __this->___itemDatabase_12;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_112 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_111, L_112, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_113 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_113, 1));
	}

IL_022e:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_115 = V_0;
		int32_t L_116;
		L_116 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_115, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_114) < ((int32_t)L_116)))
		{
			goto IL_0030;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_117 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_118;
		L_118 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_117, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_118;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0291:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0286_1;
			}

IL_0244_1:
			{
				// foreach (string item in items)
				String_t* L_119;
				L_119 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_119;
				// if (item.Contains("Mosfet") || item.Contains("Mosfets"))
				String_t* L_120 = V_17;
				bool L_121;
				L_121 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_120, _stringLiteral77433E583240E26992DA7E6DFAA703B2EED57EE4, NULL);
				if (L_121)
				{
					goto IL_0269_1;
				}
			}
			{
				String_t* L_122 = V_17;
				bool L_123;
				L_123 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_122, _stringLiteral84F1EA7C59FF2336381FF056AF0BA0BF616B7732, NULL);
				if (!L_123)
				{
					goto IL_0286_1;
				}
			}

IL_0269_1:
			{
				// Mosfet.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_124 = __this->___Mosfet_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_125;
				L_125 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_124, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_126 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_126, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_127 = L_126;
				String_t* L_128 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_127, L_128, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_125, L_127, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_0286_1:
			{
				// foreach (string item in items)
				bool L_129;
				L_129 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_129)
				{
					goto IL_0244_1;
				}
			}
			{
				goto IL_029f;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_029f:
	{
		// Mosfet.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_130 = __this->___Mosfet_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_130, NULL);
		// Mosfet.onValueChanged.AddListener(delegate { dropDownSelected(Mosfet); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_131 = __this->___Mosfet_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_132;
		L_132 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_131, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_133 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_133, __this, (intptr_t)((void*)DropDownHandlerMosfet_U3CStartU3Eb__9_0_m65B83DAF56665DAE980B0CE398DF8F6D9FA2C934_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_132, L_133, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandlerMosfet::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerMosfet_dropDownSelected_mDB38E8B59C4BD1A37B3904DDD4140DD0B908D29D (DropDownHandlerMosfet_tFE41C42B0AC339D1ECC3E696D89C5C586D28EE40* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (precisionDrop != 0)
		double L_2 = __this->___precisionDrop_8;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setPrecision(0, precisionDrop);
		double L_3 = __this->___precisionDrop_8;
		Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (alcanceDrop != 0)
		double L_4 = __this->___alcanceDrop_7;
		if ((((double)L_4) == ((double)(0.0))))
		{
			goto IL_006f;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_5 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_5, NULL);
	}

IL_006f:
	{
		// if (cadenciaDrop != 0)
		double L_6 = __this->___cadenciaDrop_9;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_0094;
		}
	}
	{
		// Window_Graph.setCadencia(0, cadenciaDrop);
		double L_7 = __this->___cadenciaDrop_9;
		Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142((0.0), L_7, NULL);
	}

IL_0094:
	{
		// if (precio != 0)
		double L_8 = __this->___precio_10;
		if ((((double)L_8) == ((double)(0.0))))
		{
			goto IL_00b9;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_9 = __this->___precio_10;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_9, NULL);
	}

IL_00b9:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_10 = ___dropdown0;
		int32_t L_11;
		L_11 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_10, NULL);
		V_0 = L_11;
		// Debug.Log(Mosfet.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_12 = __this->___Mosfet_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_13;
		L_13 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_12, NULL);
		int32_t L_14 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_15;
		L_15 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_13, L_14, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_16;
		L_16 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_15, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_16, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_17 = __this->___itemDatabase_12;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_18;
		L_18 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_17, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_18;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0192:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0184_1;
			}

IL_00ec_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_19;
				L_19 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_19;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_21 = L_20;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_22 = V_2;
				String_t* L_23 = L_22->___tipo_0;
				ArrayElementTypeCheck (L_21, L_23);
				(L_21)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_23);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_24 = L_21;
				ArrayElementTypeCheck (L_24, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_24)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = L_24;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_26 = V_2;
				String_t* L_27 = L_26->___marca_1;
				ArrayElementTypeCheck (L_25, L_27);
				(L_25)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_27);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_28 = L_25;
				ArrayElementTypeCheck (L_28, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_28)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_29 = L_28;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_30 = V_2;
				String_t* L_31 = L_30->___modelo_10;
				ArrayElementTypeCheck (L_29, L_31);
				(L_29)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_31);
				String_t* L_32;
				L_32 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_29, NULL);
				// if (comparar == Mosfet.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_33 = __this->___Mosfet_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_34;
				L_34 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_33, NULL);
				int32_t L_35 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_36;
				L_36 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_34, L_35, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_37;
				L_37 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_36, NULL);
				bool L_38;
				L_38 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_32, L_37, NULL);
				if (!L_38)
				{
					goto IL_0184_1;
				}
			}
			{
				// precio = int.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_39 = V_2;
				String_t* L_40 = L_39->___precio_9;
				int32_t L_41;
				L_41 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_40, NULL);
				__this->___precio_10 = ((double)L_41);
				// extra = Componente.extra;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_42 = V_2;
				String_t* L_43 = L_42->___extra_11;
				__this->___extra_11 = L_43;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_11), (void*)L_43);
				// Window_Graph.setExtra(extra);
				String_t* L_44 = __this->___extra_11;
				Window_Graph_setExtra_m8774145D45E480AAFA50F295A5FB4E00A289FF91_inline(L_44, NULL);
				// Window_Graph.setPrecio(precio,0);
				double L_45 = __this->___precio_10;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_45, (0.0), NULL);
			}

IL_0184_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_46;
				L_46 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_46)
				{
					goto IL_00ec_1;
				}
			}
			{
				goto IL_01a0;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_01a0:
	{
		// }
		return;
	}
}
// System.Void DropDownHandlerMosfet::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerMosfet__ctor_m86D5296B61216253111F673D9BBF0EFC30126473 (DropDownHandlerMosfet_tFE41C42B0AC339D1ECC3E696D89C5C586D28EE40* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_12 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_12), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandlerMosfet::<Start>b__9_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerMosfet_U3CStartU3Eb__9_0_m65B83DAF56665DAE980B0CE398DF8F6D9FA2C934 (DropDownHandlerMosfet_tFE41C42B0AC339D1ECC3E696D89C5C586D28EE40* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// Mosfet.onValueChanged.AddListener(delegate { dropDownSelected(Mosfet); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___Mosfet_5;
		DropDownHandlerMosfet_dropDownSelected_mDB38E8B59C4BD1A37B3904DDD4140DD0B908D29D(__this, L_0, NULL);
		// Mosfet.onValueChanged.AddListener(delegate { dropDownSelected(Mosfet); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandlerMuelle::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerMuelle_Start_mDA29863D45802136CD5E5049DD123ABCFE5D8980 (DropDownHandlerMuelle_t0A8E8E1330E2BAD5A501E135B42DCFC94CA3486E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandlerMuelle_U3CStartU3Eb__11_0_m3D9CAB8C350531D5B12E67230396AB80A0D567F9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD0645BCF97335DB32AF235D32E280B4D57A8465D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// Muelle = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___Muelle_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Muelle_5), (void*)L_0);
		// MuellePieza = GameObject.Find("Muelle");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralD0645BCF97335DB32AF235D32E280B4D57A8465D, NULL);
		__this->___MuellePieza_13 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___MuellePieza_13), (void*)L_1);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2;
		L_2 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_2;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_3 = V_0;
		int32_t L_4;
		L_4 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_3, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_4);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_6 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_6, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_6;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_023e;
	}

IL_0040:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_7 = V_0;
		int32_t L_8 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_9;
		L_9 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_7, L_8, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_10;
		L_10 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_9, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_11;
		L_11 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_12 = V_0;
		int32_t L_13 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_14;
		L_14 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_12, L_13, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_15;
		L_15 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_14, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_3 = L_16;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_17 = V_0;
		int32_t L_18 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_19;
		L_19 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_17, L_18, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_20;
		L_20 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_19, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_21;
		L_21 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		V_4 = L_21;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_22 = V_0;
		int32_t L_23 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_24;
		L_24 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_22, L_23, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_25;
		L_25 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_24, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_26;
		L_26 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		V_5 = L_26;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_27 = V_0;
		int32_t L_28 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_29;
		L_29 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_27, L_28, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_30;
		L_30 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_29, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_31;
		L_31 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		V_6 = L_31;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_32 = V_0;
		int32_t L_33 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_34;
		L_34 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_32, L_33, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_35;
		L_35 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_34, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_36;
		L_36 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		V_7 = L_36;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_37 = V_0;
		int32_t L_38 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_39;
		L_39 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_37, L_38, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_40;
		L_40 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_39, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_41;
		L_41 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_40);
		V_8 = L_41;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_42 = V_0;
		int32_t L_43 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_44;
		L_44 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_42, L_43, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_45;
		L_45 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_44, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_46;
		L_46 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_45);
		V_9 = L_46;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_47 = V_0;
		int32_t L_48 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_49;
		L_49 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_47, L_48, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_50;
		L_50 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_49, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_51;
		L_51 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_50);
		V_10 = L_51;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_52 = V_0;
		int32_t L_53 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_54;
		L_54 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_52, L_53, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_55;
		L_55 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_54, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_56;
		L_56 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_55);
		V_11 = L_56;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_57 = V_0;
		int32_t L_58 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_59;
		L_59 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_57, L_58, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_60;
		L_60 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_59, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_61;
		L_61 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_60);
		V_12 = L_61;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_62 = V_0;
		int32_t L_63 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_64;
		L_64 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_62, L_63, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_65;
		L_65 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_64, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_66;
		L_66 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_65);
		V_13 = L_66;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_67 = V_0;
		int32_t L_68 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_69;
		L_69 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_67, L_68, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_70;
		L_70 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_69, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_71;
		L_71 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_70);
		V_14 = L_71;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_73, L_72, NULL);
		V_15 = L_73;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_74 = V_15;
		String_t* L_75 = V_3;
		L_74->___tipo_0 = L_75;
		Il2CppCodeGenWriteBarrier((void**)(&L_74->___tipo_0), (void*)L_75);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_76 = V_15;
		String_t* L_77 = V_4;
		L_76->___marca_1 = L_77;
		Il2CppCodeGenWriteBarrier((void**)(&L_76->___marca_1), (void*)L_77);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_78 = V_15;
		String_t* L_79 = V_5;
		L_78->___color_2 = L_79;
		Il2CppCodeGenWriteBarrier((void**)(&L_78->___color_2), (void*)L_79);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_80 = V_15;
		String_t* L_81 = V_6;
		L_80->___material_3 = L_81;
		Il2CppCodeGenWriteBarrier((void**)(&L_80->___material_3), (void*)L_81);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_82 = V_15;
		String_t* L_83 = V_7;
		L_82->___potencia_4 = L_83;
		Il2CppCodeGenWriteBarrier((void**)(&L_82->___potencia_4), (void*)L_83);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_84 = V_15;
		String_t* L_85 = V_8;
		L_84->___precision_5 = L_85;
		Il2CppCodeGenWriteBarrier((void**)(&L_84->___precision_5), (void*)L_85);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_86 = V_15;
		String_t* L_87 = V_9;
		L_86->___alcance_6 = L_87;
		Il2CppCodeGenWriteBarrier((void**)(&L_86->___alcance_6), (void*)L_87);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_88 = V_15;
		String_t* L_89 = V_10;
		L_88->___cadencia_7 = L_89;
		Il2CppCodeGenWriteBarrier((void**)(&L_88->___cadencia_7), (void*)L_89);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_90 = V_15;
		String_t* L_91 = V_11;
		L_90->___estabilidad_8 = L_91;
		Il2CppCodeGenWriteBarrier((void**)(&L_90->___estabilidad_8), (void*)L_91);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_92 = V_15;
		String_t* L_93 = V_12;
		L_92->___precio_9 = L_93;
		Il2CppCodeGenWriteBarrier((void**)(&L_92->___precio_9), (void*)L_93);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_94 = V_15;
		String_t* L_95 = V_13;
		L_94->___modelo_10 = L_95;
		Il2CppCodeGenWriteBarrier((void**)(&L_94->___modelo_10), (void*)L_95);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_96 = V_15;
		String_t* L_97 = V_14;
		L_96->___extra_11 = L_97;
		Il2CppCodeGenWriteBarrier((void**)(&L_96->___extra_11), (void*)L_97);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_98 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_100 = L_99;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_101 = V_15;
		String_t* L_102 = L_101->___tipo_0;
		ArrayElementTypeCheck (L_100, L_102);
		(L_100)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_102);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_100;
		ArrayElementTypeCheck (L_103, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_104 = L_103;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_15;
		String_t* L_106 = L_105->___marca_1;
		ArrayElementTypeCheck (L_104, L_106);
		(L_104)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_106);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_104;
		ArrayElementTypeCheck (L_107, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_108 = L_107;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_109 = V_15;
		String_t* L_110 = L_109->___modelo_10;
		ArrayElementTypeCheck (L_108, L_110);
		(L_108)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_110);
		String_t* L_111;
		L_111 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_108, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_98, L_111, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_112 = __this->___itemDatabase_14;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_113 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_112, L_113, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_114, 1));
	}

IL_023e:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_115 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_116 = V_0;
		int32_t L_117;
		L_117 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_116, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_115) < ((int32_t)L_117)))
		{
			goto IL_0040;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_118 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_119;
		L_119 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_118, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_119;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0293:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0288_1;
			}

IL_0254_1:
			{
				// foreach (string item in items)
				String_t* L_120;
				L_120 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_120;
				// if (item.Contains("Muelle"))
				String_t* L_121 = V_17;
				bool L_122;
				L_122 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_121, _stringLiteralD0645BCF97335DB32AF235D32E280B4D57A8465D, NULL);
				if (!L_122)
				{
					goto IL_0288_1;
				}
			}
			{
				// Muelle.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_123 = __this->___Muelle_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_124;
				L_124 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_123, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_125 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_125, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_126 = L_125;
				String_t* L_127 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_126, L_127, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_124, L_126, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_0288_1:
			{
				// foreach (string item in items)
				bool L_128;
				L_128 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_128)
				{
					goto IL_0254_1;
				}
			}
			{
				goto IL_02a1;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_02a1:
	{
		// Muelle.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_129 = __this->___Muelle_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_129, NULL);
		// Muelle.onValueChanged.AddListener(delegate { dropDownSelected(Muelle); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_130 = __this->___Muelle_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_131;
		L_131 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_130, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_132 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_132, __this, (intptr_t)((void*)DropDownHandlerMuelle_U3CStartU3Eb__11_0_m3D9CAB8C350531D5B12E67230396AB80A0D567F9_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_131, L_132, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandlerMuelle::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerMuelle_dropDownSelected_m11D7CA3A3A1DA2257974480DE1B4DEA71AC3DF60 (DropDownHandlerMuelle_t0A8E8E1330E2BAD5A501E135B42DCFC94CA3486E* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4DE573297F69792F2A52321674F0590D7424E99A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* V_3 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (alcanceDrop != 0)
		double L_2 = __this->___alcanceDrop_7;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_3 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (varianzaDrop != 0)
		float L_4 = __this->___varianzaDrop_10;
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_0067;
		}
	}
	{
		// Window_Graph.setVarianza(0, varianzaDrop);
		float L_5 = __this->___varianzaDrop_10;
		Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8((0.0f), L_5, NULL);
	}

IL_0067:
	{
		// if (precisionDrop != 0)
		double L_6 = __this->___precisionDrop_8;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_008c;
		}
	}
	{
		// Window_Graph.setPrecision(0, precisionDrop);
		double L_7 = __this->___precisionDrop_8;
		Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6((0.0), L_7, NULL);
	}

IL_008c:
	{
		// if (cadenciaDrop != 0)
		double L_8 = __this->___cadenciaDrop_9;
		if ((((double)L_8) == ((double)(0.0))))
		{
			goto IL_00b1;
		}
	}
	{
		// Window_Graph.setCadencia(0, cadenciaDrop);
		double L_9 = __this->___cadenciaDrop_9;
		Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142((0.0), L_9, NULL);
	}

IL_00b1:
	{
		// if (precio != 0)
		double L_10 = __this->___precio_11;
		if ((((double)L_10) == ((double)(0.0))))
		{
			goto IL_00d6;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_11 = __this->___precio_11;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_11, NULL);
	}

IL_00d6:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_12 = ___dropdown0;
		int32_t L_13;
		L_13 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_12, NULL);
		V_0 = L_13;
		// Debug.Log(Muelle.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_14 = __this->___Muelle_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_15;
		L_15 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_14, NULL);
		int32_t L_16 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_17;
		L_17 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_15, L_16, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_18;
		L_18 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_17, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_18, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_19 = __this->___itemDatabase_14;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_20;
		L_20 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_19, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_20;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_03f3:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_03e5_1;
			}

IL_0109_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_21;
				L_21 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_21;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_24 = V_2;
				String_t* L_25 = L_24->___tipo_0;
				ArrayElementTypeCheck (L_23, L_25);
				(L_23)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_25);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_23;
				ArrayElementTypeCheck (L_26, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_26)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_27 = L_26;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_28 = V_2;
				String_t* L_29 = L_28->___marca_1;
				ArrayElementTypeCheck (L_27, L_29);
				(L_27)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_29);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_30 = L_27;
				ArrayElementTypeCheck (L_30, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_30)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_31 = L_30;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_32 = V_2;
				String_t* L_33 = L_32->___modelo_10;
				ArrayElementTypeCheck (L_31, L_33);
				(L_31)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_33);
				String_t* L_34;
				L_34 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_31, NULL);
				// if (comparar == Muelle.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_35 = __this->___Muelle_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_36;
				L_36 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_35, NULL);
				int32_t L_37 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_38;
				L_38 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_36, L_37, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_39;
				L_39 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_38, NULL);
				bool L_40;
				L_40 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_34, L_39, NULL);
				if (!L_40)
				{
					goto IL_03e5_1;
				}
			}
			{
				// potenciaDrop = int.Parse(Componente.potencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_41 = V_2;
				String_t* L_42 = L_41->___potencia_4;
				int32_t L_43;
				L_43 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_42, NULL);
				__this->___potenciaDrop_6 = ((double)L_43);
				// alcanceDrop = int.Parse(Componente.alcance);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_44 = V_2;
				String_t* L_45 = L_44->___alcance_6;
				int32_t L_46;
				L_46 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_45, NULL);
				__this->___alcanceDrop_7 = ((double)L_46);
				// varianzaDrop = int.Parse(Componente.estabilidad);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_47 = V_2;
				String_t* L_48 = L_47->___estabilidad_8;
				int32_t L_49;
				L_49 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_48, NULL);
				__this->___varianzaDrop_10 = ((float)L_49);
				// precisionDrop = int.Parse(Componente.precision);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_50 = V_2;
				String_t* L_51 = L_50->___precision_5;
				int32_t L_52;
				L_52 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_51, NULL);
				__this->___precisionDrop_8 = ((double)L_52);
				// cadenciaDrop = int.Parse(Componente.cadencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_53 = V_2;
				String_t* L_54 = L_53->___cadencia_7;
				int32_t L_55;
				L_55 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_54, NULL);
				__this->___cadenciaDrop_9 = ((double)L_55);
				// precio = int.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_56 = V_2;
				String_t* L_57 = L_56->___precio_9;
				int32_t L_58;
				L_58 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_57, NULL);
				__this->___precio_11 = ((double)L_58);
				// extra = Componente.extra;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_59 = V_2;
				String_t* L_60 = L_59->___extra_11;
				__this->___extra_12 = L_60;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_12), (void*)L_60);
				// Window_Graph.setAlcance(alcanceDrop);
				double L_61 = __this->___alcanceDrop_7;
				Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2(L_61, (0.0), NULL);
				// Window_Graph.setPotencia(potenciaDrop);
				double L_62 = __this->___potenciaDrop_6;
				Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E(L_62, (0.0), NULL);
				// Window_Graph.setVarianza(varianzaDrop);
				float L_63 = __this->___varianzaDrop_10;
				Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8(L_63, (0.0f), NULL);
				// Window_Graph.setPrecision(precisionDrop);
				double L_64 = __this->___precisionDrop_8;
				Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6(L_64, (0.0), NULL);
				// Window_Graph.setCadencia(cadenciaDrop);
				double L_65 = __this->___cadenciaDrop_9;
				Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142(L_65, (0.0), NULL);
				// Window_Graph.setPrecio(precio,0);
				double L_66 = __this->___precio_11;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_66, (0.0), NULL);
				// var renderer = MuellePieza.GetComponent<Renderer>();
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_67 = __this->___MuellePieza_13;
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_68;
				L_68 = GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A(L_67, GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
				V_3 = L_68;
				// if (Componente.color == "Rojo")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_69 = V_2;
				String_t* L_70 = L_69->___color_2;
				bool L_71;
				L_71 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_70, _stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C, NULL);
				if (!L_71)
				{
					goto IL_0286_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.red);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_72 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_73;
				L_73 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_72, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_74;
				L_74 = Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_73, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_74, NULL);
			}

IL_0286_1:
			{
				// if (Componente.color == "Gris")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_75 = V_2;
				String_t* L_76 = L_75->___color_2;
				bool L_77;
				L_77 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_76, _stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116, NULL);
				if (!L_77)
				{
					goto IL_02ad_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_78 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_79;
				L_79 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_78, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_80;
				L_80 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_79, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_80, NULL);
			}

IL_02ad_1:
			{
				// if (Componente.color == "Gris oscuro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_81 = V_2;
				String_t* L_82 = L_81->___color_2;
				bool L_83;
				L_83 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_82, _stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC, NULL);
				if (!L_83)
				{
					goto IL_02d4_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_84 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_85;
				L_85 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_84, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_86;
				L_86 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_85, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_86, NULL);
			}

IL_02d4_1:
			{
				// if (Componente.color == "Gris claro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_87 = V_2;
				String_t* L_88 = L_87->___color_2;
				bool L_89;
				L_89 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_88, _stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6, NULL);
				if (!L_89)
				{
					goto IL_02fb_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_90 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_91;
				L_91 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_90, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_92;
				L_92 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_91, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_92, NULL);
			}

IL_02fb_1:
			{
				// if (Componente.color == "")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_93 = V_2;
				String_t* L_94 = L_93->___color_2;
				bool L_95;
				L_95 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_94, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
				if (!L_95)
				{
					goto IL_0322_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_96 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_97;
				L_97 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_96, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_98;
				L_98 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_97, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_98, NULL);
			}

IL_0322_1:
			{
				// if (Componente.color == "Azul")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_99 = V_2;
				String_t* L_100 = L_99->___color_2;
				bool L_101;
				L_101 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_100, _stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510, NULL);
				if (!L_101)
				{
					goto IL_0349_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.blue);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_102 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_103;
				L_103 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_102, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_104;
				L_104 = Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_103, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_104, NULL);
			}

IL_0349_1:
			{
				// if (Componente.color == "Negro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_2;
				String_t* L_106 = L_105->___color_2;
				bool L_107;
				L_107 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_106, _stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9, NULL);
				if (!L_107)
				{
					goto IL_0370_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_108 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_109;
				L_109 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_108, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_110;
				L_110 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_109, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_110, NULL);
			}

IL_0370_1:
			{
				// if (Componente.color == "Dorado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_111 = V_2;
				String_t* L_112 = L_111->___color_2;
				bool L_113;
				L_113 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_112, _stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E, NULL);
				if (!L_113)
				{
					goto IL_0397_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.yellow);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_114 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_115;
				L_115 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_114, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_116;
				L_116 = Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_115, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_116, NULL);
			}

IL_0397_1:
			{
				// if (Componente.color == "Plateado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_117 = V_2;
				String_t* L_118 = L_117->___color_2;
				bool L_119;
				L_119 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_118, _stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF, NULL);
				if (!L_119)
				{
					goto IL_03be_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_120 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_121;
				L_121 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_120, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_122;
				L_122 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_121, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_122, NULL);
			}

IL_03be_1:
			{
				// if (Componente.color == "Cobre")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_123 = V_2;
				String_t* L_124 = L_123->___color_2;
				bool L_125;
				L_125 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_124, _stringLiteral4DE573297F69792F2A52321674F0590D7424E99A, NULL);
				if (!L_125)
				{
					goto IL_03e5_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_126 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_127;
				L_127 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_126, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_128;
				L_128 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_127, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_128, NULL);
			}

IL_03e5_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_129;
				L_129 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_129)
				{
					goto IL_0109_1;
				}
			}
			{
				goto IL_0401;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0401:
	{
		// }
		return;
	}
}
// System.Void DropDownHandlerMuelle::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerMuelle__ctor_m363953D24C991C90BC79F47A2887A12140C1EEE1 (DropDownHandlerMuelle_t0A8E8E1330E2BAD5A501E135B42DCFC94CA3486E* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_14 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_14), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandlerMuelle::<Start>b__11_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerMuelle_U3CStartU3Eb__11_0_m3D9CAB8C350531D5B12E67230396AB80A0D567F9 (DropDownHandlerMuelle_t0A8E8E1330E2BAD5A501E135B42DCFC94CA3486E* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// Muelle.onValueChanged.AddListener(delegate { dropDownSelected(Muelle); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___Muelle_5;
		DropDownHandlerMuelle_dropDownSelected_m11D7CA3A3A1DA2257974480DE1B4DEA71AC3DF60(__this, L_0, NULL);
		// Muelle.onValueChanged.AddListener(delegate { dropDownSelected(Muelle); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandlerNozzle::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerNozzle_Start_mE513D8D1DB5A9A916BA30B09D9BF61C74E0D5070 (DropDownHandlerNozzle_tD13C58D6F1C773675C2AB0134F6C96E7C8D0490A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandlerNozzle_U3CStartU3Eb__11_0_mE7253AF7301F0734BE270E30D436097400B56689_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6B17125BFE29CB975A7E9D3A67FC7104EC9AB7C3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// Nozzle = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___Nozzle_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Nozzle_5), (void*)L_0);
		// nozzlePieza = GameObject.Find("Nozzle");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral6B17125BFE29CB975A7E9D3A67FC7104EC9AB7C3, NULL);
		__this->___nozzlePieza_14 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___nozzlePieza_14), (void*)L_1);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2;
		L_2 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_2;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_3 = V_0;
		int32_t L_4;
		L_4 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_3, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_4);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_6 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_6, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_6;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_023e;
	}

IL_0040:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_7 = V_0;
		int32_t L_8 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_9;
		L_9 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_7, L_8, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_10;
		L_10 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_9, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_11;
		L_11 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_12 = V_0;
		int32_t L_13 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_14;
		L_14 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_12, L_13, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_15;
		L_15 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_14, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_3 = L_16;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_17 = V_0;
		int32_t L_18 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_19;
		L_19 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_17, L_18, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_20;
		L_20 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_19, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_21;
		L_21 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		V_4 = L_21;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_22 = V_0;
		int32_t L_23 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_24;
		L_24 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_22, L_23, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_25;
		L_25 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_24, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_26;
		L_26 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		V_5 = L_26;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_27 = V_0;
		int32_t L_28 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_29;
		L_29 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_27, L_28, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_30;
		L_30 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_29, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_31;
		L_31 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		V_6 = L_31;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_32 = V_0;
		int32_t L_33 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_34;
		L_34 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_32, L_33, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_35;
		L_35 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_34, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_36;
		L_36 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		V_7 = L_36;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_37 = V_0;
		int32_t L_38 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_39;
		L_39 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_37, L_38, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_40;
		L_40 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_39, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_41;
		L_41 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_40);
		V_8 = L_41;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_42 = V_0;
		int32_t L_43 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_44;
		L_44 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_42, L_43, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_45;
		L_45 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_44, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_46;
		L_46 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_45);
		V_9 = L_46;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_47 = V_0;
		int32_t L_48 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_49;
		L_49 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_47, L_48, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_50;
		L_50 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_49, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_51;
		L_51 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_50);
		V_10 = L_51;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_52 = V_0;
		int32_t L_53 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_54;
		L_54 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_52, L_53, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_55;
		L_55 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_54, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_56;
		L_56 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_55);
		V_11 = L_56;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_57 = V_0;
		int32_t L_58 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_59;
		L_59 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_57, L_58, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_60;
		L_60 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_59, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_61;
		L_61 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_60);
		V_12 = L_61;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_62 = V_0;
		int32_t L_63 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_64;
		L_64 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_62, L_63, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_65;
		L_65 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_64, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_66;
		L_66 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_65);
		V_13 = L_66;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_67 = V_0;
		int32_t L_68 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_69;
		L_69 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_67, L_68, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_70;
		L_70 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_69, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_71;
		L_71 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_70);
		V_14 = L_71;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_73, L_72, NULL);
		V_15 = L_73;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_74 = V_15;
		String_t* L_75 = V_3;
		L_74->___tipo_0 = L_75;
		Il2CppCodeGenWriteBarrier((void**)(&L_74->___tipo_0), (void*)L_75);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_76 = V_15;
		String_t* L_77 = V_4;
		L_76->___marca_1 = L_77;
		Il2CppCodeGenWriteBarrier((void**)(&L_76->___marca_1), (void*)L_77);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_78 = V_15;
		String_t* L_79 = V_5;
		L_78->___color_2 = L_79;
		Il2CppCodeGenWriteBarrier((void**)(&L_78->___color_2), (void*)L_79);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_80 = V_15;
		String_t* L_81 = V_6;
		L_80->___material_3 = L_81;
		Il2CppCodeGenWriteBarrier((void**)(&L_80->___material_3), (void*)L_81);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_82 = V_15;
		String_t* L_83 = V_7;
		L_82->___potencia_4 = L_83;
		Il2CppCodeGenWriteBarrier((void**)(&L_82->___potencia_4), (void*)L_83);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_84 = V_15;
		String_t* L_85 = V_8;
		L_84->___precision_5 = L_85;
		Il2CppCodeGenWriteBarrier((void**)(&L_84->___precision_5), (void*)L_85);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_86 = V_15;
		String_t* L_87 = V_9;
		L_86->___alcance_6 = L_87;
		Il2CppCodeGenWriteBarrier((void**)(&L_86->___alcance_6), (void*)L_87);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_88 = V_15;
		String_t* L_89 = V_10;
		L_88->___cadencia_7 = L_89;
		Il2CppCodeGenWriteBarrier((void**)(&L_88->___cadencia_7), (void*)L_89);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_90 = V_15;
		String_t* L_91 = V_11;
		L_90->___estabilidad_8 = L_91;
		Il2CppCodeGenWriteBarrier((void**)(&L_90->___estabilidad_8), (void*)L_91);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_92 = V_15;
		String_t* L_93 = V_12;
		L_92->___precio_9 = L_93;
		Il2CppCodeGenWriteBarrier((void**)(&L_92->___precio_9), (void*)L_93);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_94 = V_15;
		String_t* L_95 = V_13;
		L_94->___modelo_10 = L_95;
		Il2CppCodeGenWriteBarrier((void**)(&L_94->___modelo_10), (void*)L_95);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_96 = V_15;
		String_t* L_97 = V_14;
		L_96->___extra_11 = L_97;
		Il2CppCodeGenWriteBarrier((void**)(&L_96->___extra_11), (void*)L_97);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_98 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_100 = L_99;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_101 = V_15;
		String_t* L_102 = L_101->___tipo_0;
		ArrayElementTypeCheck (L_100, L_102);
		(L_100)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_102);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_100;
		ArrayElementTypeCheck (L_103, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_104 = L_103;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_15;
		String_t* L_106 = L_105->___marca_1;
		ArrayElementTypeCheck (L_104, L_106);
		(L_104)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_106);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_104;
		ArrayElementTypeCheck (L_107, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_108 = L_107;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_109 = V_15;
		String_t* L_110 = L_109->___modelo_10;
		ArrayElementTypeCheck (L_108, L_110);
		(L_108)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_110);
		String_t* L_111;
		L_111 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_108, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_98, L_111, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_112 = __this->___itemDatabase_13;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_113 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_112, L_113, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_114, 1));
	}

IL_023e:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_115 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_116 = V_0;
		int32_t L_117;
		L_117 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_116, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_115) < ((int32_t)L_117)))
		{
			goto IL_0040;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_118 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_119;
		L_119 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_118, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_119;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0293:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0288_1;
			}

IL_0254_1:
			{
				// foreach (string item in items)
				String_t* L_120;
				L_120 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_120;
				// if (item.Contains("Nozzle"))
				String_t* L_121 = V_17;
				bool L_122;
				L_122 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_121, _stringLiteral6B17125BFE29CB975A7E9D3A67FC7104EC9AB7C3, NULL);
				if (!L_122)
				{
					goto IL_0288_1;
				}
			}
			{
				// Nozzle.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_123 = __this->___Nozzle_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_124;
				L_124 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_123, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_125 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_125, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_126 = L_125;
				String_t* L_127 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_126, L_127, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_124, L_126, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_0288_1:
			{
				// foreach (string item in items)
				bool L_128;
				L_128 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_128)
				{
					goto IL_0254_1;
				}
			}
			{
				goto IL_02a1;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_02a1:
	{
		// Nozzle.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_129 = __this->___Nozzle_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_129, NULL);
		// Nozzle.onValueChanged.AddListener(delegate { dropDownSelected(Nozzle); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_130 = __this->___Nozzle_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_131;
		L_131 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_130, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_132 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_132, __this, (intptr_t)((void*)DropDownHandlerNozzle_U3CStartU3Eb__11_0_mE7253AF7301F0734BE270E30D436097400B56689_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_131, L_132, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandlerNozzle::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerNozzle_dropDownSelected_m904609CE01849C3F4E66CB1B76942E9995CFFDD3 (DropDownHandlerNozzle_tD13C58D6F1C773675C2AB0134F6C96E7C8D0490A* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4DE573297F69792F2A52321674F0590D7424E99A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* V_3 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (alcanceDrop != 0)
		double L_2 = __this->___alcanceDrop_7;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_3 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (varianzaDrop != 0)
		float L_4 = __this->___varianzaDrop_10;
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_0067;
		}
	}
	{
		// Window_Graph.setVarianza(0, varianzaDrop);
		float L_5 = __this->___varianzaDrop_10;
		Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8((0.0f), L_5, NULL);
	}

IL_0067:
	{
		// if (precisionDrop != 0)
		double L_6 = __this->___precisionDrop_8;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_008c;
		}
	}
	{
		// Window_Graph.setPrecision(0, precisionDrop);
		double L_7 = __this->___precisionDrop_8;
		Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6((0.0), L_7, NULL);
	}

IL_008c:
	{
		// if (cadenciaDrop != 0)
		double L_8 = __this->___cadenciaDrop_9;
		if ((((double)L_8) == ((double)(0.0))))
		{
			goto IL_00b1;
		}
	}
	{
		// Window_Graph.setCadencia(0, cadenciaDrop);
		double L_9 = __this->___cadenciaDrop_9;
		Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142((0.0), L_9, NULL);
	}

IL_00b1:
	{
		// if (precio != 0)
		double L_10 = __this->___precio_11;
		if ((((double)L_10) == ((double)(0.0))))
		{
			goto IL_00d6;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_11 = __this->___precio_11;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_11, NULL);
	}

IL_00d6:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_12 = ___dropdown0;
		int32_t L_13;
		L_13 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_12, NULL);
		V_0 = L_13;
		// Debug.Log(Nozzle.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_14 = __this->___Nozzle_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_15;
		L_15 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_14, NULL);
		int32_t L_16 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_17;
		L_17 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_15, L_16, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_18;
		L_18 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_17, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_18, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_19 = __this->___itemDatabase_13;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_20;
		L_20 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_19, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_20;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_03f3:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_03e5_1;
			}

IL_0109_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_21;
				L_21 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_21;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_24 = V_2;
				String_t* L_25 = L_24->___tipo_0;
				ArrayElementTypeCheck (L_23, L_25);
				(L_23)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_25);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_23;
				ArrayElementTypeCheck (L_26, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_26)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_27 = L_26;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_28 = V_2;
				String_t* L_29 = L_28->___marca_1;
				ArrayElementTypeCheck (L_27, L_29);
				(L_27)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_29);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_30 = L_27;
				ArrayElementTypeCheck (L_30, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_30)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_31 = L_30;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_32 = V_2;
				String_t* L_33 = L_32->___modelo_10;
				ArrayElementTypeCheck (L_31, L_33);
				(L_31)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_33);
				String_t* L_34;
				L_34 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_31, NULL);
				// if (comparar == Nozzle.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_35 = __this->___Nozzle_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_36;
				L_36 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_35, NULL);
				int32_t L_37 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_38;
				L_38 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_36, L_37, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_39;
				L_39 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_38, NULL);
				bool L_40;
				L_40 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_34, L_39, NULL);
				if (!L_40)
				{
					goto IL_03e5_1;
				}
			}
			{
				// potenciaDrop = int.Parse(Componente.potencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_41 = V_2;
				String_t* L_42 = L_41->___potencia_4;
				int32_t L_43;
				L_43 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_42, NULL);
				__this->___potenciaDrop_6 = ((double)L_43);
				// alcanceDrop = int.Parse(Componente.alcance);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_44 = V_2;
				String_t* L_45 = L_44->___alcance_6;
				int32_t L_46;
				L_46 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_45, NULL);
				__this->___alcanceDrop_7 = ((double)L_46);
				// varianzaDrop = int.Parse(Componente.estabilidad);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_47 = V_2;
				String_t* L_48 = L_47->___estabilidad_8;
				int32_t L_49;
				L_49 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_48, NULL);
				__this->___varianzaDrop_10 = ((float)L_49);
				// precisionDrop = int.Parse(Componente.precision);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_50 = V_2;
				String_t* L_51 = L_50->___precision_5;
				int32_t L_52;
				L_52 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_51, NULL);
				__this->___precisionDrop_8 = ((double)L_52);
				// cadenciaDrop = int.Parse(Componente.cadencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_53 = V_2;
				String_t* L_54 = L_53->___cadencia_7;
				int32_t L_55;
				L_55 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_54, NULL);
				__this->___cadenciaDrop_9 = ((double)L_55);
				// precio = int.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_56 = V_2;
				String_t* L_57 = L_56->___precio_9;
				int32_t L_58;
				L_58 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_57, NULL);
				__this->___precio_11 = ((double)L_58);
				// extra = Componente.extra;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_59 = V_2;
				String_t* L_60 = L_59->___extra_11;
				__this->___extra_12 = L_60;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_12), (void*)L_60);
				// Window_Graph.setAlcance(alcanceDrop);
				double L_61 = __this->___alcanceDrop_7;
				Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2(L_61, (0.0), NULL);
				// Window_Graph.setPotencia(potenciaDrop);
				double L_62 = __this->___potenciaDrop_6;
				Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E(L_62, (0.0), NULL);
				// Window_Graph.setVarianza(varianzaDrop);
				float L_63 = __this->___varianzaDrop_10;
				Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8(L_63, (0.0f), NULL);
				// Window_Graph.setPrecision(precisionDrop);
				double L_64 = __this->___precisionDrop_8;
				Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6(L_64, (0.0), NULL);
				// Window_Graph.setCadencia(cadenciaDrop);
				double L_65 = __this->___cadenciaDrop_9;
				Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142(L_65, (0.0), NULL);
				// Window_Graph.setPrecio(precio,0);
				double L_66 = __this->___precio_11;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_66, (0.0), NULL);
				// var renderer = nozzlePieza.GetComponent<Renderer>();
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_67 = __this->___nozzlePieza_14;
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_68;
				L_68 = GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A(L_67, GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
				V_3 = L_68;
				// if (Componente.color == "Rojo")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_69 = V_2;
				String_t* L_70 = L_69->___color_2;
				bool L_71;
				L_71 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_70, _stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C, NULL);
				if (!L_71)
				{
					goto IL_0286_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.red);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_72 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_73;
				L_73 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_72, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_74;
				L_74 = Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_73, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_74, NULL);
			}

IL_0286_1:
			{
				// if (Componente.color == "Gris")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_75 = V_2;
				String_t* L_76 = L_75->___color_2;
				bool L_77;
				L_77 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_76, _stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116, NULL);
				if (!L_77)
				{
					goto IL_02ad_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_78 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_79;
				L_79 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_78, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_80;
				L_80 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_79, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_80, NULL);
			}

IL_02ad_1:
			{
				// if (Componente.color == "")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_81 = V_2;
				String_t* L_82 = L_81->___color_2;
				bool L_83;
				L_83 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_82, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
				if (!L_83)
				{
					goto IL_02d4_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_84 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_85;
				L_85 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_84, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_86;
				L_86 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_85, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_86, NULL);
			}

IL_02d4_1:
			{
				// if (Componente.color == "Gris oscuro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_87 = V_2;
				String_t* L_88 = L_87->___color_2;
				bool L_89;
				L_89 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_88, _stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC, NULL);
				if (!L_89)
				{
					goto IL_02fb_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_90 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_91;
				L_91 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_90, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_92;
				L_92 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_91, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_92, NULL);
			}

IL_02fb_1:
			{
				// if (Componente.color == "Gris claro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_93 = V_2;
				String_t* L_94 = L_93->___color_2;
				bool L_95;
				L_95 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_94, _stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6, NULL);
				if (!L_95)
				{
					goto IL_0322_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_96 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_97;
				L_97 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_96, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_98;
				L_98 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_97, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_98, NULL);
			}

IL_0322_1:
			{
				// if (Componente.color == "Azul")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_99 = V_2;
				String_t* L_100 = L_99->___color_2;
				bool L_101;
				L_101 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_100, _stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510, NULL);
				if (!L_101)
				{
					goto IL_0349_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.blue);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_102 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_103;
				L_103 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_102, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_104;
				L_104 = Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_103, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_104, NULL);
			}

IL_0349_1:
			{
				// if (Componente.color == "Negro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_2;
				String_t* L_106 = L_105->___color_2;
				bool L_107;
				L_107 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_106, _stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9, NULL);
				if (!L_107)
				{
					goto IL_0370_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_108 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_109;
				L_109 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_108, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_110;
				L_110 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_109, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_110, NULL);
			}

IL_0370_1:
			{
				// if (Componente.color == "Dorado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_111 = V_2;
				String_t* L_112 = L_111->___color_2;
				bool L_113;
				L_113 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_112, _stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E, NULL);
				if (!L_113)
				{
					goto IL_0397_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.yellow);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_114 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_115;
				L_115 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_114, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_116;
				L_116 = Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_115, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_116, NULL);
			}

IL_0397_1:
			{
				// if (Componente.color == "Plateado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_117 = V_2;
				String_t* L_118 = L_117->___color_2;
				bool L_119;
				L_119 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_118, _stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF, NULL);
				if (!L_119)
				{
					goto IL_03be_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_120 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_121;
				L_121 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_120, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_122;
				L_122 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_121, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_122, NULL);
			}

IL_03be_1:
			{
				// if (Componente.color == "Cobre")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_123 = V_2;
				String_t* L_124 = L_123->___color_2;
				bool L_125;
				L_125 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_124, _stringLiteral4DE573297F69792F2A52321674F0590D7424E99A, NULL);
				if (!L_125)
				{
					goto IL_03e5_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_126 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_127;
				L_127 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_126, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_128;
				L_128 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_127, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_128, NULL);
			}

IL_03e5_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_129;
				L_129 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_129)
				{
					goto IL_0109_1;
				}
			}
			{
				goto IL_0401;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_0401:
	{
		// }
		return;
	}
}
// System.Void DropDownHandlerNozzle::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerNozzle__ctor_m851645F8CC0096BAA69B82E65B4BD48CC3E39183 (DropDownHandlerNozzle_tD13C58D6F1C773675C2AB0134F6C96E7C8D0490A* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_13 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_13), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandlerNozzle::<Start>b__11_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerNozzle_U3CStartU3Eb__11_0_mE7253AF7301F0734BE270E30D436097400B56689 (DropDownHandlerNozzle_tD13C58D6F1C773675C2AB0134F6C96E7C8D0490A* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// Nozzle.onValueChanged.AddListener(delegate { dropDownSelected(Nozzle); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___Nozzle_5;
		DropDownHandlerNozzle_dropDownSelected_m904609CE01849C3F4E66CB1B76942E9995CFFDD3(__this, L_0, NULL);
		// Nozzle.onValueChanged.AddListener(delegate { dropDownSelected(Nozzle); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandlerNub::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerNub_Start_mA1703EC4ED324023C6E587C455145ABC3BAD1CE6 (DropDownHandlerNub_t3291D567FEB2BCA3659581ED9FB356B6C317E48F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandlerNub_U3CStartU3Eb__9_0_m2D04559A348CAE089EF1217B21237F6EA2C0B151_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBF81BBF805E3CA6AAA5C990682F8CCF6B2C4F5E7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// Nub = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___Nub_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Nub_5), (void*)L_0);
		// nubPieza = GameObject.Find("Nub");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralBF81BBF805E3CA6AAA5C990682F8CCF6B2C4F5E7, NULL);
		__this->___nubPieza_12 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___nubPieza_12), (void*)L_1);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2;
		L_2 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_2;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_3 = V_0;
		int32_t L_4;
		L_4 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_3, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_4);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_6 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_6, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_6;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_023e;
	}

IL_0040:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_7 = V_0;
		int32_t L_8 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_9;
		L_9 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_7, L_8, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_10;
		L_10 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_9, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_11;
		L_11 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_12 = V_0;
		int32_t L_13 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_14;
		L_14 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_12, L_13, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_15;
		L_15 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_14, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_3 = L_16;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_17 = V_0;
		int32_t L_18 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_19;
		L_19 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_17, L_18, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_20;
		L_20 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_19, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_21;
		L_21 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		V_4 = L_21;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_22 = V_0;
		int32_t L_23 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_24;
		L_24 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_22, L_23, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_25;
		L_25 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_24, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_26;
		L_26 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		V_5 = L_26;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_27 = V_0;
		int32_t L_28 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_29;
		L_29 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_27, L_28, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_30;
		L_30 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_29, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_31;
		L_31 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		V_6 = L_31;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_32 = V_0;
		int32_t L_33 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_34;
		L_34 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_32, L_33, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_35;
		L_35 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_34, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_36;
		L_36 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		V_7 = L_36;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_37 = V_0;
		int32_t L_38 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_39;
		L_39 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_37, L_38, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_40;
		L_40 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_39, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_41;
		L_41 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_40);
		V_8 = L_41;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_42 = V_0;
		int32_t L_43 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_44;
		L_44 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_42, L_43, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_45;
		L_45 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_44, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_46;
		L_46 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_45);
		V_9 = L_46;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_47 = V_0;
		int32_t L_48 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_49;
		L_49 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_47, L_48, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_50;
		L_50 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_49, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_51;
		L_51 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_50);
		V_10 = L_51;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_52 = V_0;
		int32_t L_53 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_54;
		L_54 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_52, L_53, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_55;
		L_55 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_54, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_56;
		L_56 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_55);
		V_11 = L_56;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_57 = V_0;
		int32_t L_58 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_59;
		L_59 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_57, L_58, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_60;
		L_60 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_59, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_61;
		L_61 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_60);
		V_12 = L_61;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_62 = V_0;
		int32_t L_63 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_64;
		L_64 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_62, L_63, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_65;
		L_65 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_64, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_66;
		L_66 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_65);
		V_13 = L_66;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_67 = V_0;
		int32_t L_68 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_69;
		L_69 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_67, L_68, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_70;
		L_70 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_69, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_71;
		L_71 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_70);
		V_14 = L_71;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_73, L_72, NULL);
		V_15 = L_73;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_74 = V_15;
		String_t* L_75 = V_3;
		L_74->___tipo_0 = L_75;
		Il2CppCodeGenWriteBarrier((void**)(&L_74->___tipo_0), (void*)L_75);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_76 = V_15;
		String_t* L_77 = V_4;
		L_76->___marca_1 = L_77;
		Il2CppCodeGenWriteBarrier((void**)(&L_76->___marca_1), (void*)L_77);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_78 = V_15;
		String_t* L_79 = V_5;
		L_78->___color_2 = L_79;
		Il2CppCodeGenWriteBarrier((void**)(&L_78->___color_2), (void*)L_79);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_80 = V_15;
		String_t* L_81 = V_6;
		L_80->___material_3 = L_81;
		Il2CppCodeGenWriteBarrier((void**)(&L_80->___material_3), (void*)L_81);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_82 = V_15;
		String_t* L_83 = V_7;
		L_82->___potencia_4 = L_83;
		Il2CppCodeGenWriteBarrier((void**)(&L_82->___potencia_4), (void*)L_83);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_84 = V_15;
		String_t* L_85 = V_8;
		L_84->___precision_5 = L_85;
		Il2CppCodeGenWriteBarrier((void**)(&L_84->___precision_5), (void*)L_85);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_86 = V_15;
		String_t* L_87 = V_9;
		L_86->___alcance_6 = L_87;
		Il2CppCodeGenWriteBarrier((void**)(&L_86->___alcance_6), (void*)L_87);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_88 = V_15;
		String_t* L_89 = V_10;
		L_88->___cadencia_7 = L_89;
		Il2CppCodeGenWriteBarrier((void**)(&L_88->___cadencia_7), (void*)L_89);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_90 = V_15;
		String_t* L_91 = V_11;
		L_90->___estabilidad_8 = L_91;
		Il2CppCodeGenWriteBarrier((void**)(&L_90->___estabilidad_8), (void*)L_91);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_92 = V_15;
		String_t* L_93 = V_12;
		L_92->___precio_9 = L_93;
		Il2CppCodeGenWriteBarrier((void**)(&L_92->___precio_9), (void*)L_93);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_94 = V_15;
		String_t* L_95 = V_13;
		L_94->___modelo_10 = L_95;
		Il2CppCodeGenWriteBarrier((void**)(&L_94->___modelo_10), (void*)L_95);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_96 = V_15;
		String_t* L_97 = V_14;
		L_96->___extra_11 = L_97;
		Il2CppCodeGenWriteBarrier((void**)(&L_96->___extra_11), (void*)L_97);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_98 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_100 = L_99;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_101 = V_15;
		String_t* L_102 = L_101->___tipo_0;
		ArrayElementTypeCheck (L_100, L_102);
		(L_100)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_102);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_100;
		ArrayElementTypeCheck (L_103, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_104 = L_103;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_15;
		String_t* L_106 = L_105->___marca_1;
		ArrayElementTypeCheck (L_104, L_106);
		(L_104)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_106);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_104;
		ArrayElementTypeCheck (L_107, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_108 = L_107;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_109 = V_15;
		String_t* L_110 = L_109->___modelo_10;
		ArrayElementTypeCheck (L_108, L_110);
		(L_108)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_110);
		String_t* L_111;
		L_111 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_108, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_98, L_111, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_112 = __this->___itemDatabase_11;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_113 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_112, L_113, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_114, 1));
	}

IL_023e:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_115 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_116 = V_0;
		int32_t L_117;
		L_117 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_116, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_115) < ((int32_t)L_117)))
		{
			goto IL_0040;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_118 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_119;
		L_119 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_118, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_119;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0293:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0288_1;
			}

IL_0254_1:
			{
				// foreach (string item in items)
				String_t* L_120;
				L_120 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_120;
				// if (item.Contains("Nub"))
				String_t* L_121 = V_17;
				bool L_122;
				L_122 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_121, _stringLiteralBF81BBF805E3CA6AAA5C990682F8CCF6B2C4F5E7, NULL);
				if (!L_122)
				{
					goto IL_0288_1;
				}
			}
			{
				// Nub.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_123 = __this->___Nub_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_124;
				L_124 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_123, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_125 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_125, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_126 = L_125;
				String_t* L_127 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_126, L_127, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_124, L_126, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_0288_1:
			{
				// foreach (string item in items)
				bool L_128;
				L_128 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_128)
				{
					goto IL_0254_1;
				}
			}
			{
				goto IL_02a1;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_02a1:
	{
		// Nub.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_129 = __this->___Nub_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_129, NULL);
		// Nub.onValueChanged.AddListener(delegate { dropDownSelected(Nub); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_130 = __this->___Nub_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_131;
		L_131 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_130, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_132 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_132, __this, (intptr_t)((void*)DropDownHandlerNub_U3CStartU3Eb__9_0_m2D04559A348CAE089EF1217B21237F6EA2C0B151_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_131, L_132, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandlerNub::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerNub_dropDownSelected_m6B267A9871CA0EC1C84734C8931CE8E992568C02 (DropDownHandlerNub_t3291D567FEB2BCA3659581ED9FB356B6C317E48F* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4DE573297F69792F2A52321674F0590D7424E99A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* V_3 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (precisionDrop != 0)
		double L_2 = __this->___precisionDrop_8;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setPrecision(0, precisionDrop);
		double L_3 = __this->___precisionDrop_8;
		Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (alcanceDrop != 0)
		double L_4 = __this->___alcanceDrop_7;
		if ((((double)L_4) == ((double)(0.0))))
		{
			goto IL_006f;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_5 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_5, NULL);
	}

IL_006f:
	{
		// if (cadenciaDrop != 0)
		double L_6 = __this->___cadenciaDrop_9;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_0094;
		}
	}
	{
		// Window_Graph.setCadencia(0, cadenciaDrop);
		double L_7 = __this->___cadenciaDrop_9;
		Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142((0.0), L_7, NULL);
	}

IL_0094:
	{
		// if (precio != 0)
		double L_8 = __this->___precio_10;
		if ((((double)L_8) == ((double)(0.0))))
		{
			goto IL_00b9;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_9 = __this->___precio_10;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_9, NULL);
	}

IL_00b9:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_10 = ___dropdown0;
		int32_t L_11;
		L_11 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_10, NULL);
		V_0 = L_11;
		// Debug.Log(Nub.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_12 = __this->___Nub_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_13;
		L_13 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_12, NULL);
		int32_t L_14 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_15;
		L_15 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_13, L_14, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_16;
		L_16 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_15, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_16, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_17 = __this->___itemDatabase_11;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_18;
		L_18 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_17, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_18;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0310:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0302_1;
			}

IL_00ec_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_19;
				L_19 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_19;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_21 = L_20;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_22 = V_2;
				String_t* L_23 = L_22->___tipo_0;
				ArrayElementTypeCheck (L_21, L_23);
				(L_21)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_23);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_24 = L_21;
				ArrayElementTypeCheck (L_24, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_24)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = L_24;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_26 = V_2;
				String_t* L_27 = L_26->___marca_1;
				ArrayElementTypeCheck (L_25, L_27);
				(L_25)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_27);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_28 = L_25;
				ArrayElementTypeCheck (L_28, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_28)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_29 = L_28;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_30 = V_2;
				String_t* L_31 = L_30->___modelo_10;
				ArrayElementTypeCheck (L_29, L_31);
				(L_29)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_31);
				String_t* L_32;
				L_32 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_29, NULL);
				// if (comparar == Nub.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_33 = __this->___Nub_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_34;
				L_34 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_33, NULL);
				int32_t L_35 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_36;
				L_36 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_34, L_35, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_37;
				L_37 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_36, NULL);
				bool L_38;
				L_38 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_32, L_37, NULL);
				if (!L_38)
				{
					goto IL_0302_1;
				}
			}
			{
				// precio = int.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_39 = V_2;
				String_t* L_40 = L_39->___precio_9;
				int32_t L_41;
				L_41 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_40, NULL);
				__this->___precio_10 = ((double)L_41);
				// Window_Graph.setPrecio(precio,0);
				double L_42 = __this->___precio_10;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_42, (0.0), NULL);
				// var renderer = nubPieza.GetComponent<Renderer>();
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_43 = __this->___nubPieza_12;
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_44;
				L_44 = GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A(L_43, GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
				V_3 = L_44;
				// if (Componente.color == "Rojo")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_45 = V_2;
				String_t* L_46 = L_45->___color_2;
				bool L_47;
				L_47 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_46, _stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C, NULL);
				if (!L_47)
				{
					goto IL_01a3_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.red);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_48 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_49;
				L_49 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_48, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_50;
				L_50 = Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_49, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_50, NULL);
			}

IL_01a3_1:
			{
				// if (Componente.color == "Gris")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_51 = V_2;
				String_t* L_52 = L_51->___color_2;
				bool L_53;
				L_53 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_52, _stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116, NULL);
				if (!L_53)
				{
					goto IL_01ca_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_54 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_55;
				L_55 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_54, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_56;
				L_56 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_55, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_56, NULL);
			}

IL_01ca_1:
			{
				// if (Componente.color == "Gris oscuro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_57 = V_2;
				String_t* L_58 = L_57->___color_2;
				bool L_59;
				L_59 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_58, _stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC, NULL);
				if (!L_59)
				{
					goto IL_01f1_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_60 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_61;
				L_61 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_60, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_62;
				L_62 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_61, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_62, NULL);
			}

IL_01f1_1:
			{
				// if (Componente.color == "Gris claro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_63 = V_2;
				String_t* L_64 = L_63->___color_2;
				bool L_65;
				L_65 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_64, _stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6, NULL);
				if (!L_65)
				{
					goto IL_0218_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_66 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_67;
				L_67 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_66, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_68;
				L_68 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_67, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_68, NULL);
			}

IL_0218_1:
			{
				// if (Componente.color == "")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_69 = V_2;
				String_t* L_70 = L_69->___color_2;
				bool L_71;
				L_71 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_70, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, NULL);
				if (!L_71)
				{
					goto IL_023f_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_72 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_73;
				L_73 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_72, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_74;
				L_74 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_73, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_74, NULL);
			}

IL_023f_1:
			{
				// if (Componente.color == "Azul")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_75 = V_2;
				String_t* L_76 = L_75->___color_2;
				bool L_77;
				L_77 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_76, _stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510, NULL);
				if (!L_77)
				{
					goto IL_0266_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.blue);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_78 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_79;
				L_79 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_78, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_80;
				L_80 = Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_79, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_80, NULL);
			}

IL_0266_1:
			{
				// if (Componente.color == "Negro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_81 = V_2;
				String_t* L_82 = L_81->___color_2;
				bool L_83;
				L_83 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_82, _stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9, NULL);
				if (!L_83)
				{
					goto IL_028d_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_84 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_85;
				L_85 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_84, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_86;
				L_86 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_85, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_86, NULL);
			}

IL_028d_1:
			{
				// if (Componente.color == "Dorado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_87 = V_2;
				String_t* L_88 = L_87->___color_2;
				bool L_89;
				L_89 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_88, _stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E, NULL);
				if (!L_89)
				{
					goto IL_02b4_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.yellow);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_90 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_91;
				L_91 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_90, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_92;
				L_92 = Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_91, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_92, NULL);
			}

IL_02b4_1:
			{
				// if (Componente.color == "Plateado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_93 = V_2;
				String_t* L_94 = L_93->___color_2;
				bool L_95;
				L_95 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_94, _stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF, NULL);
				if (!L_95)
				{
					goto IL_02db_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_96 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_97;
				L_97 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_96, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_98;
				L_98 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_97, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_98, NULL);
			}

IL_02db_1:
			{
				// if (Componente.color == "Cobre")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_99 = V_2;
				String_t* L_100 = L_99->___color_2;
				bool L_101;
				L_101 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_100, _stringLiteral4DE573297F69792F2A52321674F0590D7424E99A, NULL);
				if (!L_101)
				{
					goto IL_0302_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_102 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_103;
				L_103 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_102, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_104;
				L_104 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_103, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_104, NULL);
			}

IL_0302_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_105;
				L_105 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_105)
				{
					goto IL_00ec_1;
				}
			}
			{
				goto IL_031e;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_031e:
	{
		// }
		return;
	}
}
// System.Void DropDownHandlerNub::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerNub__ctor_mFDAD3B0E51BC9E5C8FF931531F80CBA45FEC733C (DropDownHandlerNub_t3291D567FEB2BCA3659581ED9FB356B6C317E48F* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_11 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_11), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandlerNub::<Start>b__9_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerNub_U3CStartU3Eb__9_0_m2D04559A348CAE089EF1217B21237F6EA2C0B151 (DropDownHandlerNub_t3291D567FEB2BCA3659581ED9FB356B6C317E48F* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// Nub.onValueChanged.AddListener(delegate { dropDownSelected(Nub); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___Nub_5;
		DropDownHandlerNub_dropDownSelected_m6B267A9871CA0EC1C84734C8931CE8E992568C02(__this, L_0, NULL);
		// Nub.onValueChanged.AddListener(delegate { dropDownSelected(Nub); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandlerPiston::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerPiston_Start_mAEC4FBFA84579B0963B97647E1CB677CC9E385AE (DropDownHandlerPiston_t77866A4A0DE8B23BC9092FB3674F015B06842109* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandlerPiston_U3CStartU3Eb__11_0_mEFB1EE1B45CCF22BAAE3D2E819B80540781C4AB8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral502C0122C400F22C3242F2FEB38F5D42DABE7996);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF83DD1C827D937E89B9D7B6506D0F30C133B3D16);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFC4ABD7D01F00B4D077136F6A39C82C63CC9C000);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// Piston = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___Piston_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___Piston_5), (void*)L_0);
		// PistonPieza = GameObject.Find("Piston");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteralFC4ABD7D01F00B4D077136F6A39C82C63CC9C000, NULL);
		__this->___PistonPieza_14 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___PistonPieza_14), (void*)L_1);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2;
		L_2 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_2;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_3 = V_0;
		int32_t L_4;
		L_4 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_3, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_4);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_6 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_6, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_6;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_023e;
	}

IL_0040:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_7 = V_0;
		int32_t L_8 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_9;
		L_9 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_7, L_8, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_10;
		L_10 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_9, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_11;
		L_11 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_12 = V_0;
		int32_t L_13 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_14;
		L_14 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_12, L_13, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_15;
		L_15 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_14, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_3 = L_16;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_17 = V_0;
		int32_t L_18 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_19;
		L_19 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_17, L_18, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_20;
		L_20 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_19, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_21;
		L_21 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		V_4 = L_21;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_22 = V_0;
		int32_t L_23 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_24;
		L_24 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_22, L_23, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_25;
		L_25 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_24, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_26;
		L_26 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		V_5 = L_26;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_27 = V_0;
		int32_t L_28 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_29;
		L_29 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_27, L_28, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_30;
		L_30 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_29, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_31;
		L_31 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		V_6 = L_31;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_32 = V_0;
		int32_t L_33 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_34;
		L_34 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_32, L_33, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_35;
		L_35 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_34, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_36;
		L_36 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		V_7 = L_36;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_37 = V_0;
		int32_t L_38 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_39;
		L_39 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_37, L_38, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_40;
		L_40 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_39, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_41;
		L_41 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_40);
		V_8 = L_41;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_42 = V_0;
		int32_t L_43 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_44;
		L_44 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_42, L_43, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_45;
		L_45 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_44, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_46;
		L_46 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_45);
		V_9 = L_46;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_47 = V_0;
		int32_t L_48 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_49;
		L_49 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_47, L_48, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_50;
		L_50 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_49, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_51;
		L_51 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_50);
		V_10 = L_51;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_52 = V_0;
		int32_t L_53 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_54;
		L_54 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_52, L_53, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_55;
		L_55 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_54, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_56;
		L_56 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_55);
		V_11 = L_56;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_57 = V_0;
		int32_t L_58 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_59;
		L_59 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_57, L_58, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_60;
		L_60 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_59, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_61;
		L_61 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_60);
		V_12 = L_61;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_62 = V_0;
		int32_t L_63 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_64;
		L_64 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_62, L_63, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_65;
		L_65 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_64, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_66;
		L_66 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_65);
		V_13 = L_66;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_67 = V_0;
		int32_t L_68 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_69;
		L_69 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_67, L_68, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_70;
		L_70 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_69, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_71;
		L_71 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_70);
		V_14 = L_71;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_73, L_72, NULL);
		V_15 = L_73;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_74 = V_15;
		String_t* L_75 = V_3;
		L_74->___tipo_0 = L_75;
		Il2CppCodeGenWriteBarrier((void**)(&L_74->___tipo_0), (void*)L_75);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_76 = V_15;
		String_t* L_77 = V_4;
		L_76->___marca_1 = L_77;
		Il2CppCodeGenWriteBarrier((void**)(&L_76->___marca_1), (void*)L_77);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_78 = V_15;
		String_t* L_79 = V_5;
		L_78->___color_2 = L_79;
		Il2CppCodeGenWriteBarrier((void**)(&L_78->___color_2), (void*)L_79);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_80 = V_15;
		String_t* L_81 = V_6;
		L_80->___material_3 = L_81;
		Il2CppCodeGenWriteBarrier((void**)(&L_80->___material_3), (void*)L_81);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_82 = V_15;
		String_t* L_83 = V_7;
		L_82->___potencia_4 = L_83;
		Il2CppCodeGenWriteBarrier((void**)(&L_82->___potencia_4), (void*)L_83);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_84 = V_15;
		String_t* L_85 = V_8;
		L_84->___precision_5 = L_85;
		Il2CppCodeGenWriteBarrier((void**)(&L_84->___precision_5), (void*)L_85);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_86 = V_15;
		String_t* L_87 = V_9;
		L_86->___alcance_6 = L_87;
		Il2CppCodeGenWriteBarrier((void**)(&L_86->___alcance_6), (void*)L_87);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_88 = V_15;
		String_t* L_89 = V_10;
		L_88->___cadencia_7 = L_89;
		Il2CppCodeGenWriteBarrier((void**)(&L_88->___cadencia_7), (void*)L_89);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_90 = V_15;
		String_t* L_91 = V_11;
		L_90->___estabilidad_8 = L_91;
		Il2CppCodeGenWriteBarrier((void**)(&L_90->___estabilidad_8), (void*)L_91);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_92 = V_15;
		String_t* L_93 = V_12;
		L_92->___precio_9 = L_93;
		Il2CppCodeGenWriteBarrier((void**)(&L_92->___precio_9), (void*)L_93);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_94 = V_15;
		String_t* L_95 = V_13;
		L_94->___modelo_10 = L_95;
		Il2CppCodeGenWriteBarrier((void**)(&L_94->___modelo_10), (void*)L_95);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_96 = V_15;
		String_t* L_97 = V_14;
		L_96->___extra_11 = L_97;
		Il2CppCodeGenWriteBarrier((void**)(&L_96->___extra_11), (void*)L_97);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_98 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_100 = L_99;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_101 = V_15;
		String_t* L_102 = L_101->___tipo_0;
		ArrayElementTypeCheck (L_100, L_102);
		(L_100)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_102);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_100;
		ArrayElementTypeCheck (L_103, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_104 = L_103;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_15;
		String_t* L_106 = L_105->___marca_1;
		ArrayElementTypeCheck (L_104, L_106);
		(L_104)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_106);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_104;
		ArrayElementTypeCheck (L_107, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_108 = L_107;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_109 = V_15;
		String_t* L_110 = L_109->___modelo_10;
		ArrayElementTypeCheck (L_108, L_110);
		(L_108)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_110);
		String_t* L_111;
		L_111 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_108, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_98, L_111, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_112 = __this->___itemDatabase_13;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_113 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_112, L_113, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_114, 1));
	}

IL_023e:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_115 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_116 = V_0;
		int32_t L_117;
		L_117 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_116, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_115) < ((int32_t)L_117)))
		{
			goto IL_0040;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_118 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_119;
		L_119 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_118, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_119;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_02a1:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0296_1;
			}

IL_0254_1:
			{
				// foreach (string item in items)
				String_t* L_120;
				L_120 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_120;
				// if (item.Contains("Pist?n") || item.Contains("Pistones"))
				String_t* L_121 = V_17;
				bool L_122;
				L_122 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_121, _stringLiteralF83DD1C827D937E89B9D7B6506D0F30C133B3D16, NULL);
				if (L_122)
				{
					goto IL_0279_1;
				}
			}
			{
				String_t* L_123 = V_17;
				bool L_124;
				L_124 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_123, _stringLiteral502C0122C400F22C3242F2FEB38F5D42DABE7996, NULL);
				if (!L_124)
				{
					goto IL_0296_1;
				}
			}

IL_0279_1:
			{
				// Piston.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_125 = __this->___Piston_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_126;
				L_126 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_125, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_127 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_127, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_128 = L_127;
				String_t* L_129 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_128, L_129, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_126, L_128, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_0296_1:
			{
				// foreach (string item in items)
				bool L_130;
				L_130 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_130)
				{
					goto IL_0254_1;
				}
			}
			{
				goto IL_02af;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_02af:
	{
		// Piston.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_131 = __this->___Piston_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_131, NULL);
		// Piston.onValueChanged.AddListener(delegate { dropDownSelected(Piston); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_132 = __this->___Piston_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_133;
		L_133 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_132, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_134 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_134, __this, (intptr_t)((void*)DropDownHandlerPiston_U3CStartU3Eb__11_0_mEFB1EE1B45CCF22BAAE3D2E819B80540781C4AB8_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_133, L_134, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandlerPiston::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerPiston_dropDownSelected_m878407BD641F22179B3AE671147075555421573C (DropDownHandlerPiston_t77866A4A0DE8B23BC9092FB3674F015B06842109* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4DE573297F69792F2A52321674F0590D7424E99A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* V_3 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (alcanceDrop != 0)
		double L_2 = __this->___alcanceDrop_7;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_3 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (varianzaDrop != 0)
		float L_4 = __this->___varianzaDrop_10;
		if ((((float)L_4) == ((float)(0.0f))))
		{
			goto IL_0067;
		}
	}
	{
		// Window_Graph.setVarianza(0, varianzaDrop);
		float L_5 = __this->___varianzaDrop_10;
		Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8((0.0f), L_5, NULL);
	}

IL_0067:
	{
		// if (precisionDrop != 0)
		double L_6 = __this->___precisionDrop_8;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_008c;
		}
	}
	{
		// Window_Graph.setPrecision(0, precisionDrop);
		double L_7 = __this->___precisionDrop_8;
		Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6((0.0), L_7, NULL);
	}

IL_008c:
	{
		// if (cadenciaDrop != 0)
		double L_8 = __this->___cadenciaDrop_9;
		if ((((double)L_8) == ((double)(0.0))))
		{
			goto IL_00b1;
		}
	}
	{
		// Window_Graph.setCadencia(0, cadenciaDrop);
		double L_9 = __this->___cadenciaDrop_9;
		Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142((0.0), L_9, NULL);
	}

IL_00b1:
	{
		// if (precio != 0)
		double L_10 = __this->___precio_11;
		if ((((double)L_10) == ((double)(0.0))))
		{
			goto IL_00d6;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_11 = __this->___precio_11;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_11, NULL);
	}

IL_00d6:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_12 = ___dropdown0;
		int32_t L_13;
		L_13 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_12, NULL);
		V_0 = L_13;
		// Debug.Log(Piston.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_14 = __this->___Piston_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_15;
		L_15 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_14, NULL);
		int32_t L_16 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_17;
		L_17 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_15, L_16, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_18;
		L_18 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_17, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_18, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_19 = __this->___itemDatabase_13;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_20;
		L_20 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_19, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_20;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_03cc:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_03be_1;
			}

IL_0109_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_21;
				L_21 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_21;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_22 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_23 = L_22;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_24 = V_2;
				String_t* L_25 = L_24->___tipo_0;
				ArrayElementTypeCheck (L_23, L_25);
				(L_23)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_25);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_26 = L_23;
				ArrayElementTypeCheck (L_26, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_26)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_27 = L_26;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_28 = V_2;
				String_t* L_29 = L_28->___marca_1;
				ArrayElementTypeCheck (L_27, L_29);
				(L_27)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_29);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_30 = L_27;
				ArrayElementTypeCheck (L_30, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_30)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_31 = L_30;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_32 = V_2;
				String_t* L_33 = L_32->___modelo_10;
				ArrayElementTypeCheck (L_31, L_33);
				(L_31)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_33);
				String_t* L_34;
				L_34 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_31, NULL);
				// if (comparar == Piston.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_35 = __this->___Piston_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_36;
				L_36 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_35, NULL);
				int32_t L_37 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_38;
				L_38 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_36, L_37, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_39;
				L_39 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_38, NULL);
				bool L_40;
				L_40 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_34, L_39, NULL);
				if (!L_40)
				{
					goto IL_03be_1;
				}
			}
			{
				// potenciaDrop = int.Parse(Componente.potencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_41 = V_2;
				String_t* L_42 = L_41->___potencia_4;
				int32_t L_43;
				L_43 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_42, NULL);
				__this->___potenciaDrop_6 = ((double)L_43);
				// alcanceDrop = int.Parse(Componente.alcance);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_44 = V_2;
				String_t* L_45 = L_44->___alcance_6;
				int32_t L_46;
				L_46 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_45, NULL);
				__this->___alcanceDrop_7 = ((double)L_46);
				// varianzaDrop = int.Parse(Componente.estabilidad);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_47 = V_2;
				String_t* L_48 = L_47->___estabilidad_8;
				int32_t L_49;
				L_49 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_48, NULL);
				__this->___varianzaDrop_10 = ((float)L_49);
				// precisionDrop = int.Parse(Componente.precision);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_50 = V_2;
				String_t* L_51 = L_50->___precision_5;
				int32_t L_52;
				L_52 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_51, NULL);
				__this->___precisionDrop_8 = ((double)L_52);
				// cadenciaDrop = int.Parse(Componente.cadencia);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_53 = V_2;
				String_t* L_54 = L_53->___cadencia_7;
				int32_t L_55;
				L_55 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_54, NULL);
				__this->___cadenciaDrop_9 = ((double)L_55);
				// precio = int.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_56 = V_2;
				String_t* L_57 = L_56->___precio_9;
				int32_t L_58;
				L_58 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_57, NULL);
				__this->___precio_11 = ((double)L_58);
				// extra = Componente.extra;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_59 = V_2;
				String_t* L_60 = L_59->___extra_11;
				__this->___extra_12 = L_60;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_12), (void*)L_60);
				// Window_Graph.setAlcance(alcanceDrop);
				double L_61 = __this->___alcanceDrop_7;
				Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2(L_61, (0.0), NULL);
				// Window_Graph.setPotencia(potenciaDrop);
				double L_62 = __this->___potenciaDrop_6;
				Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E(L_62, (0.0), NULL);
				// Window_Graph.setVarianza(varianzaDrop);
				float L_63 = __this->___varianzaDrop_10;
				Window_Graph_setVarianza_m67643DEFAF99114E4CE8813C1C75955FC2D2ABA8(L_63, (0.0f), NULL);
				// Window_Graph.setPrecision(precisionDrop);
				double L_64 = __this->___precisionDrop_8;
				Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6(L_64, (0.0), NULL);
				// Window_Graph.setCadencia(cadenciaDrop);
				double L_65 = __this->___cadenciaDrop_9;
				Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142(L_65, (0.0), NULL);
				// Window_Graph.setPrecio(precio,0);
				double L_66 = __this->___precio_11;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_66, (0.0), NULL);
				// var renderer = PistonPieza.GetComponent<Renderer>();
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_67 = __this->___PistonPieza_14;
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_68;
				L_68 = GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A(L_67, GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
				V_3 = L_68;
				// if (Componente.color == "Rojo")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_69 = V_2;
				String_t* L_70 = L_69->___color_2;
				bool L_71;
				L_71 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_70, _stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C, NULL);
				if (!L_71)
				{
					goto IL_0286_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.red);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_72 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_73;
				L_73 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_72, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_74;
				L_74 = Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_73, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_74, NULL);
			}

IL_0286_1:
			{
				// if (Componente.color == "Gris")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_75 = V_2;
				String_t* L_76 = L_75->___color_2;
				bool L_77;
				L_77 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_76, _stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116, NULL);
				if (!L_77)
				{
					goto IL_02ad_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_78 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_79;
				L_79 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_78, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_80;
				L_80 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_79, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_80, NULL);
			}

IL_02ad_1:
			{
				// if (Componente.color == "Gris oscuro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_81 = V_2;
				String_t* L_82 = L_81->___color_2;
				bool L_83;
				L_83 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_82, _stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC, NULL);
				if (!L_83)
				{
					goto IL_02d4_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_84 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_85;
				L_85 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_84, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_86;
				L_86 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_85, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_86, NULL);
			}

IL_02d4_1:
			{
				// if (Componente.color == "Gris claro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_87 = V_2;
				String_t* L_88 = L_87->___color_2;
				bool L_89;
				L_89 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_88, _stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6, NULL);
				if (!L_89)
				{
					goto IL_02fb_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_90 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_91;
				L_91 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_90, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_92;
				L_92 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_91, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_92, NULL);
			}

IL_02fb_1:
			{
				// if (Componente.color == "Azul")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_93 = V_2;
				String_t* L_94 = L_93->___color_2;
				bool L_95;
				L_95 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_94, _stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510, NULL);
				if (!L_95)
				{
					goto IL_0322_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.blue);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_96 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_97;
				L_97 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_96, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_98;
				L_98 = Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_97, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_98, NULL);
			}

IL_0322_1:
			{
				// if (Componente.color == "Negro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_99 = V_2;
				String_t* L_100 = L_99->___color_2;
				bool L_101;
				L_101 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_100, _stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9, NULL);
				if (!L_101)
				{
					goto IL_0349_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_102 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_103;
				L_103 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_102, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_104;
				L_104 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_103, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_104, NULL);
			}

IL_0349_1:
			{
				// if (Componente.color == "Dorado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_2;
				String_t* L_106 = L_105->___color_2;
				bool L_107;
				L_107 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_106, _stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E, NULL);
				if (!L_107)
				{
					goto IL_0370_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.yellow);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_108 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_109;
				L_109 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_108, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_110;
				L_110 = Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_109, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_110, NULL);
			}

IL_0370_1:
			{
				// if (Componente.color == "Plateado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_111 = V_2;
				String_t* L_112 = L_111->___color_2;
				bool L_113;
				L_113 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_112, _stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF, NULL);
				if (!L_113)
				{
					goto IL_0397_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_114 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_115;
				L_115 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_114, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_116;
				L_116 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_115, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_116, NULL);
			}

IL_0397_1:
			{
				// if (Componente.color == "Cobre")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_117 = V_2;
				String_t* L_118 = L_117->___color_2;
				bool L_119;
				L_119 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_118, _stringLiteral4DE573297F69792F2A52321674F0590D7424E99A, NULL);
				if (!L_119)
				{
					goto IL_03be_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_120 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_121;
				L_121 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_120, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_122;
				L_122 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_121, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_122, NULL);
			}

IL_03be_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_123;
				L_123 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_123)
				{
					goto IL_0109_1;
				}
			}
			{
				goto IL_03da;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_03da:
	{
		// }
		return;
	}
}
// System.Void DropDownHandlerPiston::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerPiston__ctor_m4779FBCB6E4CA958D9CAEB15C6F95516D6043189 (DropDownHandlerPiston_t77866A4A0DE8B23BC9092FB3674F015B06842109* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_13 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_13), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandlerPiston::<Start>b__11_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerPiston_U3CStartU3Eb__11_0_mEFB1EE1B45CCF22BAAE3D2E819B80540781C4AB8 (DropDownHandlerPiston_t77866A4A0DE8B23BC9092FB3674F015B06842109* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// Piston.onValueChanged.AddListener(delegate { dropDownSelected(Piston); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___Piston_5;
		DropDownHandlerPiston_dropDownSelected_m878407BD641F22179B3AE671147075555421573C(__this, L_0, NULL);
		// Piston.onValueChanged.AddListener(delegate { dropDownSelected(Piston); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DropDownHandlerTapperPlate::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerTapperPlate_Start_mAC0D9E06037CE03CABBDE405055E0C9795F433AF (DropDownHandlerTapperPlate_tAB05E5983A13525B787E9DD6A6504E1E0AFFA449* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&DropDownHandlerTapperPlate_U3CStartU3Eb__10_0_mBAF90C067D081839DA913AFD018DC7468ADDDE56_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral26014C007F47E634C331A4FA9ABFFFD635A75C5E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4FFDA33A1930AB06F6D39F0F75AC7C29FAFF6119);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9CBEAD6D497673D701FDF242F8766A289057EA14);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tE020B120302E34B781278C33685F2DA255337446* V_0 = NULL;
	List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	String_t* V_4 = NULL;
	String_t* V_5 = NULL;
	String_t* V_6 = NULL;
	String_t* V_7 = NULL;
	String_t* V_8 = NULL;
	String_t* V_9 = NULL;
	String_t* V_10 = NULL;
	String_t* V_11 = NULL;
	String_t* V_12 = NULL;
	String_t* V_13 = NULL;
	String_t* V_14 = NULL;
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_15 = NULL;
	Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 V_16;
	memset((&V_16), 0, sizeof(V_16));
	String_t* V_17 = NULL;
	{
		// TapperPlate = GetComponent<Dropdown>();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0;
		L_0 = Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00(__this, Component_GetComponent_TisDropdown_t54C0BDC1441E058BE37E796F68886671C270EF89_m0DC2A1698BB6D852352E5460A07E91DA649EED00_RuntimeMethod_var);
		__this->___TapperPlate_5 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___TapperPlate_5), (void*)L_0);
		// TapperPlatePieza = GameObject.Find("TapperPlate");
		GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_1;
		L_1 = GameObject_Find_mFF1D6C65A7E2CD82443F4DCE4C53472FB30B7F51(_stringLiteral9CBEAD6D497673D701FDF242F8766A289057EA14, NULL);
		__this->___TapperPlatePieza_13 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___TapperPlatePieza_13), (void*)L_1);
		// List<Dictionary<string, object>> data = LeerExcel.Read("ExcelPiezasV2");
		il2cpp_codegen_runtime_class_init_inline(LeerExcel_tD3E0798DEE98E76C8BE489A289305A9B5B56169E_il2cpp_TypeInfo_var);
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_2;
		L_2 = LeerExcel_Read_mF7E05F808287F8E42E1EA72964A3DF6EC3F5FB05(_stringLiteral01226F2077A24DD906BC004072EC41D87B39284B, NULL);
		V_0 = L_2;
		// string[] options = new string[data.Count];
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_3 = V_0;
		int32_t L_4;
		L_4 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_3, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_5 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)L_4);
		// List<string> items = new List<string>();
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_6 = (List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD*)il2cpp_codegen_object_new(List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD_il2cpp_TypeInfo_var);
		List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E(L_6, List_1__ctor_mCA8DD57EAC70C2B5923DBB9D5A77CEAC22E7068E_RuntimeMethod_var);
		V_1 = L_6;
		// for (var i = 0; i < data.Count; i++)
		V_2 = 0;
		goto IL_023e;
	}

IL_0040:
	{
		// string idPiezas = data[i]["idPiezas"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_7 = V_0;
		int32_t L_8 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_9;
		L_9 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_7, L_8, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_10;
		L_10 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_9, _stringLiteral74F6104B51B84FE48E73DDE7F73049028502DC51, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_11;
		L_11 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		// string tipo = data[i]["tipo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_12 = V_0;
		int32_t L_13 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_14;
		L_14 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_12, L_13, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_15;
		L_15 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_14, _stringLiteral8A0428BBD174CCB939367CC6C83978322BEC78F9, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_16;
		L_16 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_3 = L_16;
		// string marca = data[i]["marca"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_17 = V_0;
		int32_t L_18 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_19;
		L_19 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_17, L_18, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_20;
		L_20 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_19, _stringLiteralC87181AA10120D87F2B4314CD2733EA4372F7704, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_21;
		L_21 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		V_4 = L_21;
		// string color = data[i]["color"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_22 = V_0;
		int32_t L_23 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_24;
		L_24 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_22, L_23, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_25;
		L_25 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_24, _stringLiteralDD0F04E0A3F0BA1F904E0E27720F367F4228C69D, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_26;
		L_26 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_25);
		V_5 = L_26;
		// string material = data[i]["material"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_27 = V_0;
		int32_t L_28 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_29;
		L_29 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_27, L_28, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_30;
		L_30 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_29, _stringLiteral3450A2BB8216F70D528215A296AEDB3D212C7EED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_31;
		L_31 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_30);
		V_6 = L_31;
		// string potencia = data[i]["potencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_32 = V_0;
		int32_t L_33 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_34;
		L_34 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_32, L_33, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_35;
		L_35 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_34, _stringLiteral986E6C4FAA50D0971729AFA0C737F7F8FA3CC9ED, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_36;
		L_36 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_35);
		V_7 = L_36;
		// string precision = data[i]["precision"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_37 = V_0;
		int32_t L_38 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_39;
		L_39 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_37, L_38, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_40;
		L_40 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_39, _stringLiteralC8B5F8CFFD59A7B59061F52A426CC740232A7314, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_41;
		L_41 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_40);
		V_8 = L_41;
		// string alcance = data[i]["alcance"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_42 = V_0;
		int32_t L_43 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_44;
		L_44 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_42, L_43, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_45;
		L_45 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_44, _stringLiteralA7F1384652182BABA2C2DD27FD84414563AA2834, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_46;
		L_46 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_45);
		V_9 = L_46;
		// string cadencia = data[i]["cadencia"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_47 = V_0;
		int32_t L_48 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_49;
		L_49 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_47, L_48, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_50;
		L_50 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_49, _stringLiteral36E26D38E749E96068000BAB09A8DD057954B3F1, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_51;
		L_51 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_50);
		V_10 = L_51;
		// string estabilidad = data[i]["estabilidad"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_52 = V_0;
		int32_t L_53 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_54;
		L_54 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_52, L_53, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_55;
		L_55 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_54, _stringLiteralD25D1D697AD30842F12FE2DA487BCB5CC19C753B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_56;
		L_56 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_55);
		V_11 = L_56;
		// string precio = data[i]["precio"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_57 = V_0;
		int32_t L_58 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_59;
		L_59 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_57, L_58, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_60;
		L_60 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_59, _stringLiteralC0E730B787F8A55FFE2CE1A407CE40B756D42C0B, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_61;
		L_61 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_60);
		V_12 = L_61;
		// string modelo = data[i]["modelo"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_62 = V_0;
		int32_t L_63 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_64;
		L_64 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_62, L_63, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_65;
		L_65 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_64, _stringLiteral79023C74ADAA764754D5B655D9009BB0A0E03AB7, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_66;
		L_66 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_65);
		V_13 = L_66;
		// string extra = data[i]["extra"].ToString();
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_67 = V_0;
		int32_t L_68 = V_2;
		Dictionary_2_tA348003A3C1CEFB3096E9D2A0BC7F1AC8EC4F710* L_69;
		L_69 = List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752(L_67, L_68, List_1_get_Item_m0C76489809FFD07F5320A6D62040DCE70D1DF752_RuntimeMethod_var);
		RuntimeObject* L_70;
		L_70 = Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5(L_69, _stringLiteralF1A98AC16CC0C558BF6DD665DE5E8D1A5C975E58, Dictionary_2_get_Item_m371FC5B3D39406E297F2626B159BA1A3E32917B5_RuntimeMethod_var);
		String_t* L_71;
		L_71 = VirtualFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_70);
		V_14 = L_71;
		// Componente tempItem = new Componente(blankItem);
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = __this->___blankItem_4;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_73 = (Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F*)il2cpp_codegen_object_new(Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F_il2cpp_TypeInfo_var);
		Componente__ctor_m0FC87FB8EA6F7F048F1169B866759DC960086756(L_73, L_72, NULL);
		V_15 = L_73;
		// tempItem.tipo = tipo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_74 = V_15;
		String_t* L_75 = V_3;
		L_74->___tipo_0 = L_75;
		Il2CppCodeGenWriteBarrier((void**)(&L_74->___tipo_0), (void*)L_75);
		// tempItem.marca = marca;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_76 = V_15;
		String_t* L_77 = V_4;
		L_76->___marca_1 = L_77;
		Il2CppCodeGenWriteBarrier((void**)(&L_76->___marca_1), (void*)L_77);
		// tempItem.color = color;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_78 = V_15;
		String_t* L_79 = V_5;
		L_78->___color_2 = L_79;
		Il2CppCodeGenWriteBarrier((void**)(&L_78->___color_2), (void*)L_79);
		// tempItem.material = material;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_80 = V_15;
		String_t* L_81 = V_6;
		L_80->___material_3 = L_81;
		Il2CppCodeGenWriteBarrier((void**)(&L_80->___material_3), (void*)L_81);
		// tempItem.potencia = potencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_82 = V_15;
		String_t* L_83 = V_7;
		L_82->___potencia_4 = L_83;
		Il2CppCodeGenWriteBarrier((void**)(&L_82->___potencia_4), (void*)L_83);
		// tempItem.precision = precision;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_84 = V_15;
		String_t* L_85 = V_8;
		L_84->___precision_5 = L_85;
		Il2CppCodeGenWriteBarrier((void**)(&L_84->___precision_5), (void*)L_85);
		// tempItem.alcance = alcance;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_86 = V_15;
		String_t* L_87 = V_9;
		L_86->___alcance_6 = L_87;
		Il2CppCodeGenWriteBarrier((void**)(&L_86->___alcance_6), (void*)L_87);
		// tempItem.cadencia = cadencia;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_88 = V_15;
		String_t* L_89 = V_10;
		L_88->___cadencia_7 = L_89;
		Il2CppCodeGenWriteBarrier((void**)(&L_88->___cadencia_7), (void*)L_89);
		// tempItem.estabilidad = estabilidad;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_90 = V_15;
		String_t* L_91 = V_11;
		L_90->___estabilidad_8 = L_91;
		Il2CppCodeGenWriteBarrier((void**)(&L_90->___estabilidad_8), (void*)L_91);
		// tempItem.precio = precio;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_92 = V_15;
		String_t* L_93 = V_12;
		L_92->___precio_9 = L_93;
		Il2CppCodeGenWriteBarrier((void**)(&L_92->___precio_9), (void*)L_93);
		// tempItem.modelo = modelo;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_94 = V_15;
		String_t* L_95 = V_13;
		L_94->___modelo_10 = L_95;
		Il2CppCodeGenWriteBarrier((void**)(&L_94->___modelo_10), (void*)L_95);
		// tempItem.extra = extra;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_96 = V_15;
		String_t* L_97 = V_14;
		L_96->___extra_11 = L_97;
		Il2CppCodeGenWriteBarrier((void**)(&L_96->___extra_11), (void*)L_97);
		// items.Add(tempItem.tipo + " " + tempItem.marca + " " + tempItem.modelo);
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_98 = V_1;
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_99 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_100 = L_99;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_101 = V_15;
		String_t* L_102 = L_101->___tipo_0;
		ArrayElementTypeCheck (L_100, L_102);
		(L_100)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_102);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_103 = L_100;
		ArrayElementTypeCheck (L_103, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_103)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_104 = L_103;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_105 = V_15;
		String_t* L_106 = L_105->___marca_1;
		ArrayElementTypeCheck (L_104, L_106);
		(L_104)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_106);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_107 = L_104;
		ArrayElementTypeCheck (L_107, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		(L_107)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_108 = L_107;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_109 = V_15;
		String_t* L_110 = L_109->___modelo_10;
		ArrayElementTypeCheck (L_108, L_110);
		(L_108)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_110);
		String_t* L_111;
		L_111 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_108, NULL);
		List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_inline(L_98, L_111, List_1_Add_mF10DB1D3CBB0B14215F0E4F8AB4934A1955E5351_RuntimeMethod_var);
		// itemDatabase.Add(tempItem);
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_112 = __this->___itemDatabase_12;
		Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_113 = V_15;
		List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_inline(L_112, L_113, List_1_Add_mAA00AB7C4E817BC63D4EC0303C7E90C99CC07ACB_RuntimeMethod_var);
		// for (var i = 0; i < data.Count; i++)
		int32_t L_114 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add(L_114, 1));
	}

IL_023e:
	{
		// for (var i = 0; i < data.Count; i++)
		int32_t L_115 = V_2;
		List_1_tE020B120302E34B781278C33685F2DA255337446* L_116 = V_0;
		int32_t L_117;
		L_117 = List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_inline(L_116, List_1_get_Count_mBE391563AD77CA4C4E18715CEF71AFE50866F8F7_RuntimeMethod_var);
		if ((((int32_t)L_115) < ((int32_t)L_117)))
		{
			goto IL_0040;
		}
	}
	{
		// foreach (string item in items)
		List_1_tF470A3BE5C1B5B68E1325EF3F109D172E60BD7CD* L_118 = V_1;
		Enumerator_tA7A4B718FE1ED1D87565680D8C8195EC8AEAB3D1 L_119;
		L_119 = List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D(L_118, List_1_GetEnumerator_m7692B5F182858B7D5C72C920D09AD48738D1E70D_RuntimeMethod_var);
		V_16 = L_119;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_02a1:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7((&V_16), Enumerator_Dispose_m592BCCE7B7933454DED2130C810F059F8D85B1D7_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_0296_1;
			}

IL_0254_1:
			{
				// foreach (string item in items)
				String_t* L_120;
				L_120 = Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_inline((&V_16), Enumerator_get_Current_m143541DD8FBCD313E7554EA738FA813B8F4DB11A_RuntimeMethod_var);
				V_17 = L_120;
				// if (item.Contains("Tapper plate") || item.Contains("Tapper plates"))
				String_t* L_121 = V_17;
				bool L_122;
				L_122 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_121, _stringLiteral4FFDA33A1930AB06F6D39F0F75AC7C29FAFF6119, NULL);
				if (L_122)
				{
					goto IL_0279_1;
				}
			}
			{
				String_t* L_123 = V_17;
				bool L_124;
				L_124 = String_Contains_m6D77B121FADA7CA5F397C0FABB65DA62DF03B6C3(L_123, _stringLiteral26014C007F47E634C331A4FA9ABFFFD635A75C5E, NULL);
				if (!L_124)
				{
					goto IL_0296_1;
				}
			}

IL_0279_1:
			{
				// TapperPlate.options.Add(new Dropdown.OptionData() { text = item });
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_125 = __this->___TapperPlate_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_126;
				L_126 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_125, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_127 = (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F*)il2cpp_codegen_object_new(OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F_il2cpp_TypeInfo_var);
				OptionData__ctor_m6321993E5D83F3A7E52ADC14C9276508D1129166(L_127, NULL);
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_128 = L_127;
				String_t* L_129 = V_17;
				OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline(L_128, L_129, NULL);
				List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_inline(L_126, L_128, List_1_Add_mD4C8E5C7DAB7FBB4A72C861A16DFED06E2DCA03C_RuntimeMethod_var);
			}

IL_0296_1:
			{
				// foreach (string item in items)
				bool L_130;
				L_130 = Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED((&V_16), Enumerator_MoveNext_mDB47EEC4531D33B9C33FD2E70BA15E1535A0F3ED_RuntimeMethod_var);
				if (L_130)
				{
					goto IL_0254_1;
				}
			}
			{
				goto IL_02af;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_02af:
	{
		// TapperPlate.RefreshShownValue();
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_131 = __this->___TapperPlate_5;
		Dropdown_RefreshShownValue_mA112A95E8653859FC2B6C2D0CC89660D36E8970E(L_131, NULL);
		// TapperPlate.onValueChanged.AddListener(delegate { dropDownSelected(TapperPlate); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_132 = __this->___TapperPlate_5;
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_133;
		L_133 = Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline(L_132, NULL);
		UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60* L_134 = (UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60*)il2cpp_codegen_object_new(UnityAction_1_tA5B7125BEF9EB3092B91D1E2AA64249C44903A60_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m48C26C8BF8CF832FAFF2818DC9246BACBC3CCF2B(L_134, __this, (intptr_t)((void*)DropDownHandlerTapperPlate_U3CStartU3Eb__10_0_mBAF90C067D081839DA913AFD018DC7468ADDDE56_RuntimeMethod_var), NULL);
		UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE(L_133, L_134, UnityEvent_1_AddListener_m846E66E06483E90ED947176C8DB2BE23495A93AE_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void DropDownHandlerTapperPlate::dropDownSelected(UnityEngine.UI.Dropdown)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerTapperPlate_dropDownSelected_m2A302B20964AF7F7E3A2F804454003DDDB968969 (DropDownHandlerTapperPlate_tAB05E5983A13525B787E9DD6A6504E1E0AFFA449* __this, Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* ___dropdown0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4DE573297F69792F2A52321674F0590D7424E99A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 V_1;
	memset((&V_1), 0, sizeof(V_1));
	Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* V_2 = NULL;
	Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* V_3 = NULL;
	{
		// if (potenciaDrop != 0)
		double L_0 = __this->___potenciaDrop_6;
		if ((((double)L_0) == ((double)(0.0))))
		{
			goto IL_0025;
		}
	}
	{
		// Window_Graph.setPotencia(0, potenciaDrop);
		double L_1 = __this->___potenciaDrop_6;
		Window_Graph_setPotencia_mC82BA2848404A8087674B5B35164470234A2382E((0.0), L_1, NULL);
	}

IL_0025:
	{
		// if (precisionDrop != 0)
		double L_2 = __this->___precisionDrop_8;
		if ((((double)L_2) == ((double)(0.0))))
		{
			goto IL_004a;
		}
	}
	{
		// Window_Graph.setPrecision(0, precisionDrop);
		double L_3 = __this->___precisionDrop_8;
		Window_Graph_setPrecision_m2A5C314CF808CFCAE8BFAAD2A8E759AF93DAFCE6((0.0), L_3, NULL);
	}

IL_004a:
	{
		// if (alcanceDrop != 0)
		double L_4 = __this->___alcanceDrop_7;
		if ((((double)L_4) == ((double)(0.0))))
		{
			goto IL_006f;
		}
	}
	{
		// Window_Graph.setAlcance(0, alcanceDrop);
		double L_5 = __this->___alcanceDrop_7;
		Window_Graph_setAlcance_m706A76E98D683FEE96AC233DDBB9E9356FA680B2((0.0), L_5, NULL);
	}

IL_006f:
	{
		// if (cadenciaDrop != 0)
		double L_6 = __this->___cadenciaDrop_9;
		if ((((double)L_6) == ((double)(0.0))))
		{
			goto IL_0094;
		}
	}
	{
		// Window_Graph.setCadencia(0, cadenciaDrop);
		double L_7 = __this->___cadenciaDrop_9;
		Window_Graph_setCadencia_m2A72FA5AACAFC75046F174656A54765CD478F142((0.0), L_7, NULL);
	}

IL_0094:
	{
		// if (precio != 0)
		double L_8 = __this->___precio_10;
		if ((((double)L_8) == ((double)(0.0))))
		{
			goto IL_00b9;
		}
	}
	{
		// Window_Graph.setPrecio(0, precio);
		double L_9 = __this->___precio_10;
		Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2((0.0), L_9, NULL);
	}

IL_00b9:
	{
		// int index = dropdown.value;
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_10 = ___dropdown0;
		int32_t L_11;
		L_11 = Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline(L_10, NULL);
		V_0 = L_11;
		// Debug.Log(TapperPlate.options[index].text);
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_12 = __this->___TapperPlate_5;
		List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_13;
		L_13 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_12, NULL);
		int32_t L_14 = V_0;
		OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_15;
		L_15 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_13, L_14, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
		String_t* L_16;
		L_16 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_15, NULL);
		il2cpp_codegen_runtime_class_init_inline(Debug_t8394C7EEAECA3689C2C9B9DE9C7166D73596276F_il2cpp_TypeInfo_var);
		Debug_Log_m86567BCF22BBE7809747817453CACA0E41E68219(L_16, NULL);
		// foreach (Componente Componente in itemDatabase)
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_17 = __this->___itemDatabase_12;
		Enumerator_t695B6BD37D4161120A81348D69B0F1ED03DA5D80 L_18;
		L_18 = List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443(L_17, List_1_GetEnumerator_m04046101DB246FE5057372CEDA25BD522417D443_RuntimeMethod_var);
		V_1 = L_18;
	}
	{
		auto __finallyBlock = il2cpp::utils::Finally([&]
		{

FINALLY_0300:
			{// begin finally (depth: 1)
				Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A((&V_1), Enumerator_Dispose_m4A7EDDD1BE77DD724CDB61D1C5A8E200AA81986A_RuntimeMethod_var);
				return;
			}// end finally (depth: 1)
		});
		try
		{// begin try (depth: 1)
			{
				goto IL_02f2_1;
			}

IL_00ec_1:
			{
				// foreach (Componente Componente in itemDatabase)
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_19;
				L_19 = Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_inline((&V_1), Enumerator_get_Current_m28C27CDB9A101153C2710EEDC8A4F3409486EAED_RuntimeMethod_var);
				V_2 = L_19;
				// string comparar = Componente.tipo + " " + Componente.marca + " " + Componente.modelo;
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_20 = (StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248*)SZArrayNew(StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248_il2cpp_TypeInfo_var, (uint32_t)5);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_21 = L_20;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_22 = V_2;
				String_t* L_23 = L_22->___tipo_0;
				ArrayElementTypeCheck (L_21, L_23);
				(L_21)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)L_23);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_24 = L_21;
				ArrayElementTypeCheck (L_24, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_24)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_25 = L_24;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_26 = V_2;
				String_t* L_27 = L_26->___marca_1;
				ArrayElementTypeCheck (L_25, L_27);
				(L_25)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)L_27);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_28 = L_25;
				ArrayElementTypeCheck (L_28, _stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				(L_28)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)_stringLiteral2386E77CF610F786B06A91AF2C1B3FD2282D2745);
				StringU5BU5D_t7674CD946EC0CE7B3AE0BE70E6EE85F2ECD9F248* L_29 = L_28;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_30 = V_2;
				String_t* L_31 = L_30->___modelo_10;
				ArrayElementTypeCheck (L_29, L_31);
				(L_29)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)L_31);
				String_t* L_32;
				L_32 = String_Concat_m6B0734B65813C8EA093D78E5C2D16534EB6FE8C0(L_29, NULL);
				// if (comparar == TapperPlate.options[index].text)
				Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_33 = __this->___TapperPlate_5;
				List_1_tCEC1993B65ACD0529D8C0BB5006274E22D7D8A55* L_34;
				L_34 = Dropdown_get_options_m30F757DBA22980CB77DADB8315207D5B87307816(L_33, NULL);
				int32_t L_35 = V_0;
				OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* L_36;
				L_36 = List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366(L_34, L_35, List_1_get_Item_m6248FAA3EA9E9148D0E784C78FA4B1F5FFDD6366_RuntimeMethod_var);
				String_t* L_37;
				L_37 = OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline(L_36, NULL);
				bool L_38;
				L_38 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_32, L_37, NULL);
				if (!L_38)
				{
					goto IL_02f2_1;
				}
			}
			{
				// precio = int.Parse(Componente.precio);
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_39 = V_2;
				String_t* L_40 = L_39->___precio_9;
				int32_t L_41;
				L_41 = Int32_Parse_m59B9CC9D5E5B6C99C14251E57FB43BE6AB658767(L_40, NULL);
				__this->___precio_10 = ((double)L_41);
				// extra = Componente.extra;
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_42 = V_2;
				String_t* L_43 = L_42->___extra_11;
				__this->___extra_11 = L_43;
				Il2CppCodeGenWriteBarrier((void**)(&__this->___extra_11), (void*)L_43);
				// Window_Graph.setExtra(extra);
				String_t* L_44 = __this->___extra_11;
				Window_Graph_setExtra_m8774145D45E480AAFA50F295A5FB4E00A289FF91_inline(L_44, NULL);
				// Window_Graph.setPrecio(precio,0);
				double L_45 = __this->___precio_10;
				Window_Graph_setPrecio_m66E32BAD037135E5FD60C9E381BFFD9D414379F2(L_45, (0.0), NULL);
				// var renderer = TapperPlatePieza.GetComponent<Renderer>();
				GameObject_t76FEDD663AB33C991A9C9A23129337651094216F* L_46 = __this->___TapperPlatePieza_13;
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_47;
				L_47 = GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A(L_46, GameObject_GetComponent_TisRenderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF_m026EB44DB6238F13E2AFBECF1FBBE924CB1B040A_RuntimeMethod_var);
				V_3 = L_47;
				// if (Componente.color == "Rojo")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_48 = V_2;
				String_t* L_49 = L_48->___color_2;
				bool L_50;
				L_50 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_49, _stringLiteral7AF5F092ADAD8E6F8389B46CF7697C404D90E08C, NULL);
				if (!L_50)
				{
					goto IL_01ba_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.red);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_51 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_52;
				L_52 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_51, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_53;
				L_53 = Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_52, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_53, NULL);
			}

IL_01ba_1:
			{
				// if (Componente.color == "Gris")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_54 = V_2;
				String_t* L_55 = L_54->___color_2;
				bool L_56;
				L_56 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_55, _stringLiteralBFF81F5688BDE7B30E8D93BC2C9BF92990BA6116, NULL);
				if (!L_56)
				{
					goto IL_01e1_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_57 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_58;
				L_58 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_57, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_59;
				L_59 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_58, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_59, NULL);
			}

IL_01e1_1:
			{
				// if (Componente.color == "Gris oscuro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_60 = V_2;
				String_t* L_61 = L_60->___color_2;
				bool L_62;
				L_62 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_61, _stringLiteral7F006EC4D5506886618BDD2E34CEB5EEC9A9C2EC, NULL);
				if (!L_62)
				{
					goto IL_0208_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_63 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_64;
				L_64 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_63, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_65;
				L_65 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_64, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_65, NULL);
			}

IL_0208_1:
			{
				// if (Componente.color == "Gris claro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_66 = V_2;
				String_t* L_67 = L_66->___color_2;
				bool L_68;
				L_68 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_67, _stringLiteralC99FB0F075F1C4D11451B5E309CCBFA13A0309F6, NULL);
				if (!L_68)
				{
					goto IL_022f_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_69 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_70;
				L_70 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_69, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_71;
				L_71 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_70, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_71, NULL);
			}

IL_022f_1:
			{
				// if (Componente.color == "Azul")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_72 = V_2;
				String_t* L_73 = L_72->___color_2;
				bool L_74;
				L_74 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_73, _stringLiteral97997FBCDCFAC66581BC3C11304E29521CB5F510, NULL);
				if (!L_74)
				{
					goto IL_0256_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.blue);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_75 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_76;
				L_76 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_75, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_77;
				L_77 = Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_76, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_77, NULL);
			}

IL_0256_1:
			{
				// if (Componente.color == "Negro")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_78 = V_2;
				String_t* L_79 = L_78->___color_2;
				bool L_80;
				L_80 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_79, _stringLiteralE07A095093C9FCFA9F7BCFAEBDA7FB5D1E47D6A9, NULL);
				if (!L_80)
				{
					goto IL_027d_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_81 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_82;
				L_82 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_81, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_83;
				L_83 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_82, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_83, NULL);
			}

IL_027d_1:
			{
				// if (Componente.color == "Dorado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_84 = V_2;
				String_t* L_85 = L_84->___color_2;
				bool L_86;
				L_86 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_85, _stringLiteralBFE1B63EBF9280F3321A8DB3C097A4D494B8BF3E, NULL);
				if (!L_86)
				{
					goto IL_02a4_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.yellow);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_87 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_88;
				L_88 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_87, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_89;
				L_89 = Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_88, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_89, NULL);
			}

IL_02a4_1:
			{
				// if (Componente.color == "Plateado")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_90 = V_2;
				String_t* L_91 = L_90->___color_2;
				bool L_92;
				L_92 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_91, _stringLiteralB5CDE395D4D4B93EA56DA482703CE20EC7F3FAFF, NULL);
				if (!L_92)
				{
					goto IL_02cb_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.gray);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_93 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_94;
				L_94 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_93, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_95;
				L_95 = Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_94, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_95, NULL);
			}

IL_02cb_1:
			{
				// if (Componente.color == "Cobre")
				Componente_t61457E85DAEB5EEF186CD349E6524704976BD05F* L_96 = V_2;
				String_t* L_97 = L_96->___color_2;
				bool L_98;
				L_98 = String_op_Equality_m0D685A924E5CD78078F248ED1726DA5A9D7D6AC0(L_97, _stringLiteral4DE573297F69792F2A52321674F0590D7424E99A, NULL);
				if (!L_98)
				{
					goto IL_02f2_1;
				}
			}
			{
				// renderer.material.SetColor("_Color", Color.black);
				Renderer_t320575F223BCB177A982E5DDB5DB19FAA89E7FBF* L_99 = V_3;
				Material_t18053F08F347D0DCA5E1140EC7EC4533DD8A14E3* L_100;
				L_100 = Renderer_get_material_m5BA2A00816C4CC66580D4B2E409CF10718C15656(L_99, NULL);
				Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_101;
				L_101 = Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline(NULL);
				Material_SetColor_mFAB32FAA44461E46FD707B34184EC080CBB3539F(L_100, _stringLiteral47A3FAF17D89549FD0F0ECA7370B81F7C80DFCDE, L_101, NULL);
			}

IL_02f2_1:
			{
				// foreach (Componente Componente in itemDatabase)
				bool L_102;
				L_102 = Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0((&V_1), Enumerator_MoveNext_mAF76D7580DD887F45A8EAFE7A5C932A6932842D0_RuntimeMethod_var);
				if (L_102)
				{
					goto IL_00ec_1;
				}
			}
			{
				goto IL_030e;
			}
		}// end try (depth: 1)
		catch(Il2CppExceptionWrapper& e)
		{
			__finallyBlock.StoreException(e.ex);
		}
	}

IL_030e:
	{
		// }
		return;
	}
}
// System.Void DropDownHandlerTapperPlate::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerTapperPlate__ctor_m894A0E44957BDEDD2C3669B5CC851884B73D66E8 (DropDownHandlerTapperPlate_tAB05E5983A13525B787E9DD6A6504E1E0AFFA449* __this, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public List<Componente> itemDatabase = new List<Componente>();
		List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083* L_0 = (List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083*)il2cpp_codegen_object_new(List_1_t5F844BC4D9DEAB02693AF92EDA2134B196D63083_il2cpp_TypeInfo_var);
		List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6(L_0, List_1__ctor_mD22FEE072CEF8C0383E24BCFD2BDAC7A7F78CAA6_RuntimeMethod_var);
		__this->___itemDatabase_12 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___itemDatabase_12), (void*)L_0);
		MonoBehaviour__ctor_m592DB0105CA0BC97AA1C5F4AD27B12D68A3B7C1E(__this, NULL);
		return;
	}
}
// System.Void DropDownHandlerTapperPlate::<Start>b__10_0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DropDownHandlerTapperPlate_U3CStartU3Eb__10_0_mBAF90C067D081839DA913AFD018DC7468ADDDE56 (DropDownHandlerTapperPlate_tAB05E5983A13525B787E9DD6A6504E1E0AFFA449* __this, int32_t ___U3Cp0U3E0, const RuntimeMethod* method) 
{
	{
		// TapperPlate.onValueChanged.AddListener(delegate { dropDownSelected(TapperPlate); });
		Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* L_0 = __this->___TapperPlate_5;
		DropDownHandlerTapperPlate_dropDownSelected_m2A302B20964AF7F7E3A2F804454003DDDB968969(__this, L_0, NULL);
		// TapperPlate.onValueChanged.AddListener(delegate { dropDownSelected(TapperPlate); });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void OptionData_set_text_mA6022A455FC38025B0CA97B4E3629DA10FDE259E_inline (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* __this, String_t* ___value0, const RuntimeMethod* method) 
{
	{
		// public string text  { get { return m_Text; }  set { m_Text = value;  } }
		String_t* L_0 = ___value0;
		__this->___m_Text_0 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&__this->___m_Text_0), (void*)L_0);
		// public string text  { get { return m_Text; }  set { m_Text = value;  } }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* Dropdown_get_onValueChanged_mAC49CE9A83E258FEC024662127057567275CAC12_inline (Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* __this, const RuntimeMethod* method) 
{
	{
		// public DropdownEvent onValueChanged { get { return m_OnValueChanged; } set { m_OnValueChanged = value; } }
		DropdownEvent_t8A008B010A742724CFC93576D6976E474BB13059* L_0 = __this->___m_OnValueChanged_27;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Dropdown_get_value_m386913162D5E273B762657FE5156DC567602BC3C_inline (Dropdown_t54C0BDC1441E058BE37E796F68886671C270EF89* __this, const RuntimeMethod* method) 
{
	{
		// return m_Value;
		int32_t L_0 = __this->___m_Value_25;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* OptionData_get_text_m147C3EFE4B7D157914D2C6CF653B32CE2D987AF1_inline (OptionData_t68DC820D58A3ABBAE844326B15A7F14D48FAE55F* __this, const RuntimeMethod* method) 
{
	{
		// public string text  { get { return m_Text; }  set { m_Text = value;  } }
		String_t* L_0 = __this->___m_Text_0;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_red_m27D04C1E5FE794AD933B7B9364F3D34B9EA25109_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (1.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_gray_mC62F535A52768B992F144E443D201F749C5DE932_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (0.5f), (0.5f), (0.5f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_blue_m0D04554379CB8606EF48E3091CDC3098B81DD86D_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_black_mBF96B603B41BED9BAFAA10CE8D946D24260F9729_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (0.0f), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tD001788D726C3A7F1379BEED0260B9591F440C1F Color_get_yellow_m1EF7276EF58050DFBA8921E2383F0249C08D346F_inline (const RuntimeMethod* method) 
{
	Color_tD001788D726C3A7F1379BEED0260B9591F440C1F V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline((&L_0), (1.0f), (0.921568632f), (0.0156862754f), (1.0f), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_001d;
	}

IL_001d:
	{
		Color_tD001788D726C3A7F1379BEED0260B9591F440C1F L_1 = V_0;
		return L_1;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Window_Graph_setExtra_m8774145D45E480AAFA50F295A5FB4E00A289FF91_inline (String_t* ___extraEnviada0, const RuntimeMethod* method) 
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// extra = extraEnviada;
		String_t* L_0 = ___extraEnviada0;
		((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___extra_14 = L_0;
		Il2CppCodeGenWriteBarrier((void**)(&((Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_StaticFields*)il2cpp_codegen_static_fields_for(Window_Graph_t8BB461E507EEA9AC016A8AE2224253CB1A963DE8_il2cpp_TypeInfo_var))->___extra_14), (void*)L_0);
		// }
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Clear_m16C1F2C61FED5955F10EB36BC1CB2DF34B128994_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) 
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		if (!true)
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->____size_2;
		V_0 = L_1;
		__this->____size_2 = 0;
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_003c;
		}
	}
	{
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_3 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		int32_t L_4 = V_0;
		Array_Clear_m48B57EC27CADC3463CA98A33373D557DA587FF1B((RuntimeArray*)L_3, 0, L_4, NULL);
		return;
	}

IL_0035:
	{
		__this->____size_2 = 0;
	}

IL_003c:
	{
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void List_1_Add_mEBCF994CC3814631017F46A387B1A192ED6C85C7_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, RuntimeObject* ___item0, const RuntimeMethod* method) 
{
	ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* V_0 = NULL;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)__this->____version_3;
		__this->____version_3 = ((int32_t)il2cpp_codegen_add(L_0, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_1 = (ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918*)__this->____items_1;
		V_0 = L_1;
		int32_t L_2 = (int32_t)__this->____size_2;
		V_1 = L_2;
		int32_t L_3 = V_1;
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_4 = V_0;
		if ((!(((uint32_t)L_3) < ((uint32_t)((int32_t)(((RuntimeArray*)L_4)->max_length))))))
		{
			goto IL_0034;
		}
	}
	{
		int32_t L_5 = V_1;
		__this->____size_2 = ((int32_t)il2cpp_codegen_add(L_5, 1));
		ObjectU5BU5D_t8061030B0A12A55D5AD8652A20C922FE99450918* L_6 = V_0;
		int32_t L_7 = V_1;
		RuntimeObject* L_8 = ___item0;
		(L_6)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_7), (RuntimeObject*)L_8);
		return;
	}

IL_0034:
	{
		RuntimeObject* L_9 = ___item0;
		((  void (*) (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D*, RuntimeObject*, const RuntimeMethod*))il2cpp_codegen_get_method_pointer(il2cpp_rgctx_method(method->klass->rgctx_data, 11)))(__this, L_9, il2cpp_rgctx_method(method->klass->rgctx_data, 11));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m4407E4C389F22B8CEC282C15D56516658746C383_gshared_inline (List_1_tA239CB83DE5615F348BB0507E45F490F4F7C9A8D* __this, const RuntimeMethod* method) 
{
	{
		int32_t L_0 = (int32_t)__this->____size_2;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject* Enumerator_get_Current_m6330F15D18EE4F547C05DF9BF83C5EB710376027_gshared_inline (Enumerator_t9473BAB568A27E2339D48C1F91319E0F6D244D7A* __this, const RuntimeMethod* method) 
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->____current_3;
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Color__ctor_m3786F0D6E510D9CFA544523A955870BD2A514C8C_inline (Color_tD001788D726C3A7F1379BEED0260B9591F440C1F* __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method) 
{
	{
		float L_0 = ___r0;
		__this->___r_0 = L_0;
		float L_1 = ___g1;
		__this->___g_1 = L_1;
		float L_2 = ___b2;
		__this->___b_2 = L_2;
		float L_3 = ___a3;
		__this->___a_3 = L_3;
		return;
	}
}
