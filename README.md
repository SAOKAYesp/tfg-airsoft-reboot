# TFG AIRSOFT REBOOT

```

   _____  .__                     _____  __    __________      ___.                  __   
  /  _  \ |__|______  ___________/ ____\/  |_  \______   \ ____\_ |__   ____   _____/  |_ 
 /  /_\  \|  \_  __ \/  ___/  _ \   __\\   __\  |       _// __ \| __ \ /  _ \ /  _ \   __\
/    |    \  ||  | \/\___ (  <_> )  |   |  |    |    |   \  ___/| \_\ (  <_> |  <_> )  |  
\____|__  /__||__|  /____  >____/|__|   |__|    |____|_  /\___  >___  /\____/ \____/|__|  
        \/               \/                            \/     \/    \/                    


```




![Semantic description of image](Logos/AIRSOFT-REBOOT-LOGO.png)

## Descripción

Este proyecto trata de una página web en la que podrás encontrar información útil para el mundo del airsoft y, lo más importante, configurar tu réplica.

## Web
url: https://airsoftre.000webhostapp.com/Inicio.php

## Autores
Desarrollado por Sergio González y Fernando Macías para el grado superior de DAM.

## License

Copyright © 2022 AIRSOFT REBOOT
